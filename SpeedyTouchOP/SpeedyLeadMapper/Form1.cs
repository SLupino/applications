﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using SpeedyLeadMapper.Properties;

namespace SpeedyLeadMapper
{
    public partial class Form1 : Form
    {
        private readonly string _catalogConnectionString =
            ConfigurationManager.ConnectionStrings["SL_CATALOG"].ConnectionString;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var tenantList = new List<string>();
            using (var connection = new SqlConnection(_catalogConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(
                    "select distinct Id from [cat].[Tenant]",
                    connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read()) tenantList.Add(reader.GetString(0));
                    }
                }
            }

            comboBoxTenants.DataSource = tenantList;

            Refresh();
        }

        private void Refresh(bool bothViews = true)
        {
            var salesForceEntity = new DataTable();

            var appUserIdList = new Dictionary<string,List<string>>();
            using (var connection = new SqlConnection(_catalogConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(
                    "select distinct AppUserId, UserId from [cat].[UserTenantRelation] where AppUserId is not null and TenantId = @TenantId; ",
                    connection))
                {
                    command.Parameters.AddWithValue("TenantId", comboBoxTenants.Text);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var appUserId = reader.GetString(0);
                            if (!appUserIdList.ContainsKey(appUserId))
                            {
                                appUserIdList.Add(appUserId, new List<string>());
                            }
                            appUserIdList[appUserId].Add(reader.GetString(1));
                        }
                    }
                }
            }

            var conn = ConfigurationManager.ConnectionStrings[comboBoxTenants.Text]?.ConnectionString;
            if (string.IsNullOrEmpty(conn))
            {
                MessageBox.Show($"Missing connection string for tenant '{comboBoxTenants.Text}'.", "Error");

                comboBoxTenants.SelectedIndex = 0;
                conn = ConfigurationManager.ConnectionStrings[comboBoxTenants.Text].ConnectionString;

                appUserIdList = new Dictionary<string,List<string>>();
                using (var connection = new SqlConnection(_catalogConnectionString))
                {
                    connection.Open();
                    using (var command = new SqlCommand(
                        "select distinct AppUserId, UserId from [cat].[UserTenantRelation] where AppUserId is not null and TenantId = @TenantId; ",
                        connection))
                    {
                        command.Parameters.AddWithValue("TenantId", comboBoxTenants.Text);

                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var appUserId = reader.GetString(0);
                                if (!appUserIdList.ContainsKey(appUserId))
                                {
                                    appUserIdList.Add(appUserId, new List<string>());
                                }
                                appUserIdList[appUserId].Add(reader.GetString(1));
                            }
                        }
                    }
                }
            }

            using (var connection = new SqlConnection(conn))
            {
                connection.Open();
                using (var command = new SqlCommand(
                    @";WITH CTE as(
select a.Level, a.Id, a.SystemAccount, a.FirstName, a.LastName, b.SalesRepStatus from [sf].[SalesForceEntity] a left join [sf].[SalesForceEntity] b on a.Id = b.ParentId where a.Level > 0 and a.Level < 4)
select CASE WHEN a.Level = 3 THEN 'Area' ELSE CASE WHEN a.Level = 2 THEN 'Regional' ELSE 'Top' END END as 'Type', a.Id, a.SystemAccount, a.FirstName, a.LastName, count(a.SalesRepStatus) as 'SubordinateCount', 
coalesce(WithStatus1, 0) as 'WithStatus1', 
coalesce(WithStatus2, 0) as 'WithStatus2', 
coalesce(WithStatus3, 0) as 'WithStatus3', 
coalesce(WithStatus4, 0) as 'WithStatus4' from CTE a 
left join (Select Id, count(SalesRepStatus) as 'WithStatus1' from CTE where SalesRepStatus = 1 group by Id) b on a.Id = b.Id 
left join (Select Id, count(SalesRepStatus) as 'WithStatus2' from CTE where SalesRepStatus = 2 group by Id) c on a.Id = c.Id 
left join (Select Id, count(SalesRepStatus) as 'WithStatus3' from CTE where SalesRepStatus = 3 group by Id) d on a.Id = d.Id 
left join (Select Id, count(SalesRepStatus) as 'WithStatus4' from CTE where SalesRepStatus = 4 group by Id) e on a.Id = e.Id
group by a.Level, a.Id, a.FirstName, a.LastName, a.SystemAccount, WithStatus1, WithStatus2, WithStatus3, WithStatus4;",
                    connection))
                {
                    var da = new SqlDataAdapter(command);
                    da.Fill(salesForceEntity);
                    da.Dispose();
                }
            }

            var unmappedRows = from row in salesForceEntity.AsEnumerable()
                join id in appUserIdList.Keys
                    on row.Field<string>("Id") equals id into rowId
                from subrow in rowId.DefaultIfEmpty()
                where subrow == null
                select row;

            var uDataRows = unmappedRows.ToList();
            dataGridView1.DataSource = uDataRows.Any() ? uDataRows.CopyToDataTable() : null;

            if (bothViews)
            {
                var mappedTable = salesForceEntity.Clone();
                mappedTable.Columns.Add("MappedAccount");

                foreach (DataRow row in salesForceEntity.Rows)
                {
                    var id = row.Field<string>("Id");
                    if(!appUserIdList.Keys.Contains(id)) continue;

                    foreach (var mappedAccount in appUserIdList[id])
                    {
                        var mappedrow = mappedTable.NewRow();

                        mappedrow["Type"]= row["Type"];
                        mappedrow["Id"]= row["Id"];
                        mappedrow["SystemAccount"]= row["SystemAccount"];
                        mappedrow["FirstName"]= row["FirstName"];
                        mappedrow["LastName"]= row["LastName"];
                        mappedrow["SubordinateCount"]= row["SubordinateCount"];
                        mappedrow["WithStatus1"]= row["WithStatus1"];
                        mappedrow["WithStatus2"]= row["WithStatus2"];
                        mappedrow["WithStatus3"]= row["WithStatus3"];
                        mappedrow["WithStatus4"]= row["WithStatus4"];
                        mappedrow["MappedAccount"] = mappedAccount;

                        mappedTable.Rows.Add(mappedrow);
                    }
                }

                dataGridView2.DataSource = mappedTable;
            }
            else
            {
                var account = textBox1.Text;
                var tenant = comboBoxTenants.Text;
                if (!string.IsNullOrEmpty(account) && !string.IsNullOrEmpty(tenant))
                {
                    CheckMapping(account, tenant, out var isToAdd);
                    if (isToAdd)
                    {
                        textBoxFname.Enabled = true;
                        textBoxLname.Enabled = true;
                    }
                    else
                    {
                        textBoxFname.Text = "";
                        textBoxLname.Text = "";
                        textBoxFname.Enabled = false;
                        textBoxLname.Enabled = false;
                    }
                }
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Refresh();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedCells.Count == 0)
            {
                MessageBox.Show(Resources.Missing_manager_ID);
                return;
            }

            var selectedrowindex = dataGridView1.SelectedCells[0].RowIndex;
            var selectedRow = dataGridView1.Rows[selectedrowindex];

            var id = Convert.ToString(selectedRow.Cells["Id"].Value);

            if (string.IsNullOrEmpty(id))
            {
                MessageBox.Show(Resources.Missing_manager_ID);
                return;
            }

            var account = textBox1.Text;
            if (string.IsNullOrEmpty(account))
            {
                MessageBox.Show(Resources.Null_account);
                return;
            }

            var tenant = comboBoxTenants.Text;
            if (string.IsNullOrEmpty(tenant))
            {
                MessageBox.Show(Resources.Null_tenant);
                return;
            }

            var firstName = textBoxFname.Text;
            var lastName = textBoxLname.Text;

            var mapped = CheckMapping(account, tenant, out var isToAdd);
            if (isToAdd)
            {
                
                textBoxFname.Enabled = true;
                textBoxLname.Enabled = true;

                if (string.IsNullOrEmpty(firstName))
                {
                    MessageBox.Show(Resources.Empty_first_name);
                    return;
                }

                if (string.IsNullOrEmpty(lastName))
                {
                    MessageBox.Show(Resources.Empty_last_name);
                    return;
                }
            }
            else
            {
                textBoxFname.Text = "";
                textBoxLname.Text = "";
                textBoxFname.Enabled = false;
                textBoxLname.Enabled = false;
            }

            var message = mapped
                ? $"Are you sure you want to CHANGE the mapping of the account '{account}' to the manager id '{id}' in the tenant '{tenant}'?"
                : $"Are you sure you want to INSERT the mapping for the account '{account}' to the manager id '{id}' in the tenant '{tenant}'?";

            var res = MessageBox.Show(message,
                Resources.Confirmation, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (res == DialogResult.OK)
            {
                if (mapped)
                    ChangeUserMapping(account, tenant, id, _catalogConnectionString);
                else
                    InsertUserMapping(account, firstName, lastName, tenant, id, _catalogConnectionString);

                Refresh(false);
            }
        }

        private void ComboBoxTenants_SelectedIndexChanged(object sender, EventArgs e)
        {
            Refresh();
        }
        
        private void Button3_Click(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedCells.Count == 0)
            {
                MessageBox.Show(Resources.Missing_manager_ID);
                return;
            }

            var selectedrowindex = dataGridView2.SelectedCells[0].RowIndex;
            var selectedRow = dataGridView2.Rows[selectedrowindex];

            var tenant = dataGridView2.Columns.Contains("TenantId") ? Convert.ToString(selectedRow.Cells["TenantId"].Value) : comboBoxTenants.Text;
            var id = dataGridView2.Columns.Contains("AppUserId") ? Convert.ToString(selectedRow.Cells["AppUserId"].Value) : dataGridView2.Columns.Contains("Id") ? Convert.ToString(selectedRow.Cells["Id"].Value) : "";
            var account = dataGridView2.Columns.Contains("UserId") ? Convert.ToString(selectedRow.Cells["UserId"].Value) : dataGridView2.Columns.Contains("MappedAccount") ? Convert.ToString(selectedRow.Cells["MappedAccount"].Value) : "";


            if (string.IsNullOrEmpty(tenant) || string.IsNullOrEmpty(id) || string.IsNullOrEmpty(account))
            {
                MessageBox.Show($"Mapping not found for tenant {tenant}, AppUserId {id} and account {account}.");
                return;
            }

            var message =  $"Are you sure you want to unmap the relation between the account'{account}' and the manager '{id}' in the tenant '{tenant}'?";

            var res = MessageBox.Show(message,
                Resources.Confirmation, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (res == DialogResult.OK)
            {
                UserUnMapping(account, tenant, id, _catalogConnectionString);
                Refresh(dataGridView2.Columns.Contains("MappedAccount"));
            }
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            var account = textBox1.Text;
            if (string.IsNullOrEmpty(account))
            {
                MessageBox.Show(Resources.Null_account);
                return;
            }

            var tenant = comboBoxTenants.Text;
            if (string.IsNullOrEmpty(tenant))
            {
                MessageBox.Show(Resources.Null_tenant);
                return;
            }

            CheckMapping(textBox1.Text, tenant, out var isToAdd);
            if (isToAdd)
            {
                textBoxFname.Enabled = true;
                textBoxLname.Enabled = true;
            }
            else
            {
                textBoxFname.Text = "";
                textBoxLname.Text = "";
                textBoxFname.Enabled = false;
                textBoxLname.Enabled = false;
            }
        }

        private bool CheckMapping(string account, string tenant, out bool isToAdd)
        {
            var salesForceEntity = new DataTable();

            using (var connection = new SqlConnection(_catalogConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(
                    $"select UserId, TenantId, AppUserId from [cat].[UserTenantRelation] where UserId = '{account}'",
                    connection))
                {
                    var da = new SqlDataAdapter(command);
                    da.Fill(salesForceEntity);
                    da.Dispose();
                }
            }

            isToAdd = salesForceEntity.Rows.Count == 0;
            dataGridView2.DataSource = salesForceEntity;

            return salesForceEntity.Select($"TenantId = '{tenant}'").Length > 0;
        }

        private static void InsertUserMapping(string account, string firstName,string lastName,string tenant, string id, string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();

                using (var command = new SqlCommand(
                    @"
IF NOT EXISTS (SELECT * FROM [cat].[User] WITH (NOLOCK) 
                   WHERE Id = @Id)
   BEGIN
       INSERT INTO [cat].[User] (Id, Role, FirstName, LastName, PasswordIteration)
       VALUES (@Id, 'NormalUser', @FirstName, @LastName, 0)
   END",
                    connection))
                {
                    command.Parameters.AddWithValue("Id", account);
                    command.Parameters.AddWithValue("FirstName", firstName);
                    command.Parameters.AddWithValue("LastName", lastName);

                    command.ExecuteNonQuery();
                }

                using (var command = new SqlCommand(
                    "insert into [cat].[UserTenantRelation] (UserId, TenantId, ApplicationType, AppUserId) values (@UserId, @TenantId, 'speedylead', @AppUserId);",
                    connection))
                {
                    command.Parameters.AddWithValue("UserId", account);
                    command.Parameters.AddWithValue("TenantId", tenant);
                    command.Parameters.AddWithValue("AppUserId", id);

                    command.ExecuteNonQuery();
                }
            }
        }

        private static void ChangeUserMapping(string account, string tenant, string id, string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(
                    "update [cat].[UserTenantRelation] set AppUserId = @AppUserId where UserId = @UserId and TenantId = @TenantId;",
                    connection))
                {
                    command.Parameters.AddWithValue("UserId", account);
                    command.Parameters.AddWithValue("TenantId", tenant);
                    command.Parameters.AddWithValue("AppUserId", id);

                    command.ExecuteNonQuery();
                }
            }
        }

        private static void UserUnMapping(string account, string tenant, string id, string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(
                    "delete from [cat].[UserTenantRelation] where AppUserId = @AppUserId and UserId = @UserId and TenantId = @TenantId;",
                    connection))
                {
                    command.Parameters.AddWithValue("UserId", account);
                    command.Parameters.AddWithValue("TenantId", tenant);
                    command.Parameters.AddWithValue("AppUserId", id);

                    command.ExecuteNonQuery();
                }
            }

        }

        private static void CheckUserData(string id, string tenantId)
        {
            var userDataEntity = new DataTable();

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings[tenantId].ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(
                    @"select
(select count(*) from [act].[ActivityEntity] where CreatorId = @id) as 'ActivityEntity',
(select count(*) from [rat].[Rate] where EventId in (select Id from [cal].[RideAlong] where User = @id)) as 'Rate',
(select count(*) from [cal].[RideAlong] where User = @id) as 'RideAlong' ,
(select count(*) from [cal].[LockedEvent] where User = @id)  as 'LockedEvent',
(select count(*) from [cal].[PlanningEvent] where User = @id)  as 'PlanningEvent';",
                    connection))
                {
                    command.Parameters.AddWithValue("id", id);

                    var da = new SqlDataAdapter(command);
                    da.Fill(userDataEntity);
                    da.Dispose();
                }
            }

            var from2 = new Form2
            {
                SourceTable = {DataSource = userDataEntity}, Text = $"{tenantId} - {id} record counting"
            };

            from2.ShowDialog();
        }

        private static void DropUserMapping(string id, string tenantId)
        {
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings[tenantId].ConnectionString))
            {
                connection.Open();  

                using (var tran = connection.BeginTransaction()) {
                    try
                    {
                        var command1 = new SqlCommand(
                            "delete from [act].[ActivityEntity] where CreatorId = @id;",
                            connection, tran);
                            command1.Parameters.AddWithValue("id", id);
                            command1.ExecuteNonQuery();
                      

                        var command2 = new SqlCommand(
                            "delete from [rat].[Rate] where EventId in (select id from [cal].[RideAlong] where User = @id);",
                            connection, tran);
                            command2.Parameters.AddWithValue("id", id);
                            command2.ExecuteNonQuery();
                      

                        var command3 = new SqlCommand(
                            "delete from [cal].[RideAlong] where User = @id;",
                            connection, tran);
                            command3.Parameters.AddWithValue("id", id);
                            command3.ExecuteNonQuery();
                       

                        var command4 = new SqlCommand(
                            "delete from [cal].[LockedEvent] where User = @id;",
                            connection, tran);
                            command4.Parameters.AddWithValue("id", id);
                            command4.ExecuteNonQuery();
                      

                        var command5 = new SqlCommand(
                            "delete from [cal].[PlanningEvent] where User = @id;",
                            connection, tran);
                            command5.Parameters.AddWithValue("id", id);
                            command5.ExecuteNonQuery();
                      

                        tran.Commit();
                    }  catch (Exception e) {

                        tran.Rollback();
                        MessageBox.Show($"Rollback due to: {e.Message}", "SQL Error", MessageBoxButtons.OK);
                    }
                }
            }
        }

        private void SelectForMappingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var selectedrowindex = dataGridView1.SelectedCells[0].RowIndex;
            var selectedRow = dataGridView1.Rows[selectedrowindex];

            textBox1.Text = Convert.ToString(selectedRow.Cells["SystemAccount"].Value);
            textBoxFname.Text = Convert.ToString(selectedRow.Cells["firstName"].Value);
            textBoxLname.Text = Convert.ToString(selectedRow.Cells["lastName"].Value);
        }

        private void ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var tenant = comboBoxTenants.Text;
            if (string.IsNullOrEmpty(tenant))
            {
                MessageBox.Show(Resources.Null_tenant);
                return;
            }

            if (dataGridView2.SelectedCells.Count == 0)
            {
                MessageBox.Show(Resources.Missing_manager_ID);
                return;
            }

            var selectedrowindex = dataGridView2.SelectedCells[0].RowIndex;
            var selectedRow = dataGridView2.Rows[selectedrowindex];

            var id = dataGridView2.Columns.Contains("Id") ? Convert.ToString(selectedRow.Cells["Id"].Value) : dataGridView2.Columns.Contains("AppUserId") ? Convert.ToString(selectedRow.Cells["AppUserId"].Value) : "";

            if (string.IsNullOrEmpty(id))
            {
                MessageBox.Show(Resources.Missing_manager_ID);
                return;
            }

            var tenantId = dataGridView2.Columns.Contains("TenantId") ? Convert.ToString(selectedRow.Cells["TenantId"].Value) : comboBoxTenants.Text;
       
            CheckUserData(id, tenantId);
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedCells.Count == 0)
            {
                MessageBox.Show(Resources.Missing_manager_ID);
                return;
            }

            var selectedrowindex = dataGridView2.SelectedCells[0].RowIndex;
            var selectedRow = dataGridView2.Rows[selectedrowindex];

            var id = dataGridView2.Columns.Contains("Id") ? Convert.ToString(selectedRow.Cells["Id"].Value) : dataGridView2.Columns.Contains("AppUserId") ? Convert.ToString(selectedRow.Cells["AppUserId"].Value) : "";

            if (string.IsNullOrEmpty(id))
            {
                MessageBox.Show(Resources.Missing_manager_ID);
                return;
            }

            var tenantId = dataGridView2.Columns.Contains("TenantId") ? Convert.ToString(selectedRow.Cells["TenantId"].Value) : comboBoxTenants.Text;
             
            var message =  $"Are you sure you want to cleanup all the data of the manager id '{id}' from the tenant '{tenantId}'?";

            var res = MessageBox.Show(message,
                Resources.Confirmation, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (res == DialogResult.OK)
            {
                DropUserMapping( id, tenantId);
                Refresh(false);
            }
        }
    }
}