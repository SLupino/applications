﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpeedyLeadMapper
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            SourceTable = new BindingSource();
            dataGridView1.DataSource = SourceTable;
        }

        public BindingSource SourceTable { get; set; }

        private void DataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            dataGridView1.ClearSelection();  
        }
    }
}
