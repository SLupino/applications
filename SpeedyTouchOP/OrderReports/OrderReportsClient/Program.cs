﻿using System;
using System.Configuration;
using System.Linq;
using OrderReportsClient.Helpers;
using NLog;
using Helpers;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel.Description;
using NLog.Layouts;

namespace OrderReportsClient
{
    class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        static void Main()
        {
            Logger.Trace("Start Report Generator");
            try
            {
                var companyCodes = ConfigurationManager.AppSettings.AllKeys.Where(x => x.Split('.').Count() > 1).Select(x => x.Split('.')[0]).Distinct().ToList();
                foreach (var companyCode in companyCodes)
                {
                    Logger.Trace("company in proces : {0}", companyCode);
                    string response;
                    using (var client = new OrderInformationServiceReference.OrderInformationServiceClient("OrderInformationService"))
                    {

                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                        client.ClientCredentials.UserName.UserName = "WGS\\saKONZsbSOrecoreca";
                        client.ClientCredentials.UserName.Password = "eLytod9cTShF";

                        Logger.Trace("client instantiated");
                        response = client.GetOrderData(XmlHelper.GetXmlForProxyCall(Settings.StagingDatabase(companyCode), Settings.IncomingDatabase(companyCode), companyCode));
                        Logger.Trace("Service Responce: {0}", response);
                    }

                    string generalReportPath = ExcelHelper.WriteGeneralReportExcel(response, companyCode);
                    Logger.Trace("general report path: {0}", generalReportPath);
                    string montlyReportPath = string.Empty;
                    var dateNow = DateTime.Now;
                    if ((dateNow.Day == 1) || ((dateNow.DayOfWeek == DayOfWeek.Monday) && ((dateNow.Day == 2) || (dateNow.Day == 3))))
                    {
                        montlyReportPath = ExcelHelper.WriteMontlyReportExcel(response, companyCode);
                        Logger.Trace("montly report path: {0}", montlyReportPath);
                    }
                    EmailHelper.SendEmail(Settings.SmtpConfig, 
                        Settings.FromEmail(companyCode), Settings.ToEmails(companyCode), 
                        Settings.CcEmails(companyCode), new List<string> { generalReportPath, montlyReportPath }, 
                        Settings.Subject(companyCode), Settings.Body(companyCode));
                }
            }
            catch (Exception e)
            {
                Logger.Trace("Exception: {0}", e.Message);
                throw;
            }
        }
    }
}
