﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Microsoft.Office.Interop.Excel;
using OrderReportsClient.Classes;
using NLog;

namespace OrderReportsClient.Helpers
{
    class ExcelHelper
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public static string WriteGeneralReportExcel(string data, string companyCode)
        {
            Logger.Trace("Start general report generator");
            var generalPathTemplate = ConfigurationManager.AppSettings["ReportTemplate"];
            var mondayPathTemplate = ConfigurationManager.AppSettings["ReportTemplateMonday"];
            var outputPath = ConfigurationManager.AppSettings[string.Format("{0}.OrderReportDestination", companyCode)];
            var characterNumber = Convert.ToInt32(ConfigurationManager.AppSettings[string.Format("{0}.UserCharacterCount", companyCode)]);
            var dateNow = DateTime.Now;
            var attachmentPath = string.Format("{0}_{1}-{2}-{3}.xlsx", outputPath, dateNow.Year, dateNow.Month, dateNow.Day);

            try
            {
                var xlApp = new Application {DisplayAlerts = false};
                Workbook excelWorkbook;
                Worksheet saturdayOrdersShit = null;
                Worksheet fridayOrdersShit = null;
                Sheets excelSheets;
                if (DateTime.Now.DayOfWeek == DayOfWeek.Monday)
                {
                    Logger.Trace("Is monday");
                    excelWorkbook = xlApp.Workbooks.Open(mondayPathTemplate, 0, false, 5, "", "",
                        true, XlPlatform.xlWindows, "", true, false, 0, true, false, false);

                    excelSheets = excelWorkbook.Worksheets;

                    saturdayOrdersShit = (Worksheet) excelSheets.Item["Saturday orders"];
                    fridayOrdersShit = (Worksheet) excelSheets.Item["Friday orders"];
                }
                else
                {
                    Logger.Trace("Isn't monday");
                    excelWorkbook =
                        xlApp.Workbooks.Open(
                            generalPathTemplate, 0, false, 5, "", "", true,
                            XlPlatform.xlWindows, "", true, false, 0, true, false, false);

                    excelSheets = excelWorkbook.Worksheets;
                }

                var lastMonthOrdersShit = (Worksheet) excelSheets.Item["Last 30 days"];
                var yesterdayOrdersShit = (Worksheet) excelSheets.Item["Yesterday orders"];
                var adminAMonthShit = (Worksheet) excelSheets.Item["ADM in a month"];

                XElement xData = XElement.Parse(data);
                var monthOrders = CreateLastMonthStatisticsList(xData.XPathSelectElement("//LastMonthData"));
                var yesterdayOrders = CreateOldDayList(xData.XPathSelectElement("//YesterdayData"));
                var saturdayOrders = CreateOldDayList(xData.XPathSelectElement("//SaturdayData"));
                var fridayOrders = CreateOldDayList(xData.XPathSelectElement("//FridayData"));
                var adminAMonth = CreateOldDayList(xData.XPathSelectElement("//LastMonthAdmData"));

                ModifyReport(monthOrders, lastMonthOrdersShit, true, characterNumber);
                ModifyReport(yesterdayOrders, yesterdayOrdersShit, false, characterNumber);
                if (saturdayOrdersShit != null)
                    ModifyReport(saturdayOrders, saturdayOrdersShit, false, characterNumber);
                if (fridayOrdersShit != null)
                    ModifyReport(fridayOrders, fridayOrdersShit, false, characterNumber);
                ModifyReport(adminAMonth, adminAMonthShit, false, characterNumber);
                Logger.Trace("start save file in path: {0}", attachmentPath);
                excelWorkbook.SaveAs(attachmentPath);
                xlApp.Workbooks.Close();
                Logger.Trace("end save file in path: {0}", attachmentPath);
            }
            catch (Exception e)
            {
                Logger.Trace("EXCEPTION: {0}", e.Message);
                throw;
            }

            return attachmentPath;
        }

        public static string WriteMontlyReportExcel(string data, string companyCode)
        {
            Logger.Trace("Start montly report generator");
            var montlyPathTemplate = ConfigurationManager.AppSettings["ReportTemplateMontly"];
            var outputmontlyReportPath = ConfigurationManager.AppSettings[string.Format("{0}.MontlyReportDestination", companyCode)];
            var characterNumber = Convert.ToInt32(ConfigurationManager.AppSettings[string.Format("{0}.UserCharacterCount", companyCode)]);
            var dateNow = DateTime.Now;
            var year = dateNow.Month == 1 ? dateNow.Year - 1 : dateNow.Year;
            var month = dateNow.Month == 1 ? 12 : dateNow.Month - 1;
            var attachmentPath = string.Format("{0}_{1}-{2}.xlsx", outputmontlyReportPath, year, month);

            try
            {
                var xlApp = new Application { DisplayAlerts = false };
                Workbook excelWorkbook;
                Sheets excelSheets;

                excelWorkbook =
                    xlApp.Workbooks.Open(
                        montlyPathTemplate, 0, false, 5, "", "", true,
                        XlPlatform.xlWindows, "", true, false, 0, true, false, false);

                excelSheets = excelWorkbook.Worksheets;

                var lastMonthOrdersShit = (Worksheet)excelSheets.Item["Montly Order"];

                XElement xData = XElement.Parse(data);
                var monthOrders = CreateOldDayList(xData.XPathSelectElement("//MontlyData"));

                ModifyReport(monthOrders, lastMonthOrdersShit, false, characterNumber);

                excelWorkbook.SaveAs(attachmentPath);
                xlApp.Workbooks.Close();
            }
            catch (Exception e)
            {
                Logger.Trace("EXCEPTION: {0}", e.Message);
            }

            return attachmentPath;
        }

        private static List<LastMonthStatistics> CreateLastMonthStatisticsList(XElement lastMonth)
        {
            var statisticsList = lastMonth.XPathSelectElements("Statistics");

            return statisticsList.Select(statistics => new LastMonthStatistics {Date = Convert.ToDateTime(statistics.XPathSelectElement("Date").Value), OrderCount = Convert.ToInt32(statistics.XPathSelectElement("OrderCount").Value)}).ToList();
        }

        private static List<YesterdayStatistics> CreateOldDayList(XElement oldDay)
        {
            if (oldDay != null)
            {
                var statisticsList = oldDay.XPathSelectElements("User");

                return statisticsList.Select(statistics => new YesterdayStatistics
                {
                    ManagerId = statistics.XPathSelectElement("ManagerId").Value,
                    UserId = statistics.XPathSelectElement("Id").Value,
                    UserName = statistics.XPathSelectElement("Name").Value,
                    UserType = statistics.XPathSelectElement("Type").Value,
                    OrderCount = Convert.ToInt32(statistics.XPathSelectElement("OrderCount").Value)
                }).ToList();
            }

            return new List<YesterdayStatistics>();
        }

        private static void ModifyReport(IEnumerable<object> orders, Worksheet dataShit, bool isMontlyShit, int characterNumber)
        {
            Logger.Trace("Start modify report");
            int rowcount = 2;
            object misValue = System.Reflection.Missing.Value;
            var stats = orders as IList<object> ?? orders.ToList();
            int allRowsCount = 0;
            IEnumerable<YesterdayStatistics> admStats = new List<YesterdayStatistics>();

            if (!isMontlyShit)
            {
                admStats = stats.Cast<YesterdayStatistics>().Where(x => x.UserType != "AM").OrderBy(x => x.UserType).ToList();
                allRowsCount = dataShit.Name == "Yesterday orders" || dataShit.Name == "Saturday orders" || dataShit.Name == "Friday orders" ?
                stats.Count() :
                admStats.Count();
                Logger.Trace("Montly sheet");
                dataShit.Range[string.Format("B{0}", 2 + allRowsCount), string.Format("C{0}", 2 + allRowsCount)].Merge();
                dataShit.Range[string.Format("B{0}", 2 + allRowsCount), string.Format("C{0}", 2 + allRowsCount)].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                dataShit.Range[string.Format("B{0}", 2 + allRowsCount), string.Format("C{0}", 2 + allRowsCount)].Value = "TOTAL";

                dataShit.Cells[2 + allRowsCount, 4].Formula = string.Format("=SUM(D2:D{0})", allRowsCount + 1);
                dataShit.Range[string.Format("B{0}", 2 + allRowsCount), string.Format("C{0}", 2 + allRowsCount)].BorderAround(Type.Missing, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);
                dataShit.Range[string.Format("D{0}", 2 + allRowsCount), string.Format("D{0}", 2 + allRowsCount)].BorderAround(Type.Missing, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);

                dataShit.Range[string.Format("B{0}", 2), string.Format("B{0}", 1 + allRowsCount)].BorderAround(Type.Missing, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);
                dataShit.Range[string.Format("C{0}", 2), string.Format("C{0}", 1 + allRowsCount)].BorderAround(Type.Missing, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);
                dataShit.Range[string.Format("D{0}", 2), string.Format("D{0}", 1 + allRowsCount)].BorderAround(Type.Missing, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);

                var xlCharts = (ChartObjects)dataShit.ChartObjects(Type.Missing);
                var myChart = xlCharts.Add(400, 13, 3000, 600);
                Chart chartPage = myChart.Chart;

                Range chartRange = dataShit.Range[string.Format("C{0}", 2), string.Format("D{0}", 1 + allRowsCount)];
                chartPage.SetSourceData(chartRange, misValue);
                chartPage.ChartType = XlChartType.xlColumnClustered;
            }

            if (isMontlyShit)
            {
                foreach (LastMonthStatistics order in stats)
                {
                    ((Range) dataShit.Cells[rowcount, 2]).Value = order.Date;
                    ((Range) dataShit.Cells[rowcount, 3]).Value = order.OrderCount;
                    rowcount++;
                }
            }
            else
            {
                if (dataShit.Name == "Yesterday orders" || dataShit.Name == "Saturday orders" || dataShit.Name == "Friday orders")
                {
                    IEnumerable<YesterdayStatistics> admWithoutAmStats =
                        stats.Cast<YesterdayStatistics>()
                            .Where(x => x.UserType != "AM" && (x.ManagerId == "null" || x.ManagerId == ""))
                            .OrderBy(x => x.UserName)
                            .ToList();

                    stats = stats.Except(admWithoutAmStats).ToList();

                    List<YesterdayStatistics> amStats =
                        stats.Cast<YesterdayStatistics>()
                            .Where(x => x.UserType == "AM")
                            .OrderBy(x => x.UserType)
                            .ToList();
                    stats = stats.Except(amStats).ToList();

                    List<YesterdayStatistics> admStatsFiltered = (from YesterdayStatistics amStat in amStats.AsEnumerable()
                        join YesterdayStatistics stat in stats.AsEnumerable()
                            on amStat.UserId equals stat.ManagerId
                        select stat).ToList();
                    stats = stats.Except(admStatsFiltered).ToList();

                    foreach (YesterdayStatistics admStat in admWithoutAmStats)
                    {
                        if (rowcount <= allRowsCount) SetCelStyle(dataShit, rowcount);
                        SetCelValues(admStat, dataShit, rowcount, characterNumber);

                        rowcount++;
                    }

                    foreach (YesterdayStatistics stat in stats.ToList())
                    {
                        if (rowcount <= allRowsCount) SetCelStyle(dataShit, rowcount);
                        SetCelValues(stat, dataShit, rowcount, characterNumber);
                        rowcount++;
                    }

                    foreach (YesterdayStatistics amStat in amStats)
                    {
                        if (rowcount <= allRowsCount) SetCelStyle(dataShit, rowcount);
                        SetCelValues(amStat, dataShit, rowcount, characterNumber);

                        //var rowCell = rowcount;
                        var formatRange = dataShit.get_Range(string.Format("b{0}", rowcount),
                            string.Format("e{0}", rowcount));
                        formatRange.Font.Color = 255;
                        List<YesterdayStatistics> admStatsFromAm =
                            admStatsFiltered.Where(x => x.ManagerId == amStat.UserId).ToList();
                        foreach (YesterdayStatistics admStat in admStatsFromAm)
                        {
                            rowcount++;
                            if (rowcount <= allRowsCount) SetCelStyle(dataShit, rowcount);
                            SetCelValues(admStat, dataShit, rowcount, characterNumber);
                        }
                        rowcount++;
                    }
                }
                else
                {
                    foreach (YesterdayStatistics admStat in admStats)
                    {
                        if (rowcount <= allRowsCount) SetCelStyle(dataShit, rowcount);
                        SetCelValues(admStat, dataShit, rowcount, characterNumber);

                        rowcount++;
                    }
                }
            }

            if (dataShit.Name == "Yesterday orders" || dataShit.Name == "Saturday orders" || dataShit.Name == "Friday orders")
                dataShit.Range[string.Format("E{0}", 2), string.Format("E{0}", 1 + allRowsCount)].BorderAround(Type.Missing, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);
        }

        private static void SetCelValues(YesterdayStatistics orderAdm, Worksheet dataShit, int rowcount, int characterNumber)
        {
            var admId = orderAdm.UserId;
            ((Range)dataShit.Cells[rowcount, 2]).Value = admId.Substring((admId.Count() - characterNumber), characterNumber);
            ((Range)dataShit.Cells[rowcount, 3]).Value = orderAdm.UserName;
            ((Range)dataShit.Cells[rowcount, 4]).Value = orderAdm.OrderCount;
            if (dataShit.Name == "Yesterday orders" || dataShit.Name == "Saturday orders" ||
                dataShit.Name == "Friday orders")
            {
                ((Range)dataShit.Cells[rowcount, 5]).Value = orderAdm.UserType;
            }
        }

        private static void SetCelStyle(Worksheet dataShit, int rowcount)
        {
            try
            {
                Logger.Trace("start pasteSpecial row {0}", rowcount + 1);
                var r1 = (Range) dataShit.Cells[rowcount, 2];
                r1.Copy(Type.Missing);
                var r2 = (Range) dataShit.Cells[rowcount + 1, 2];
                r2.PasteSpecial(XlPasteType.xlPasteFormats,
                    XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);

                r1 = (Range) dataShit.Cells[rowcount, 3];
                r1.Copy(Type.Missing);
                r2 = (Range) dataShit.Cells[rowcount + 1, 3];
                r2.PasteSpecial(XlPasteType.xlPasteFormats,
                    XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);

                r1 = (Range) dataShit.Cells[rowcount, 4];
                r1.Copy(Type.Missing);
                r2 = (Range) dataShit.Cells[rowcount + 1, 4];
                r2.PasteSpecial(XlPasteType.xlPasteFormats,
                    XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);

                r1 = (Range) dataShit.Cells[rowcount, 5];
                r1.Copy(Type.Missing);
                r2 = (Range) dataShit.Cells[rowcount + 1, 5];
                r2.PasteSpecial(XlPasteType.xlPasteFormats,
                    XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);
                Logger.Trace("end pasteSpecial row {0}", rowcount + 1);
            }
            catch (Exception e)
            {
                Logger.Trace("Exception: {0}", e.Message);
            }
        }
    }
}
