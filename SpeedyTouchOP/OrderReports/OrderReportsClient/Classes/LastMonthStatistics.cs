﻿using System;

namespace OrderReportsClient.Classes
{
    public class LastMonthStatistics
    {
        public DateTime Date { get; set; }
        public int OrderCount { get; set; }
    }
}
