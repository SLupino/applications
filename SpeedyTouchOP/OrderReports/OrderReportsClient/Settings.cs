﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace OrderReportsClient
{
    public static class Settings
    {
        public static List<string> SmtpConfig { get { return new List<string> { ConfigurationManager.AppSettings["SMTPServer"],
                ConfigurationManager.AppSettings["SMTPUser"],
                ConfigurationManager.AppSettings["SMTPPassword"],
                ConfigurationManager.AppSettings["SMTPDomain"] }; } }

        public static string FromEmail(string companyCode)
        {
            return ConfigurationManager.AppSettings[companyCode + ".FromEmail"];
        }
        public static List<string> ToEmails(string companyCode)
        {
            return ConfigurationManager.AppSettings[companyCode + ".ToEmail"].Split(';').ToList();
        }
        public static List<string> CcEmails(string companyCode)
        {
            return ConfigurationManager.AppSettings[companyCode + ".CCEmail"].Split(';').ToList();
        }
        public static string Subject(string companyCode)
        {
            return ConfigurationManager.AppSettings[companyCode + ".SubjectEmail"];
        }
        public static string Body(string companyCode)
        {
            return ConfigurationManager.AppSettings[companyCode + ".BodyEmail"].Replace("|", @"<br\>");
        }
        public static string StagingDatabase(string companyCode)
        {
            return ConfigurationManager.ConnectionStrings[companyCode + ".StagingDatabase"].ConnectionString;
        }
        public static string IncomingDatabase(string companyCode)
        {
            return ConfigurationManager.ConnectionStrings[companyCode + ".IncomingDatabase"].ConnectionString;
        }
        public static string ConsolidatedConnection(string companyCode)
        {
            return ConfigurationManager.ConnectionStrings[companyCode + ".ConsolidatedConnection"].ConnectionString;
        }
    }
    
}

                                                                        