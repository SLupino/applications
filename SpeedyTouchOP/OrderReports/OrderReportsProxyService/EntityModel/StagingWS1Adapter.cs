﻿using System.Data.Entity;
using System.Text;

namespace OrderReportsProxyService.EntityModel
{
    public partial class StagingWS1 : DbContext
    {
        public StagingWS1(string connectionString)
            : base(connectionString)
        {

        }

        public static StagingWS1 GetStagingWS1Context(string connectionString, StringBuilder logline)
        {/*;MultipleActiveResultSets=True;App=EntityFramework*/
            var efConnectionString =
                $@"metadata=res://*/EntityModel.StagingWS1.csdl|res://*/EntityModel.StagingWS1.ssdl|res://*/EntityModel.StagingWS1.msl;provider=System.Data.SqlClient;provider connection string=""{connectionString}MultipleActiveResultSets=True;App=EntityFramework"";";
            logline.AppendLine(efConnectionString);
            return new StagingWS1(efConnectionString);
        }
    }
}