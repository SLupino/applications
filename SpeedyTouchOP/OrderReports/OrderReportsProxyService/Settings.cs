﻿using System;
using System.Configuration;

namespace OrderReportsProxyService
{
    internal class Settings
    {
        internal static string GetCatalogItemConnectionString()
        {
            return ConnectionString("StagingWS1");
        }

        private static string ConnectionString(string connectionString)
        {
            ConnectionStringSettings connectionStringSettings = ConfigurationManager.ConnectionStrings[connectionString];

            if (connectionStringSettings == null)
            {
                throw new Exception($"Missing connection string {connectionString}");
            }
            return connectionStringSettings.ConnectionString;
        }
    }
}