﻿using Helpers.Classes;
using NLog;
using OrderReportsProxyService.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;
using OrderReportsProxyService.EntityModel;

namespace OrderReportsProxyService
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class OrderInformationService : IOrderInformationService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private StringBuilder globalLogLines = new StringBuilder();

        /// <summary>
        /// Receive xml from client for extract order data with query SQL,
        /// And generate xml response with all order data.
        /// </summary>
        /// <param name="companyInfo">
        /// XML like this:
        /// <CompanyInfo>
        ///     <CompanyCode>0000</CompanyCode>
        ///     <StagingConnection>Server={serverName};Database={databaseName};User Id={userNameSQL};Password={passwordSQL};</StagingConnection>
        ///     <IncomingConnection>Server={serverName};Database={databaseName};User Id={userNameSQL};Password={passwordSQL};</IncomingConnection>
        ///     <ConsolidateConnection>Server={serverName};Database={databaseName};User Id={userNameSQL};Password={passwordSQL};</ConsolidateConnection>
        /// </CompanyInfo>
        /// </param>
        /// <returns>
        /// call method that return XML with orders data like this:
        /// <OrderInfo>
        ///    <CompanyCode></CompanyCode>
        ///    <LastMonthData>
        ///        <Statistics>
        ///            <Date></Date>
        ///            <OrderCount></OrderCount>
        ///        </Statistics>
        ///        <Statistics>
        ///            <Date></Date>
        ///            <OrderCount></OrderCount>
        ///        </Statistics>
        ///    </LastMonthData>
        ///    <YesterdayData>
        ///        <User>
        ///            <Id></Id>
        ///            <Name></Name>
        ///            <OrderCount></OrderCount>
        ///            <Type></Type>
        ///        </User>
        ///        <User>
        ///            <Id></Id>
        ///            <Name></Name>
        ///            <OrderCount></OrderCount>
        ///            <Type></Type>
        ///        </User>
        ///    </YesterdayData>
        ///    <SaturdayData></SaturdayData> optional come yesterday
        ///    <FridayData></FridayData> optional come yesterday
        ///    <LastMonthAdmData>
        ///        <User>
        ///            <Id></Id>
        ///            <Name></Name>
        ///            <OrderCount></OrderCount>
        ///            <Type></Type>
        ///        </User>
        ///        <User>
        ///            <Id></Id>
        ///            <Name></Name>
        ///            <OrderCount></OrderCount>
        ///            <Type></Type>
        ///        </User>
        ///    </LastMonthAdmData>
        ///</OrderInfo>"
        /// </returns>
        public string GetOrderData(string companyInfo)
        {
            try
            {
                //assign company data into variables
                Logger.Trace("Start Service with request : {0}", companyInfo);
                var xCompanyInfo = XDocument.Parse(companyInfo);
                var companyCode = xCompanyInfo.XPathSelectElement("/*/CompanyCode").Value;
                var stagingConnection = xCompanyInfo.XPathSelectElement("/*/StagingConnection").Value;
                var incomingConnection = xCompanyInfo.XPathSelectElement("/*/IncomingConnection").Value;
                Logger.Trace("Start SQL Queries staging connection: {0} AND incoming connection {1}", stagingConnection,
                    incomingConnection);
                var listUserClass = Helpers.DatabaseHelper.SelectStagingUsers(stagingConnection, incomingConnection);
                Logger.Trace("success staging query");
                var lastMonthOrders = Helpers.DatabaseHelper.SelectOrderForLastMonth(incomingConnection);
                Logger.Trace("success last month order query");
                var yesterdayOrders = Helpers.DatabaseHelper.SelectOrderForYesterday(incomingConnection, -1, listUserClass, stagingConnection);
                Logger.Trace("success yesterday order query");
                var saturdayOrders = new List<YesterdayStatistics>();
                var fridayOrders = new List<YesterdayStatistics>();

                //if today is monday must send firday, saturday and sunday orders
                if (DateTime.Now.DayOfWeek == DayOfWeek.Monday)
                {
                    saturdayOrders = Helpers.DatabaseHelper.SelectOrderForYesterday(incomingConnection, -2, listUserClass, stagingConnection);
                    Logger.Trace("success saturday order query");
                    fridayOrders = Helpers.DatabaseHelper.SelectOrderForYesterday(incomingConnection, -3, listUserClass, stagingConnection);
                    Logger.Trace("success friday order query");
                }
                var lastMonthAdmOrders = Helpers.DatabaseHelper.SelectAdmOrderForLastMonth(listUserClass, incomingConnection);
                Logger.Trace("success last month order query");
                var dateNow = DateTime.Now;
                var montlyOrders = new List<YesterdayStatistics>();
                if ((dateNow.Day == 1) ||
                    ((dateNow.DayOfWeek == DayOfWeek.Monday) && ((dateNow.Day == 2) || (dateNow.Day == 3))))
                    montlyOrders = Helpers.DatabaseHelper.SelectMontlyOrders(listUserClass, incomingConnection).ToList();
                Logger.Trace("success montly order query");
                return Helpers.XmlHelper.GetOrderInfo(companyCode, lastMonthOrders.ToList(), yesterdayOrders, saturdayOrders,
                    fridayOrders, lastMonthAdmOrders.ToList(), montlyOrders);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Exception in GetOrderData with companyInfo = {0}", companyInfo);
                throw;
            }
        }

        public string GetIncomingErrorData(string queryInformations)
        {
            var queryInfo = queryInformations.Split('|');
            Logger.Trace("Start Service with request : {0}", queryInfo[0]);
            var dateNow = DateTime.UtcNow;
            var limitDate = dateNow.AddMinutes(-Convert.ToDouble(queryInfo[1]));
            Logger.Trace("date now is {0} subtract: {1}minutes. The result is: {2}",dateNow, queryInfo[1], limitDate);
            var incomingErrors = Helpers.DatabaseHelper.SelectIncomingErrors(queryInfo[0], limitDate);
            var lastIscRun = Helpers.DatabaseHelper.SelectLastIscRun(queryInfo[0]);
            var whenIscRun = dateNow.Subtract((Convert.ToDateTime(lastIscRun)));
            return Helpers.XmlHelper.GetIncomingErrors(incomingErrors, whenIscRun.ToString());
        }

        public List<Job> GetStagingJobsMonitoring(string stagingsConnection)
        {
            return Helpers.DatabaseHelper.SelectStagingsJobInErrorNames(stagingsConnection);
        }

        public List<UserClientInfo> GetUserClientChanegedInfos(string incomingConnection, string targetVersion)
        {
            return Helpers.DatabaseHelper.SelectUserClientChanegedInfos(incomingConnection, targetVersion);
        }

        public string WriteItemsPreStaging(List<CatalogItem> items, string connectionString)
        {
            var logLines = new StringBuilder("-----------Start Server Logs-----------");
            if (items != null)
            {
                try
                {
                    var companyCode = items.Select(x => x.CompanyCode).FirstOrDefault();
                    logLines.AppendLine(string.Format("processing {1} rows for company: {0}", companyCode, items.Count));
                    var itemList = items.Select(item => new ValidArticles
                    {
                        ArticleId = item.ArticleId,
                        CompanyCode = item.CompanyCode,
                        ProductId = item.ProductId,
                        isMinPackSize = item.IsMinSize
                    }).ToList();

                    using (var context = StagingWS1.GetStagingWS1Context(connectionString, logLines))
                    {
                        context.ValidArticles.RemoveRange(context.ValidArticles.Where(x => x.CompanyCode == companyCode));
                        context.SaveChanges();
                        logLines.AppendLine("Rows are removed");

                        const int batchCount = 5000;
                        for (var idx = 0; idx < itemList.Count; idx += batchCount)
                        {
                            context.ValidArticles.AddRange(itemList.GetRange(idx, Math.Min(batchCount, itemList.Count - idx)));
                            context.SaveChanges();
                            logLines.AppendLine($"{idx} Rows are added");
                        }
                        
                    }
                }
                catch (Exception e)
                {
                    logLines.AppendLine($"EXCEPTION: {e.Message}");
                    logLines.AppendLine($"EXCEPTION: {e.StackTrace}");
                    logLines.AppendLine($"EXCEPTION: {e.InnerException}");
                }
            }
            else
                logLines.AppendLine("Items is null");

            logLines.AppendLine("-----------End Server Logs-----------");

            return logLines.ToString();
        }

        public string CheckProcess()
        {
            var log = globalLogLines.ToString();
            globalLogLines.Clear();
            return log;
        }

        public string WriteItemsPreStagingNew(List<CatalogItem> items, string connectionString)
        {
            globalLogLines.Clear();

            if (items != null)
            {
                Task.Factory.StartNew(() => ProcessRequest(items, connectionString));

                //do not change this value
                return "--START--";
            }
            return "Items is null";
        }

        private async void ProcessRequest(List<CatalogItem> items, string connectionString)
        {
            await Task.Yield();

            try
            {
                var companyCode = items.Select(x => x.CompanyCode).FirstOrDefault();
                globalLogLines.AppendLine(string.Format("processing {1} rows for company: {0}", companyCode, items.Count));
                var itemList = items.Select(item => new ValidArticles_Temp
                {
                    ArticleId = item.ArticleId,
                    CompanyCode = item.CompanyCode,
                    ProductId = item.ProductId,
                    isMinPackSize = item.IsMinSize
                }).ToList();

                using (var context = StagingWS1.GetStagingWS1Context(connectionString, globalLogLines))
                {
                    context.Database.ExecuteSqlCommand("TRUNCATE TABLE ValidArticles_Temp");
                    context.SaveChanges();
                    globalLogLines.AppendLine("Temporary table cleaned");
 
                    const int batchCount = 5000;
                    for (var idx = 0; idx < itemList.Count; idx += batchCount)
                    {
                        int idxEnd = Math.Min(batchCount, itemList.Count - idx);
                        context.ValidArticles_Temp.AddRange(itemList.GetRange(idx, idxEnd));
                        context.SaveChanges();
                        globalLogLines.AppendLine($"{idx + idxEnd} Rows are added in temporary table");
                    }

                    context.Database.ExecuteSqlCommand("SET XACT_ABORT ON");
                    context.SaveChanges();
                    globalLogLines.AppendLine("Set XACT_ABORT ON");

                    context.Database.ExecuteSqlCommand($@"BEGIN TRANSACTION;
		                                    DELETE ValidArticles WHERE CompanyCode = '{companyCode}'
                                            INSERT INTO ValidArticles SELECT * FROM ValidArticles_Temp WHERE CompanyCode = '{companyCode}'
                                    COMMIT TRANSACTION");
                    context.SaveChanges();
                    globalLogLines.AppendLine("Rows are updated");

                    context.Database.ExecuteSqlCommand("SET XACT_ABORT OFF");
                    context.SaveChanges();
                    globalLogLines.AppendLine("Set XACT_ABORT OFF");
                }
            }
            catch (Exception e)
            {
                globalLogLines.AppendLine($"EXCEPTION: {e.Message}");
                globalLogLines.AppendLine($"EXCEPTION: {e.StackTrace}");
                globalLogLines.AppendLine($"EXCEPTION: {e.InnerException}");
            }

            //do not change this value
            globalLogLines.AppendLine("--END--");
        }
    }
}
