﻿
namespace OrderReportsProxyService.Classes
{
    public class User
    {
        public  string ManagerId { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string WnNumber { get; set; }
        public string UserType { get; set; }
    }
}
