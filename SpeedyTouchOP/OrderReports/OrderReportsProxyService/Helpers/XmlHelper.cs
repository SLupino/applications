﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Helpers.Classes;
using OrderReportsProxyService.Classes;
using NLog;

namespace OrderReportsProxyService.Helpers
{
    public static class XmlHelper
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public static string GetOrderInfo(string companyCode, List<LastMonthStatistics> lMs, List<YesterdayStatistics> yS, List<YesterdayStatistics> sS, List<YesterdayStatistics> fS, List<YesterdayStatistics> lMadm, List<YesterdayStatistics> lMontlyReport)
        {
            Logger.Trace("Start xml responce generator");
            var xOrderInfo = new XElement("OrderInfo", new XElement("CompanyCode", companyCode),
                new XElement("LastMonthData"),
                new XElement("YesterdayData"));
            AddLastMonthDataElements(xOrderInfo, lMs);
            AddUsersData(xOrderInfo, yS, DayForStatistics.YesterdayData);
            if (sS.Count > 0)
            {
                xOrderInfo.Add(new XElement("SaturdayData"));
                AddUsersData(xOrderInfo, sS, DayForStatistics.SaturdayData);
            }
            if (fS.Count > 0)
            {
                xOrderInfo.Add(new XElement("FridayData"));
                AddUsersData(xOrderInfo, fS, DayForStatistics.FridayData);
            }
            xOrderInfo.Add(new XElement("LastMonthAdmData"));
            AddUsersData(xOrderInfo, lMadm, DayForStatistics.LastMonthAdmData);
            xOrderInfo.Add(new XElement("MontlyData"));
            AddUsersData(xOrderInfo, lMontlyReport, DayForStatistics.MontlyData);
            Logger.Trace("End xml responce generator");
            return xOrderInfo.ToString();
        }

        public static string GetIncomingErrors(Dictionary<string, string> reqInError, string lastIscRun)
        {
            Logger.Trace("Start xml responce generator");
            var xOrderInfo = new XElement("ErrorInfo");
            var xrequestsInError = new XElement("RequestsInError");
            var xlastIscRun = new XElement("LastIscRun", lastIscRun);
            //var xtime = new XElement("Time...");
            AddErrorData(xrequestsInError, reqInError);
            xOrderInfo.Add(xrequestsInError);
            xOrderInfo.Add(xlastIscRun);

            return xOrderInfo.ToString();
        }

        public static string GetJobsInError(List<Job> jobsInError)
        {
            Logger.Trace("Start xml responce generator");
            var xJobsInError = new XElement("JobsInError");
            var xJobs = new XElement("Jobs");
            
            foreach (var jobInError in jobsInError)
            {
                var xJob = new XElement("Job", 
                    new XElement("Name", jobInError.Name), 
                    new XElement("StepName", jobInError.StepName), 
                    new XElement("Message", jobInError.Message),
                    new XElement("RunDate", jobInError.RunDate),
                    new XElement("RunTime", jobInError.RunTime),
                    new XElement("Enabled", jobInError.Enabled));
                xJobs.Add(xJob);
            }

            xJobsInError.Add(xJobs);
            return xJobsInError.ToString();
        }

        private static void AddLastMonthDataElements(XElement xOrderInfo, List<LastMonthStatistics> lMs)
        {
            var xLastMonthData = xOrderInfo.XPathSelectElement("//LastMonthData");
            lMs.ForEach(dayOrder => xLastMonthData.Add(new XElement("Statistics", new XElement("Date", dayOrder.Date),
                new XElement("OrderCount", dayOrder.OrderCount))));
        }

        private static void AddUsersData(XElement xOrderInfo, List<YesterdayStatistics> yS, DayForStatistics xPathName)
        {
            var xYesterdayData = xOrderInfo.XPathSelectElement(string.Format("//{0}", xPathName));
            yS.ForEach(oldDayOrder => xYesterdayData.Add(new XElement("User", new XElement("ManagerId", oldDayOrder.ManagerId), new XElement("Id", oldDayOrder.UserId),
                new XElement("Name", oldDayOrder.UserName),
                new XElement("OrderCount", oldDayOrder.OrderCount),
                new XElement("Type", oldDayOrder.UserType))));
        }

        private static void AddErrorData(XElement xrequestInError, Dictionary<string, string> eL)
        {
            eL.Keys.ToList().ForEach(key => xrequestInError.Add(new XElement(key, eL[key])));
        }
    }

    public enum DayForStatistics
    {
        YesterdayData,
        SaturdayData,
        FridayData,
        LastMonthAdmData,
        MontlyData
    }
}
