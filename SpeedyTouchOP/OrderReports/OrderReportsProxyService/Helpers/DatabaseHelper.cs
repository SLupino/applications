﻿using System.Text;
using Helpers.Classes;
using NLog;
using NLog.Fluent;
using OrderReportsProxyService.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace OrderReportsProxyService.Helpers
{
    public static class DatabaseHelper
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static int _maxStringCount;

        public static List<Job> SelectStagingsJobInErrorNames(string stagingConnection)
        {
            const string SelectJobWithProblems = @"WITH CTE AS (SELECT step_name, message, run_date, run_time, run_status, job_id, step_id,
    ROW_NUMBER() OVER(PARTITION BY job_id, step_id ORDER BY run_date DESC, run_time DESC) as rn 
FROM MSDB.DBO.SYSJOBHISTORY 
WHERE step_name <> '(Job outcome)')
SELECT sysjobs.name as 'job_name', cte.step_name, cte.message, cte.run_date, cte.run_time, sysjobs.enabled as 'JobEnabled', cte.run_status, sysschedules.enabled as 'SchedulerEnabled'
FROM msdb.dbo.sysjobs 
	JOIN  MSDB.[dbo].[sysjobschedules] ON sysjobs.JOB_ID = [sysjobschedules].JOB_ID
	join [dbo].[sysschedules] on sysjobschedules.schedule_id = sysschedules.schedule_id
	LEFT JOIN CTE ON CTE.job_id = sysjobs.job_id AND CTE.rn = 1 AND (CTE.run_status <> 1)
WHERE sysjobs.enabled <> 1 OR CTE.run_status IS NOT NULL or sysschedules.enabled <> 1
ORDER BY sysjobs.name, step_id";

            Logger.Trace("Start using sql connection {0}", stagingConnection);
            var jobs = new List<Job>();
            try
            {
                using (var connection = new SqlConnection(stagingConnection))
                {
                    connection.Open();
                    Logger.Trace("Connection is opened");
                    using (var command = new SqlCommand(SelectJobWithProblems, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            Logger.Trace("Start using data reader");
                            while (reader.Read())
                            {
                                if (Logger.IsTraceEnabled)
                                {
                                    StringBuilder sb = new StringBuilder();
                                    for (int i = 0; i < reader.FieldCount; i++)
                                    {
                                        sb.AppendFormat("{0}: '{1}', ", reader.GetName(i), reader[i]);
                                    }
                                    Logger.Trace(sb.ToString());
                                }


                                jobs.Add(new Job
                                {
                                    Name = reader[0].ToString(),
                                    StepName = reader[1].ToString(),
                                    Message = reader[2].ToString(),
                                    RunDate = reader[3].ToString(),
                                    RunTime = reader[4].ToString(),
                                    Enabled = Convert.ToBoolean(Convert.ToInt16(reader.IsDBNull(5) ? "1" : reader[5].ToString())),
                                    Status = Convert.ToBoolean(Convert.ToInt16(reader.IsDBNull(6) ? "1" : reader[6].ToString())),
                                    SchedulerEnabled = Convert.ToBoolean(Convert.ToInt16(reader.IsDBNull(7) ? "1" : reader[7].ToString()))
                                });
                            }
                        }
                    }
                }
                return jobs;
            }
            catch (Exception e)
            {
                Logger.Error(e, e.Message);
                throw new Exception("Exception while reading from jobs query", e);
            }
        }

        public static List<User> SelectStagingUsers(string stagingConnection, string incomingConnection )
        {
            var users = new List<User>();
            try
            {
                using (var connection = new SqlConnection(stagingConnection))
                {
                    Logger.Trace("Start using sql connection {0}", stagingConnection);
                    var command = new SqlCommand(string.Format(Queries.SelectUserIdAndName, string.Join(",", GetAllCompanyUsers(incomingConnection))), connection);
                    connection.Open();
                    Logger.Trace("Connection is opened");
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            Logger.Trace("Start using data reader");
                            while (reader.Read())
                            {
                                var row = ((IDataRecord)reader);
                                users.Add(new User { ManagerId = row[0] == DBNull.Value? "null" : GenerateGuid(row[0].ToString()),
                                    Id = GenerateGuid(row[1].ToString()), Name = row[2].ToString(),
                                    WnNumber = row[3].ToString(), UserType = (row[4].ToString() == "200") ? "AM" : "ADM" });
                            }
                        }
                        finally
                        {
                            reader.Close();
                            Logger.Trace("Connection is closed");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Trace("{0}", e.Message);
            }

            return users;
        }

        private static List<string> GetAllCompanyUsers(string incomingConnection)
        {
            List<string> users = new List<string>();

            try
            {
                using (var connection = new SqlConnection(incomingConnection))
                {
                    Logger.Trace("Start using sql connection {0}", incomingConnection);
                    var command = new SqlCommand(Queries.SelectAllUsersInLastTwoMonth, connection);
                    connection.Open();
                    Logger.Trace("Connection is opened");
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            Logger.Trace("Start using data reader");
                            while (reader.Read())
                            {
                                var row = ((IDataRecord)reader);
                                users.Add("'" + row[0] + "'");
                            }
                        }
                        finally
                        {
                            reader.Close();
                            Logger.Trace("Connection is closed");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Trace("{0}", e.Message);
            }

            return users;
        }

        public static List<YesterdayStatistics> SelectOrderForYesterday(string incomingConnection, int days, List<User> users, string stagingConnection)
        {
            var yS = new List<YesterdayStatistics>();
            try
            {
                List<User> usersForCheck = new List<User>();
                List<YesterdayStatistics> usersForAdd = new List<YesterdayStatistics>();
                usersForCheck.AddRange(users.Where(x => x.ManagerId == "null" && x.UserType != "AM").OrderBy(x => x.Id).ToList());
                usersForCheck.AddRange(users.Where(x => x.ManagerId == "null" && x.UserType == "AM").OrderBy(x => x.Id).ToList());
                usersForCheck.AddRange(users.Where(x => x.ManagerId != "null" && x.UserType != "AM").OrderBy(x => x.Id).ToList());
                users = usersForCheck;
                Logger.Trace("add {1} users for day {0}", days, users.Count);
                foreach (var user in users)
                {
                    if (yS.Count(x => (x.UserId == user.Id.Replace("'", ""))) <= 0)
                    {
                        Logger.Trace("User {0} order", user.Id.Replace("'", ""));
                        yS.Add(new YesterdayStatistics { ManagerId = user.ManagerId.Replace("'", ""), UserId = user.Id.Replace("'", ""), UserName = user.Name, OrderCount = 0, UserType = user.UserType });
                    }
                    if (user.Name.Count() > _maxStringCount)
                        _maxStringCount = user.Name.Count();
                }
                Logger.Trace("yesterday list count {0}", yS.Count);
                using (var connection = new SqlConnection(incomingConnection))
                {
                    Logger.Trace("Start using sql connection {0}", incomingConnection);
                    var command = new SqlCommand(string.Format(Queries.SelectOrderForYesterday, days), connection);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var row = ((IDataRecord)reader);
                            Logger.Trace("User {0} order {1}", row[0], row[1]);
                            var valueForChange = yS.FirstOrDefault(x => x.UserId == row[0].ToString());
                            if (valueForChange != null)
                            { 
                                valueForChange.OrderCount = (int)row[1];
                                Logger.Trace("User ID {0} User Name {1} order {2}", valueForChange.UserId, valueForChange.UserName, valueForChange.OrderCount);
                            }
                            else
                            {
                                usersForAdd.Add(new YesterdayStatistics { UserId = row[0].ToString(), OrderCount = (int)row[1]});
                            }
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                    Logger.Trace("End yesterday order");
                }

                try
                {
                    using (var connection = new SqlConnection(stagingConnection))
                    {
                        Logger.Trace("Start using sql connection {0}", stagingConnection);
                        var command = new SqlCommand(string.Format(Queries.SelectUserIdAndName, string.Join(",", usersForAdd.Select(x => "'" + x.UserId + "'").Distinct())), connection);
                        Logger.Trace("query is : {0}", command.CommandText);
                        connection.Open();
                        Logger.Trace("Connection is opened");
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            try
                            {
                                Logger.Trace("Start using data reader");
                                while (reader.Read())
                                {
                                    var row = ((IDataRecord)reader);
                                    var valueForChange = usersForAdd.FirstOrDefault(x => x.UserId == GenerateGuid(row[1].ToString()).Replace("'", ""));
                                    if (valueForChange != null)
                                    {
                                        Logger.Trace("start add user");
                                        valueForChange.ManagerId = row[0] == DBNull.Value
                                            ? "null"
                                            : GenerateGuid(row[0].ToString());
                                        valueForChange.UserName = row[2].ToString();
                                        valueForChange.UserType = (row[4].ToString() == "200") ? "AM" : "ADM";
                                        usersForAdd.Where(x => x.UserId == valueForChange.UserId)
                                            .Select(x => x = valueForChange);
                                    }
                                }
                            }
                            finally
                            {
                                reader.Close();
                                
                                yS.AddRange(usersForAdd);
                                Logger.Trace("Connection is closed");
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Logger.Trace("{0}", e.Message);
                }
            }
            catch (Exception e)
            {
                Logger.Trace("{0}", e.Message);
            }
            return yS;
        }

        public static IEnumerable<LastMonthStatistics> SelectOrderForLastMonth(string incomingConnection)
        {
            var lMs = new List<LastMonthStatistics>();
            try
            {
                using (var connection = new SqlConnection(incomingConnection))
                {
                    Logger.Trace("Start using sql connection {0}", incomingConnection);
                    var command = new SqlCommand(Queries.SelectOrderForLastMonth, connection);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var row = ((IDataRecord)reader);
                            lMs.Add(new LastMonthStatistics { Date = (DateTime)row[0], OrderCount = (int)row[1] });
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
                Logger.Trace("End last month order");
            }
            catch (Exception e)
            {
                Logger.Trace("{0}", e.Message);
            }
            return lMs.OrderBy(x => x.Date).ToList();
        }

        public static Dictionary<string, string> SelectIncomingErrors(string incomingConnection, DateTime date)
        {
            var incomingErrors = new Dictionary<string, string>();
            try
            {
                using (var connection = new SqlConnection(incomingConnection))
                {
                    Logger.Trace("Start using sql connection {0}", incomingConnection);
                    var command = new SqlCommand(Queries.SelectIncomingErrors, connection);
                    command.Parameters.AddWithValue("date", date);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var row = ((IDataRecord)reader);
                            incomingErrors.Add(row[0].ToString(), row[1].ToString());
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Trace("{0}", e.Message);
            }
            return incomingErrors;
        }

        public static string SelectLastIscRun(string incomingConnection)
        {
            var lastIscRun = string.Empty;
            try
            {
                using (var connection = new SqlConnection(incomingConnection))
                {
                    Logger.Trace("Start using sql connection {0}", incomingConnection);
                    var command = new SqlCommand(Queries.SelectLastIscRun, connection);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var row = ((IDataRecord)reader);
                            lastIscRun = row[0].ToString();
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Trace("{0}", e.Message);
            }
            return lastIscRun;
        }

        public static IEnumerable<YesterdayStatistics> SelectMontlyOrders(List<User> users, string incomingConnection)
        {
            var yS = new List<YesterdayStatistics>();
            try
            {
                string idForQuery = "";

                foreach (var user in users)
                {
                    idForQuery += string.Format("{0},", user.Id);
                    if (yS.All(x => x.UserId != user.Id))
                    {
                        Logger.Trace("User {0} order", user.Id.Replace("'", ""));
                        yS.Add(new YesterdayStatistics { UserId = user.Id.Replace("'", ""), UserName = user.Name, OrderCount = 0, UserType = user.UserType });
                    }
                    if (user.Name.Count() > _maxStringCount)
                        _maxStringCount = user.Name.Count();
                }

                if (idForQuery != "")
                {
                    idForQuery = idForQuery.Substring(0, idForQuery.Count() - 1);
                    using (var connection = new SqlConnection(incomingConnection))
                    {
                        Logger.Trace("Start using sql connection {0}", incomingConnection);
                        var command = new SqlCommand(string.Format(Queries.SelectMontlyOrders, idForQuery), connection);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        try
                        {
                            while (reader.Read())
                            {
                                var row = ((IDataRecord)reader);
                                Logger.Trace("User {0} order {1}", GenerateGuid(row[0].ToString()).Replace("'", ""), row[1]);
                                var valueForChange = yS.FirstOrDefault(x => x.UserId == GenerateGuid(row[0].ToString()).Replace("'", ""));
                                if (valueForChange != null)
                                {
                                    valueForChange.OrderCount = (int)row[1];
                                    Logger.Trace("User Id {0} User Name {1} order {2}", valueForChange.UserId, valueForChange.UserName, valueForChange.OrderCount);
                                }
                            }
                        }
                        finally
                        {
                            reader.Close();
                        }
                    }
                }
                Logger.Trace("End montly order");
            }
            catch (Exception e)
            {
                Logger.Trace("{0}", e.Message);
            }
            return yS;
        }

        public static IEnumerable<YesterdayStatistics> SelectAdmOrderForLastMonth(List<User> users, string incomingConnection)
        {
            var yS = new List<YesterdayStatistics>();
            try
            {
                string idForQuery = "";

                List<string> allAdmUsers = users.Where(user => user.UserType == "ADM").Select(user => user.Id).ToList();

                allAdmUsers.ForEach(x => { idForQuery += string.Format("{0},", x); });
                idForQuery = idForQuery != "" ? idForQuery.Substring(0, idForQuery.Count() - 1) : "";

                foreach (var user in users)
                {
                    if (yS.All(x => x.UserId != user.Id))
                    {
                        Logger.Trace("User {0} order", user.Id.Replace("'", ""));
                        if (yS.Count(x => (x.UserId == user.Id.Replace("'", ""))) == 0)
                            yS.Add(new YesterdayStatistics { UserId = user.Id.Replace("'", ""), UserName = user.Name, OrderCount = 0, UserType = user.UserType });
                    }
                    if (user.Name.Count() > _maxStringCount)
                        _maxStringCount = user.Name.Count();
                }

                if (idForQuery != "")
                {
                    using (var connection = new SqlConnection(incomingConnection))
                    {
                        Logger.Trace("Start using sql connection {0}", incomingConnection);
                        var command = new SqlCommand(string.Format(Queries.SelectLastMonthAdm, idForQuery), connection);
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        try
                        {
                            while (reader.Read())
                            {
                                var row = ((IDataRecord)reader);
                                Logger.Trace("User {0} order {1}", GenerateGuid(row[0].ToString()).Replace("'", ""), row[1]);
                                var valueForChange = yS.FirstOrDefault(x => x.UserId == GenerateGuid(row[0].ToString()).Replace("'", ""));
                                if (valueForChange != null)
                                {
                                    valueForChange.OrderCount = (int)row[1];
                                    Logger.Trace("User Id {0} User Name {1} order {2}", valueForChange.UserId, valueForChange.UserName, valueForChange.OrderCount);
                                }
                            }
                        }
                        finally
                        {
                            reader.Close();
                        }
                    }
                }
                Logger.Trace("End adm last month order");
            }
            catch (Exception e)
            {
                Logger.Trace("{0}", e.Message);
            }
            return yS;
        }

        private static string GenerateGuid(string id)
        {
            var guidTemplate = "'00000000-0000-0000-0000-";
            var charCount = 12 - id.Count();

            for (var i = charCount; i > 0; i--)
                guidTemplate += "0";

            guidTemplate += id + "'";

            return guidTemplate;
        }

        public static List<UserClientInfo> SelectUserClientChanegedInfos(string incomingConnection, string targetVersion)
        {
            const string sql = @"
	DECLARE @utc datetime = GETUTCDATE()
	DECLARE @baseday datetime = DATEADD(day, dateDiff(day, 0, @utc), 0)
	DECLARE @difference int = Datepart(hour, @utc)
	DECLARE @datefrom datetime
	DECLARE @dateto datetime 

	if(@difference > 16)
		BEGIN
			SET @dateto = DATEADD(hour, +17, @baseday)
			SET @datefrom = @baseday
		END
	ELSE
		BEGIN
			SET @dateto = DATEADD(hour, 0, @baseday)
			SET @datefrom = DATEADD(hour, -7, @baseday)
		END

	SELECT DISTINCT today.* FROM (
		SELECT RIGHT(UserId, 4) as UserId, [Request].value('(//@Speedy-App-Version)[1]','nvarchar(max)') as 'Target', @dateto as 'Date'
		  FROM [dbo].[MSG_AsyncMessageQueue]
		  WHERE
			InsertTime >= @datefrom
			and InsertTime < @dateto
			and UPPER([Request].value('(//userNumber/@id)[1]','nvarchar(max)')) not in ('WGS\WN00086636', 'WGS\WN00066784', 'WGS\WN00079555', 'WGS\WN00086637', 'WGS\EX31260030', 'WGS\EX31260036', 'WGS\EX31260043', 'WGS\EX31260047')
			and [Request].value('(//@Speedy-App-Version)[1]','nvarchar(max)') in ('16.04.18.1611031658','16.04.16.1610271655')
	) today 
	left join (
		SELECT RIGHT(UserId, 4) as UserId
		  FROM [dbo].[MSG_AsyncMessageQueue]
		  WHERE 
			InsertTime < @datefrom
			and UPPER([Request].value('(//userNumber/@id)[1]','nvarchar(max)')) not in ('WGS\WN00086636', 'WGS\WN00066784', 'WGS\WN00079555', 'WGS\WN00086637', 'WGS\EX31260030', 'WGS\EX31260036', 'WGS\EX31260043', 'WGS\EX31260047')
			and [Request].value('(//@Speedy-App-Version)[1]','nvarchar(max)') in ('16.04.18.1611031658','16.04.16.1610271655')
	) before ON today.UserId = before.UserId
	WHERE before.UserId is null ORDER BY today.UserId asc;
";

            Logger.Info("SelectUserClientChanegedInfos target version {0}.", targetVersion);
            Logger.Trace(sql);

            var response = new List<UserClientInfo>();

            try
            {
                using (var sqlConnection = new SqlConnection(incomingConnection))
                {
                    sqlConnection.Open();

                    using (var sqlCommand = new SqlCommand(sql, sqlConnection))
                    {
                        //sqlCommand.Parameters.AddWithValue("targetVersion", targetVersion);

                        sqlCommand.CommandTimeout = 120;

                        Logger.Trace("Statement executed.");
                        using (var reader = sqlCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var userId = reader.GetString(0);
                                var target = reader.GetString(1);
                                var from = $"{reader.GetDateTime(2):O}";

                            response.Add(new UserClientInfo {User = userId, Target = target, From = from });
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error("SelectUserClientChanegedInfos error: {0}", e.Message);
            }

            Logger.Info("SelectUserClientChanegedInfos finds {0} records.", response.Count);

            return response;
        }
    }
}
