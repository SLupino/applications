﻿using Helpers.Classes;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace OrderReportsProxyService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IOrderInformationService
    {
        [OperationContract]
        string GetOrderData(string value);

        [OperationContract]
        string GetIncomingErrorData(string value);

        [OperationContract]
        List<Job> GetStagingJobsMonitoring(string value);

        [OperationContract]
        List<UserClientInfo> GetUserClientChanegedInfos(string connectionString, string value);

        [OperationContract]
        string CheckProcess();

        [OperationContract]
        string WriteItemsPreStaging(List<CatalogItem> items, string connectionString);

        [OperationContract]
        string WriteItemsPreStagingNew(List<CatalogItem> items, string connectionString);
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
