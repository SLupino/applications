﻿using System.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Net.Mail;

namespace Helpers
{
    public class EmailHelper
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public static void SendEmail(List<string> smtpConfig, string fromEmail, List<string> toEmails, List<string> ccEmails, List<string> attachments, string subject, string body)
        {
            Logger.Info("mail instanced");
            try
            {
                var mail = new MailMessage
                {
                    Subject = subject,
                    IsBodyHtml = true,
                    Body = body,
                    From = new MailAddress(fromEmail)
                };

                using (mail)
                {

                    foreach (string toEmail in toEmails.Where(toEmail => toEmail != ""))
                        mail.To.Add(new MailAddress(toEmail));
                    foreach (string ccEmail in ccEmails.Where(ccEmail => ccEmail != ""))
                        mail.CC.Add(new MailAddress(ccEmail));
                    foreach (var attachment in attachments.Where(attachment => attachment != ""))
                    {
                        mail.Attachments.Add(new Attachment(attachment));
                    }
                    Logger.Trace("file attached");
                    if (smtpConfig.Count == 4)
                    {
                        var client = new SmtpClient
                        {
                            Port = 25,
                            Host = smtpConfig[0],
                            Timeout = 10000,
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = false
                        };

                        if (string.IsNullOrEmpty(smtpConfig[1]))
                            client.Credentials = new System.Net.NetworkCredential(smtpConfig[1], smtpConfig[2], smtpConfig[3]);

                        using (client)
                        {
                            client.Send(mail);
                            Logger.Info("mail sent");
                        }
                    }
                    else
                    {
                        Logger.Trace("Wrong Smtp Configuration");
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Email not sent. Exception: {0}", e.InnerException?.Message ?? e.Message);
            }
        }
    }
}
