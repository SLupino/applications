﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpers.Classes
{
    public class ValidItems
    {
        public List<CatalogItem> CatalogItems { get; set; } 
        public string StagingConnectionString { get; set; } 
    }
}
