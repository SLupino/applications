﻿namespace Helpers.Classes
{
    public class OrderUserStatistics
    {
        public string ManagerId { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public int OrderCount { get; set; }
        public string UserType { get; set; }
    }
}
