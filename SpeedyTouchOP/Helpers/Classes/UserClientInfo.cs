﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpers.Classes
{
    public class UserClientInfo
    {
        public string User { get; set; }
        public string Target { get; set; }
        public string From { get; set; }
    }
}
