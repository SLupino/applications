﻿using System;

namespace Helpers.Classes
{
    public class OrderMonthStatistics
    {
        public DateTime Date { get; set; }
        public int OrderCount { get; set; }
    }
}
