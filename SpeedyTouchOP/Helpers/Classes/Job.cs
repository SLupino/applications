﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Helpers.Classes
{
    public class Job
    {
        public string Name { get; set; }
        public string StepName { get; set; }
        public string Message { get; set; }
        public string RunDate { get; set; }
        public string RunTime { get; set; }
        public bool Enabled { get; set; }
        public bool Status { get; set; }
        public bool SchedulerEnabled { get; set; }
    }
}
