﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Helpers.Classes
{
    public class CatalogItem
    {
        public string CompanyCode { get; set; }
        public string ProductId { get; set; }
        public string ArticleId { get; set; }
        public int PackageSize { get; set; }
        public bool IsMinSize { get; set; }
    }
}
