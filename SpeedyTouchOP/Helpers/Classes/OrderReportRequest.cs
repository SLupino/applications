﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpers.Classes
{
    public class OrderReportRequest
    {
        public string CompanyCode { get; set; } 

        public string StagingConnectionString { get; set; } 

        public string IncomingConnectionString { get; set; } 

        public bool ForceMonthlyReport { get; set; } 

    }
}
