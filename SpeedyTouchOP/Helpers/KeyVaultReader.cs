﻿using System;
using NLog;

namespace Helpers
{
    public class KeyVaultReader
    {
        public static string GetValue(Logger nlog, string key, string speedyEnvironmentApiUrl)
        {
            nlog.Debug("KeyVaultReader.GetValue");

            try
            {
                var api = $"{speedyEnvironmentApiUrl}/KeyVaultApi/Get?key={key}";

                var response = SpeedyApiConnector.Http.GetAsync(new Uri(api)).GetAwaiter().GetResult();
                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                    return content;
                }

                nlog.Error($"KeyVaultReader.GetValue: Http error {response.StatusCode}");
            }
            catch (Exception e)
            {
                nlog.Error(e, "KeyVaultReader.GetValue");
            }

            return "";
        }
    }
}
