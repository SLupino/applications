﻿using NLog;
using System;
using System.Xml.Linq;

namespace Helpers
{
    public static class XmlHelper
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public static string GetXmlForProxyCall(string stagingDatabase, string incomingDatabase, string companyCode )
        {
            var xRequest = string.Empty;
            try
            {
                Logger.Trace("Staging ConnectionString: {0}", stagingDatabase);
                Logger.Trace("Incoming ConnectionString: {0}", incomingDatabase);
                xRequest = (new XElement("CompanyInfo", new XElement("CompanyCode", companyCode), new XElement("StagingConnection", stagingDatabase), new XElement("IncomingConnection", incomingDatabase))).ToString();
            }
            catch (Exception e)
            {
                Logger.Trace("EXCEPTION: {0}", e.Message);
            }
            Logger.Trace("Request: {0}", xRequest);
            return xRequest;
        } 
    }
}
