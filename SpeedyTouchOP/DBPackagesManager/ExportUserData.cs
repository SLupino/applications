﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;

namespace DBPackagesManager
{
    internal class ExportUserData
    {
        private static readonly Logger Logger = LogManager.GetLogger(typeof(ExportUserData).FullName);

        private class User
        {
            public string Account { get; set; }
            public string Path { get; set; }
            public int Status { get; set; }
            public string Guid { get; set; }
            public DateTime? FirstAttempt { get; set; }
            public DateTime? LastAttempt { get; set; }
        }

        private static int Execute(List<User> userList)
        {
            Logger.Debug("   {0} users to process.", userList.Count);

            var userToCheck = userList.Where(u => u.Status > 0).ToList();
            Logger.Debug("   ....whereof {0} to check work items status.", userToCheck.Count);

            if (userToCheck.Any())
                GetRequestedStatus(userToCheck);

            var userToExport = userList.Where(u => u.Status == 0).ToList();
            Logger.Debug("   ....whereof {0} to export new packages.", userToExport.Count);

            if (userToExport.Any())
                ExportRequest(userToExport);

            if (userToCheck.Any() || userToExport.Any()) return 0;

            Logger.Debug("   Didn't find any user to process.");
            return 100;
        }

        public static int Execute(string filePath)
        {
            Logger.Debug("Start file process.");
            var userList = GetUserList(filePath);
            return Execute(userList);
        }

        public static int Execute()
        {
            Logger.Debug("Start folder process.");
            var exitCode = 0;
            var fileList = GetFileList();

            if (fileList.Count > 0)
            {
                var userList = new List<User>();
                Logger.Trace("Found {0} files to process.", fileList.Count);
                foreach (var filePath in fileList)
                {
                    var users = GetUserList(filePath);
                    Logger.Trace("Found {0} users from file '{1}'.", users.Count, filePath);
                    userList.AddRange(users.ToArray());
                }

                Execute(userList);
            }
            else
            {
                Logger.Warn("Didn't find any file to process.");
                exitCode = 100;
            }

            return exitCode;
        }

        private static List<string> GetFileList()
        {
            var folderPath = Settings.FolderPath();
            Logger.Debug("GetFileList: get txt files in folder '{0}'.", folderPath);

            return Directory.EnumerateFiles(folderPath, "*.txt").ToList();
        }

        private static void GetRequestedStatus(List<User> userList)
        {
            var requestUrl =
                $"{Settings.Url()}/SpeedyContentService.svc/GetWorkItemContent?username={Settings.Username()}&password={Settings.Password()}&type=downloadAndZipWi&status=&statusNot=&attempts=&top=500&systemAccount=";
            Logger.Debug("       GetRequestedStatus: get work items status using URL '{0}'", requestUrl);

            using (var httpClient = new HttpClient())
            {
                Logger.Trace("       GetRequestedStatus: start GET request.");
                var httpResponse = httpClient.GetAsync(requestUrl);

                if (!httpResponse.Result.IsSuccessStatusCode)
                    throw new Exception(httpResponse.Result.ReasonPhrase);

                var response = httpResponse.Result;
                var content = response.Content.ReadAsStringAsync();

                Logger.Trace("       GetRequestedStatus: parse Json response.");
                var jsonContent = JArray.Parse(content.Result);

                Logger.Trace("       GetRequestedStatus: extract work items information from Json response.");
                var userStatusList = jsonContent.Select(t => new object[]
                        {t.SelectToken("UserName").ToObject<string>(), t.SelectToken("Status").ToObject<int>(), t.SelectToken("AttemptEndTime").HasValues ? t.SelectToken("AttemptEndTime").ToObject<DateTime>() : DateTime.Now})
                    .ToList();

                Logger.Trace("       GetRequestedStatus: found {0} work items.", userStatusList.Count);
                foreach (var user in userList)
                {
                    Logger.Trace("       GetRequestedStatus: check last work item of user '{0}'.", user.Account);
                    var lastWi = userStatusList.OrderByDescending(s => s[2])
                        .FirstOrDefault(s => string.Equals(s[0].ToString(), user.Account, StringComparison.InvariantCultureIgnoreCase));

                    if (lastWi != null)
                    {
                        Logger.Trace("       GetRequestedStatus: found work items with status '{0}'.", lastWi[1]);
                        user.Status = (int)lastWi[1];
                    }
                    else
                    {
                        Logger.Trace("       GetRequestedStatus: didn't find any work item, user set to reprocess.");
                        user.Status = 0;
                    }
                }
            }

            SetUserStatus(userList);
        }

        private static void ExportRequest(List<User> userList)
        {
            var requestUrl =
                $"{Settings.Url()}/CatalogWebService.svc/ExportDataForUser?username={Settings.Username()}&password={Settings.Password()}";
            Logger.Debug("       ExportRequest: process export request using URL '{0}'", requestUrl);

            var availableWorkItemsNumber = GetAvailableWorkItemsNumber();
            if (availableWorkItemsNumber == 0)
            {
                Logger.Trace("       ExportRequest: the cloud is busy, can't process new export request.");
                return;
            }

            GetUserGuids(userList);

            var usersNotFound = userList.Where(u => string.IsNullOrEmpty(u.Guid)).ToList();
            var usersToWork = userList.Where(u => !string.IsNullOrEmpty(u.Guid)).Take(availableWorkItemsNumber).ToList();

            if (usersToWork.Count > 0)
            {
                Logger.Trace("       ExportRequest: {0} users packages will be export.", usersToWork.Count);
                var guids = usersToWork.Select(u => u.Guid).ToArray<object>();

                dynamic postData = new JObject();
                postData.UserConsolGuids = new JArray(guids);
                postData.Tenant = Settings.Tenant();
                postData.CreateDBPackage = true;
                postData.ExportCustomer = "true";
                postData.ToRevisionCustomer = null;
                postData.FromRevisionCustomer = null;
                postData.ExportCatalog = "true";
                postData.ToRevisionCatalog = null;
                postData.FromRevisionCatalog = null;

                var stringPayload = JsonConvert.SerializeObject(postData);
                Logger.Trace("       ExportRequest: serializeed Json object for the export request '{0}'.", stringPayload);

                var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");

                using (var httpClient = new HttpClient())
                {
                    Logger.Trace("       ExportRequest: start POST request.");
                    var httpResponse = httpClient.PostAsync(requestUrl, httpContent);

                    if (!httpResponse.Result.IsSuccessStatusCode)
                        throw new Exception(httpResponse.Result.ReasonPhrase);
                }

                usersToWork.ForEach(u => u.Status = 1);

                Logger.Trace("       ExportRequest: set user status to 1 (requested) for users within the request.");
                SetUserStatus(usersToWork);
            }
            else
                Logger.Trace("       ExportRequest: didm't find any user guid to process.");

            if (usersNotFound.Count <= 0) return;

            usersNotFound.Where(u => u.FirstAttempt != null && (DateTime.Now - (DateTime) u.FirstAttempt).TotalDays > Settings.DaysOfRetry()).ToList().ForEach(u => u.Status = 404);
            Logger.Trace("       ExportRequest: set user status to 404 (not found) for user without Guid by more than {0} days.", Settings.DaysOfRetry());
            SetUserStatus(usersNotFound);
        }

        private static void GetUserGuids(List<User> userList)
        {
            var accounts = string.Join(",", userList.Select(u => u.Account).ToArray());
            int guidCount;

            var requestUrl =
                $"{Settings.Url()}/SpeedyContentService.svc/GetCustUsers?username={Settings.Username()}&password={Settings.Password()}&systemAccount={accounts}";
            Logger.Debug("           GetUserGuids: get user guids using URL '{0}'", requestUrl);

            using (var httpClient = new HttpClient())
            {
                Logger.Trace("           GetUserGuids: start GET request.");
                var httpResponse = httpClient.GetAsync(requestUrl);

                if (!httpResponse.Result.IsSuccessStatusCode)
                    throw new Exception(httpResponse.Result.ReasonPhrase);

                var response = httpResponse.Result;
                var content = response.Content.ReadAsStringAsync();

                Logger.Trace("           GetUserGuids: parse Json response.");
                var jsonContent = JArray.Parse(content.Result);

                Logger.Trace("           GetUserGuids: get guids info.");
                foreach (var user in userList)
                {
                    var guidInfo = jsonContent.FirstOrDefault(c => string.Equals(c.SelectToken("SystemAccount").ToString(), user.Account, StringComparison.InvariantCultureIgnoreCase));
                    if (guidInfo != null)
                        user.Guid = guidInfo.SelectToken("Id").ToString();
                    if (user.FirstAttempt == null) user.FirstAttempt = DateTime.Now;
                    user.LastAttempt = DateTime.Now;
                }

                guidCount = userList.Where(u => !string.IsNullOrEmpty(u.Guid)).Select(u => u.Guid).ToList().Count;
            }

            Logger.Trace("           GetUserGuids: found {0} guids.", guidCount);
        }

        private static void SetUserStatus(List<User> userList)
        {
            var fileGroupUserList = userList.GroupBy(u => u.Path).ToList();

            foreach (var fileGroupUser in fileGroupUserList)
            {
                var text = new StringBuilder();

                Logger.Trace("           SetUserStatus: read lines from file '{0}'.", fileGroupUser.Key);
                var lines = File.ReadAllLines(fileGroupUser.Key);

                foreach (var l in lines)
                {
                    var user = fileGroupUser.FirstOrDefault(u => l.Contains(u.Account));
                    if (user != null)
                    {
                        Logger.Trace("           SetUserStatus: replace line '{0}' with '{1}'.", l, $"{user.Account}|{user.Status}");
                        text.AppendLine($"{user.Account}|{user.Status}|{user.FirstAttempt}|{user.LastAttempt}");
                    }
                    else
                        text.AppendLine(l);

                }

                Logger.Trace("           SetUserStatus: save file '{0}'.", fileGroupUser.Key);
                File.WriteAllText(fileGroupUser.Key, text.ToString());
            }
        }

        private static int GetAvailableWorkItemsNumber()
        {
            var requestUrl =
                $"{Settings.Url()}/SpeedyContentService.svc/GetWorkItemContent?username={Settings.Username()}&password={Settings.Password()}&type=downloadAndZipWi&status=&statusNot=100&attempts=&top=500&systemAccount=";
            Logger.Debug("           GetAvailableWorkItemsNumber: get work items availability using URL '{0}'", requestUrl);

            int availableWorkItemsNumber;

            using (var httpClient = new HttpClient())
            {
                Logger.Trace("           GetAvailableWorkItemsNumber: start GET request.");
                var httpResponse = httpClient.GetAsync(requestUrl);

                if (!httpResponse.Result.IsSuccessStatusCode)
                    throw new Exception(httpResponse.Result.ReasonPhrase);

                var response = httpResponse.Result;
                var content = response.Content.ReadAsStringAsync();

                Logger.Trace("           GetAvailableWorkItemsNumber: parse Json response.");
                var jsonContent = JArray.Parse(content.Result);

                var wiList = jsonContent.Where(t => t.SelectToken("Status").ToObject<int>() != 99).ToList();

                availableWorkItemsNumber = Settings.MaxSimultaneousExport() - wiList.Count;
                Logger.Trace("           GetAvailableWorkItemsNumber: {0} work items in process, {1} max simultaneous, {2} available", wiList.Count, Settings.MaxSimultaneousExport(), availableWorkItemsNumber);
            }

            return availableWorkItemsNumber;
        }

        private static List<User> GetUserList(string filePath)
        {
            Logger.Debug("  GetUserList: get users list from path '{0}'", filePath);

            var userList = new List<User>();

            foreach (var line in File.ReadAllLines(filePath))
            {
                if (string.IsNullOrEmpty(line)) continue;

                var splittedLine = line.Split('|');

                var status = splittedLine.Length > 1 ? int.Parse(splittedLine[1]) : 0;
                if (status >= 100)
                {
                    Logger.Trace("  GetUserList: read line '{0}': user with status '{1}' discarded.", line, status);
                    continue;
                }

                var firstAttemptString = splittedLine.Length > 2 ? splittedLine[2] : "";
                var lastAttemptString = splittedLine.Length > 3 ? splittedLine[3] : "";

                DateTime firstAttempt;
                DateTime lastAttempt;

                var user = new User
                {
                    Account = splittedLine[0],
                    Path = filePath,
                    Status = status,
                    FirstAttempt = DateTime.TryParse(firstAttemptString, out firstAttempt) ? (DateTime?) firstAttempt : null,
                    LastAttempt = DateTime.TryParse(lastAttemptString, out lastAttempt) ? (DateTime?) lastAttempt : null
                };

                Logger.Trace("  GetUserList: read line '{0}': add user '{1}' with status '{2}', fist attempt '{3}' and last attempt '{4}'.", line, user.Account, user.Status, user.FirstAttempt, user.LastAttempt);
                userList.Add(user);
            }

            return userList;
        }
    }
}
