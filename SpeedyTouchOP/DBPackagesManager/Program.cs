﻿using System;
using NLog;

namespace DBPackagesManager
{
    class Program
    {
        private static readonly Logger Logger = LogManager.GetLogger(typeof(Program).FullName);

        static int Main(string[] args)
        {
            Logger.Info("Start Database Packages Management.");
            int exitCode;

            LogSettings();

            try
            {
                if (args.Length == 1)
                {
                    exitCode = ExportUserData.Execute(args[0]);
                }
                else if (!string.IsNullOrEmpty(Settings.FolderPath()))
                {
                    exitCode = ExportUserData.Execute();
                }
                else
                {
                    if (args.Length < 1) Logger.Info("Missing user list file.");
                    if (args.Length > 1) Logger.Info("Unknown arguments.");

                    exitCode = 2;
                }
            }
            catch (Exception ex)
            {
                exitCode = 1;
                Logger.Error(ex, "Oops, something went wrong.");
            }

            Logger.Info("End Database Packages Management with code {0}.", exitCode);
            return exitCode;
        }

        private static void LogSettings()
        {
            Logger.Debug("Cloud url: {0}", Settings.Url());
            Logger.Debug("Tenant: {0}", Settings.Tenant());
            Logger.Debug("Username: {0}", Settings.Username());
            Logger.Debug("Password: {0}", Settings.Password());
            Logger.Debug("Max simultaneous export: {0}", Settings.MaxSimultaneousExport());
        }
    }
}

