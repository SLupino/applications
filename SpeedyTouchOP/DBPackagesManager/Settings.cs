﻿using System.Configuration;

namespace DBPackagesManager
{
    internal class Settings
    {
        public static string Tenant()
        {
            return ConfigurationManager.AppSettings["Tenant"];
        }

        public static string Url()
        {
            return ConfigurationManager.AppSettings["SmcURL"];
        }

        public static string Username()
        {
            return ConfigurationManager.AppSettings["SmcUsername"];
        }

        public static string Password()
        {
            return ConfigurationManager.AppSettings["SmcPassword"];
        }

        public static string FolderPath()
        {
            return ConfigurationManager.AppSettings["FolderPath"];
        }

        public static int MaxSimultaneousExport()
        {
            int n;
            int.TryParse(ConfigurationManager.AppSettings["MaxSimultaneousExport"], out n);
            return n;
        }

        public static int DaysOfRetry()
        {
            int n;
            int.TryParse(ConfigurationManager.AppSettings["DaysOfRetry"], out n);
            return n;
        }
    }
}
