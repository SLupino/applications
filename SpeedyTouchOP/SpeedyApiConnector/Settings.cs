﻿using System.Configuration;

namespace SpeedyApiConnector
{
    internal static class Settings
    {
        public static string Thumbprint => ConfigurationManager.AppSettings["Thumbprint"] ?? "";
    }
}
