﻿using System;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace SpeedyApiConnector.Helpers
{
    internal class Helper
    {
        internal static string EncryptRsa(X509Certificate2 cert, string data)
        {
            if (string.IsNullOrEmpty(data)) return "";

            var csp = (RSACryptoServiceProvider) cert.PublicKey.Key;

            var bytesData = Encoding.UTF8.GetBytes(data);
            var bytesEncrypted = csp.Encrypt(bytesData, false);
            var output = Convert.ToBase64String(bytesEncrypted);

            return output;
        }

        internal static readonly Lazy<HttpClient> HttpClient = new Lazy<HttpClient>(() =>
        {
            ServicePointManager.SecurityProtocol =
                SecurityProtocolType.Tls
                | SecurityProtocolType.Tls11
                | SecurityProtocolType.Tls12;

            var certificate = ClientCertificate(Settings.Thumbprint);
            var webRequestHandler = new WebRequestHandler() {ClientCertificates = {certificate}};

            var client = new HttpClient(webRequestHandler, false);
            client.DefaultRequestHeaders.Add("User-Agent", "SpeedyApiConnector");
            return client;
        });

        public static X509Certificate2 ClientCertificate(string key)
        {
            var cert = new X509Certificate2();

            try
            {
                var certStore = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                certStore.Open(OpenFlags.ReadOnly);
                var certCollection = certStore.Certificates.Find(X509FindType.FindByThumbprint, key, false);

                if (certCollection.Count > 0)
                {
                    cert = certCollection[0];
                }

                certStore.Close();
            }
            catch
            {
                throw new Exception("Unable to get the certificate to connect the SpeedyEnvironmentApi");
            }

            return cert;
        }
    }
}
