﻿using System;

namespace SpeedyApiConnector.Models
{
    [Serializable]
    internal class SpeedyTokenModel
    {
        internal SpeedyTokenModel(string userAccount, string companyCode)
        {
            UserAccount = userAccount;
            CompanyCode = companyCode;
            ExpirationDate = DateTime.UtcNow.AddMinutes(150);
        }

        public string CompanyCode { get; }
        public string UserAccount { get; }
        public DateTime ExpirationDate { get; }
    }
}
