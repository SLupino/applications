﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SpeedyApiConnector.Helpers;
using SpeedyApiConnector.Models;

namespace SpeedyApiConnector
{
    public class Http
    {
        public static async Task<HttpResponseMessage> GetAsync(Uri uri)
        {
            return await GetAsync(uri, "0000");
        }

        public static async Task<HttpResponseMessage> GetAsync(Uri uri, string company)
        {
            if (string.IsNullOrEmpty(Settings.Thumbprint))
                throw new Exception("Missing appSettings key=\"Thumbprint\"");

            var client = Helper.HttpClient.Value;

            var userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

            var speedyToken = new SpeedyTokenModel(userName, "0000");
            var token = Helper.EncryptRsa(Helper.ClientCertificate(Settings.Thumbprint),
                JsonConvert.SerializeObject(speedyToken));

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            return await client.GetAsync(uri);
        }

        public static async Task<HttpResponseMessage> PostAsync(Uri uri, HttpContent content)
        {
            return await PostAsync(uri, content, "0000");
        }

        public static async Task<HttpResponseMessage> PostAsync(Uri uri, HttpContent content, string company)
        {
            if (string.IsNullOrEmpty(Settings.Thumbprint))
                throw new Exception("Missing appSettings key=\"Thumbprint\"");

            var client = Helper.HttpClient.Value;

            var userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

            var speedyToken = new SpeedyTokenModel(userName, "0000");
            var token = Helper.EncryptRsa(Helper.ClientCertificate(Settings.Thumbprint),
                JsonConvert.SerializeObject(speedyToken));

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            return await client.PostAsync(uri, content);
        }
    }
}
