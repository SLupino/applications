﻿namespace ERPStub
{
    public interface ISettings
    {
        string IncomingConnectionString { get; }
        string StagingConnectionString { get; }
        int MessageBatchSize { get; }
    }
}
