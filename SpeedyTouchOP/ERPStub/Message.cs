﻿using System;
using System.Xml.Linq;

namespace ERPStub
{
    public class Message
    {
        public Guid ID { get; private set; }
        public int Status { get; private set; }
        public string RequestType { get; private set; }
        public XDocument XRequestDocument { get; private set; }

        public Message(Guid id, int status, String requestType, String request)
        {
            ID = id;
            Status = status;
            RequestType = requestType;
            XRequestDocument = XDocument.Parse(request);
        }

        public Message(Message message)
        {
            ID = message.ID;
            Status = message.Status;
            RequestType = message.RequestType;
            XRequestDocument = new XDocument(message.XRequestDocument);
        }
    }
}
