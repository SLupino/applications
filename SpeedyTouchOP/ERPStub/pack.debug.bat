@echo off
setLocal
set _program=%CD%\bin\Debug\ERPStub.exe
set _program=%_program:\=\\%
wmic datafile where Name="%_program%" get Version | find "." > version.tmp
for /f "tokens=1" %%a in (version.tmp) do set "_version=%%a"
del version.tmp
set _dir=ERPStub.%_version%
IF EXIST %_dir% (rmdir /s /q %_dir%)
mkdir %_dir%
copy .\bin\Debug\ERPStub.exe %_dir%
copy .\bin\Debug\ERPStub.exe.config %_dir%
copy .\bin\Debug\NLog.config %_dir%
copy .\bin\Debug\NLog.dll %_dir%
copy .\bin\Debug\EntityFramework.dll %_dir%
copy .\bin\Debug\EntityFramework.SqlServer.dll %_dir%
endLocal
