﻿namespace ERPStub
{
    class Enumerations
    {
        public enum StartRequestType
        {
            Delta = 0,
            Complete = 10
        }

        public enum Operation
        {
            N = 0, //for new and update operation
            D = 1  //for delete operation
        }

        public enum OrderConfirmation_MailingType
        {
            NOT_NECESSARY = 100,
            SEND_AS_FAX = 200,
            SEND_AS_MAIL = 300,
            SEND_AS_LETTER_WITH_CREDIT_SLIP = 400,
            SEND_AS_LETTER_WITHOUT_CREDIT_SLIP = 500
        }
    }
}
