﻿using System;
using System.Configuration;
using NLog;

namespace ERPStub
{
    class Program : ISettings
    {
        private static readonly Logger Logger = LogManager.GetLogger(typeof (ERPStub).FullName);

        public string IncomingConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["Incoming"].ConnectionString; }
        }

        public string StagingConnectionString
        {
            get
            {
                return string.Format(@"metadata=res://*/DataModel.Staging.csdl|res://*/DataModel.Staging.ssdl|res://*/DataModel.Staging.msl;provider=System.Data.SqlClient;provider connection string=""{0};persist security info=True;MultipleActiveResultSets=True;App=EntityFramework"";", ConfigurationManager.ConnectionStrings["Staging"].ConnectionString);
            }
        }
        public int MessageBatchSize
        {
            get { return int.Parse(ConfigurationManager.AppSettings["MessageBatchSize"]); }
        }

        public static void Main(string[] args)
        {
            try
            {
                new ERPStub(new Program()).ReadIncoming();
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw;
            }
        }
    }
}
