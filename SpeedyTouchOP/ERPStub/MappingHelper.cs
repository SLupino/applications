﻿
namespace ERPStub
{
    class MappingHelper
    {
        public static string PricingUnitToNumber(string pricingUnit)
        {
            return pricingUnit.Substring(4, pricingUnit.Length - 4);
        }

        public static int QuoteFollowUpBy(string xmlValue)
        {
            switch (xmlValue)
            {
                case "salesPerson":
                    return 200;
                case "backOffice":
                    return 300;
                default:
                    return 100; /*NOT_APPLICABLE*/
            }
        }

        public static int QuoteDeliveredBy(string xmlValue)
        {
            switch (xmlValue)
            {
                case "salesPerson":
                    return 200;
                case "fax":
                    return 300;
                case "email":
                    return 400;
                default:
                    return 100; /*NOT_APPLICABLE*/
            }
        }

        public static int PriceUnitToVoucherPriceUnit(int priceUnit)
        {
            switch (priceUnit)
            {
                case 1:
                    return 100; // PER_UNIT
                case 5:
                    return 150; // PER_5_UNITS
                case 10:
                    return 200; // PER_10_UNITS
                case 100:
                    return 300; // PER_100_UNITS
                case 1000:
                    return 400; // PER_1000_UNITS
                case 10000:
                    return 500; // PER_10000_UNITS
                default:
                    return 100; // PER_UNIT
            }
        }
    }
}
