﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.XPath;
using ERPStub.DataModel;
using NLog;

namespace ERPStub
{
    public delegate bool Step(out string statusMessage);

    // ReSharper disable once InconsistentNaming
    internal class ERPStub
    {
        private static readonly Logger Logger = LogManager.GetLogger(typeof(ERPStub).FullName);
        private readonly ISettings _settings;

        public ERPStub(ISettings settings)
        {
            _settings = settings;
        }

        public void ReadIncoming()
        {
            var databaseDate = GetDatabaseDate();
            for (var retrieveErrorMessages = RetrieveErrorMessages(databaseDate);
                retrieveErrorMessages.Count > 0;
                retrieveErrorMessages = RetrieveErrorMessages(databaseDate))
                ProcessMessages(retrieveErrorMessages);

            for (var messages = RetrieveNewMessages(); messages.Count > 0; messages = RetrieveNewMessages())
                ProcessMessages(messages);
        }

        private DateTime GetDatabaseDate()
        {
            using (var connection = new SqlConnection(_settings.IncomingConnectionString))
            {
                connection.Open();
                const string statement = "SELECT getutcdate()";
                using (var command = new SqlCommand(statement, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        reader.Read();
                        return reader.GetDateTime(0);
                    }
                }
            }
        }
        private List<Message> RetrieveErrorMessages(DateTime currentDate)
        {
            const string selectErrorSql = @"SELECT TOP {0} [Id], [Status], [RequestType], [Request]
FROM [dbo].[MSG_AsyncMessageQueue] amq
WHERE amq.[Status] >= 0 AND amq.[ExecutionTime] IS NOT NULL AND [ExecutionTime] <= @currentDate
ORDER BY [CreationTime]";

            var messages = new List<Message>();
            using (var connection = new SqlConnection(_settings.IncomingConnectionString))
            {
                connection.Open();

                var cmdText = string.Format(selectErrorSql, _settings.MessageBatchSize);
                using (var command = new SqlCommand(cmdText, connection))
                {
                    command.Parameters.AddWithValue("currentDate", currentDate);
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                            messages.Add(new Message(reader.GetGuid(0), reader.GetInt32(1),
                                reader.GetString(2), reader.GetString(3)));
                    }
                }
            }

            return messages;
        }

        private static void UpdateStatus(SqlConnection connection, Guid messageId, int status, string message)
        {
            var updateSql = $@"UPDATE [dbo].[MSG_AsyncMessageQueue]
SET [Status] = @Status{(status == -1 ? ",[StatusMessage] = @StatusMessage,[ExecutionTime] = getutcdate()" : "")}
WHERE [Id] = @Id";

            using (var sqlCommand = new SqlCommand(updateSql, connection))
            {
                sqlCommand.Parameters.AddWithValue("Status", status);
                sqlCommand.Parameters.AddWithValue("Id", messageId);
                if (status == -1) sqlCommand.Parameters.AddWithValue("StatusMessage", message ?? (object) DBNull.Value);
                sqlCommand.ExecuteNonQuery();
            }
        }

        private static void UpdateExecutionTime(SqlConnection connection, Guid messageId)
        {
            const string updateSql = @"UPDATE [dbo].[MSG_AsyncMessageQueue]
SET [ExecutionTime] = getutcdate()
WHERE [Id] = @Id";

            using (var sqlCommand = new SqlCommand(updateSql, connection))
            {
                sqlCommand.Parameters.AddWithValue("Id", messageId);
                sqlCommand.ExecuteNonQuery();
            }
        }

        private List<Message> RetrieveNewMessages()
        {
            const string selectSql = @"SELECT TOP {0} [Id], [Status], [RequestType], [Request]
FROM [dbo].[MSG_AsyncMessageQueue] amq
WHERE amq.[Status] = 0 AND amq.[ExecutionTime] IS NULL
ORDER BY [CreationTime]";

            var messages = new List<Message>();
            using (var connection = new SqlConnection(_settings.IncomingConnectionString))
            {
                connection.Open();

                var cmdText = string.Format(selectSql, _settings.MessageBatchSize);
                using (var command = new SqlCommand(cmdText, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                            messages.Add(new Message(reader.GetGuid(0), reader.GetInt32(1),
                                reader.GetString(2), reader.GetString(3)));
                    }
                }
            }

            return messages;
        }

        private void ProcessMessages(IEnumerable<Message> messages)
        {
            var enumerable = messages.ToList();
            if (!enumerable.Any()) return;

            using (var context = new StagingContext(_settings.StagingConnectionString))
            {
                var prRequestId = new ObjectParameter("prRequestId", typeof(Guid));
                Logger.Trace("Call sp_startRequest");
                context.sp_startRequest(prRequestId, (int?) Enumerations.StartRequestType.Delta, null);
                Logger.Trace($"Executed sp_startRequest, prRequestId value = {prRequestId.Value}");

                var anyProcessed = false;

                foreach (var message in enumerable)
                {
                    Exception e = null;

                    try
                    {
                        ProcessMessage(message, prRequestId, context);
                        anyProcessed = true;
                    }
                    catch (Exception ex)
                    {
                        e = ex;
                    }

                    if (e == null) continue;

                    try
                    {
                        var exceptionMessage = new StringBuilder(e.Message);
                        for (var ie = e.InnerException; ie != null; ie = ie.InnerException)
                            exceptionMessage.AppendFormat("\n{0}", ie.Message);

                        if (e is DbEntityValidationException dbe)
                        {
                            var errorMessages = dbe.EntityValidationErrors
                                .SelectMany(x => x.ValidationErrors)
                                .Select(x => $"'{x.PropertyName}' - {x.ErrorMessage}");

                            // Join the list to a single string.
                            var fullErrorMessage = string.Join("; ", errorMessages);

                            // Combine the original exception message with the new one.
                            exceptionMessage.AppendLine($"validation errors are: {fullErrorMessage}");
                        }

                        using (var incomingConn = new SqlConnection(_settings.IncomingConnectionString))
                        {
                            incomingConn.Open();
                            UpdateForErrorProcessing(incomingConn, message.ID,
                                exceptionMessage.ToString().Length > 500
                                    ? exceptionMessage.ToString().Substring(0, 500)
                                    : exceptionMessage.ToString());
                        }

                        Logger.Error("Exception processing {0} message with id {1}. Message XML:\n{2}",
                            message.RequestType, message.ID, message.XRequestDocument.ToString());
                        Logger.Error(e, "Exception thrown: {0}", e.Message);
                    }
                    catch (Exception ex2)
                    {
                        Logger.Error(ex2, "SubException thrown: {0}", ex2.Message);
                    }
                }

                if (anyProcessed)
                {
                    Logger.Trace($"Call sp_endRequest, prRequestId value = {prRequestId.Value}");
                    context.sp_endRequest((Guid) prRequestId.Value);
                    Logger.Trace($"Executed sp_endRequest, prRequestId value = {prRequestId.Value}");
                }
                else
                {
                    Logger.Trace($"Call sp_rollbackRequest, prRequestId value = {prRequestId.Value}");
                    context.spg_rollbackRequest((Guid) prRequestId.Value);
                    Logger.Trace($"Executed sp_rollbackRequest, prRequestId value = {prRequestId.Value}");
                }
            }
        }
        private static void UpdateForErrorProcessing(SqlConnection connection, Guid messageId, string message)
        {
            const string updateSql = @"UPDATE [dbo].[MSG_AsyncMessageQueue]
SET [StatusMessage] = @StatusMessage,
    [ExecutionTime] = getutcdate()
WHERE [Id] = @Id";

            using (var sqlCommand = new SqlCommand(updateSql, connection))
            {
                sqlCommand.Parameters.AddWithValue("StatusMessage", message ?? (object) DBNull.Value);
                sqlCommand.Parameters.AddWithValue("Id", messageId);
                sqlCommand.ExecuteNonQuery();
            }
        }

        private void ProcessMessage(Message message, ObjectParameter prRequestId, StagingContext context)
        {
            var company = message.XRequestDocument.XPathSelectElement("/*/companyCode");
            var companyCode = company != null ? company.Value : "";

            string boTypeName;
            string methodName;

            switch (message.RequestType)
            {
                case MessageNames.SendCartRequest:
                case MessageNames.SendCartRequest2:
                case MessageNames.SendCartRequest3:
                case MessageNames.SendCartRequest4:
                case MessageNames.SendCartRequest5:
                case MessageNames.SendCartRequest6:
                case MessageNames.SendCartRequest7:
                case MessageNames.SendCartRequest8:
                case MessageNames.SendCartRequest10:
                case MessageNames.SendCartRequest11:
                case MessageNames.SendCartRequest12:
                    boTypeName = "Cart";
                    methodName = "GetCartChain";
                    break;
                case MessageNames.CreateContactRequest:
                    boTypeName = "Contact";
                    methodName = "GetCreateContactChain";
                    break;
                case MessageNames.ModifyContactRequest:
                    boTypeName = "Contact";
                    methodName = "GetModifyContactChain";
                    break;
                case MessageNames.DeleteContactRequest:
                    boTypeName = "Contact";
                    methodName = "GetDeleteContactChain";
                    break;
                case MessageNames.CreateProspectRequest:
                case MessageNames.CreateProspectRequest3:
                    boTypeName = "Prospect";
                    methodName = "GetCreateProspectChain";
                    break;
                case MessageNames.ModifyCustomerRequest2:
                case MessageNames.ModifyCustomerRequest3:
                case MessageNames.ModifyCustomerRequest:
                    boTypeName = "Customer";
                    methodName = "GetModifyCustomerChain";
                    break;
                case MessageNames.ModifyVisitFrequencyRequest:
                    boTypeName = "Customer";
                    methodName = "GetModifyVisitFrequencyChain";
                    break;
                case MessageNames.CreateDailyReport2:
                case MessageNames.CreateDailyReport4:
                    boTypeName = "DailyReport";
                    methodName = "GetCreateDailyReport2Chain";
                    break;
                case MessageNames.CreateNoteRequest:
                    boTypeName = "Note";
                    methodName = "GetCreateNoteRequestChain";
                    break;
                case MessageNames.ModifyNoteRequest:
                    boTypeName = "Note";
                    methodName = "GetModifyNoteRequestChain";
                    break;
                case MessageNames.DeleteNoteRequest:
                    boTypeName = "Note";
                    methodName = "GetDeleteNoteRequestChain";
                    break;
                case MessageNames.CreateDocumentRequest:
                    boTypeName = "Document";
                    methodName = "GetCreateDocumentChain";
                    break;
                case MessageNames.CreateArticleCustomerInformationRequest:
                    boTypeName = "CustomerItemReference";
                    methodName = "GetCreateArticleCustomerInformationRequestChain";
                    break;
                case MessageNames.ModifyArticleCustomerInformationRequest:
                    boTypeName = "CustomerItemReference";
                    methodName = "GetModifyArticleCustomerInformationRequestChain";
                    break;
                case MessageNames.CreateTravelReportRequest:
                    boTypeName = "TravelReport";
                    methodName = "GetCreateTravelReportChain";
                    break;
                case MessageNames.CreateExpenseClaimRequest:
                    boTypeName = "ExpenseClaim";
                    methodName = "GetCreateExpenseClaimChain";
                    break;
                case MessageNames.CreateMemoRequest:
                    boTypeName = "Memos";
                    methodName = "GetCreateMemoChain";
                    break;
                case MessageNames.CreateMemoCommentRequest:
                    boTypeName = "Memos";
                    methodName = "GetCreateMemoCommentChain";
                    break;
                default:
                    boTypeName = "EmptyChain";
                    methodName = "GetUnhandledMessageEmtpyChain";
                    break;
            }

            var boTypeCompanyFullName = $"ERPStub.Chains.cc{companyCode}.{boTypeName}";
            var boTypeFullName = $"ERPStub.Chains.{boTypeName}";

            var type = Type.GetType(boTypeCompanyFullName) ?? Type.GetType(boTypeFullName);
            if (type != null)
            {
                var methodInfo = type.GetMethod(methodName,
                    BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public,
                    null /*binder*/,
                    new[] {typeof(ISettings), typeof(Message), typeof(ObjectParameter), typeof(StagingContext)},
                    null /*modifiers*/);

                var steps = (List<Step>) methodInfo?.Invoke(null,
                    new object[] {_settings, message, prRequestId, context});

                if (steps != null && message.Status < steps.Count)
                    for (var status = message.Status; status < steps.Count; status++)
                    {
                        var step = steps[status];
                        var success = step.Invoke(out var statusMessage);
                        context.SaveChanges();

                        using (var incomingConn = new SqlConnection(_settings.IncomingConnectionString))
                        {
                            incomingConn.Open();
                            if (success)
                            {
                                var newStatus = status + 1;
                                if (newStatus == steps.Count) newStatus = -1;
                                UpdateStatus(incomingConn, message.ID, newStatus, statusMessage);
                            }
                        }
                    }
                else
                    using (var incomingConn = new SqlConnection(_settings.IncomingConnectionString))
                    {
                        incomingConn.Open();
                        UpdateExecutionTime(incomingConn, message.ID);
                    }
            }
        }
    }
}