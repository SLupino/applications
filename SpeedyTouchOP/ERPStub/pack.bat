@echo off
setLocal
set _program=%CD%\bin\Release\ERPStub.exe
set _program=%_program:\=\\%
wmic datafile where Name="%_program%" get Version | find "." > version.tmp
for /f "tokens=1" %%a in (version.tmp) do set "_version=%%a"
del version.tmp
set _dir=ERPStub.%_version%
IF EXIST %_dir% (rmdir /s /q %_dir%)
mkdir %_dir%
copy .\bin\Release\ERPStub.exe %_dir%
copy .\bin\Release\ERPStub.exe.config %_dir%
copy .\bin\Release\EntityFramework.dll %_dir%
copy .\bin\Release\EntityFramework.SqlServer.dll %_dir%
copy .\bin\Release\Nlog.dll %_dir%
copy .\bin\Release\Nlog.config %_dir%
endLocal
