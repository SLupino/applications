﻿using ERPStub.Chains;
using ERPStub.DataModel;
using System;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;

namespace ERPStub
{
    class CustomerHelper
    {
        public static string CreateProspectCustomer(XElement xCustomer, StagingContext stagingContext,
                                    ObjectParameter prRequestId)
        {
            return CreateCustomer(xCustomer, stagingContext, prRequestId, 300 /*PROSPECT*/);
        }

        public static string CreateNewCustomer(ISettings settings, XElement xCustomer,
                                            StagingContext stagingContext,
                                            ObjectParameter prRequestId)
        {
            return CreateCustomer(xCustomer, stagingContext, prRequestId, 100 /*NEW*/);
        }

        public static string CreateNewCustomerWithCurrency( ISettings settings, 
                                                            XElement xCustomer,
                                                            StagingContext stagingContext,
                                                            ObjectParameter prRequestId,
                                                            String currency)
        {
            return CreateCustomer(xCustomer, stagingContext, prRequestId, 100, currency);
        }

        public static string CreateCustomer(XElement xCustomer, StagingContext stagingContext, ObjectParameter prRequestId, int state, string currency = "")
        {
            var customer = new Customers();
            customer.invoice = 100;
            var custSalesAreaRel = new CustomerSalesAreaRelation();

            var newCustomerId = "031";
            var num = new Random();
            do
            {
                var nums = num.Next(1000000, 9999999);
                newCustomerId = $"031{nums}";
            } while (stagingContext.Customers.Any(x => x.id == newCustomerId));

            customer.id = newCustomerId;

            SetCustomerInitialValues(customer, prRequestId, Enumerations.Operation.N);
            SetCustomerSalesAreaRelationInitialValues(custSalesAreaRel, prRequestId, Enumerations.Operation.N);

            SetCustomerValuesFromXML(xCustomer, customer);
            if (string.IsNullOrEmpty(customer.currencyIsoCode))
            {
                customer.currencyIsoCode = currency;
            }
            customer.state = state;
            customer.VMIstate = 100;
            customer.association = "";
            customer.pricingGroup = "";
            customer.paymentTerms = "";
            customer.collectiveInvoicing = "";
            customer.type = 100; //100 normal, 200 branch office
            customer.SMLClassificationSalesPotential = 110;
            custSalesAreaRel.customerId = customer.id;
            custSalesAreaRel.salesAreaId = xCustomer.Descendants("salesAreaId").FirstOrDefault().Value;

            stagingContext.Customers.Add(customer);
            stagingContext.CustomerSalesAreaRelation.Add(custSalesAreaRel);

            foreach (XElement xContact in xCustomer.Elements("contact"))
            {
                Contacts contact = Contact.CreateNewContact(xContact, prRequestId);
                contact.customerId = customer.id;
                stagingContext.Contacts.Add(contact);
            }

            return customer.id;
        }

        public static void SetCustomerInitialValues(Customers customer, ObjectParameter prRequestId, Enumerations.Operation operation)
        {
            customer.requestId = (Guid)prRequestId.Value;
            customer.operation = operation.ToString();
            customer.C_boId = Guid.NewGuid();
            customer.C_boCreated = DateTime.Now;
            customer.C_boElaborationErrorMessage = "";
            customer.C_boOperationState = 0;
            customer.C_boreplacedBy = null;
            customer.C_consolGuid = null;
            customer.C_boElaborationErrorCode = 0;
            customer.C_consolRequestID = null;
        }

        public static void SetCustomerSalesAreaRelationInitialValues(CustomerSalesAreaRelation custSalesAreaRel, ObjectParameter prRequestId, Enumerations.Operation operation)
        {
            custSalesAreaRel.requestId = (Guid)prRequestId.Value;
            custSalesAreaRel.operation = operation.ToString();
            custSalesAreaRel.C_boId = Guid.NewGuid();
            custSalesAreaRel.C_boCreated = DateTime.Now;
            custSalesAreaRel.C_boElaborationErrorMessage = "";
            custSalesAreaRel.C_boOperationState = 0;
            custSalesAreaRel.C_boreplacedBy = null;
            custSalesAreaRel.C_consolGuid = null;
            custSalesAreaRel.C_boElaborationErrorCode = 0;
            custSalesAreaRel.C_consolRequestID = null;
        }

        public static void SetCustomerValuesFromXML(XElement xCustomer, Customers customer)
        {
            var name1 = xCustomer.Descendants("name1").FirstOrDefault();
            customer.name1 = name1 != null ? name1.Value : customer.name1;
            
            var name2 = xCustomer.Descendants("name2").FirstOrDefault();
            customer.name2 = name2 != null ? name2.Value : customer.name2;
            
            var street = xCustomer.Descendants("street").FirstOrDefault();
            customer.street = street != null ? street.Value : customer.street;
            
            var zip = xCustomer.Descendants("zip").FirstOrDefault();
            customer.zipCode = zip != null ? zip.Value : customer.zipCode;
            
            var city = xCustomer.Descendants("city").FirstOrDefault();
            customer.city = city != null ? city.Value : customer.city;
            
            var poBox = xCustomer.Descendants("POBox").FirstOrDefault();
            customer.postOfficeBox = poBox != null ? poBox.Value : "";
            
            var poBoxZip = xCustomer.Descendants("POBoxZip").FirstOrDefault();
            customer.postOfficeBoxZipCode = poBoxZip != null ? poBoxZip.Value : "";
            
            var phone = xCustomer.Descendants("phone").FirstOrDefault();
            customer.phone = phone != null ? phone.Value : customer.phone;
            
            var fax = xCustomer.Descendants("fax").FirstOrDefault();
            customer.fax = fax != null ? fax.Value : customer.fax;
            
            var website = xCustomer.Descendants("website").FirstOrDefault();
            customer.website = website != null ? website.Value : customer.website;
            
            var email = xCustomer.Descendants("email").FirstOrDefault();
            customer.email = email != null ? email.Value : customer.email;
            
            customer.isoCountryCode = xCustomer.Descendants("country").FirstOrDefault() != null ? xCustomer.Descendants("country").FirstOrDefault().Value : customer.isoCountryCode;
            
            customer.longitude = xCustomer.Descendants("longitude").FirstOrDefault() != null ? Convert.ToDecimal(xCustomer.Descendants("longitude").FirstOrDefault().Value, CultureInfo.InvariantCulture) : customer.longitude;
            
            customer.latitude = xCustomer.Descendants("latitude").FirstOrDefault() != null ? Convert.ToDecimal(xCustomer.Descendants("latitude").FirstOrDefault().Value, CultureInfo.InvariantCulture) : customer.latitude;
            
            customer.orderReceipt = xCustomer.Descendants("orderReceipt").FirstOrDefault() != null ? (xCustomer.Descendants("orderReceipt").FirstOrDefault().Value == "NOT_NECESSARY" ? 100 : (xCustomer.Descendants("orderReceipt").FirstOrDefault().Value == "SEND_AS_FAX" ? 200 : 300)) : 100;
            
            customer.orderConfirmation = xCustomer.Descendants("orderConfirmation").FirstOrDefault() != null ? getOrderConfirmation_MailingType(xCustomer.Descendants("orderConfirmation").FirstOrDefault().Value) : 100;

            customer.invoice = xCustomer.Descendants("invoice").FirstOrDefault() != null ? MapInvoice(xCustomer.Descendants("invoice").FirstOrDefault().Value) : customer.invoice;

            customer.region = xCustomer.Descendants("region").FirstOrDefault() != null ? xCustomer.Descendants("region").FirstOrDefault().Value : customer.region;
            
            customer.businessNumber = xCustomer.Descendants("businessNumber").FirstOrDefault() != null ? xCustomer.Descendants("businessNumber").FirstOrDefault().Value : "";
            
            customer.membershipNumber = xCustomer.Descendants("membershipNumber").FirstOrDefault() != null ? xCustomer.Descendants("membershipNumber").FirstOrDefault().Value : "";
            
            customer.postOfficeBoxCity = xCustomer.Descendants("POBoxCity").FirstOrDefault() != null ? xCustomer.Descendants("POBoxCity").FirstOrDefault().Value : customer.postOfficeBoxCity;
            
            customer.backOrdersAllowed = xCustomer.Descendants("backOrdersAllowed").FirstOrDefault() != null ? (xCustomer.Descendants("backOrdersAllowed").FirstOrDefault().Value == "true" ? Convert.ToInt16(1) : Convert.ToInt16(0)) : customer.backOrdersAllowed;
            
            customer.customerOrderRefRequired = xCustomer.Descendants("customerOrderRefRequired").FirstOrDefault() != null ? (xCustomer.Descendants("customerOrderRefRequired").FirstOrDefault().Value == "true" ? Convert.ToInt16(1) : Convert.ToInt16(0)) : customer.customerOrderRefRequired;

            customer.currencyIsoCode = xCustomer.Descendants("currency").FirstOrDefault() != null ? xCustomer.Descendants("currency").FirstOrDefault().Value : "";

            customer.industryId = xCustomer.Descendants("industry").Attributes("id").FirstOrDefault() != null ? xCustomer.Descendants("industry").Attributes("id").FirstOrDefault().Value : customer.industryId;
            
            customer.numberOfEmployees = xCustomer.Descendants("numberOfEmployees").FirstOrDefault() != null ? Convert.ToInt32(xCustomer.Descendants("numberOfEmployees").FirstOrDefault().Value) : customer.numberOfEmployees;
            
            var priceSetting = xCustomer.Descendants("priceSetting").FirstOrDefault();
            customer.grossNetPrice = priceSetting != null && priceSetting.Value == "gross" ? 100 : 200;
            
            customer.invoiceParty = xCustomer.Descendants("invoiceParty").FirstOrDefault() != null ? xCustomer.Descendants("invoiceParty").FirstOrDefault().Value : "";

            XElement xVisitFrequency = xCustomer.Element("visitFrequency");
            if (xVisitFrequency != null)
            {
                customer.visitsRecurrenceRule = xVisitFrequency.Element("recurrenceRule") != null ? xVisitFrequency.Element("recurrenceRule").Value : customer.visitsRecurrenceRule;

                customer.visitsStartDate = DateTime.Parse(xVisitFrequency.Element("startDate").Value);

                customer.visitsHour = xVisitFrequency.Element("hour") != null
                                          ? TimeSpan.Parse(xVisitFrequency.Element("hour").Value)
                                          : customer.visitsHour;

                customer.visitsCreateRecurringAppointments =
                    Boolean.Parse(xVisitFrequency.Element("visitsCreateRecurringAppointments").Value) ? (short)1 : (short)0;

                customer.visitsAlarmRelativeOffset = xVisitFrequency.Element("visitsAlarmRelativeOffset") != null
                                                         ? xVisitFrequency.Element("visitsAlarmRelativeOffset").Value
                                                         : customer.visitsAlarmRelativeOffset;

                customer.visitsIsFixedAppointment = Boolean.Parse(xVisitFrequency.Element("isFixed").Value)
                                                        ? (short) 1
                                                        : (short) 0;

            }
        }

        private static int getOrderConfirmation_MailingType(string value)
        {
            switch (value)
            {
                case "NOT_NECESSARY":
                    return (int)Enumerations.OrderConfirmation_MailingType.NOT_NECESSARY;
                case "SEND_AS_FAX":
                    return (int)Enumerations.OrderConfirmation_MailingType.SEND_AS_FAX;
                case "SEND_AS_LETTER_WITHOUT_CREDIT_SLIP":
                    return (int)Enumerations.OrderConfirmation_MailingType.SEND_AS_LETTER_WITHOUT_CREDIT_SLIP;
                case "SEND_AS_LETTER_WITH_CREDIT_SLIP":
                    return (int)Enumerations.OrderConfirmation_MailingType.SEND_AS_LETTER_WITH_CREDIT_SLIP;
                case "SEND_AS_MAIL":
                    return (int)Enumerations.OrderConfirmation_MailingType.SEND_AS_MAIL;
                default:
                    return (int)Enumerations.OrderConfirmation_MailingType.NOT_NECESSARY;
            }
        }

        private static int MapInvoice(string invoice)
        {
            // 100=SEND_AS_LETTER,200=SEND_AS_LETTER_AND_EMAIL
            switch (invoice)
            {
                case "SEND_AS_LETTER":
                    return 100;
                case "SEND_AS_LETTER_AND_EMAIL":
                    return 200;
                default:
                    return 100;
            }
        }

    }
}