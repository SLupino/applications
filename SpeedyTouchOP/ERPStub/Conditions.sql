
CREATE TABLE [dbo].[Conditions](
	[customerId] [nvarchar](20) NOT NULL,
	[itemId] [nvarchar](50) NOT NULL,
	[isGroup] [bit] NOT NULL,
	[discount] [decimal](5, 2) NOT NULL,
	[surcharge] [decimal](5, 2) NOT NULL,
 CONSTRAINT [PK_PriceAgreements] PRIMARY KEY CLUSTERED 
(
	[customerId] ASC,
	[itemId] ASC,
	[isGroup] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


