﻿namespace ERPStub
{
    class MessageNames
    {
        public const string CreateContactRequest = "createContactRequest",
            ModifyContactRequest = "modifyContactRequest",
            DeleteContactRequest = "deleteContactRequest",
            CreateProspectRequest = "createProspectRequest",
            CreateProspectRequest3 = "createProspectRequest3",
            SendCartRequest = "sendCartRequest",
            SendCartRequest2 = "sendCartRequest2",
            SendCartRequest3 = "sendCartRequest3",
            SendCartRequest4 = "sendCartRequest4",
            SendCartRequest5 = "sendCartRequest5",
            SendCartRequest6 = "sendCartRequest6",
            SendCartRequest7 = "sendCartRequest7",
            SendCartRequest8 = "sendCartRequest9",
            SendCartRequest10 = "sendCartRequest10",
            SendCartRequest11 = "sendCartRequest11",
            SendCartRequest12 = "sendCartRequest12",
            CreateNoteRequest = "createNoteRequest",
            ModifyNoteRequest = "modifyNoteRequest",
            DeleteNoteRequest = "deleteNoteRequest",
            CreateDailyReport = "createDailyReport",
            CreateDailyReport2 = "createDailyReport2",
            CreateDailyReport4 = "createDailyReport4",
            ModifyCustomerRequest = "modifyCustomerRequest",
            ModifyCustomerRequest2 = "modifyCustomerRequest2",
            ModifyCustomerRequest3 = "modifyCustomerRequest3",
            ModifyVisitFrequencyRequest = "modifyVisitFrequencyRequest",
            SendPayment = "sendPayment",
            SendPayment3 = "sendPayment3",
            CreateDocumentRequest = "createDocumentRequest",
            CreateTravelReportRequest = "createTravelReportRequest",
            CreateExpenseClaimRequest = "createExpenseClaimRequest",
            CreateProspectRequest2 = "createProspectRequest2",
            CreateArticleCustomerInformationRequest = "createArticleCustomerInformationRequest",
            ModifyArticleCustomerInformationRequest = "modifyArticleCustomerInformationRequest",
            CreateOrModifyAppointmentRequest = "createOrModifyAppointmentRequest", //TPL
            CreateMemoRequest = "createMemoRequest",
            CreateMemoCommentRequest = "createMemoCommentRequest"; 
    }
}
