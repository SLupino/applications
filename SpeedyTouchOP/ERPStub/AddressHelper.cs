﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ERPStub.DataModel;

namespace ERPStub
{
    class AddressHelper
    {
        public static void SetStagingControlValuesCustomers(Customers deliveryAddress, ObjectParameter prRequestId, Enumerations.Operation operation)
        {
            if (deliveryAddress == null) return;

            deliveryAddress.requestId = (Guid)prRequestId.Value;
            deliveryAddress.operation = operation.ToString();
            deliveryAddress.C_boId = Guid.NewGuid();
            deliveryAddress.C_boCreated = DateTime.Now;
            deliveryAddress.C_boElaborationErrorMessage = "";
            deliveryAddress.C_boOperationState = 0;
            deliveryAddress.C_boreplacedBy = null;
            deliveryAddress.C_consolGuid = null;
            deliveryAddress.C_boElaborationErrorCode = 0;
            deliveryAddress.C_consolRequestID = null;
        }

        public static void SetStagingControlValuesPartnerRelations(PartnerRelations partnerRelation, ObjectParameter prRequestId, Enumerations.Operation operation)
        {
            if (partnerRelation == null) return;

            partnerRelation.requestId = (Guid)prRequestId.Value;
            partnerRelation.operation = operation.ToString();
            partnerRelation.C_boId = Guid.NewGuid();
            partnerRelation.C_boCreated = DateTime.Now;
            partnerRelation.C_boElaborationErrorMessage = "";
            partnerRelation.C_boOperationState = 0;
            partnerRelation.C_boreplacedBy = null;
            partnerRelation.C_consolGuid = null;
            partnerRelation.C_boElaborationErrorCode = 0;
            partnerRelation.C_consolRequestID = null;
        }

        public static void SetStagingControlValuesCustomerSalesAreaRelations(CustomerSalesAreaRelation custSalesAreaRelation, ObjectParameter prRequestId, Enumerations.Operation operation)
        {
            if (custSalesAreaRelation == null ) return;

            custSalesAreaRelation.requestId = (Guid)prRequestId.Value;
            custSalesAreaRelation.operation = operation.ToString();
            custSalesAreaRelation.C_boId = Guid.NewGuid();
            custSalesAreaRelation.C_boCreated = DateTime.Now;
            custSalesAreaRelation.C_boElaborationErrorMessage = "";
            custSalesAreaRelation.C_boOperationState = 0;
            custSalesAreaRelation.C_boreplacedBy = null;
            custSalesAreaRelation.C_consolGuid = null;
            custSalesAreaRelation.C_boElaborationErrorCode = 0;
            custSalesAreaRelation.C_consolRequestID = null;
        }
    }
}
