﻿using System;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Xml.Linq;
using ERPStub.DataModel;
using NLog;
using System.Xml.XPath;

namespace ERPStub
{
    class ExpenseClaimHelper
    {
        private static readonly Logger Logger = LogManager.GetLogger(typeof(ERPStub).FullName);

        internal static void CreateExpenseClaim(XElement xCreateExpenseClaimRequest, StagingContext context,
            ObjectParameter prRequestId, XDocument xdoc)
        {
            ExpenseReports expenseReports = new ExpenseReports();
            SetExpensesInitialValues(prRequestId, expenseReports, Enumerations.Operation.N);
            expenseReports.id = Guid.Parse(xCreateExpenseClaimRequest.Element("id").Value);
            expenseReports.userId = Guid.Parse(xCreateExpenseClaimRequest.XPathSelectElement("user").Attribute("id").Value);
            expenseReports.reportName = ((xCreateExpenseClaimRequest.Element("reportName") != null) && !String.IsNullOrEmpty(xCreateExpenseClaimRequest.Element("reportName").Value))
                                    ? xCreateExpenseClaimRequest.Element("reportName").Value
                                    : "ND";
            expenseReports.creationDate = ((xCreateExpenseClaimRequest.Element("creationDate") != null) && !String.IsNullOrEmpty(xCreateExpenseClaimRequest.Element("creationDate").Value))
                                    ? DateTime.Parse(xCreateExpenseClaimRequest.Element("creationDate").Value)
                                    : DateTime.Now;
            expenseReports.currencyCode = ((xCreateExpenseClaimRequest.Element("currencyCode") != null) && !String.IsNullOrEmpty(xCreateExpenseClaimRequest.Element("currencyCode").Value))
                                    ? xCreateExpenseClaimRequest.Element("currencyCode").Value
                                    : "EUR";
            expenseReports.reportTotal = ((xCreateExpenseClaimRequest.Element("reportTotal") != null) && !String.IsNullOrEmpty(xCreateExpenseClaimRequest.Element("reportTotal").Value))
                                    ? Convert.ToInt32(xCreateExpenseClaimRequest.Element("reportTotal").Value)
                                    : 0;
            expenseReports.personalExpenses = ((xCreateExpenseClaimRequest.Element("personalExpenses") != null) && !String.IsNullOrEmpty(xCreateExpenseClaimRequest.Element("personalExpenses").Value))
                                    ? Convert.ToInt32(xCreateExpenseClaimRequest.Element("personalExpenses").Value)
                                    : 0;
            expenseReports.paymentStatusCode = ((xCreateExpenseClaimRequest.Element("paymentStatusCode") != null) && !String.IsNullOrEmpty(xCreateExpenseClaimRequest.Element("paymentStatusCode").Value))
                                    ? xCreateExpenseClaimRequest.Element("paymentStatusCode").Value
                                    : "ND";
            expenseReports.everSentBack = ((xCreateExpenseClaimRequest.Element("everSentBack") != null) && Boolean.Parse(xCreateExpenseClaimRequest.Element("everSentBack").Value))
                                    ? (short)1
                                    : (short)0;
            expenseReports.hasException = ((xCreateExpenseClaimRequest.Element("hasException") != null) && Boolean.Parse(xCreateExpenseClaimRequest.Element("hasException").Value))
                                    ? (short)1
                                    : (short)0;
            expenseReports.receiptsReceived = ((xCreateExpenseClaimRequest.Element("receiptsReceived") != null) && Boolean.Parse(xCreateExpenseClaimRequest.Element("receiptsReceived").Value))
                                    ? (short)1
                                    : (short)0;
            context.ExpenseReports.Add(expenseReports);
            Logger.Trace("Number of expense entries in the current Claim: {0}", xdoc.Descendants("expenseEntry").Count());
            foreach (var xExpenseEntry in xdoc.Descendants("expenseEntry"))
            {
                ExpenseEntries expensesEntry = TravelReportHelper.ParseExpenseEntries(xExpenseEntry);
                expensesEntry.reportId = expenseReports.id;
                TravelReportHelper.SetExpensesEntryInitialValues(prRequestId, expensesEntry, Enumerations.Operation.N);
                Logger.Trace("Expense entry: {0}", xExpenseEntry);
                context.ExpenseEntries.Add(expensesEntry);
            }
        }

        private static void SetExpensesInitialValues(ObjectParameter prRequestId, ExpenseReports expenses, Enumerations.Operation operation)
        {
            expenses.requestId = (Guid)prRequestId.Value;
            expenses.operation = operation.ToString();
            expenses.C_boId = Guid.NewGuid();
            expenses.C_boCreated = DateTime.Now;
            expenses.C_boElaborationErrorMessage = "";
            expenses.C_boOperationState = 0;
            expenses.C_boreplacedBy = null;
            expenses.C_consolGuid = null;
            expenses.C_boElaborationErrorCode = 0;
            expenses.C_consolRequestID = null;
        }
    }
}
