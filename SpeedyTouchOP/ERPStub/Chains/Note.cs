﻿using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using ERPStub.DataModel;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Collections.Generic;

namespace ERPStub.Chains
{
    internal class Note
    {
        internal static List<Step> GetCreateNoteRequestChain(ISettings settings, Message message, ObjectParameter prRequestId, StagingContext context)
        {
            var steps = new List<Step>
                {
                    delegate(out string statusMessage)
                        {
                            Notes note = new Notes();

                            note = SetInitialValues(note, prRequestId, Enumerations.Operation.N);
                            XElement xCreateNoteRequest = (XElement) message.XRequestDocument.FirstNode;
                            note = SetValuesFromXml(xCreateNoteRequest, note);
                            context.Notes.Add(note);
                            statusMessage = "Note Created";
                            return true;
                        }
                };

            return steps;
        }

        internal static List<Step> GetModifyNoteRequestChain(ISettings settings, Message message, ObjectParameter prRequestId, StagingContext context)
        {
            var steps = new List<Step>
                {
                    delegate(out string statusMessage)
                        {
                            ModifyOrDeleteRow(message, Enumerations.Operation.N, prRequestId, context);

                            statusMessage = "Note Modified";
                            return true;
                        }
                };

            return steps;
        }

        internal static List<Step> GetDeleteNoteRequestChain(ISettings settings, Message message, ObjectParameter prRequestId, StagingContext context)
        {
            var steps = new List<Step>
            {
                delegate(out string statusMessage)
                {
                    ModifyOrDeleteRow(message, Enumerations.Operation.D, prRequestId, context);

                    statusMessage = "Note Deleted";
                    return true;
                }
            };

            return steps;
        }

        private static Notes SetInitialValues(Notes row, ObjectParameter prRequestId, Enumerations.Operation operation)
        {
            row.requestId = (Guid)prRequestId.Value;
            row.operation = operation.ToString();
            row.C_boId = Guid.NewGuid();
            row.C_boCreated = DateTime.Now;
            row.C_boElaborationErrorMessage = "";
            row.C_boOperationState = 0;
            row.C_boreplacedBy = null;
            row.C_consolGuid = null;
            row.C_boElaborationErrorCode = 0;
            row.C_consolRequestID = null;

            return row;
        }

        private static Notes SetValuesFromXml(XElement xNoteRequest, Notes note)
        {
            XAttribute customerId = xNoteRequest.Descendants("customer").Attributes("id").FirstOrDefault();
            if(customerId != null)
                note.customerId = customerId.Value;

            XAttribute modifiedBy = xNoteRequest.Descendants("modifiedBy").Attributes("id").FirstOrDefault();
            if (modifiedBy != null)
                note.modifiedBy = new Guid(modifiedBy.Value);

            XElement xModificationTimestamp = xNoteRequest.Descendants("modificationTimestamp").FirstOrDefault();
            note.modifiedOn = xModificationTimestamp != null ? DateTime.Parse(xModificationTimestamp.Value) : note.modifiedOn;

            XAttribute noteId = xNoteRequest.Descendants("note").Attributes("id").FirstOrDefault();
            if (noteId != null)
                note.id = new Guid(noteId.Value);

            XElement color = xNoteRequest.Descendants("color").FirstOrDefault();
            note.color = color != null ? Convert.ToInt16(color.Value) : note.color;

            XAttribute createdBy = xNoteRequest.Descendants("createdBy").Attributes("id").FirstOrDefault();
            note.createdBy = createdBy != null ? new Guid(createdBy.Value) : note.createdBy;

            XElement xCreationTimestamp = xNoteRequest.Descendants("creationTimestamp").FirstOrDefault();
            note.createdOn = xCreationTimestamp != null ? DateTime.Parse(xCreationTimestamp.Value) : note.createdOn;

            XElement pinned = xNoteRequest.Descendants("pinned").FirstOrDefault();
            note.isPinned = pinned != null ? (pinned.Value == "true" ? Convert.ToInt16(1) : Convert.ToInt16(0) ): note.isPinned;

            XElement text = xNoteRequest.Descendants("text").FirstOrDefault();
            note.text = text != null ? text.Value : note.text;

            return note;
        }

        private static void ModifyOrDeleteRow(Message message, Enumerations.Operation operation,
                                              ObjectParameter prRequestId, StagingContext context)
        {
            var elements = (XElement) message.XRequestDocument.FirstNode;
            var noteId = (Guid) (elements.XPathSelectElement("note").Attribute("id"));
            Notes note = (from Notes r in context.Notes
                       join NotesKey ckRow in context.NotesKey
                           on r.C_boId equals ckRow.appliedBy
                       where ckRow.id == noteId
                       select r).AsNoTracking().Take(1).FirstOrDefault();

            if (note != null)
            {
                note = SetInitialValues(note, prRequestId, operation);
                if (operation == Enumerations.Operation.N)
                {
                    note = SetValuesFromXml(elements, note);
                }
                context.Notes.Add(note);
            }
        }
    }
}