﻿using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using ERPStub.DataModel;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Collections.Generic;

namespace ERPStub.Chains
{
    internal class Customer
    {
        public static List<Step> GetModifyCustomerChain(ISettings settings, Message message, ObjectParameter prRequestId,
                                                        StagingContext context)
        {
            var steps = new List<Step>
                {
                    delegate(out string statusMessage)
                        {
                            ModifyCustomer(message, Enumerations.Operation.N, prRequestId, context);

                            statusMessage = "Customer Modified";
                            return true;
                        }
                };

            return steps;
        }

        public static List<Step> GetModifyVisitFrequencyChain(ISettings settings, Message message,
                                                              ObjectParameter prRequestId, StagingContext context)
        {
            var steps = new List<Step>
                {
                    delegate(out string statusMessage)
                        {
                            ModifyVisitFrequency(message, Enumerations.Operation.N, prRequestId, context);

                            statusMessage = "Customer Modified";
                            return true;
                        }
                };

            return steps;
        }

        private static void ModifyVisitFrequency(Message message, Enumerations.Operation operation, ObjectParameter prRequestId, StagingContext context)
        {
            ModifyCustomer(message, operation, prRequestId, context, true);
        }

        private static void ModifyCustomer(Message message, Enumerations.Operation operation,
                                              ObjectParameter prRequestId, StagingContext context,
                                              bool isFrequency = false)
        {
            var xModifyCustomerRequest = (XElement)message.XRequestDocument.FirstNode;
            var customerId = xModifyCustomerRequest.XPathSelectElement("customer")?.Attribute("id")?.Value;
            
            //TODO: check if salesAreaId may be present for modifyCustomer
            //var salesAreaId = xModifyCustomerRequest.XPathSelectElement("salesAreaId").Attribute
            var customer = (from Customers r in context.Customers
                                                 join CustomersKey ckRow in context.CustomersKey on r.C_boId equals ckRow.appliedBy
                                                 where r.id == customerId
                                                 select r).AsNoTracking().Take(1).FirstOrDefault();
            
            if (customer != null)
            {
                CustomerHelper.SetCustomerInitialValues(customer, prRequestId, operation);
                if (operation == Enumerations.Operation.N && !isFrequency)
                {
                    CustomerHelper.SetCustomerValuesFromXML(xModifyCustomerRequest, customer);
                }
                else if (operation == Enumerations.Operation.N && isFrequency)
                {
                    ModifyFrequency(xModifyCustomerRequest, customer);
                }
                context.Customers.Add(customer);
            }
        }

        private static void ModifyFrequency(XContainer xModifyCustomerRequest, Customers customer)
        {
            var recurrenceRule = xModifyCustomerRequest.Descendants("recurrenceRule").FirstOrDefault();
            customer.visitsRecurrenceRule = recurrenceRule != null
                                                ? recurrenceRule.Value
                                                : customer.visitsRecurrenceRule;
            
            var startDate = xModifyCustomerRequest.Descendants("startDate").FirstOrDefault();
            customer.visitsStartDate = startDate != null
                                           ? Convert.ToDateTime(startDate.Value)
                                           : customer.visitsStartDate;

            var hour = xModifyCustomerRequest.Descendants("hour").FirstOrDefault();
            if (hour != null)
            {
                customer.visitsHour = TimeSpan.Parse(hour.Value);
            }

            var visitsCreateRecurringAppointments =
                xModifyCustomerRequest.Descendants("visitsCreateRecurringAppointments").FirstOrDefault();
            customer.visitsCreateRecurringAppointments = (bool.Parse(visitsCreateRecurringAppointments?.Value ?? "false")
                                                              ? Convert.ToInt16(1)
                                                              : Convert.ToInt16(0));

            var visitsAlarmRelativeOffset = xModifyCustomerRequest.Descendants("visitsAlarmRelativeOffset").FirstOrDefault();
            customer.visitsAlarmRelativeOffset = visitsAlarmRelativeOffset != null
                                                     ? visitsAlarmRelativeOffset.Value
                                                     : customer.visitsAlarmRelativeOffset;

            var isFixed = xModifyCustomerRequest.Descendants("isFixed").FirstOrDefault();
            customer.visitsIsFixedAppointment = xModifyCustomerRequest.Descendants("isFixed").FirstOrDefault() != null
                                                    ? (isFixed != null && isFixed.Value == "true"
                                                           ? Convert.ToInt16(1)
                                                           : Convert.ToInt16(0))
                                                    : customer.visitsIsFixedAppointment;

            var tourplanStartDateSystemProposed =
                xModifyCustomerRequest.Descendants("tourplanStartDateSystemProposed").FirstOrDefault();
            customer.tourplanStartDateSystemProposed = tourplanStartDateSystemProposed != null
                                                           ? Convert.ToDateTime(tourplanStartDateSystemProposed.Value)
                                                           : customer.tourplanStartDateSystemProposed;

            var tourplanStartDateBySalesRep = xModifyCustomerRequest.Descendants("tourplanStartDateBySalesRep").FirstOrDefault();
            customer.tourplanStartDateBySalesRep = tourplanStartDateBySalesRep != null
                                                       ? Convert.ToDateTime(tourplanStartDateBySalesRep.Value)
                                                       : customer.tourplanStartDateBySalesRep;

            var tourplanVisitHourSystemProposed =
                xModifyCustomerRequest.Descendants("tourplanVisitHourSystemProposed").FirstOrDefault();
            if (tourplanVisitHourSystemProposed != null)
            {
                var hourSplitted = tourplanVisitHourSystemProposed.Value.Split(':');
                if (hourSplitted.Length == 2)
                    customer.tourplanVisitHourSystemProposed = new TimeSpan(Convert.ToInt32(hourSplitted[0]),
                                                                            Convert.ToInt32(hourSplitted[1]), 0);
            }

            if (xModifyCustomerRequest.Descendants("tourplanVisitHourBySalesRep").FirstOrDefault() != null)
            {
                var hourSplitted = xModifyCustomerRequest.Descendants("tourplanVisitHourBySalesRep").FirstOrDefault()?.Value.Split(':');
                if (hourSplitted?.Length == 2)
                    customer.tourplanVisitHourSystemProposed = new TimeSpan(Convert.ToInt32(hourSplitted[0]),
                                                                            Convert.ToInt32(hourSplitted[1]), 0);
            }
            customer.tourplanVisitDaySystemProposed =
                xModifyCustomerRequest.Descendants("tourplanVisitDaySystemProposed").FirstOrDefault() != null
                    ? Convert.ToInt32(xModifyCustomerRequest.Descendants("tourplanVisitDaySystemProposed").FirstOrDefault().Value)
                    : customer.tourplanVisitDaySystemProposed;

            customer.tourplanVisitDayBySalesRep = xModifyCustomerRequest.Descendants("tourplanVisitDayBySalesRep").FirstOrDefault() !=
                                                  null
                                                      ? Convert.ToInt32(
                                                          xModifyCustomerRequest.Descendants("tourplanVisitDayBySalesRep")
                                                                  .FirstOrDefault()
                                                                  .Value)
                                                      : customer.tourplanVisitDayBySalesRep;

            customer.tourplanVisitFrequencySystemProposed =
                xModifyCustomerRequest.Descendants("tourplanVisitFrequencySystemProposed").FirstOrDefault() != null
                    ? xModifyCustomerRequest.Descendants("tourplanVisitFrequencySystemProposed").FirstOrDefault().Value
                    : "";

            customer.tourplanVisitFrequencyBySalesRep =
                xModifyCustomerRequest.Descendants("tourplanVisitFrequencyBySalesRep").FirstOrDefault() != null
                    ? xModifyCustomerRequest.Descendants("tourplanVisitFrequencyBySalesRep").FirstOrDefault().Value
                    : "";

            XElement xFirstOrDefault = xModifyCustomerRequest.Descendants("tourplanCreateRecurringAppointments").FirstOrDefault();
            customer.tourplanCreateRecurringAppointments = xFirstOrDefault != null && Boolean.Parse(xFirstOrDefault.Value)
                                                           ? Convert.ToInt16(1)
                                                           : Convert.ToInt16(0);

            var xRecurrenceRuleChangedAt = xModifyCustomerRequest.Descendants("recurrenceRuleChangedAt").FirstOrDefault();
            customer.recurrenceRuleChangedAt = xRecurrenceRuleChangedAt != null
                ? Convert.ToDateTime(xRecurrenceRuleChangedAt.Value)
                : customer.recurrenceRuleChangedAt;

            var xRecurrenceRuleChangedBy = xModifyCustomerRequest.Descendants("recurrenceRuleChangedBy").FirstOrDefault();
            customer.recurrenceRuleChangedBy = xRecurrenceRuleChangedBy != null
                ? Convert.ToInt32(xRecurrenceRuleChangedBy.Value)
                : customer.recurrenceRuleChangedBy;
        }
    }
}