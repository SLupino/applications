﻿using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using ERPStub.DataModel;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Collections.Generic;

namespace ERPStub.Chains
{
    internal class Contact
    {

        internal static List<Step> GetCreateContactChain(ISettings settings, Message message, ObjectParameter prRequestId, StagingContext context)
        {
            var steps = new List<Step>
                {
                    delegate(out string statusMessage)
                        {
                            Contacts contact = CreateNewContact((XElement) message.XRequestDocument.FirstNode, prRequestId);
                            context.Contacts.Add(contact);

                            statusMessage = "Contact Created";
                            return true;
                        }
                };

            return steps;
        }

        public static Contacts CreateNewContact(XElement xContact, ObjectParameter prRequestId)
        {
            var contact = new Contacts();

            SetInitialValues(contact, prRequestId, Enumerations.Operation.N);
            SetValuesFromXML(xContact, contact);

            return contact;
        }

        internal static List<Step> GetModifyContactChain(ISettings settings, Message message, ObjectParameter prRequestId, StagingContext context)
        {
            var steps = new List<Step>
                {
                    delegate(out string statusMessage)
                        {
                            ModifyOrDeleteContact(message, Enumerations.Operation.N, prRequestId, context);

                            statusMessage = "Contact Modified";
                            return true;
                        }
                };

            return steps;
        }

        internal static List<Step> GetDeleteContactChain(ISettings settings, Message message, ObjectParameter prRequestId, StagingContext context)
        {
            var steps = new List<Step>
            {
                delegate(out string statusMessage)
                {
                    ModifyOrDeleteContact(message, Enumerations.Operation.D, prRequestId, context);

                    statusMessage = "Contact Deleted";
                    return true;
                }
            };

            return steps;
        }

        private static Contacts SetInitialValues(Contacts row, ObjectParameter prRequestId, Enumerations.Operation operation)
        {
            row.requestId = (Guid)prRequestId.Value;
            row.operation = operation.ToString();
            row.C_boId = Guid.NewGuid();
            row.C_boCreated = DateTime.Now;
            row.C_boElaborationErrorMessage = "";
            row.C_boOperationState = 0;
            row.C_boreplacedBy = null;
            row.C_consolGuid = null;
            row.C_boElaborationErrorCode = 0;
            row.C_consolRequestID = null;
            row.C_consolFK_Contacts_Customers = null;

            return row;
        }

        private static void ModifyOrDeleteContact(Message message, Enumerations.Operation operation,
                                              ObjectParameter prRequestId, StagingContext context)
        {
            var xElement = (XElement) message.XRequestDocument.FirstNode;
            var contactId = (Guid) (xElement.XPathSelectElement("contact").Attribute("id"));
            Contacts contact = (from Contacts r in context.Contacts
                       join ContactsKey ckRow in context.ContactsKey
                           on r.C_boId equals ckRow.appliedBy
                       where ckRow.id == contactId
                       select r).AsNoTracking().Take(1).FirstOrDefault();

            if (contact == null)
            {
                throw new Exception(string.Format("Contact {0} not found on staging DB.", contactId));
            }
            contact = SetInitialValues(contact, prRequestId, operation);

            XElement xContact = xElement.XPathSelectElement("contact");

            if (operation == Enumerations.Operation.N)
            {
                contact = SetValuesFromXML(xElement, contact);
            }
            context.Contacts.Add(contact);
            
        }

        private static Contacts SetValuesFromXML(XElement xContact, Contacts contact)
        {
            var customerId = xContact.Descendants("customer").Attributes("id").FirstOrDefault();
            if (customerId != null)
                contact.customerId = customerId.Value;

            var contactId = xContact.Descendants("contact").Attributes("id").FirstOrDefault();

            if (contactId != null)
                contact.id = new Guid(contactId.Value);
            else
                contact.id = Guid.NewGuid();
            
            var modifiedBy = xContact.Descendants("modifiedBy").Attributes("id").FirstOrDefault();

            if (modifiedBy != null)
                contact.modifiedBy = new Guid(modifiedBy.Value);

            var first = xContact.Descendants("first").FirstOrDefault();
            contact.firstName = first != null ? first.Value : contact.firstName;

            var last = xContact.Descendants("last").FirstOrDefault();
            contact.lastName = last != null ? last.Value : contact.lastName;

            var prefix = xContact.Descendants("prefix").FirstOrDefault();
            contact.namePrefix = prefix != null ? prefix.Value : contact.namePrefix;

            var suffix = xContact.Descendants("suffix").FirstOrDefault();
            contact.nameSuffix = suffix != null ? suffix.Value : contact.nameSuffix;

            var department = xContact.Descendants("department").FirstOrDefault();
            contact.department = department != null ? department.Value : contact.department;

            var jobtitle = xContact.Descendants("jobtitle").FirstOrDefault();
            contact.jobtitle = jobtitle != null ? jobtitle.Value : contact.jobtitle;

            var note = xContact.Descendants("note").FirstOrDefault();
            contact.note = note != null ? note.Value : contact.note;

            var birthday = xContact.Descendants("birthday").FirstOrDefault();
            contact.birthday = birthday != null && !String.IsNullOrEmpty(birthday.Value) ? Convert.ToDateTime(birthday.Value) : contact.birthday;

            var anniversary = xContact.Descendants("anniversary").FirstOrDefault();
            contact.anniversary = anniversary != null && !String.IsNullOrEmpty(anniversary.Value) ? Convert.ToDateTime(anniversary.Value) : contact.anniversary;

            contact.spouse = xContact.Descendants("spouse").FirstOrDefault() != null ? xContact.Descendants("spouse").FirstOrDefault().Value : contact.spouse;

            var xImage = xContact.Descendants("image").FirstOrDefault();
            contact.image = xImage != null ? Convert.FromBase64String(xImage.Value) : contact.image;
            
            if (xContact.Element("privateAddress") != null)
            {
                contact.privateAddressStreet = xContact.Descendants("privateAddress").Elements("street").FirstOrDefault() != null ? xContact.Descendants("privateAddress").Elements("street").FirstOrDefault().Value : contact.privateAddressStreet;
                contact.privateAddressCity = xContact.Descendants("privateAddress").Elements("city").FirstOrDefault() != null ? xContact.Descendants("privateAddress").Elements("city").FirstOrDefault().Value : contact.privateAddressCity;
                contact.privateAddressZip = xContact.Descendants("privateAddress").Elements("zip").FirstOrDefault() != null ? xContact.Descendants("privateAddress").Elements("zip").FirstOrDefault().Value : contact.privateAddressZip;
                contact.privateAddressCountry = xContact.Descendants("privateAddress").Elements("country").FirstOrDefault() != null ? xContact.Descendants("privateAddress").Elements("country").FirstOrDefault().Value : contact.privateAddressCountry;
                contact.privateAddressRegion = xContact.Descendants("privateAddress").Elements("region").FirstOrDefault() != null ? xContact.Descendants("privateAddress").Elements("region").FirstOrDefault().Value : contact.privateAddressRegion;
            }
            
            if (xContact.Element("otherAddress") != null)
            {
                contact.otherAddressStreet = xContact.Descendants("otherAddress").Elements("street").FirstOrDefault() != null ? xContact.Descendants("otherAddress").Elements("street").FirstOrDefault().Value : contact.otherAddressStreet;
                contact.otherAdressCity = xContact.Descendants("otherAddress").Elements("city").FirstOrDefault() != null ? xContact.Descendants("otherAddress").Elements("city").FirstOrDefault().Value : contact.otherAdressCity;
                contact.otherAdressZip = xContact.Descendants("otherAddress").Elements("zip").FirstOrDefault() != null ? xContact.Descendants("otherAddress").Elements("zip").FirstOrDefault().Value : contact.otherAdressZip;
                contact.otherAdressCountry = xContact.Descendants("otherAddress").Elements("country").FirstOrDefault() != null ? xContact.Descendants("otherAddress").Elements("country").FirstOrDefault().Value : contact.otherAdressCountry;
                contact.otherAdressRegion = xContact.Descendants("otherAddress").Elements("region").FirstOrDefault() != null ? xContact.Descendants("otherAddress").Elements("region").FirstOrDefault().Value : contact.otherAdressRegion;
            }
            contact.website = xContact.Descendants("website").FirstOrDefault() != null ? xContact.Descendants("website").FirstOrDefault().Value : contact.website;
            contact.phoneMobile = xContact.Descendants("phoneMobile").FirstOrDefault() != null ? xContact.Descendants("phoneMobile").FirstOrDefault().Value : contact.phoneMobile;
            contact.phonePrivate = xContact.Descendants("phonePrivate").FirstOrDefault() != null ? xContact.Descendants("phonePrivate").FirstOrDefault().Value : contact.phonePrivate;
            contact.phoneWork = xContact.Descendants("phoneWork").FirstOrDefault() != null ? xContact.Descendants("phoneWork").FirstOrDefault().Value : contact.phoneWork;
            contact.faxPrivate = xContact.Descendants("faxPrivate").FirstOrDefault() != null ? xContact.Descendants("faxPrivate").FirstOrDefault().Value : contact.faxPrivate;
            contact.faxWork = xContact.Descendants("faxWork").FirstOrDefault() != null ? xContact.Descendants("faxWork").FirstOrDefault().Value : contact.faxWork;
            contact.pager = xContact.Descendants("pager").FirstOrDefault() != null ? xContact.Descendants("pager").FirstOrDefault().Value : contact.pager;
            contact.phoneAssistant = xContact.Descendants("phoneAssistant").FirstOrDefault() != null ? xContact.Descendants("phoneAssistant").FirstOrDefault().Value : contact.phoneAssistant;
            contact.phoneCar = xContact.Descendants("phoneCar").FirstOrDefault() != null ? xContact.Descendants("phoneCar").FirstOrDefault().Value : contact.phoneCar;
            contact.phoneCompany = xContact.Descendants("phoneCompany").FirstOrDefault() != null ? xContact.Descendants("phoneCompany").FirstOrDefault().Value : contact.phoneCompany;
            contact.phoneRadio = xContact.Descendants("phoneRadio").FirstOrDefault() != null ? xContact.Descendants("phoneRadio").FirstOrDefault().Value : contact.phoneRadio;
            contact.email1 = xContact.Descendants("email1").FirstOrDefault() != null ? xContact.Descendants("email1").FirstOrDefault().Value : contact.email1;
            contact.email2 = xContact.Descendants("email2").FirstOrDefault() != null ? xContact.Descendants("email2").FirstOrDefault().Value : contact.email2;
            contact.email3 = xContact.Descendants("email3").FirstOrDefault() != null ? xContact.Descendants("email3").FirstOrDefault().Value : contact.email3;
            contact.instantMessage1 = xContact.Descendants("instantMessage1").FirstOrDefault() != null ? xContact.Descendants("instantMessage1").FirstOrDefault().Value : contact.instantMessage1;
            contact.instantMessage2 = xContact.Descendants("instantMessage2").FirstOrDefault() != null ? xContact.Descendants("instantMessage2").FirstOrDefault().Value : contact.instantMessage2;
            contact.instantMessage3 = xContact.Descendants("instantMessage3").FirstOrDefault() != null ? xContact.Descendants("instantMessage3").FirstOrDefault().Value : contact.instantMessage3;

            return contact;
        }
    }
}