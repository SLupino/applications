﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Xml.Linq;
using ERPStub.DataModel;

namespace ERPStub.Chains
{
    class Document
    {
        internal static List<Step> GetCreateDocumentChain(ISettings settings, Message message, ObjectParameter prRequestId, StagingContext context)
        {
            var steps = new List<Step>
                {
                    delegate(out string statusMessage)
                        {
                            var document = new Documents();

                            SetInitialValues(document, prRequestId, Enumerations.Operation.N);
                            XElement xCreateDocument = (XElement) message.XRequestDocument.FirstNode;
                            SetValuesFromXML(xCreateDocument, document);

                            context.Documents.Add(document);
                            statusMessage = "Document Created";
                            return true;
                        }
                };

            return steps;
        }

        private static void SetInitialValues(Documents document, ObjectParameter prRequestId, Enumerations.Operation operation)
        {
            document.requestId = (Guid)prRequestId.Value;
            document.operation = operation.ToString();
            document.C_boId = Guid.NewGuid();
            document.C_boCreated = DateTime.Now;
            document.C_boElaborationErrorMessage = "";
            document.C_boOperationState = 0;
            document.C_boreplacedBy = null;
            document.C_consolGuid = null;
            document.C_boElaborationErrorCode = 0;
            document.C_consolRequestID = null;
        }

        private static void SetValuesFromXML(XElement xCreateDocumentRequest, Documents document)
        {
            document.id = new Guid(xCreateDocumentRequest.Element("document").Attribute("id").Value);

            XAttribute customer = xCreateDocumentRequest.Descendants("customer").Attributes("id").FirstOrDefault();
            if (customer != null)
            {
                document.customerId = customer.Value;
            }

            XAttribute createdBy = xCreateDocumentRequest.Descendants("createdBy").Attributes("id").FirstOrDefault();
            document.createdBy = createdBy != null ? new Guid(createdBy.Value) : document.createdBy;

            XElement xCreationTimestamp = xCreateDocumentRequest.Element("creationTimestamp");
            document.createdOn = xCreationTimestamp != null
                                     ? DateTime.Parse(xCreationTimestamp.Value)
                                     : document.createdOn;

            XAttribute modifiedBy = xCreateDocumentRequest.Descendants("modifiedBy").Attributes("id").FirstOrDefault();
            if (modifiedBy != null)
            {
                document.modifiedBy = new Guid(modifiedBy.Value);
            }

            XElement xModificationTimestamp = xCreateDocumentRequest.Element("modificationTimestamp");
            document.modifiedOn = xModificationTimestamp != null ? DateTime.Parse(xModificationTimestamp.Value) : document.modifiedOn;

            XElement title = xCreateDocumentRequest.Descendants("title").FirstOrDefault();
            document.title = title != null ? title.Value : document.title;
            
            XElement mimeType = xCreateDocumentRequest.Descendants("mimeType").FirstOrDefault();
            document.MIMEType = mimeType != null ? mimeType.Value : document.MIMEType;
        }
    }
}
