﻿using System;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.Linq;
using ERPStub.DataModel;
using System.Xml.Linq;
using System.Collections.Generic;

namespace ERPStub.Chains
{
    internal class Prospect
    {
        internal static List<Step> GetCreateProspectChain(ISettings settings, Message message, ObjectParameter prRequestId, StagingContext context)
        {
            var steps = new List<Step>
                {
                    delegate(out string statusMessage)
                        {
                            XElement xProspect = message.XRequestDocument.Descendants("prospect").FirstOrDefault();
                            CustomerHelper.CreateProspectCustomer(xProspect, context, prRequestId);
                            statusMessage = "Prospect created";
                            return true;
                        }
                };

            return steps;
        }



    }
}