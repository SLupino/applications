﻿using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Xml.Linq;
using ERPStub.DataModel;

namespace ERPStub.Chains
{
    class ExpenseClaim
    {
        internal static List<Step> GetCreateExpenseClaimChain(ISettings settings, Message message, ObjectParameter prRequestId, StagingContext context)
        {
            var steps = new List<Step>
                {
                    delegate(out string statusMessage)
                        {
                            var xCreateExpenseClaimRequest = (XElement) message.XRequestDocument.FirstNode;
                            var xDoc = (XDocument) message.XRequestDocument;
                            ExpenseClaimHelper.CreateExpenseClaim(xCreateExpenseClaimRequest, context, prRequestId, xDoc);
                            statusMessage = "Expense Claim Created";
                            return true;
                        }
                };
            return steps;
        }
    }
}
