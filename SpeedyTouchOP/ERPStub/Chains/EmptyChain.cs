﻿using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Xml.Linq;
using ERPStub.DataModel;

namespace ERPStub.Chains
{
    internal class EmptyChain
    {
        internal static List<Step> GetEmtpyChain(string message)
        {
            string statusMessageString = string.IsNullOrEmpty(message)
                                             ? "Empty chain."
                                             : string.Format("Empty chain: {0}.", message);
            return new List<Step>
                {
                    delegate(out string statusMessage)
                        {
                            statusMessage = statusMessageString;
                            return true;
                        }
                };
        }

        internal static List<Step> GetUnhandledMessageEmtpyChain(ISettings settings, Message message, ObjectParameter prRequestId, StagingContext context)
        {
            XName messageName = ((XElement) message.XRequestDocument.FirstNode).Name;
            return GetEmtpyChain(string.Format("Unhandled request type {0}",
                                       messageName));
        }
    }
}