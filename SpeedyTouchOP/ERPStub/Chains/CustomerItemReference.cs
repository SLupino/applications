﻿using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using ERPStub.DataModel;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Collections.Generic;

namespace ERPStub.Chains
{
    internal class CustomerItemReference
    {
        internal static List<Step> GetCreateArticleCustomerInformationRequestChain(ISettings settings, Message message, ObjectParameter prRequestId, StagingContext context)
        {
            var steps = new List<Step>
                {
                    delegate(out string statusMessage)
                        {
                            CustomerItemReferences custItemRef = new CustomerItemReferences();

                            custItemRef = SetInitialValues(custItemRef, prRequestId, Enumerations.Operation.N);
                            XElement xCreateArticleCustomerInformationRequest = (XElement)message.XRequestDocument.FirstNode;
                            custItemRef = SetValuesFromXml(xCreateArticleCustomerInformationRequest, custItemRef);
                            context.CustomerItemReferences.Add(custItemRef);
                            statusMessage = "CustomerItemReferences Created";
                            return true;
                        }
                };

            return steps;
        }

        internal static List<Step> GetModifyArticleCustomerInformationRequestChain(ISettings settings, Message message, ObjectParameter prRequestId, StagingContext context)
        {
            var steps = new List<Step>
                {
                    delegate(out string statusMessage)
                        {
                            ModifyOrDeleteRow(message, Enumerations.Operation.N, prRequestId, context);

                            statusMessage = "CustomerItemReferences Modified";
                            return true;
                        }
                };

            return steps;
        }

        private static CustomerItemReferences SetInitialValues(CustomerItemReferences row, ObjectParameter prRequestId, Enumerations.Operation operation)
        {
            row.requestId = (Guid)prRequestId.Value;
            row.operation = operation.ToString();
            row.C_boId = Guid.NewGuid();
            row.C_boCreated = DateTime.Now;
            row.C_boElaborationErrorMessage = "";
            row.C_boOperationState = 0;
            row.C_boreplacedBy = null;
            row.C_consolGuid = null;
            row.C_boElaborationErrorCode = 0;
            row.C_consolRequestID = null;

            return row;
        }

        private static CustomerItemReferences SetValuesFromXml(XElement xCreateArticleCustomerInformationRequest, CustomerItemReferences custItemRef)
        {
            XAttribute customerId = xCreateArticleCustomerInformationRequest.Descendants("customerId").Attributes("id").FirstOrDefault();
            if (customerId != null)
                custItemRef.customerId = customerId.Value;

            XAttribute articleId = xCreateArticleCustomerInformationRequest.Descendants("articleId").Attributes("id").FirstOrDefault();
            if (articleId != null)
                custItemRef.itemId = articleId.Value;

            XElement customerItemNumber = xCreateArticleCustomerInformationRequest.Descendants("customerItemNumber").FirstOrDefault();
            if (customerItemNumber != null){
                custItemRef.customerItemId = customerItemNumber.Value;
            }
            else {
                custItemRef.customerItemId = string.Empty;
            }

            XElement customerItemDescription = xCreateArticleCustomerInformationRequest.Descendants("customerItemDescription").FirstOrDefault();
            if (customerItemDescription != null) {
                custItemRef.customerItemDescription = customerItemDescription.Value;
            }
            else {
                custItemRef.customerItemDescription = String.Empty;
            }
            return custItemRef;
        }

        private static void ModifyOrDeleteRow(Message message, Enumerations.Operation operation,
                                              ObjectParameter prRequestId, StagingContext context)
        {
            var elements = (XElement)message.XRequestDocument.FirstNode;
            var customerId = (String)elements.XPathSelectElement("customerId").Attribute("id");
            var articleId = (String)elements.XPathSelectElement("articleId").Attribute("id");

            CustomerItemReferences custItemRef = (from CustomerItemReferences r in context.CustomerItemReferences
                          join CustomerItemReferencesKey ckRow in context.CustomerItemReferencesKey
                              on r.C_boId equals ckRow.appliedBy
                          where ckRow.customerId == customerId && ckRow.itemId == articleId
                          select r).AsNoTracking().Take(1).FirstOrDefault();

            if (custItemRef != null)
            {
                custItemRef = SetInitialValues(custItemRef, prRequestId, operation);
                if (operation == Enumerations.Operation.N)
                {
                    custItemRef = SetValuesFromXml(elements, custItemRef);
                }
                context.CustomerItemReferences.Add(custItemRef);
            }
        }
    }
}
