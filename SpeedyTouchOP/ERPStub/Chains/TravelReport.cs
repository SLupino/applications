﻿using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Xml.Linq;
using ERPStub.DataModel;

namespace ERPStub.Chains
{
    
    internal class TravelReport
    {

        internal static List<Step> GetCreateTravelReportChain(ISettings settings, Message message, ObjectParameter prRequestId, StagingContext context)
        {
            var steps = new List<Step>
                {
                    delegate(out string statusMessage)
                        {
                            var xCreateTravelReportRequest = (XElement) message.XRequestDocument.FirstNode;
                            var xDoc = (XDocument) message.XRequestDocument;
                            TravelReportHelper.CreateTravelReport(xCreateTravelReportRequest, context, prRequestId, xDoc);
                            statusMessage = "Travel Report Created";
                            return true;
                        }
                };
            return steps;
        }
    }
}
