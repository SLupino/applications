﻿using System;
using System.Data.Entity.Core.Objects;
using System.Linq;
using ERPStub.DataModel;
using System.Xml.Linq;
using System.Collections.Generic;

namespace ERPStub.Chains
{
    internal class DailyReport
    {
        public static List<Step> GetCreateDailyReport2Chain(ISettings settings, Message message, ObjectParameter prRequestId, StagingContext context)
        {
            var steps = new List<Step>
                {
                    delegate(out string statusMessage)
                        {
                            var elements = (XElement) message.XRequestDocument.FirstNode;
                            context.VisitReports.AddRange(ModifyRow(elements, prRequestId));
                            statusMessage = "Daily report created";
                            return true;
                        }
                };

            return steps;
        }

        private static VisitReports SetInitialValues(VisitReports visitReport, ObjectParameter prRequestId, Enumerations.Operation operation)
        {
            visitReport.requestId = (Guid)prRequestId.Value;
            visitReport.operation = operation.ToString();
            visitReport.C_boId = Guid.NewGuid();
            visitReport.C_boCreated = DateTime.Now;
            visitReport.C_boElaborationErrorMessage = "";
            visitReport.C_boOperationState = 0;
            visitReport.C_boreplacedBy = null;
            visitReport.C_consolGuid = null;
            visitReport.C_boElaborationErrorCode = 0;
            visitReport.C_consolRequestID = null;

            return visitReport;
        }

        private static IEnumerable<VisitReports> ModifyRow(XElement xCreateDailyReport2, ObjectParameter prRequestId)
        {
            var rows = new List<VisitReports>();
            XAttribute createdBy = xCreateDailyReport2.Descendants("createdBy").Attributes("id").FirstOrDefault();

            foreach (var xVisitReport in xCreateDailyReport2.Descendants("visitReport"))
            {
                var visitReport = new VisitReports {id = new Guid(xVisitReport.Attribute("id").Value)};
                
                visitReport = SetInitialValues(visitReport, prRequestId, Enumerations.Operation.N);
                if(createdBy != null)
                    visitReport.createdBy = new Guid(createdBy.Value);

                var customerId = xVisitReport.Descendants("customer").Attributes("id").FirstOrDefault();
                if (customerId != null)
                    visitReport.customerId = customerId.Value;

                XElement xVisitOutcome = xVisitReport.Descendants("outcome").FirstOrDefault();
                visitReport.outcome = xVisitOutcome != null ? MapOutcome(xVisitOutcome.Value) : visitReport.outcome;

                var hour = xVisitReport.Descendants("hour").FirstOrDefault();
                if (hour != null)
                {
                    var hourSplitted = hour.Value.Split(':');
                    if (hourSplitted.Length == 2)
                        visitReport.hour = new TimeSpan(Convert.ToInt32(hourSplitted[0]), Convert.ToInt32(hourSplitted[1]), 0);
                }

                var date = xVisitReport.Descendants("date").FirstOrDefault();
                visitReport.date = date != null ? Convert.ToDateTime(date.Value) : visitReport.date;
                
                var note = xVisitReport.Descendants("note").FirstOrDefault();
                visitReport.note = note != null ? note.Value : visitReport.note;

                rows.Add(visitReport);
            }
            //TODO... insert another field (in this db schema isn't present) and create test for check

            return rows;
        }

        private static int MapOutcome(string outcome){
        //100=ORDER_PLACED, 110=ORDER_PLACED_BY_PHONE, 200=NO_DEMAND, 300=NOT_AVAILABLE, 400=NOT_PRESENT, 500=NOT_VISITED, 600=POSTPONED_BY_CUSTOMER, 700=QUOTE, 
        //800=COLLECTION,900=CONSULTATION, 1000=PRODUCT_PRESENTATION, 1100=PROJECT_PRESENTATION,1200=EXTENSION_PROJECT, 1300=COMPLAINT, 1400=SERVICE, 1500=DELIVERY,1600=PROJECT_PREPARATION'
            switch (outcome)
            {
                case "placedOrder":
                    return 100;
                case "placedOrderByPhone":
                    return 110;
                case "postponedByCustomer":
                    return 600;
                case "notVisited":
                    return 500;
                case "notAvailable":
                    return 300;
                case "notPresent":
                    return 400;
                case "noDemand":
                    return 200;
                case "itemsDelivered":
                    return 1500; 
                case "quote":
                    return 700;
                case "collection":
                    return 800;
                default:
                    throw new Exception(String.Format("No value defined for visit outcome \"{0}\"", outcome));
            }
        }
    }
}