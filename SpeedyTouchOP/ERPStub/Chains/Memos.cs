﻿using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using ERPStub.DataModel;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Collections.Generic;
using NLog;

namespace ERPStub.Chains
{
    internal class Memos
    {

        private static readonly Logger Logger = LogManager.GetLogger(typeof(ERPStub).FullName);

        internal static List<Step> GetCreateMemoChain(ISettings settings, Message message, ObjectParameter prRequestId, StagingContext context)
        {
            var steps = new List<Step>
                {
                    delegate(out string statusMessage)
                        {
                            var xCreateMemoRequest = (XElement) message.XRequestDocument.FirstNode;
                            var xDoc = (XDocument) message.XRequestDocument;
                            CreateMemo(xCreateMemoRequest, context, prRequestId, xDoc);
                            statusMessage = "Memo Created";
                            return true;
                        }
                };
            return steps;
        }

        internal static List<Step> GetCreateMemoCommentChain(ISettings settings, Message message, ObjectParameter prRequestId, StagingContext context)
        {
            var steps = new List<Step>
                {
                    delegate(out string statusMessage)
                        {
                            var xCreateMemoRequest = (XElement) message.XRequestDocument.FirstNode;
                            var xDoc = (XDocument) message.XRequestDocument;
                            CreateMemoComment(xCreateMemoRequest, context, prRequestId, xDoc);
                            statusMessage = "Memo comment Created";
                            return true;
                        }
                };
            return steps;
        }

        private static void CreateMemo(XElement xCreateMemoRequest, StagingContext context, ObjectParameter prRequestId,
            XDocument xDoc)
        {
            Memo memo = new Memo();
            SetMemoInitialValues(prRequestId, memo, Enumerations.Operation.N);
            memo.id = Guid.Parse(xCreateMemoRequest.XPathSelectElement("memo").Attribute("id").Value);//mandatory
            memo.createdBy = Guid.Parse(xCreateMemoRequest.XPathSelectElement("createdBy").Attribute("id").Value);//mandatory
            memo.createdOn = ((xCreateMemoRequest.Element("creationTimestamp") != null) && !String.IsNullOrEmpty(xCreateMemoRequest.Element("creationTimestamp").Value))
                                    ? DateTime.Parse(xCreateMemoRequest.Element("creationTimestamp").Value)
                                    : DateTime.Now;
            memo.text = ((xCreateMemoRequest.Element("text") != null) && !String.IsNullOrEmpty(xCreateMemoRequest.Element("text").Value))
                                    ? xCreateMemoRequest.Element("text").Value
                                    : "";
            memo.customerId = xCreateMemoRequest.XPathSelectElement("customer").Attribute("id").Value;//mandatory
            memo.parentId = null; //it is the comment root
            memo.level = ((xCreateMemoRequest.Element("level") != null) && !String.IsNullOrEmpty(xCreateMemoRequest.Element("level").Value))
                                   ? Convert.ToInt32(xCreateMemoRequest.Element("level").Value)
                                   : 0;
            context.Memo.Add(memo);
            Logger.Trace("Memo entry: {0}", memo);
        }

        private static void CreateMemoComment(XElement xCreateMemoRequest, StagingContext context,
            ObjectParameter prRequestId,
            XDocument xDoc)
        {
            Memo memo = new Memo();
            SetMemoInitialValues(prRequestId, memo, Enumerations.Operation.N);


            memo.id = Guid.Parse(xCreateMemoRequest.XPathSelectElement("memo").Attribute("id").Value); //mandatory
            memo.createdBy =
                Guid.Parse(xCreateMemoRequest.XPathSelectElement("createdBy").Attribute("id").Value); //mandatory
            memo.createdOn = ((xCreateMemoRequest.Element("creationTimestamp") != null) &&
                              !String.IsNullOrEmpty(xCreateMemoRequest.Element("creationTimestamp").Value))
                ? DateTime.Parse(xCreateMemoRequest.Element("creationTimestamp").Value)
                : DateTime.Now;
            memo.text = ((xCreateMemoRequest.Element("text") != null) &&
                         !String.IsNullOrEmpty(xCreateMemoRequest.Element("text").Value))
                ? xCreateMemoRequest.Element("text").Value
                : "";
            memo.customerId = xCreateMemoRequest.XPathSelectElement("customer").Attribute("id").Value; //mandatory

            var xImage = xCreateMemoRequest.Element("image");
            if (!string.IsNullOrEmpty(xImage?.Value))
            {
                memo.thumbnail = (xCreateMemoRequest.Element("image") != null)
                    ? Convert.FromBase64String(xImage.Value)
                    : null;

                if (!string.IsNullOrEmpty(xImage.Attribute("mimeType")?.Value))
                    memo.MIMEType = xImage.Attribute("mimeType")?.Value;
            }

            memo.parentId = Guid.Parse(xCreateMemoRequest.Element("parentId").Value); //mandatory
            memo.level = ((xCreateMemoRequest.Element("level") != null) &&
                          !String.IsNullOrEmpty(xCreateMemoRequest.Element("level").Value))
                ? Convert.ToInt32(xCreateMemoRequest.Element("level").Value)
                : 1;


            context.Memo.Add(memo);
            Logger.Trace("Memo comment entry: {0}", memo);
        }

        private static void SetMemoInitialValues(ObjectParameter prRequestId, Memo memo, Enumerations.Operation operation)
        {
            memo.C_consolGuid = null;
            memo.requestId = (Guid)prRequestId.Value;
            memo.operation = operation.ToString();
            memo.C_boId = Guid.NewGuid();
            memo.C_boCreated = DateTime.Now;
            memo.C_boOperationState = 0;
            memo.C_boElaborationErrorCode = 0;
            memo.C_boElaborationErrorMessage = "";
            memo.C_boreplacedBy = null;
            memo.C_consolRequestID = null;
        }
    }
}