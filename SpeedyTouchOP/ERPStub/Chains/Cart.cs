﻿using System.Data.Entity;
using System.Globalization;
using ERPStub.DataModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Xml.Linq;
using System.Data.SqlClient;

namespace ERPStub.Chains
{
    public class Cart
    {
        private static readonly Dictionary<int, int> VoucherTypeToState = new Dictionary<int, int>
        {
            // Voucher State values
            // 100=NOT_APPLICABLE, 200=CREATED, 300=QUOTE_SEND, 400=DECLINED, 500=WAITING_FOR_APPROVAL,
            // 600=APPROVED, 700=IN_WAREHOUSE, 800=THIRD_PARTY_DELIVERY, 900=DELIVERED, 1000=INVOICED, 
            // 1100=INVOICE_SEND, 1200=OPEN, 1300=PAID, 1400=IN_DISPUTE, 1500=CLEARED, 1600=ORDERED, 
            // 1700=READY_FOR_DELIVERY, 1800=QUOTE_CREATED, 1900=PARTIAL_DELIVERED

            {100 /*PRE_ORDER*/, 100 /*NOT_APPLICABLE*/},
            {200 /*ORDER*/, 1000 /*INVOICED*/},
            {300 /*DELIVERY*/, 900 /*DELIVERED*/},
            {400 /*QUOTE*/, 300 /*QUOTE_SEND*/},
            {500 /*ALL_IN_QUOTE*/, 600 /*WAITING_FOR_APPROVAL*/},
            {600 /*INVOICE*/, 600 /*WAITING_FOR_APPROVAL*/},
            {700 /*CREDIT_NOTE*/, 1000 /*INVOICED*/},
            {800 /*RFQ*/, 300 /*QUOTE_SEND*/},
            {900 /*RETURN_GOODS_CREDIT_NOTE*/, 1500 /*CLEARED*/}
        };

        public static List<Step> GetCartChain(ISettings settings, Message message, ObjectParameter prRequestId,
            StagingContext context)
        {
            var steps = new List<Step>
            {
                delegate(out string statusMessage)
                {
                    var xSendCartRequest = (XElement) message.XRequestDocument.FirstNode;

                    var xCart = xSendCartRequest.Element("cart");
                    var xCartVoucherType = xCart.Element("cartVoucherType");

                    if (xCartVoucherType.Value == "vmiList")
                    {
                        var xCustomer = xSendCartRequest.Descendants("customer").FirstOrDefault();
                        var xCartDetails = xSendCartRequest.Descendants("cartDetail");
                        foreach (var xCartDetail in xCartDetails)
                        {
                            var xVmiDetail = xCartDetail.Element("vmiDetail");

                            VMISets vmiSet = CreateVmiSet(prRequestId, Enumerations.Operation.N);
                            vmiSet.customerId = xCustomer.Attribute("id").Value;
                            var itemId = xCartDetail.Element("item");
                            vmiSet.itemId = itemId != null ? itemId.Attribute("id").Value : "";

                            if (xVmiDetail.Element("remove") != null)
                            {
                                // TODO 
                            }
                            else
                            {
                                vmiSet.maximumOnHand = Convert.ToDecimal(xVmiDetail.Element("maximumStockLevel").Value,
                                    CultureInfo.InvariantCulture);
                                vmiSet.minimumOnHand = Convert.ToDecimal(xVmiDetail.Element("minimumStockLevel").Value,
                                    CultureInfo.InvariantCulture);

                                context.VMISets.Add(vmiSet);
                            }
                        }

                        UpdateCustomerVMIState(xCustomer.Attribute("id").Value, prRequestId, context);
                    }
                    else if (xCartVoucherType.Value == "conditions")
                    {
                        using (var incomingConn = new SqlConnection(ConfigurationManager.ConnectionStrings["Incoming"].ConnectionString))
                        {
                            incomingConn.Open();
                            var xCustomer = xSendCartRequest.Descendants("customer").FirstOrDefault();
                            List<XElement> cartDetails = xSendCartRequest.Descendants("cartDetail").ToList();

                            var customerId = xCustomer?.Attribute("id")?.Value;
                            for (int idx = 0; idx < cartDetails.Count(); idx++)
                            {
                                var itemId = cartDetails[idx].Element("item")
                                    ?.Attribute("id")
                                    ?.Value;
                                var isGroup = cartDetails[idx].Element("standardDetail")?.Element("grossPrice")?.Value == "";
                                if (isGroup)
                                {
                                    itemId = itemId?.Split(' ')[0];
                                }
                                var discount = decimal.Parse(cartDetails[idx]?.Element("standardDetail")?.Element("discount")?.Value ?? "0.0");
                                var surcharge = decimal.Parse(cartDetails[idx]?.Element("standardDetail")?.Element("surcharge")?.Value ?? "0.0");
                                UpdateConditions(incomingConn, customerId, itemId, isGroup, discount, surcharge);
                            }
                        }
                    }
                    else
                    {
                        Vouchers voucher = CreateVoucherDefaultValues(prRequestId, Enumerations.Operation.N);
                        SetVoucherValuesFromXML(xSendCartRequest, voucher, context, prRequestId,
                            xCartVoucherType.Value);
                        context.Vouchers.Add(voucher);

                        CreateShippingAddress(xSendCartRequest, context, prRequestId);

                        List<XElement> cartDetails = xSendCartRequest.Descendants("cartDetail").ToList();
                        voucher.numberOfDetails = cartDetails.Count();

                        XElement xCartDetail = cartDetails.FirstOrDefault();
                        if (xCartDetail != null)
                        {
                            var xSourceVoucherDetail = xCartDetail.Element("sourceVoucherDetail");
                            voucher.sourceVoucherNumber = xSourceVoucherDetail != null
                                ? xSourceVoucherDetail.Attribute("id")?.Value.Split('|')[1] ?? xSourceVoucherDetail.Attribute("voucherNumber")?.Value
                                : null;
                        }
                        
                        decimal valueOfGoods = 0;
                        decimal GST = 0;

                        for (int idx = 0; idx < cartDetails.Count(); idx++)
                        {
                            VoucherDetails voucherDetail = CreateVoucherDetail(prRequestId,
                                Enumerations.Operation.N);

                            SetVouchersDetailFromXML(cartDetails[idx], voucherDetail);

                            voucherDetail.customerId = voucher.customerId;
                            voucherDetail.voucherDate = voucher.voucherDate;
                            voucherDetail.voucherType = voucher.voucherType;
                            voucherDetail.voucherNumber = voucher.voucherNumber;
                            voucherDetail.number = (idx + 1).ToString();

                            context.VoucherDetails.Add(voucherDetail);

                            valueOfGoods = valueOfGoods + voucherDetail.value;
                            GST = voucherDetail.lineTotal.HasValue
                                ? GST + voucherDetail.lineTotal.Value - voucherDetail.value
                                : GST;
                        }

                        voucher.valueOfGoods = valueOfGoods;
                        voucher.GST = GST;
                        voucher.totalAmount = voucher.valueOfGoods + voucher.GST.Value +
                                              (voucher.additionalCosts.HasValue
                                                  ? voucher.additionalCosts.Value
                                                  : 0);

                        if (xCartVoucherType.Value == "order")
                        {
                            Vouchers invoiceVoucher = CreateVoucherDefaultValues(prRequestId,
                                Enumerations.Operation.N);
                            SetVoucherValuesFromXML(xSendCartRequest, invoiceVoucher, context, prRequestId,
                                "Invoice");
                            invoiceVoucher.sourceVoucherNumber = voucher.sourceVoucherNumber;
                            context.Vouchers.Add(invoiceVoucher);
                            invoiceVoucher.numberOfDetails = cartDetails.Count();

                            invoiceVoucher.valueOfGoods = voucher.valueOfGoods;
                            invoiceVoucher.GST = voucher.GST;
                            invoiceVoucher.totalAmount = voucher.totalAmount;

                            for (int idx = 0; idx < cartDetails.Count(); idx++)
                            {
                                VoucherDetails invoiceDetailVoucher = CreateVoucherDetail(prRequestId,
                                    Enumerations
                                        .Operation.N);

                                SetVouchersDetailFromXML(cartDetails[idx], invoiceDetailVoucher);

                                invoiceDetailVoucher.customerId = invoiceVoucher.customerId;
                                invoiceDetailVoucher.voucherDate = invoiceVoucher.voucherDate;
                                invoiceDetailVoucher.voucherType = invoiceVoucher.voucherType;
                                invoiceDetailVoucher.voucherNumber = invoiceVoucher.voucherNumber;
                                invoiceDetailVoucher.number = (idx + 1).ToString();

                                context.VoucherDetails.Add(invoiceDetailVoucher);
                            }

                            Vouchers deliveryVoucher = CreateVoucherDefaultValues(prRequestId,
                                Enumerations.Operation.N);
                            SetVoucherValuesFromXML(xSendCartRequest, deliveryVoucher, context, prRequestId,
                                "Delivery");
                            deliveryVoucher.sourceVoucherNumber = voucher.sourceVoucherNumber;
                            context.Vouchers.Add(deliveryVoucher);
                            deliveryVoucher.numberOfDetails = cartDetails.Count();

                            deliveryVoucher.valueOfGoods = voucher.valueOfGoods;
                            deliveryVoucher.GST = voucher.GST;
                            deliveryVoucher.totalAmount = voucher.totalAmount;

                            for (int idx = 0; idx < cartDetails.Count(); idx++)
                            {
                                VoucherDetails deliveryDetailVoucher = CreateVoucherDetail(prRequestId,
                                    Enumerations
                                        .Operation.N);

                                SetVouchersDetailFromXML(cartDetails[idx], deliveryDetailVoucher);

                                deliveryDetailVoucher.customerId = deliveryVoucher.customerId;
                                deliveryDetailVoucher.voucherDate = deliveryVoucher.voucherDate;
                                deliveryDetailVoucher.voucherType = deliveryVoucher.voucherType;
                                deliveryDetailVoucher.voucherNumber = deliveryVoucher.voucherNumber;
                                deliveryDetailVoucher.number = (idx + 1).ToString();

                                context.VoucherDetails.Add(deliveryDetailVoucher);
                            }
                        }
                        else if (xCartVoucherType.Value == "requestForQuote")
                        {
                            Vouchers quoteVoucher = CreateVoucherDefaultValues(prRequestId,
                                Enumerations.Operation.N);
                            SetVoucherValuesFromXML(xSendCartRequest, quoteVoucher, context, prRequestId,
                                "quoteApproval");
                            quoteVoucher.sourceVoucherNumber = voucher.sourceVoucherNumber;
                            context.Vouchers.Add(quoteVoucher);
                            quoteVoucher.numberOfDetails = cartDetails.Count();

                            quoteVoucher.valueOfGoods = voucher.valueOfGoods;
                            quoteVoucher.GST = voucher.GST;
                            quoteVoucher.totalAmount = voucher.totalAmount;

                            for (int idx = 0; idx < cartDetails.Count(); idx++)
                            {
                                VoucherDetails quoteDetailVoucher = CreateVoucherDetail(prRequestId,
                                    Enumerations
                                        .Operation.N);

                                SetVouchersDetailFromXML(cartDetails[idx], quoteDetailVoucher);

                                quoteDetailVoucher.customerId = quoteVoucher.customerId;
                                quoteDetailVoucher.voucherDate = quoteVoucher.voucherDate;
                                quoteDetailVoucher.voucherType = quoteVoucher.voucherType;
                                quoteDetailVoucher.voucherNumber = quoteVoucher.voucherNumber;
                                quoteDetailVoucher.number = (idx + 1).ToString();

                                context.VoucherDetails.Add(quoteDetailVoucher);
                            }
                        }
                    }

                    statusMessage = "Order created";
                    return true;
                }
            };

            return steps;
        }

        private static void UpdateCustomerVMIState(string customerId, ObjectParameter prRequestId,
            StagingContext context)
        {
            // customer.VMIstate: 100=NOT_APPLICABLE,200=PROSPECT,300=CUSTOMER
            // customer.state: 100=NEW,200=COLD,300=PROSPECT,400=DELETED,500=STANDARD,600=ZERO,700=REACTIVATED
            Customers customer = (from Customers r in context.Customers
                                  join CustomersKey ckRow in context.CustomersKey on r.C_boId equals ckRow.appliedBy
                                  where r.id == customerId
                                  select r).AsNoTracking().Take(1).FirstOrDefault();

            // reset values
            customer.requestId = (Guid)prRequestId.Value;
            customer.operation = Enumerations.Operation.N.ToString();
            customer.C_boId = Guid.NewGuid();
            customer.C_boCreated = DateTime.Now;
            customer.C_boElaborationErrorMessage = "";
            customer.C_boOperationState = 0;
            customer.C_boreplacedBy = null;
            customer.C_consolGuid = null;
            customer.C_boElaborationErrorCode = 0;
            customer.C_consolRequestID = null;

            customer.VMIstate = customer.state == 300 /*PROSPECT*/ ? 200 /*PROSPECT*/ : 300 /*CUSTOMER*/;
            context.Customers.Add(customer);
        }

        private static void UpdateConditions(SqlConnection connection, string customerId, string itemId, bool isGroup, decimal discount,
            decimal surcharge)
        {
            var updateSql =
                @"IF NOT EXISTS (SELECT * FROM [dbo].[Conditions] WHERE [customerId] = @customerId AND [itemId] = @itemId AND [isGroup] = @isGroup)
                                INSERT INTO [dbo].[Conditions]
                                    VALUES(@customerId, @itemId, @isGroup, @discount, @surcharge)
                              ELSE
                                UPDATE [dbo].[Conditions]
                                    SET [discount] = @discount, [surcharge] = @surcharge
                                    WHERE [customerId] = @customerId AND [itemId] = @itemId AND [isGroup] = @isGroup";

            using (SqlCommand sqlCommand = new SqlCommand(updateSql, connection))
            {
                sqlCommand.Parameters.AddWithValue("customerId", customerId);
                sqlCommand.Parameters.AddWithValue("itemId", itemId);
                sqlCommand.Parameters.AddWithValue("isGroup", isGroup);
                sqlCommand.Parameters.AddWithValue("discount", discount);
                sqlCommand.Parameters.AddWithValue("surcharge", surcharge);
                sqlCommand.ExecuteNonQuery();
            }
        }

        #region Set Default Value

        private static Vouchers CreateVoucherDefaultValues(ObjectParameter prRequestId,
            Enumerations.Operation operation)
        {
            return new Vouchers
            {
                requestId = (Guid)prRequestId.Value,
                operation = operation.ToString(),
                C_boId = Guid.NewGuid(),
                C_boCreated = DateTime.Now,
                C_boElaborationErrorMessage = "",
                C_boOperationState = 0,
                C_boreplacedBy = null,
                C_consolGuid = null,
                C_boElaborationErrorCode = 0,
                C_consolRequestID = null
            };
        }

        private static VoucherDetails CreateVoucherDetail(ObjectParameter prRequestId, Enumerations.Operation operation)
        {
            var row = new VoucherDetails
            {
                requestId = (Guid)prRequestId.Value,
                operation = operation.ToString(),
                C_boId = Guid.NewGuid(),
                C_boCreated = DateTime.Now,
                C_boElaborationErrorMessage = "",
                C_boOperationState = 0,
                C_boreplacedBy = null,
                C_consolGuid = null,
                C_boElaborationErrorCode = 0,
                C_consolRequestID = null
            };

            return row;
        }

        private static VMISets CreateVmiSet(ObjectParameter prRequestId, Enumerations.Operation operation)
        {
            VMISets row = new VMISets
            {
                requestId = (Guid)prRequestId.Value,
                operation = operation.ToString(),
                C_boId = Guid.NewGuid(),
                C_boCreated = DateTime.Now,
                C_boElaborationErrorMessage = "",
                C_boOperationState = 0,
                C_boreplacedBy = null,
                C_consolGuid = null,
                C_boElaborationErrorCode = 0,
                C_consolRequestID = null
            };

            return row;
        }

        #endregion

        #region Set Specific Value

        #region Voucher

        private static void SetVoucherValuesFromXML(XElement xSendCartRequest, Vouchers voucher,
            StagingContext stagingContext, ObjectParameter prRequestId, string cartVoucherType)
        {
            XElement xCart = xSendCartRequest.Element("cart");

            SetCustomerId(xSendCartRequest, voucher, stagingContext, prRequestId);

            SetNote(xSendCartRequest, voucher);
            SetCustomerNote(xSendCartRequest, voucher);
            SetWarehouseNote(xSendCartRequest, voucher);

            voucher.voucherNumber = (Convert.ToInt64(stagingContext.Vouchers.Max(x => x.voucherNumber)) + 1).ToString();
            voucher.voucherType = SetVoucherType(xSendCartRequest, cartVoucherType);
            voucher.voucherDate = DateTime.Now;
            voucher.state = VoucherTypeToState[voucher.voucherType];
            voucher.freightCharge = 10;
            voucher.packageingCharge = 5;
            voucher.minimalValueSurcharge = 2;

            voucher.currencyIsoCode = xCart.Descendants("currency").FirstOrDefault() != null
                ? xCart.Descendants("currency").FirstOrDefault().Value
                : "";

            if (xCart.Descendants("trackingNumber").FirstOrDefault() != null)
            {
                voucher.trackingNumber = xCart.Descendants("trackingNumber").FirstOrDefault().Value;
            }
            //TODO: it is not clear if the value of storeGoodsInCarStock should be stored somewhere on staging?

            SetShippingAddress(xSendCartRequest, voucher, prRequestId, stagingContext);

            var shippingDate = xSendCartRequest.Descendants("shippingDate").FirstOrDefault();
            voucher.deliveryDate = shippingDate != null ? Convert.ToDateTime(shippingDate.Value) : voucher.deliveryDate;

            SetQuote(xCart, voucher);

            voucher.isSplitted = 0;
            voucher.hasAuspreiser = 0;
            voucher.hasOrderConfirmation = 0;
            voucher.orderEntrySource = 100; /*SALES_REP*/
            voucher.additionalCosts = 0;
        }

        private static int SetVoucherType(XElement elements, string cartVoucherType)
        {
            // 100=PRE_ORDER, 200=ORDER, 300=DELIVERY, 400=QUOTE,
            // 500=ALL_IN_QUOTE, 600=INVOICE, 700=CREDIT_NOTE, 800=RFQ, 900=RETURN_GOODS_CREDIT_NOTE
            switch (cartVoucherType)
            {
                case "preOrder":
                    return 100;
                case "order":
                    return 200;
                case "requestForQuote":
                    return 800;
                case "creditNote":
                    return 700;
                case "quoteApproval":
                    var quote = elements.Descendants("packagedQuote").FirstOrDefault();
                    if (quote != null && quote.Value == "true")
                        return 500;
                    return 400;
                case "Invoice":
                    return 600;
                case "Delivery":
                    return 300;
                case "returnOfGoodsCreditNote":
                    return 900;
            }
            return 100;
        }

        private static void SetWarehouseNote(XElement elements, Vouchers row)
        {
            var wareHouseNote = elements.Descendants("warehouseNote").FirstOrDefault();
            if (wareHouseNote != null)
            {
                var lines = wareHouseNote.Elements().ToList();
                for (int i = 0; i < lines.Count(); i++)
                {
                    switch (i)
                    {
                        case 0:
                            row.warehouseNote1 = lines[i].Value;
                            break;
                        case 1:
                            row.warehouseNote2 = lines[i].Value;
                            break;
                        case 2:
                            row.warehouseNote3 = lines[i].Value;
                            break;
                        case 3:
                            row.warehouseNote4 = lines[i].Value;
                            break;
                        case 4:
                            row.warehouseNote5 = lines[i].Value;
                            break;
                    }
                }
            }
        }

        private static void SetCustomerNote(XElement elements, Vouchers row)
        {
            var customerNote = elements.Descendants("customerNote").FirstOrDefault();
            if (customerNote != null)
            {
                var lines = customerNote.Elements().ToList();
                for (int i = 0; i < lines.Count(); i++)
                {
                    switch (i)
                    {
                        case 0:
                            row.customerNote1 = lines[i].Value;
                            break;
                        case 1:
                            row.customerNote2 = lines[i].Value;
                            break;
                        case 2:
                            row.customerNote3 = lines[i].Value;
                            break;
                        case 3:
                            row.customerNote4 = lines[i].Value;
                            break;
                        case 4:
                            row.customerNote5 = lines[i].Value;
                            break;
                    }
                }
            }
        }

        private static void SetNote(XElement elements, Vouchers row)
        {
            var note = elements.Descendants("note").FirstOrDefault();
            if (note != null)
            {
                var lines = note.Elements().ToList();
                for (int i = 0; i < lines.Count(); i++)
                {
                    switch (i)
                    {
                        case 0:
                            row.note1 = lines[i].Value;
                            break;
                        case 1:
                            row.note2 = lines[i].Value;
                            break;
                        case 2:
                            row.note3 = lines[i].Value;
                            break;
                        case 3:
                            row.note4 = lines[i].Value;
                            break;
                        case 4:
                            row.note5 = lines[i].Value;
                            break;
                    }
                }
            }
        }

        private static void SetShippingAddress(XElement xSendCartRequest, Vouchers voucher, ObjectParameter prRequestId,
            StagingContext context)
        {
            var xShippingAddress = xSendCartRequest.Descendants("shippingAddress").FirstOrDefault();
            if (xShippingAddress != null)
            {
                XAttribute xDeliveryAddressId = xShippingAddress.Attribute("id");
                if (xDeliveryAddressId != null)
                {
                    var deliveryAddress = (from Customers da in context.Customers
                                           join CustomersKey daKey in context.CustomersKey on da.C_boId
                                               equals daKey.appliedBy
                                           where da.id == xDeliveryAddressId.Value
                                           select da).AsNoTracking().Take(1).FirstOrDefault();
                    voucher.deliveryAddressName1 = deliveryAddress.name1;
                    voucher.deliveryAddressName2 = deliveryAddress.name2;
                    voucher.deliveryAddressStreet = deliveryAddress.street;
                    voucher.deliveryAddressZipCode = deliveryAddress.zipCode;
                    voucher.deliveryAddressCity = deliveryAddress.city;
                    voucher.region = deliveryAddress.region;
                    voucher.deliveryAddressIsoCountryCode = deliveryAddress.isoCountryCode;
                }
                else
                {
                    var deliveryAddressName1 = xShippingAddress.Descendants("name1").FirstOrDefault();
                    voucher.deliveryAddressName1 = deliveryAddressName1 != null
                        ? deliveryAddressName1.Value
                        : voucher.deliveryAddressName1;

                    var deliveryAddressName2 = xShippingAddress.Descendants("name2").FirstOrDefault();
                    voucher.deliveryAddressName2 = deliveryAddressName2 != null
                        ? deliveryAddressName2.Value
                        : voucher.deliveryAddressName2;

                    var street = xShippingAddress.Descendants("street").FirstOrDefault();
                    voucher.deliveryAddressStreet = street != null ? street.Value : voucher.deliveryAddressStreet;

                    var zip = xShippingAddress.Descendants("zip").FirstOrDefault();
                    voucher.deliveryAddressZipCode = zip != null ? zip.Value : voucher.deliveryAddressZipCode;

                    var city = xShippingAddress.Descendants("city").FirstOrDefault();
                    voucher.deliveryAddressCity = city != null ? city.Value : voucher.deliveryAddressCity;

                    var region = xShippingAddress.Descendants("region").FirstOrDefault();
                    voucher.region = region != null ? region.Value : voucher.region;

                    var country = xShippingAddress.Descendants("country").FirstOrDefault();
                    voucher.deliveryAddressIsoCountryCode =
                        country != null ? country.Value : voucher.deliveryAddressIsoCountryCode;
                }
            }
        }

        private static void CreateShippingAddress(XElement xSendCartRequest, StagingContext context,
            ObjectParameter prRequestId)
        {
            Customers deliveryAddress = null;
            PartnerRelations partnerRelation = null;
            CustomerSalesAreaRelation custSalesArea = null;
            Customers customer = null;
            CustomerSalesAreaRelation custSalesAreaCust = null;

            XAttribute customerId = xSendCartRequest.Descendants("customer").Attributes("id").FirstOrDefault();
            XElement xShippingAddress = xSendCartRequest.Descendants("shippingAddress").FirstOrDefault();
            if (xShippingAddress == null) return;

            XElement xSetting = xShippingAddress.Element("setting");

            XAttribute xDeliveryAddressId = xShippingAddress.Attribute("id");
            custSalesArea = new CustomerSalesAreaRelation();
            AddressHelper.SetStagingControlValuesCustomerSalesAreaRelations(custSalesArea, prRequestId,
                Enumerations.Operation.N);

            if (xDeliveryAddressId != null)
            {
                deliveryAddress = (from Customers da in context.Customers
                                   join CustomersKey daKey in context.CustomersKey on da.C_boId
                                       equals daKey.appliedBy
                                   where da.id == xDeliveryAddressId.Value
                                   select da).AsNoTracking().Take(1).FirstOrDefault();
                AddressHelper.SetStagingControlValuesCustomers(deliveryAddress, prRequestId,
                    Enumerations.Operation.N);
                if (deliveryAddress == null) return;
            }
            else if (xSetting != null && (xSetting.Value == "masterData" || xSetting.Value == "preferred"))
            {
                deliveryAddress = new Customers();

                partnerRelation = new PartnerRelations();
                AddressHelper.SetStagingControlValuesCustomers(deliveryAddress, prRequestId,
                    Enumerations.Operation.N);
                AddressHelper.SetStagingControlValuesPartnerRelations(partnerRelation, prRequestId,
                    Enumerations.Operation.N);

                deliveryAddress.id = customerId.Value + "-" + new Random().Next(1, 99);
                deliveryAddress.type = 100;//100 normal, 200 branch office

                partnerRelation.sourceCustomerId = deliveryAddress.id;
                partnerRelation.targetCustomerId = customerId.Value;
                partnerRelation.role = 200;

                var deliveryAddressName1 = xShippingAddress.Descendants("name1").FirstOrDefault();
                deliveryAddress.name1 =
                    deliveryAddressName1 != null ? deliveryAddressName1.Value : deliveryAddress.name1;

                var deliveryAddressName2 = xShippingAddress.Descendants("name2").FirstOrDefault();
                deliveryAddress.name2 =
                    deliveryAddressName2 != null ? deliveryAddressName2.Value : deliveryAddress.name2;

                var street = xShippingAddress.Descendants("street").FirstOrDefault();
                deliveryAddress.street = street != null ? street.Value : deliveryAddress.street;

                var zip = xShippingAddress.Descendants("zip").FirstOrDefault();
                deliveryAddress.zipCode = zip != null ? zip.Value : deliveryAddress.zipCode;

                var city = xShippingAddress.Descendants("city").FirstOrDefault();
                deliveryAddress.city = city != null ? city.Value : deliveryAddress.city;

                var region = xShippingAddress.Descendants("region").FirstOrDefault();
                deliveryAddress.region = region != null ? region.Value : "";

                var country = xShippingAddress.Descendants("country").FirstOrDefault();
                deliveryAddress.isoCountryCode = country != null ? country.Value : "";

                var xLongitude = xShippingAddress.Element("longitude");
                if (xLongitude != null && decimal.TryParse(xLongitude.Value, out var longitudeValue))
                    deliveryAddress.longitude = longitudeValue;

                var xLatitude = xShippingAddress.Element("latitude");
                if (xLatitude != null && decimal.TryParse(xLatitude.Value, out var latitudeValue))
                    deliveryAddress.latitude = latitudeValue;
            }

            if (deliveryAddress == null) return;

            // not here because if coming from then in if we just fetched it from Customers so there is no need to add it. 
            // lets discuss this. after fetching it some values have been modified so it must be added. Why modify some values?
            context.Customers.Add(deliveryAddress);


            //get the Customer to whom the new delivery address will be associated
            customer = (from Customers c in context.Customers
                        join CustomersKey cKey in context.CustomersKey on c.C_boId
                            equals cKey.appliedBy
                        where c.id == customerId.Value
                        select c).AsNoTracking().Take(1).FirstOrDefault();

            //get the CustomerSalesAreaRelation for the customer to whom the new delivery address will be associated
            custSalesAreaCust = (from CustomerSalesAreaRelation c in context.CustomerSalesAreaRelation
                                 join CustomerSalesAreaRelationKey cKey in context.CustomerSalesAreaRelationKey on c.C_boId
                                     equals cKey.appliedBy
                                 where c.customerId == customerId.Value
                                 select c).AsNoTracking().Take(1).FirstOrDefault();

            deliveryAddress.postOfficeBox = customer.postOfficeBox;
            deliveryAddress.postOfficeBoxZipCode = customer.postOfficeBoxZipCode;
            deliveryAddress.invoiceParty = customer.invoiceParty;
            deliveryAddress.association = customer.association;
            deliveryAddress.industryId = customer.industryId;
            deliveryAddress.pricingGroup = customer.pricingGroup;
            deliveryAddress.paymentTerms = customer.paymentTerms;
            deliveryAddress.collectiveInvoicing = customer.collectiveInvoicing;
            deliveryAddress.visitsRecurrenceRule = customer.visitsRecurrenceRule;
            deliveryAddress.visitsAlarmRelativeOffset = customer.visitsAlarmRelativeOffset;
            deliveryAddress.SMLClassificationSalesPotential = customer.SMLClassificationSalesPotential;
            deliveryAddress.state = customer.state;
            deliveryAddress.VMIstate = customer.VMIstate;
            deliveryAddress.orderReceipt = customer.orderReceipt;
            deliveryAddress.orderConfirmation = customer.orderConfirmation;
            deliveryAddress.invoice = customer.invoice;
            deliveryAddress.grossNetPrice = customer.grossNetPrice;


            //set CustomerSalesAreaRelation values for the newly created delivery address
            custSalesArea.customerId = deliveryAddress.id;
            custSalesArea.salesAreaId = custSalesAreaCust?.salesAreaId;

            if (partnerRelation != null)
            {
                if (xSetting.Value == "preferred")
                    partnerRelation.isPreferred = 1;

                context.PartnerRelations.Add(partnerRelation);
            }
            context.CustomerSalesAreaRelation.Add(custSalesArea);
            //TODO: it is theoretically possible that a delivery address is being created for a new customer - take care of this case 

       
                if (xSetting != null && xSetting.Value == "preferred")
                {
                    //set all the other existing delivery addresses as non preferred in PartnerRelations
                    (from PartnerRelations da in context.PartnerRelations
                        join PartnerRelationsKey daKey in context.PartnerRelationsKey on da.C_boId equals daKey.appliedBy
                        where da.targetCustomerId == customerId.Value && da.sourceCustomerId != deliveryAddress.id &&
                              da.role == 200
                        select da).AsNoTracking().ToList().ForEach(x =>
                    {
                        x.isPreferred = 0;
                        AddressHelper.SetStagingControlValuesPartnerRelations(x, prRequestId, Enumerations.Operation.N);
                        context.PartnerRelations.Add(x);
                    });
                }


        }

        private static void SetQuote(XElement xCart, Vouchers voucher)
        {
            var xQuote = xCart.Element("quote");
            if (xQuote == null)
            {
                voucher.quoteFollowUpBy = 100 /*NOT_APPLICABLE*/;
                voucher.quoteDeliveryBy = 100 /*NOT_APPLICABLE*/;
            }
            else
            {
                var quoteValidUntil = xQuote.Descendants("validUntil").FirstOrDefault();
                voucher.quoteValidUntil = quoteValidUntil != null
                    ? Convert.ToDateTime(quoteValidUntil.Value)
                    : voucher.quoteValidUntil;

                var quoteFollowUpBy = xQuote.Element("followUpBy");
                voucher.quoteFollowUpBy = quoteFollowUpBy != null
                    ? MappingHelper.QuoteFollowUpBy(quoteFollowUpBy.Value)
                    : voucher.quoteFollowUpBy;

                var quoteDeliveryBy = xQuote.Descendants("deliveryBy").FirstOrDefault();
                voucher.quoteDeliveryBy = quoteDeliveryBy != null
                    ? MapDeliveryBy(quoteDeliveryBy.Value)
                    : voucher.quoteDeliveryBy;

                var quoteFaxNumber = xQuote.Descendants("fax").FirstOrDefault();
                voucher.quoteFaxNumber = quoteFaxNumber != null ? quoteFaxNumber.Value : voucher.quoteFaxNumber;

                var quoteEmail = xQuote.Descendants("email").FirstOrDefault();
                voucher.quoteEmail = quoteEmail != null ? quoteEmail.Value : voucher.quoteEmail;

                var quoteForAttentionOf = xQuote.Descendants("forAttentionOf").FirstOrDefault();
                voucher.quoteForAttentionOf = quoteForAttentionOf != null
                    ? quoteForAttentionOf.Value
                    : voucher.quoteForAttentionOf;

                var quoteForAttentionOfContactId = xQuote.Descendants("contact").Attributes("id").FirstOrDefault();
                voucher.quoteForAttentionOfContactId = quoteForAttentionOfContactId != null
                    ? new Guid(quoteForAttentionOfContactId.Value)
                    : voucher.quoteForAttentionOfContactId;
            }
        }

        private static int MapDeliveryBy(string value)
        {
            //100=NOT_APPLICABLE,200=SALES_REP,300=FAX,400=EMAIL
            switch (value)
            {
                default:
                    return 100;
                case "salesPerson":
                    return 200;
                case "fax":
                    return 300;
                case "email":
                    return 400;
            }
        }

        private static void SetCustomerId(XElement elements, Vouchers row, StagingContext obj,
            ObjectParameter prRequestId)
        {
            var customer = elements.Descendants("customer").Attributes("id").FirstOrDefault();
            if (customer != null)
            {
                row.customerId = customer.Value;
            }
            else
            {
                var newCustomer = elements.Descendants("newCustomer").FirstOrDefault();
                var currency = elements.Descendants("currency").FirstOrDefault() != null
                    ? elements.Descendants("currency").FirstOrDefault().Value
                    : "";
                if (newCustomer != null)
                {
                    row.customerId =
                        CustomerHelper.CreateNewCustomerWithCurrency(new Program(), newCustomer, obj, prRequestId,
                            currency);
                }
            }
        }

        #endregion

        #region VoucherDetail

        private static void SetVouchersDetailFromXML(XElement xVoucherDetail, VoucherDetails row)
        {
            SetCustomerNoteDetails(xVoucherDetail, row);
            SetNoteDetails(xVoucherDetail, row);
            var xitem = xVoucherDetail.Element("item");
            if (xitem != null)
            {
                row.itemNumber = xitem.Attribute("id").Value;
            }
            SetStandardDetail(xVoucherDetail.Descendants("standardDetail").FirstOrDefault(), row);
        }

        private static void SetCustomerNoteDetails(XContainer elements, VoucherDetails row)
        {
            var customerNote = elements.Descendants("customerNote").FirstOrDefault();
            if (customerNote != null)
            {
                var lines = customerNote.Elements().ToList();
                for (int i = 0; i < lines.Count(); i++)
                {
                    switch (i)
                    {
                        case 0:
                            row.customerNote1 = lines[i].Value;
                            break;
                        case 1:
                            row.customerNote2 = lines[i].Value;
                            break;
                        case 2:
                            row.customerNote3 = lines[i].Value;
                            break;
                        case 3:
                            row.customerNote4 = lines[i].Value;
                            break;
                        case 4:
                            row.customerNote5 = lines[i].Value;
                            break;
                    }
                }
            }
        }

        private static void SetNoteDetails(XElement elements, VoucherDetails row)
        {
            var note = elements.Descendants("note").FirstOrDefault();
            if (note != null)
            {
                var lines = note.Elements().ToList();
                for (int i = 0; i < lines.Count(); i++)
                {
                    switch (i)
                    {
                        case 0:
                            row.note1 = lines[i].Value;
                            break;
                        case 1:
                            row.note2 = lines[i].Value;
                            break;
                        case 2:
                            row.note3 = lines[i].Value;
                            break;
                        case 3:
                            row.note4 = lines[i].Value;
                            break;
                        case 4:
                            row.note5 = lines[i].Value;
                            break;
                    }
                }
            }
        }

        private static void SetStandardDetail(XElement xStandardDetail, VoucherDetails voucherDetail)
        {
            if (xStandardDetail != null)
            {
                var isSpecialItem = xStandardDetail.Descendants("isSpecialItem").FirstOrDefault();
                voucherDetail.isSpecialItem = isSpecialItem != null
                    ? (isSpecialItem.Value == "true" ? Convert.ToInt16(1) : Convert.ToInt16(0))
                    : voucherDetail.isSpecialItem;

                var specialItem = xStandardDetail.Descendants("specialItem").FirstOrDefault();
                if (specialItem != null)
                {
                    var desc = specialItem.Descendants("description").FirstOrDefault();
                    if (desc != null)
                    {
                        voucherDetail.specialItemDescription = desc.Value;
                    }
                }

                var xQuantity = xStandardDetail.Element("quantity");
                decimal quantity = Convert.ToDecimal(xQuantity.Value, CultureInfo.InvariantCulture);
                voucherDetail.quanitityOrdered = quantity;
                voucherDetail.quantityDelivered = quantity;

                var xGrossPrice = xStandardDetail.Element("grossPrice");
                decimal grossPrice = Decimal.Round(Convert.ToDecimal(xGrossPrice.Value, CultureInfo.InvariantCulture),
                    2, MidpointRounding.AwayFromZero);
                voucherDetail.grossPrice = grossPrice;

                XElement xPriceUnit = xStandardDetail.Element("pricingUnit");
                int priceUnit = Convert.ToInt32(MappingHelper.PricingUnitToNumber(xPriceUnit.Value));
                int voucherPriceUnit = MappingHelper.PriceUnitToVoucherPriceUnit(priceUnit);
                voucherDetail.priceUnit = voucherPriceUnit;

                var xDiscount = xStandardDetail.Element("discount");
                decimal discount = Decimal.Round(Convert.ToDecimal(xDiscount.Value, CultureInfo.InvariantCulture), 2,
                    MidpointRounding.AwayFromZero);
                voucherDetail.discountDec = discount;
                //voucherDetail.discount = Decimal.Floor(voucherDetail.discountDec.Value);

                var xSurcharge = xStandardDetail.Element("surcharge");
                decimal surcharge = Decimal.Round(Convert.ToDecimal(xSurcharge.Value, CultureInfo.InvariantCulture), 2,
                    MidpointRounding.AwayFromZero);
                voucherDetail.surchargeDec = surcharge;
                //voucherDetail.surcharge = Decimal.Floor(voucherDetail.surchargeDec.Value);

                voucherDetail.value = CalculateLineValue(grossPrice, priceUnit, quantity, discount, surcharge);
                decimal lineTotal = CalculateLineTotal(voucherDetail.value, 22 /*fixed italian GST*/);
                voucherDetail.lineTotal = lineTotal;
                voucherDetail.GST = lineTotal - voucherDetail.value;

                var priceKind = xStandardDetail.Element("priceKind");
                voucherDetail.priceKind = priceKind != null ? priceKind.Value : voucherDetail.priceKind;

                var noCharge = xStandardDetail.Element("noCharge");
                voucherDetail.noCharge = noCharge != null
                    ? (noCharge.Value.ToLower() == "true" ? (short)1 : (short)0)
                    : voucherDetail.noCharge;
            }
        }

        private static decimal CalculateLineValue(decimal grossPrice, int priceUnit, decimal quantity, decimal discount,
            decimal surcharge)
        {
            return Decimal.Round((grossPrice * (100 - discount) * quantity / priceUnit / 100), 2,
                MidpointRounding.AwayFromZero);
        }

        private static decimal CalculateLineTotal(decimal lineValue, decimal gst)
        {
            return Decimal.Round(lineValue * (100 + gst) / 100, 2, MidpointRounding.AwayFromZero);
        }

        #endregion

        #endregion
    }
}