//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ERPStub.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class EnumValueCustomerRelationsView
    {
        public string name { get; set; }
        public string customerId { get; set; }
        public int value { get; set; }
        public Nullable<System.Guid> C_consolFK_EnumValueCustomerRelations_Customers { get; set; }
        public Nullable<System.Guid> C_consolFK_EnumValueCustomerRelations_Enums { get; set; }
        public Nullable<System.Guid> C_consolGuid { get; set; }
        public Nullable<System.Guid> C_consolRequestID { get; set; }
        public string operation { get; set; }
        public System.Guid C_boId { get; set; }
        public System.DateTime C_boCreated { get; set; }
    }
}
