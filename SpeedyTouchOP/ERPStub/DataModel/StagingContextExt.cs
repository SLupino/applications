﻿using System;
using System.Data.Entity;

namespace ERPStub.DataModel
{
    public partial class StagingContext : DbContext
    {
        public StagingContext(String connectionString)
            : base(connectionString)
        {
        }
    }
}
