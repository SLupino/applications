//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ERPStub.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Vouchers
    {
        public Vouchers()
        {
            this.VouchersKey = new HashSet<VouchersKey>();
        }
    
        public string customerId { get; set; }
        public System.DateTime voucherDate { get; set; }
        public string voucherNumber { get; set; }
        public int voucherType { get; set; }
        public int state { get; set; }
        public int numberOfDetails { get; set; }
        public Nullable<System.DateTime> sourceVoucherDate { get; set; }
        public string sourceVoucherNumber { get; set; }
        public decimal valueOfGoods { get; set; }
        public decimal totalAmount { get; set; }
        public int orderEntrySource { get; set; }
        public string note1 { get; set; }
        public string note2 { get; set; }
        public string note3 { get; set; }
        public string note4 { get; set; }
        public string note5 { get; set; }
        public string customerNote1 { get; set; }
        public string customerNote2 { get; set; }
        public string customerNote3 { get; set; }
        public string customerNote4 { get; set; }
        public string customerNote5 { get; set; }
        public decimal freightCharge { get; set; }
        public decimal packageingCharge { get; set; }
        public decimal minimalValueSurcharge { get; set; }
        public Nullable<System.DateTime> deliveryDate { get; set; }
        public string deliveryAddressName1 { get; set; }
        public string deliveryAddressName2 { get; set; }
        public string deliveryAddressStreet { get; set; }
        public string deliveryAddressIsoCountryCode { get; set; }
        public string deliveryAddressZipCode { get; set; }
        public string deliveryAddressCity { get; set; }
        public int quoteFollowUpBy { get; set; }
        public Nullable<System.DateTime> quoteValidUntil { get; set; }
        public int quoteDeliveryBy { get; set; }
        public string quoteFaxNumber { get; set; }
        public string quoteEmail { get; set; }
        public string quoteForAttentionOf { get; set; }
        public Nullable<System.Guid> quoteForAttentionOfContactId { get; set; }
        public short isSplitted { get; set; }
        public short hasAuspreiser { get; set; }
        public short hasOrderConfirmation { get; set; }
        public Nullable<System.Guid> originCartId { get; set; }
        public Nullable<System.Guid> C_consolFK_Vouchers_Customers { get; set; }
        public Nullable<System.Guid> C_consolGuid { get; set; }
        public System.Guid requestId { get; set; }
        public string operation { get; set; }
        public System.Guid C_boId { get; set; }
        public System.DateTime C_boCreated { get; set; }
        public int C_boOperationState { get; set; }
        public int C_boElaborationErrorCode { get; set; }
        public string C_boElaborationErrorMessage { get; set; }
        public Nullable<System.Guid> C_boreplacedBy { get; set; }
        public Nullable<System.Guid> C_consolRequestID { get; set; }
        public string region { get; set; }
        public string postOfficeBoxCity { get; set; }
        public Nullable<decimal> GST { get; set; }
        public Nullable<decimal> additionalCosts { get; set; }
        public string warehouseNote1 { get; set; }
        public string warehouseNote2 { get; set; }
        public string warehouseNote3 { get; set; }
        public string warehouseNote4 { get; set; }
        public string warehouseNote5 { get; set; }
        public Nullable<decimal> headDiscount { get; set; }
        public string salesRepOrderNumber { get; set; }
        public Nullable<int> sourceVoucherType { get; set; }
        public string sourceCustomerId { get; set; }
        public string trackingNumber { get; set; }
        public string courier { get; set; }
        public Nullable<System.DateTime> returnDate { get; set; }
        public Nullable<System.Guid> C_consolFK_Vouchers_Vouchers { get; set; }
        public string currencyIsoCode { get; set; }
        public Nullable<decimal> totalAmountCompanyCurrency { get; set; }
        public string customerOrderReference { get; set; }
        public Nullable<decimal> commissionAmount { get; set; }
    
        public virtual Request_consol Request_consol { get; set; }
        public virtual Request Request { get; set; }
        public virtual ICollection<VouchersKey> VouchersKey { get; set; }
    }
}
