﻿using System;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Xml.Linq;
using ERPStub.DataModel;
using System.Xml.XPath;
using NLog;
using System.Data.Entity.Validation;
using System.Text;

namespace ERPStub
{
    internal class TravelReportHelper
    {
        private static readonly Logger Logger = LogManager.GetLogger(typeof(ERPStub).FullName);

        internal static void CreateTravelReport(XElement xCreateTravelReportRequest, StagingContext context,
            ObjectParameter prRequestId, XDocument xdoc)
        {
            Itineraries itinerary = new Itineraries();
            Logger.Trace("Itineraries: {0}", xCreateTravelReportRequest);
            SetItineraryInitialValues(prRequestId, itinerary, Enumerations.Operation.N);
            itinerary.id = Guid.Parse(xCreateTravelReportRequest.Element("id").Value);
            itinerary.userId = Guid.Parse(xCreateTravelReportRequest.XPathSelectElement("user").Attribute("id").Value);
            itinerary.tripName = ((xCreateTravelReportRequest.Element("tripName") != null) && !String.IsNullOrEmpty(xCreateTravelReportRequest.Element("tripName").Value))
                  ? xCreateTravelReportRequest.Element("tripName").Value
                  : "ND";
            itinerary.startDateUTC = ((xCreateTravelReportRequest.Element("startDateUTC") != null) && !String.IsNullOrEmpty(xCreateTravelReportRequest.Element("startDateUTC").Value))
                  ? DateTime.Parse(xCreateTravelReportRequest.Element("startDateUTC").Value)
                  : DateTime.Now;
            itinerary.endDateUTC = ((xCreateTravelReportRequest.Element("endDateUTC") != null) && !String.IsNullOrEmpty(xCreateTravelReportRequest.Element("endDateUTC").Value))
                ? DateTime.Parse(xCreateTravelReportRequest.Element("endDateUTC").Value)
                : DateTime.Now;
            context.Itineraries.Add(itinerary);

            foreach (var xExpenseEntry in xdoc.Descendants("expenseEntry"))
            {
                ExpenseEntries expensesEntry = ParseExpenseEntries(xExpenseEntry);
                SetExpensesEntryInitialValues(prRequestId, expensesEntry, Enumerations.Operation.N);
                expensesEntry.itineraryId = itinerary.id;
                Logger.Trace("Expense entry: {0}", xExpenseEntry);
                context.ExpenseEntries.Add(expensesEntry);

                var xJourney = xExpenseEntry.Element("journey");
                Logger.Trace("Journey: {0}", xJourney);
                if (xJourney != null)
                {
                    Journeys journey = new Journeys();
                    SetJourneyInitialValues(prRequestId, journey, Enumerations.Operation.N);
                    journey.id = Guid.Parse(xJourney.Element("id").Value);
                    journey.expenseEntryId = expensesEntry.id;
                    journey.businessDistance = ((xJourney.Element("businessDistance") != null) && !String.IsNullOrEmpty(xJourney.Element("businessDistance").Value))
                             ? Convert.ToInt32(xJourney.Element("businessDistance").Value)
                             : 0;
                    journey.personalDistance = ((xJourney.Element("personalDistance") != null) && !String.IsNullOrEmpty(xJourney.Element("personalDistance").Value))
                             ? Convert.ToInt32(xJourney.Element("personalDistance").Value)
                             : 0;
                    journey.odometerStart = ((xJourney.Element("odometerStart") != null) && !String.IsNullOrEmpty(xJourney.Element("odometerStart").Value))
                             ? Convert.ToInt32(xJourney.Element("odometerStart").Value)
                             : 0;
                    journey.odometerEnd = ((xJourney.Element("odometerEnd") != null) && !String.IsNullOrEmpty(xJourney.Element("odometerEnd").Value))
                             ? Convert.ToInt32(xJourney.Element("odometerEnd").Value)
                             : 0;
                    journey.unitOfMeasure = ((xJourney.Element("unitOfMeasure") != null) && !String.IsNullOrEmpty(xJourney.Element("unitOfMeasure").Value))
                             ? xJourney.Element("unitOfMeasure").Value
                             : "K";
                    journey.vehicleId = ((xJourney.Element("vehicleId") != null) && !String.IsNullOrEmpty(xJourney.Element("vehicleId").Value))
                             ? xJourney.Element("vehicleId").Value
                             : "ND";
                    context.Journeys.Add(journey);
                }
            }

            foreach (var xItinerarySegment in xdoc.Descendants("itinerarySegment"))
            {
                ItinerarySegments itinerarySegment = new ItinerarySegments();
                SetItinerarySegmentInitialValues(prRequestId, itinerarySegment, Enumerations.Operation.N);
                Logger.Trace("Itinerary segment: {0}", xItinerarySegment);
                itinerarySegment.id = Guid.Parse(xItinerarySegment.Element("id").Value);
                itinerarySegment.itineraryId = itinerary.id;
                itinerarySegment.startCity = ((xItinerarySegment.Element("startCity") != null) && !String.IsNullOrEmpty(xItinerarySegment.Element("startCity").Value))
                    ? xItinerarySegment.Element("startCity").Value
                    : "ND";
                itinerarySegment.endCity = ((xItinerarySegment.Element("endCity") != null) && !String.IsNullOrEmpty(xItinerarySegment.Element("endCity").Value))
                   ? xItinerarySegment.Element("endCity").Value
                   : "ND";
                itinerarySegment.startCityCode = ((xItinerarySegment.Element("startCityCode") != null) && !String.IsNullOrEmpty(xItinerarySegment.Element("startCityCode").Value))
                    ? xItinerarySegment.Element("startCityCode").Value
                    : "ND";
                itinerarySegment.endCityCode = ((xItinerarySegment.Element("endCityCode") != null) && !String.IsNullOrEmpty(xItinerarySegment.Element("endCityCode").Value))
                    ? xItinerarySegment.Element("endCityCode").Value
                    : "ND";
                itinerarySegment.startCustomerId = ((xItinerarySegment.Element("startCustomerId") != null) && !String.IsNullOrEmpty(xItinerarySegment.Element("startCustomerId").Value))
                    ? xItinerarySegment.Element("startCustomerId").Value
                    : "ND";
                itinerarySegment.endCustomerId = ((xItinerarySegment.Element("endCustomerId") != null) && !String.IsNullOrEmpty(xItinerarySegment.Element("endCustomerId").Value))
                    ? xItinerarySegment.Element("endCustomerId").Value
                    : "ND";
                // max length 2 chars
                itinerarySegment.startCountry = ((xItinerarySegment.Element("startCountry") != null) && !String.IsNullOrEmpty(xItinerarySegment.Element("startCountry").Value))
                    ? (xItinerarySegment.Element("startCountry").Value.Length > 2 ? (xItinerarySegment.Element("startCountry").Value.Substring(0,2)) : xItinerarySegment.Element("startCountry").Value) 
                    : "ND";
                // max length 2 chars
                itinerarySegment.endCountry = ((xItinerarySegment.Element("endCountry") != null) && !String.IsNullOrEmpty(xItinerarySegment.Element("endCountry").Value))
                   ? (xItinerarySegment.Element("endCountry").Value.Length > 2 ? (xItinerarySegment.Element("endCountry").Value.Substring(0, 2)) : xItinerarySegment.Element("endCountry").Value)
                   : "ND";
                itinerarySegment.startDateUTC = ((xItinerarySegment.Element("startDateUTC") != null) && !String.IsNullOrEmpty(xItinerarySegment.Element("startDateUTC").Value))
                    ? DateTime.Parse(xItinerarySegment.Element("startDateUTC").Value)
                    : DateTime.Now;
                itinerarySegment.endDateUTC = ((xItinerarySegment.Element("endDateUTC") != null) && !String.IsNullOrEmpty(xItinerarySegment.Element("endDateUTC").Value))
                    ? DateTime.Parse(xItinerarySegment.Element("endDateUTC").Value)
                    : DateTime.Now;
                itinerarySegment.segmentTypeCode = ((xItinerarySegment.Element("segmentTypeCode") != null) && !String.IsNullOrEmpty(xItinerarySegment.Element("segmentTypeCode").Value))
                    ? xItinerarySegment.Element("segmentTypeCode").Value
                    : "ND";
                    context.ItinerarySegments.Add(itinerarySegment);
            }
        }

        internal static ExpenseEntries ParseExpenseEntries(XElement xExpenseEntry)
        {
            ExpenseEntries expensesEntry = new ExpenseEntries();
            expensesEntry.id = Guid.Parse(xExpenseEntry.Element("id").Value);
            expensesEntry.expenseTypeCode = ((xExpenseEntry.Element("expenseTypeCode") != null) && !String.IsNullOrEmpty(xExpenseEntry.Element("expenseTypeCode").Value))
                     ? xExpenseEntry.Element("expenseTypeCode").Value
                     : "ND";
            expensesEntry.isPersonal = ((xExpenseEntry.Element("isPersonal") != null) && Boolean.Parse(xExpenseEntry.Element("isPersonal").Value))
                     ? (short)1
                     : (short)0;
            expensesEntry.lastModified = ((xExpenseEntry.Element("lastModified") != null) && !String.IsNullOrEmpty(xExpenseEntry.Element("lastModified").Value))
                     ? DateTime.Parse(xExpenseEntry.Element("lastModified").Value)
                     : DateTime.Now;
            expensesEntry.paymentTypeKey = ((xExpenseEntry.Element("paymentTypeKey") != null) && !String.IsNullOrEmpty(xExpenseEntry.Element("paymentTypeKey").Value))
                     ? xExpenseEntry.Element("paymentTypeKey").Value
                     : "ND";
            expensesEntry.receiptRequired = ((xExpenseEntry.Element("receiptRequired") != null) && Boolean.Parse(xExpenseEntry.Element("receiptRequired").Value))
                     ? (short)1
                     : (short)0;
            expensesEntry.transactionAmount = ((xExpenseEntry.Element("transactionAmount") != null) && !String.IsNullOrEmpty(xExpenseEntry.Element("transactionAmount").Value))
                     ? Convert.ToDecimal(xExpenseEntry.Element("transactionAmount").Value)
                     : 0;
            expensesEntry.transactionCurrencyCode = ((xExpenseEntry.Element("transactionCurrencyCode") != null) && !String.IsNullOrEmpty(xExpenseEntry.Element("transactionCurrencyCode").Value))
                     ? xExpenseEntry.Element("transactionCurrencyCode").Value
                     : "ND";
            expensesEntry.transactionDate = ((xExpenseEntry.Element("transactionDate") != null) && !String.IsNullOrEmpty(xExpenseEntry.Element("transactionDate").Value))
                     ? Convert.ToDateTime(xExpenseEntry.Element("transactionDate").Value)
                     : DateTime.Now;
            expensesEntry.vendorDescription = ((xExpenseEntry.Element("vendorDescription") != null) && !String.IsNullOrEmpty(xExpenseEntry.Element("vendorDescription").Value))
                     ? xExpenseEntry.Element("vendorDescription").Value
                     : "ND";
            return expensesEntry;
        }

        private static void SetItinerarySegmentInitialValues(ObjectParameter prRequestId, ItinerarySegments itinerarySegment, Enumerations.Operation operation)
        {
            itinerarySegment.requestId = (Guid)prRequestId.Value;
            itinerarySegment.operation = operation.ToString();
            itinerarySegment.C_boId = Guid.NewGuid();
            itinerarySegment.C_boCreated = DateTime.Now;
            itinerarySegment.C_boElaborationErrorMessage = "";
            itinerarySegment.C_boOperationState = 0;
            itinerarySegment.C_boreplacedBy = null;
            itinerarySegment.C_consolGuid = null;
            itinerarySegment.C_boElaborationErrorCode = 0;
            itinerarySegment.C_consolRequestID = null;
        }

        internal static void SetExpensesEntryInitialValues(ObjectParameter prRequestId, ExpenseEntries expensesEntry, Enumerations.Operation operation)
        {
            expensesEntry.requestId = (Guid)prRequestId.Value;
            expensesEntry.operation = operation.ToString();
            expensesEntry.C_boId = Guid.NewGuid();
            expensesEntry.C_boCreated = DateTime.Now;
            expensesEntry.C_boElaborationErrorMessage = "";
            expensesEntry.C_boOperationState = 0;
            expensesEntry.C_boreplacedBy = null;
            expensesEntry.C_consolGuid = null;
            expensesEntry.C_boElaborationErrorCode = 0;
            expensesEntry.C_consolRequestID = null;
        }

        private static void SetItineraryInitialValues(ObjectParameter prRequestId, Itineraries itinerary, Enumerations.Operation operation)
        {
            itinerary.requestId = (Guid)prRequestId.Value;
            itinerary.operation = operation.ToString();
            itinerary.C_boId = Guid.NewGuid();
            itinerary.C_boCreated = DateTime.Now;
            itinerary.C_boElaborationErrorMessage = "";
            itinerary.C_boOperationState = 0;
            itinerary.C_boreplacedBy = null;
            itinerary.C_consolGuid = null;
            itinerary.C_boElaborationErrorCode = 0;
            itinerary.C_consolRequestID = null;
        }

        private static void SetJourneyInitialValues(ObjectParameter prRequestId, Journeys journey, Enumerations.Operation operation)
        {
            journey.requestId = (Guid)prRequestId.Value;
            journey.operation = operation.ToString();
            journey.C_boId = Guid.NewGuid();
            journey.C_boCreated = DateTime.Now;
            journey.C_boElaborationErrorMessage = "";
            journey.C_boOperationState = 0;
            journey.C_boreplacedBy = null;
            journey.C_consolGuid = null;
            journey.C_boElaborationErrorCode = 0;
            journey.C_consolRequestID = null;
        }
    }
}