﻿if (!window.RFC) window.RFC = {};

RFC.objectToTable = function (object) {
    if (typeof object === 'object') {
        var html = [];
        if (object.length && object[0] && object[0].length) {
            html = html.concat('<table>');
            for (var idxRow = 0; idxRow < object.length; idxRow++) {
                html = html.concat('<tr>');
                var row = object[idxRow];
                for (var idxCell = 0; idxCell < row.length; idxCell++) {
                    if (idxRow === 0) {
                        html = html.concat(['<th>', row[idxCell], '</th>']);
                    } else {
                       html = html.concat(['<td>', row[idxCell], '</td>']);
                    }
                }
                html = html.concat('</tr>');
            }
            html = html.concat('</table>');
        } else {
            if (object.type) {
                html = html.concat([object.type, '<br/>']);
            }
            html = html.concat('<table>');
            for (var attr in object) {
                if (attr === 'type') continue;
                html = html.concat('<tr><td>' + attr + '</td><td>' + RFC.objectToTable(object[attr]) + '</td></tr>');
            }
            html = html.concat('</table>');
        }
        return html.join('');
    }
    return $('<div/>').text(object).html();
};

jQuery(function ($) {
    $('#TestSystemInfo').click(function (/*event*/) {
        $('#SystemInfoResult').text('');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            url: 'WS1.asmx/SapSystemInfo',
            success: function (data/*, textStatus, XMLHttpRequest*/) {
                for (var name in data.d) {
                    if (name == 'RFCSI_EXPORT') {
                        $('#SystemInfoResult').append(name + ': <span id="RFCSI_EXPORT" style="display: inline-block"></span><br/>');
                        for (var subName in data.d[name]) {
                            $('#RFCSI_EXPORT').append(subName + ': ' + data.d[name][subName] + '<br/>');
                        }
                    } else {
                        $('#SystemInfoResult').append(name + ': ' + data.d[name] + '<br/>');
                    }
                }
            },
            error: function (xhr, textStatus, err) {
                $('#SystemInfoResult').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
            }
        });
    });

    $('#priceOverviewRequestXml').val('<?xml version="1.0" encoding="UTF-8"?>\n\
<priceOverviewRequest Speedy-App-Version="1.0.8584"><companyCode>3102</companyCode>\n\
<locale>DE-de</locale>\n\
<user id="00000000-0000-0000-0000-000000001324"/>\n\
<priceRequest cartGuid="EF38A77C-DE44-40EF-BC34-0D0DE16AE467">\n\
<newCustomer>\n\
<numberOfEmployees>1</numberOfEmployees>\
<pricingGroup id="AU"/><industryCode id="0110"/>\n\
</newCustomer><cartVoucherType>order</cartVoucherType>\
<item id="005616 70 "/></priceRequest></priceOverviewRequest>');
    $('#priceOverviewRequest').click(function (/*event*/) {
        var start = new Date();
        $('#priceOverviewInterval').text('');
        $('#priceOverviewResponseXml').val('');
        $('#priceOverviewResponse').text('');
        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: ['request', encodeURIComponent($('#priceOverviewRequestXml').val())].join('='),
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            url: '../priceOverview',
            success: function (data/*, textStatus, XMLHttpRequest*/) {
                var end = new Date();
                $('#priceOverviewResponseXml').val(data);
                $('#priceOverviewInterval').text(end.getTime() - start.getTime() + "ms");
            },
            error: function (xhr, textStatus, err) {
                $('#priceOverviewResponse').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
                $('#priceOverviewInterval').text('sss');
            }
        });
    });
    $('#priceOverview2RequestXml').val('<?xml version="1.0" encoding="UTF-8"?>\n\
<priceOverviewRequest2 Speedy-App-Version="1.0.8584"><companyCode>3102</companyCode>\n\
<locale>DE-de</locale>\n\
<user id="00000000-0000-0000-0000-000000001324"/>\n\
<priceRequest cartGuid="EF38A77C-DE44-40EF-BC34-0D0DE16AE467">\n\
<newCustomer>\n\
<numberOfEmployees>1</numberOfEmployees>\
<pricingGroup id="AU"/><industryCode id="0110"/>\n\
</newCustomer><cartVoucherType>order</cartVoucherType>\
<item id="005616 70 "/></priceRequest></priceOverviewRequest2>');
    $('#priceOverview2Request').click(function (/*event*/) {
        var start = new Date();
        $('#priceOverview2Interval').text('');
        $('#priceOverview2ResponseXml').val('');
        $('#priceOverview2Response').text('');
        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: ['request', encodeURIComponent($('#priceOverview2RequestXml').val())].join('='),
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            url: '../priceOverview2',
            success: function (data/*, textStatus, XMLHttpRequest*/) {
                var end = new Date();
                $('#priceOverview2ResponseXml').val(data);
                $('#priceOverview2Interval').text(end.getTime() - start.getTime() + "ms");
            },
            error: function (xhr, textStatus, err) {
                $('#priceOverview2Response').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
                $('#priceOverview2Interval').text('sss');
            }
        });
    });

    $('#priceOverview3RequestXml').val('<?xml version="1.0" encoding="UTF-8"?>\n\
<priceOverviewRequest3 Speedy-App-Version="1.0.8584"><companyCode>3102</companyCode>\n\
<locale>DE-de</locale>\n\
<user id="00000000-0000-0000-0000-000000001324"/>\n\
<priceRequest cartGuid="EF38A77C-DE44-40EF-BC34-0D0DE16AE467">\n\
<newCustomer>\n\
<numberOfEmployees>1</numberOfEmployees>\
<pricingGroup id="AU"/><industryCode id="0110"/>\n\
</newCustomer><cartVoucherType>order</cartVoucherType>\
<item id="005616 70 "/></priceRequest></priceOverviewRequest3>');
    $('#priceOverview3Request').click(function (/*event*/) {
        var start = new Date();
        $('#priceOverview3Interval').text('');
        $('#priceOverview3ResponseXml').val('');
        $('#priceOverview3Response').text('');
        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: ['request', encodeURIComponent($('#priceOverview3RequestXml').val())].join('='),
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            url: '../priceOverview3',
            success: function (data/*, textStatus, XMLHttpRequest*/) {
                var end = new Date();
                $('#priceOverview3ResponseXml').val(data);
                $('#priceOverview3Interval').text(end.getTime() - start.getTime() + "ms");
            },
            error: function (xhr, textStatus, err) {
                $('#priceOverview3Response').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
                $('#priceOverview3Interval').text('sss');
            }
        });
    });

    $('#getDocumentRequestXml').val('<?xml version="1.0" encoding="UTF-8"?>\n\
<getDocumentRequest Speedy-App-Version="1.0.5946"><companyCode>3102</companyCode><locale>DE-de</locale>\n\
<user id="00000000-0000-0000-0000-000000001324"/>\n<customer id="0321173186"/>\n<voucher id="2013-09-19|12">\n<voucherType>orderReceipt</voucherType>\n\
</voucher>\n</getDocumentRequest>');
    $('#getDocumentRequest').click(function (/*event*/) {
        $('#getDocumentResponse').text('');
        $('#getDocumentResponseXml').val('');

        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: ['request', encodeURIComponent($('#getDocumentRequestXml').val())].join('='),
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            url: '../getDocument',
            success: function (data/*, textStatus, XMLHttpRequest*/) {
                $('#getDocumentResponseXml').val(data);
            },
            error: function (xhr, textStatus, err) {
                $('#getDocumentResponse').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
            }
        });
    });

    $('#cartValidationRequestXml').val('<?xml version="1.0" encoding="UTF-8"?>\n\
<cartValidationRequest2 Speedy-App-Version="1.0.8958">\n\
  <companyCode>3102</companyCode>\n\
  <locale>en_AU</locale>\n\
  <user id="00000000-0000-0000-0000-000000001014"/>\n\
  <cart cartGuid="0AE605F2-1539-4F65-9E86-896885E2848E">\n\
    <voucherType>order</voucherType>\n\
    <createdBy id="00000000-0000-0000-0000-000000001014"/>\n\
    <createdTimestamp>2014-02-05T14:56:42</createdTimestamp>\n\
    <deliveryDate>2014-02-07</deliveryDate>\n\
    <customer id="0321173186"/>\n\
    <validUntil>2014-03-05</validUntil>\n\
    <cartDetails>\n\
      <cartDetail>\n\
        <item id="0703084160961    1"/>\n\
        <isSpecialItem>false</isSpecialItem>\n\
        <quantity>1</quantity>\n\
        <grossPrice>479.67</grossPrice>\n\
        <pricingUnit>UNIT1</pricingUnit>\n\
        <discount>0</discount>\n\
        <surcharge>0</surcharge>\n\
        <alreadyDelivered>true</alreadyDelivered>\n\
        <saveDiscount>false</saveDiscount>\n\
        <serialNumber>1234567891</serialNumber>\n\
      </cartDetail>\n\
    </cartDetails>\n\
  </cart>\n\
</cartValidationRequest2>');
    $('#cartValidationRequest').click(function (/*event*/) {
        $('#cartValidationResponse').text('');
        $('#cartDetailValidationResponseXml').val();
        /*existing customer*/
        //                var request = '<?xml version="1.0" encoding="UTF-8"?>\
        //<cartValidationRequest Speedy-App-Version="1.0.6547"><companyCode>3102</companyCode><locale>DE-de</locale>\
        //<user id="4E73D356-0651-11E3-AB06-D8D385F69AF4"/><cart cartGuid="7D50D931-02A6-4319-9F84-389E423C011A"><voucherType>order</voucherType>\
        //<createdBy id="4E73D356-0651-11E3-AB06-D8D385F69AF4"/><createdTimestamp>2013-09-18T15:17:10</createdTimestamp>\
        //<deliveryDate>2013-10-18</deliveryDate>\
        //<customer id="349773"/>\
        //<validUntil>2013-09-18</validUntil>\
        //<cartDetails>\
        //    <cartDetail><item id="0039 4   20"/><isSpecialItem>false</isSpecialItem>\
        //    <quantity>500</quantity><grossPrice>7.35</grossPrice><pricingUnit>UNIT100</pricingUnit>\
        //    <discount>45</discount><surcharge>12</surcharge><alreadyDelivered>false</alreadyDelivered>\
        //    <saveDiscount>false</saveDiscount>\
        //    </cartDetail>\
        //    <cartDetail><item id="0039 4   16"/><isSpecialItem>true</isSpecialItem>\
        //    <quantity>500</quantity><grossPrice>7.35</grossPrice><pricingUnit>UNIT100</pricingUnit>\
        //    <discount>45</discount><surcharge>12</surcharge><alreadyDelivered>false</alreadyDelivered>\
        //    <saveDiscount>false</saveDiscount>\
        //    </cartDetail>\
        //    <cartDetail><item id="0039 5   10"/><isSpecialItem>false</isSpecialItem>\
        //    <quantity>200</quantity><grossPrice>7.35</grossPrice>\
        //    <pricingUnit>UNIT100</pricingUnit><discount>45</discount><surcharge>12</surcharge>\
        //    <alreadyDelivered>false</alreadyDelivered>\
        //    <saveDiscount>false</saveDiscount>\
        //    </cartDetail>\
        //</cartDetails>\
        //</cart></cartValidationRequest>';

        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: ['request', encodeURIComponent($('#cartValidationRequestXml').val())].join('='),
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            url: '../cartValidation',
            success: function (data/*, textStatus, XMLHttpRequest*/) {
                $('#cartValidationResponseXml').val(data);
            },
            error: function (xhr, textStatus, err) {
                $('#cartValidationResponse').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
            }
        });
    });
    
    $('#cartDetailValidationRequestXml').val('<?xml version="1.0" encoding="UTF-8"?>\n\
<cartDetailValidationRequest2 Speedy-App-Version="1.0.8958">\n\
  <companyCode>3102</companyCode>\n\
  <locale>en_AU</locale>\n\
  <user id="00000000-0000-0000-0000-000000001014"/>\n\
  <voucherType>order</voucherType>\n\
  <customer id="0321173186"/>\n\
  <cartDetail>\n\
    <item id="0703084160961    1"/>\n\
    <isSpecialItem>false</isSpecialItem>\n\
    <quantity>1</quantity>\n\
    <deliveredFromCarStock>true</deliveredFromCarStock>\n\
    <grossPrice>479.67</grossPrice>\n\
    <pricingUnit>UNIT1</pricingUnit>\n\
    <discount>0</discount>\n\
    <surcharge>0</surcharge>\n\
    <noCharge>false</noCharge>\n\
    <serialNumber>1234567891</serialNumber>\n\
  </cartDetail>\n\
</cartDetailValidationRequest2>');
    $('#cartDetailValidationRequest').click(function (/*event*/) {
        $('#cartDetailValidationResponse').text('');
        $('#cartDetailValidationResponseXml').val('');

        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: ['request', encodeURIComponent($('#cartDetailValidationRequestXml').val())].join('='),
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            url: '../cartDetailValidation',
            success: function (data/*, textStatus, XMLHttpRequest*/) {
                $('#cartDetailValidationResponseXml').val(data);
            },
            error: function (xhr, textStatus, err) {
                $('#cartDetailValidationResponse').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
            }
        });
    });
    
    $('#getBackOrderAsPdfRequestXml').val('<?xml version="1.0" encoding="UTF-8"?>\n\
<getUserReportRequest Speedy-App-Version="1.0.5946">\n<companyCode>3102</companyCode>\n\
<locale>DE-de</locale>\n\
<user id="00000000-0000-0000-0000-000000001324"/>\n\
<reportType>backOrder</reportType>\n\
</getUserReportRequest>');
    $('#getBackOrderAsPdfRequest').click(function (/*event*/) {
        $('#getBackOrderAsPdfResponse').text('');
        $('#getBackOrderAsPdfResponseXml').val('');

        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: ['request', encodeURIComponent($('#getBackOrderAsPdfRequestXml').val())].join('='),
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            url: '../getUserReportRequest',
            success: function (data/*, textStatus, XMLHttpRequest*/) {
                $('#getBackOrderAsPdfResponseXml').val(data);
            },
            error: function (xhr, textStatus, err) {
                $('#getBackOrderAsPdfResponse').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
            }
        });
    });

    $('#getInventoryOnHandAsPdfRequestXml').val('<?xml version="1.0" encoding="UTF-8"?>\n\
<getUserReportRequest Speedy-App-Version="1.0.5946">\n<companyCode>3102</companyCode>\n\
<locale>DE-de</locale>\n\
<user id="00000000-0000-0000-0000-000000001324"/>\n\
<reportType>stockOnHand</reportType>\n\
</getUserReportRequest>');
    $('#getInventoryOnHandAsPdf').click(function (/*event*/) {
        $('#getInventoryOnHandAsPdfResponse').text('');
        $('#getInventoryOnHandAsPdfResponseXml').val('');

        $.ajax({
            type: 'POST',
            dataType: 'text',
            data: ['request', encodeURIComponent($('#getInventoryOnHandAsPdfRequestXml').val())].join('='),
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            url: '../getUserReportRequest',
            success: function (data/*, textStatus, XMLHttpRequest*/) {
                $('#getInventoryOnHandAsPdfResponseXml').val(data);
            },
            error: function (xhr, textStatus, err) {
                $('#getInventoryOnHandAsPdfResponse').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
            }
        });
    });

    $('#rfcPriceOverview').click(function (/*event*/) {
        $('#rfcPriceOverviewResult').text('');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: '',
            url: 'WS1.asmx/PriceOverwiew',
            success: function (data/*, textStatus, XMLHttpRequest*/) {
                $('#rfcPriceOverviewResult').html(RFC.objectToTable(data));
            },
            error: function (xhr, textStatus, err) {
                $('#rfcPriceOverviewResult').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
            }
        });
    });

    $('#rfcGetDocument').click(function (/*event*/) {
        $('#rfcGetDocumentResult').text('');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: '',
            url: 'WS1.asmx/GetDocument',
            success: function (data/*, textStatus, XMLHttpRequest*/) {
                $('#rfcGetDocumentResult').html(RFC.objectToTable(data));
            },
            error: function (xhr, textStatus, err) {
                $('#rfcGetDocumentResult').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
            }
        });
    });

    $('#rfcCartValidation').click(function (/*event*/) {
        $('#rfcCartValidationResult').text('');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: '',
            url: 'WS1.asmx/CartValidation',
            success: function (data/*, textStatus, XMLHttpRequest*/) {
                $('#rfcCartValidationResult').html(RFC.objectToTable(data));
            },
            error: function (xhr, textStatus, err) {
                $('#rfcCartValidationResult').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
            }
        });
    });

    $('#rfcCartDetailValidation').click(function () {
        $('#rfcCartDetailValidationResult').text('');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: '',
            url: 'WS1.asmx/CartDetailValidation',
            success: function (data/*, textStatus, XMLHttpRequest*/) {
                $('#rfcCartDetailValidationResult').html(RFC.objectToTable(data));
            },
            error: function (xhr, textStatus, err) {
                $('#rfcCartDetailValidationResult').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
            }
        });
    });

    $('#rfcGetBackOrderAsPdf').click(function () {
        $('#rfcGetBackOrderAsPdfResult').text('');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: '',
            url: 'WS1.asmx/GetBackOrderAsPdf',
            success: function (data/*, textStatus, XMLHttpRequest*/) {
                $('#rfcGetBackOrderAsPdfResult').html(RFC.objectToTable(data));
            },
            error: function (xhr, textStatus, err) {
                $('#rfcGetBackOrderAsPdfResult').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
            }
        });
    });

    $('#rfcGetInventoryOnHandAsPdf').click(function () {
        $('#rfcGetInventoryOnHandAsPdfResult').text('');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: '',
            url: 'WS1.asmx/GetInventoryOnHandAsPdf',
            success: function (data/*, textStatus, XMLHttpRequest*/) {
                $('#rfcGetInventoryOnHandAsPdfResult').html(RFC.objectToTable(data));
            },
            error: function (xhr, textStatus, err) {
                $('#rfcGetInventoryOnHandAsPdfResult').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
            }
        });
    });

    $('#describe').click(function (/*event*/) {
        $('#describeResult').text('');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ functionName: $('#functionName').val() }),
            url: 'WS1.asmx/DescribeRfcFunction',
            success: function (data/*, textStatus, XMLHttpRequest*/) {
                $('#describeResult').html(RFC.objectToTable(data));
            },
            error: function (xhr, textStatus, err) {
                $('#describeResult').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
            }
        });
    });

    $("#btnDescribeSFS_CUST_GETPRICETAB").click(function (/*event*/) {
        $("#describeSFS_CUST_GETPRICETAB").text("Loading RFC /WRP/SFS_CUST_GETPRICETAB definition...");
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ functionName: '/WRP/SFS_CUST_GETPRICETAB_3102' }),
            url: 'WS1.asmx/DescribeRfcFunction',
            success: function (data/*, textStatus, XMLHttpRequest*/) {
                $('#describeSFS_CUST_GETPRICETAB').html(RFC.objectToTable(data));
            },
            error: function (xhr, textStatus, err) {
                $('#describeSFS_CUST_GETPRICETAB').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
            }
        });
    });

    $("#btnDescribeSFS_ORDER_GETDOCUMENT").click(function(/*event*/) {
        $("#describeSFS_ORDER_GETDOCUMENT").text("Loading RFC /WRP/SFS_ORDER_GETDOCUMENT definition...");
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ functionName: '/WRP/SFS_ORDER_GETDOCUMENT' }),
            url: 'WS1.asmx/DescribeRfcFunction',
            success: function(data/*, textStatus, XMLHttpRequest*/) {
                $('#describeSFS_ORDER_GETDOCUMENT').html(RFC.objectToTable(data));
            },
            error: function(xhr, textStatus, err) {
                $('#describeSFS_ORDER_GETDOCUMENT').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
            }
        });
    });

    $("#btnDescribeSFS_CUST_VALIDATECART").click(function(/*event*/) {
        $("#describeSFS_CUST_VALIDATECART").text("Loading RFC /WRP/SFS_CUST_VALIDATECART definition...");
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ functionName: '/WRP/SFS_CUST_VALIDATECART' }),
            url: 'WS1.asmx/DescribeRfcFunction',
            success: function(data/*, textStatus, XMLHttpRequest*/) {
                $('#describeSFS_CUST_VALIDATECART').html(RFC.objectToTable(data));
            },
            error: function(xhr, textStatus, err) {
                $('#describeSFS_CUST_VALIDATECART').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
            }
        });
    });

    $("#btnBescribeSFS_CUST_VALIDATEITEM").click(function (/*event*/) {
        $("#describeSFS_CUST_VALIDATEITEM").text("Loading RFC /WRP/SFS_CUST_VALIDATEITEM definition...");
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ functionName: '/WRP/SFS_CUST_VALIDATEITEM' }),
            url: 'WS1.asmx/DescribeRfcFunction',
            success: function(data/*, textStatus, XMLHttpRequest*/) {
                $('#describeSFS_CUST_VALIDATEITEM').html(RFC.objectToTable(data));
            },
            error: function(xhr, textStatus, err) {
                $('#describeSFS_CUST_VALIDATEITEM').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
            }
        });
    });

    $("#btnDescribeSFS_SEL_GETBACKOR_ASPDF").click(function(/*event*/) {
        $("#describeSFS_SEL_GETBACKOR_ASPDF").text("Loading RFC /WRP/SFS_SEL_GETBACKOR_ASPDF definition...");
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ functionName: '/WRP/SFS_SEL_GETBACKOR_ASPDF' }),
            url: 'WS1.asmx/DescribeRfcFunction',
            success: function(data/*, textStatus, XMLHttpRequest*/) {
                $('#describeSFS_SEL_GETBACKOR_ASPDF').html(RFC.objectToTable(data));
            },
            error: function(xhr, textStatus, err) {
                $('#describeSFS_SEL_GETBACKOR_ASPDF').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
            }
        });
    });

    $("#btnDescribeSFS_SEL_GETCARSTOCK_ASPDF").click(function(/*event*/) {
        $("#describeSFS_SEL_GETCARSTOCK_ASPDF").text("Loading RFC /WRP/SFS_SEL_GETCARSTOCK_ASPDF definition...");
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ functionName: '/WRP/SFS_SEL_GETCARSTOCK_ASPDF' }),
            url: 'WS1.asmx/DescribeRfcFunction',
            success: function(data/*, textStatus, XMLHttpRequest*/) {
                $('#describeSFS_SEL_GETCARSTOCK_ASPDF').html(RFC.objectToTable(data));
            },
            error: function(xhr, textStatus, err) {
                $('#describeSFS_SEL_GETCARSTOCK_ASPDF').text('textStatus: ' + textStatus + ',  err: ' + err).append('<br/>').append(xhr.responseText);
            }
        });
    });
});
