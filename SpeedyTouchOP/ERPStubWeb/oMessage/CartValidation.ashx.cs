﻿using NLog;
using System.Web;
using System.Xml.Linq;
using System.Xml.XPath;

namespace ERPStubWeb.oMessage
{
    public class CartValidation : OMessage
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public override XDocument ProcessOMessageRequest(HttpContext context, XDocument xRequest)
        {
            XElement xCartValidationResponse2 = new XElement("cartValidationResponse2",
                                                             new XElement("message", "OK!"),
                                                             new XElement("carStockAvailability", true));

            XElement xCartDetails = new XElement("cartDetails");
            xCartValidationResponse2.Add(xCartDetails);

            foreach (XElement xCartDetailIn in xRequest.XPathSelectElements("/cartValidationRequest2/cart/cartDetails/cartDetail"))
            {
                string itemId = xCartDetailIn.XPathSelectElement("item").Attribute("id").Value;

                string serialNumber = xCartDetailIn.XPathSelectElement("serialNumber").Value;

                XElement xCartDetail = new XElement("cartDetail", 
                                                    new XElement("item", new XAttribute("id", itemId)),
                                                    new XElement("serialNumber", serialNumber),
                                                    new XElement("serialNumberRequired", true),
                                                    new XElement("serialNumberValidated", true));
                xCartDetails.Add(xCartDetail);
                
            }

            return new XDocument(xRequest.Declaration, xCartValidationResponse2);
        }
    }
}