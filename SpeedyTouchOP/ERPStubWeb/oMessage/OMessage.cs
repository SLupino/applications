﻿using NLog;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace ERPStubWeb.oMessage
{
    public abstract class OMessage : HttpTaskAsyncHandler
    {
        public new bool IsReusable
        {
            get { return true; }
        }

        public override Task ProcessRequestAsync(HttpContext context)
        {
            return Task.Run(() => ProcessRequest(context));
        }

        public override void ProcessRequest(HttpContext context)
        {
            DateTime start = DateTime.Now;

            Logger logger = LogManager.GetLogger(this.GetType().FullName);

            context.Response.ContentType = "application/xml";

            Stream inputStream = context.Request.GetBufferlessInputStream();

            StreamReader streamReader = new StreamReader(inputStream);
            string request = streamReader.ReadToEnd();
            streamReader.Close();

            try
            {
                logger.Debug("Request {0:yyyy.MM.dd_HH.mm.ss.fffffffzz}. Begin processing.", context.Timestamp);
                if (logger.IsTraceEnabled)
                {
                    logger.Trace("Request {0:yyyy.MM.dd_HH.mm.ss.fffffffzz}. Request body {1}.",
                                 context.Timestamp, request);
                }

                string[] kvPairs = request.Split(new string[] { "&" }, StringSplitOptions.RemoveEmptyEntries);

                if (kvPairs.Length == 0)
                {
                    logger.Error("Request {0:yyyy.MM.dd_HH.mm.ss.fffffffzz}. Missing request body.",
                                 context.Timestamp);
                    ResponseError(context, "Missing request body");
                    return;
                }

                bool requestFound = false;
                foreach (string kvPair in kvPairs)
                {
                    string[] kv = kvPair.Split('=');
                    if (kv.Length != 2 || kv[0] != "request") continue;
                    request = context.Server.UrlDecode(kv[1]);
                    requestFound = true;
                    break;
                }

                if (!requestFound)
                {
                    logger.Error("Request {0:yyyy.MM.dd_HH.mm.ss.fffffffzz}. Missing \"request\" key.",
                                 context.Timestamp);
                    ResponseError(context, "Missing \"request\" key");
                    return;
                }

                if (logger.IsTraceEnabled)
                {
                    logger.Trace("Request {0:yyyy.MM.dd_HH.mm.ss.fffffffzz}. Request XML:\n{1}",
                                 context.Timestamp, request);
                }

                XDocument xmlDocument = XDocument.Parse(request);
                
                XDocument xResponse = ProcessOMessageRequest(context, xmlDocument);
                if (logger.IsTraceEnabled)
                {
                    logger.Trace("Request {0:yyyy.MM.dd_HH.mm.ss.fffffffzz}. Response XML:\n{1}\n{2}",
                                 context.Timestamp,
                                 xResponse.Declaration, xResponse.ToString());
                }

                context.Response.Write(xResponse.Declaration);
                context.Response.Write(xResponse.ToString(SaveOptions.DisableFormatting));
                context.Response.Flush();

            }
            catch (Exception e)
            {
                logger.Error("Request {0:yyyy.MM.dd_HH.mm.ss.fffffffzz}. Exception for request {1}.", context.Timestamp, request);
                logger.Error(e, string.Format("Request {0:yyyy.MM.dd_HH.mm.ss.fffffffzz}.", context.Timestamp));
                throw;
            }
            finally
            {
                logger.Debug("Request {0:yyyy.MM.dd_HH.mm.ss.fffffffzz}. End processing.", context.Timestamp);

                if (logger.IsInfoEnabled)
                {
                    TimeSpan t = DateTime.Now - start;
                    logger.Info("Request {0:yyyy.MM.dd_HH.mm.ss.fffffffzz}. {1} processed in {2} ms.",
                                context.Timestamp, logger.Name, t.TotalMilliseconds);
                }
            }
        }

        public abstract XDocument ProcessOMessageRequest(HttpContext context, XDocument xRequest);

        protected virtual void ResponseError(HttpContext context, string message)
        {
            throw new Exception(message);
        }
    }
}