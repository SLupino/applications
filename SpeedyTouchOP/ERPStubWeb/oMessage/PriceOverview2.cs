﻿using System.Xml.Linq;
using System.Linq;
using System.Web;

namespace ERPStubWeb.oMessage
{
    public class PriceOverview2 : PriceOverview
    {
        public override XDocument ProcessOMessageRequest(HttpContext context, XDocument xRequest)
        {
            var result = base.ProcessOMessageRequest(context, xRequest);
            var discounts = result.Descendants("discount");
            foreach (var discount in discounts)
            {
                discount.Add(new XAttribute("discountType", "resellerDiscount"));
            }

            var root = result.Descendants("priceOverviewResponse").FirstOrDefault();
            if (root != null)
                root.Name = "priceOverviewResponse2";

            return result;
        }
    }
}