﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Xml.Linq;

//using NLog;

namespace ERPStubWeb.oMessage
{
    public class PriceOverview : OMessage
    {
        //private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public override XDocument ProcessOMessageRequest(HttpContext context, XDocument xRequest)
        {
            var priceRequest = xRequest.Descendants("priceRequest").FirstOrDefault();

            if (priceRequest == null)
                return new XDocument();

            var itemId = priceRequest.Descendants("item").FirstOrDefault();
            var item = new XElement("item", "");
            var customerId = priceRequest.Descendants("customer").FirstOrDefault()?.Attribute("id")?.Value;
            item.SetAttributeValue("id", itemId?.Attribute("id")?.Value ?? "");
            var cartGuid = priceRequest.Attribute("cartGuid")?.Value;
            decimal discount = 0;
            decimal surcharge = 0;
            var hasCustomPrice = false;

            const string selectSql = @"SELECT TOP 1 [discount], [surcharge]
                                                    FROM [dbo].[Conditions]
                                                    WHERE [customerId] = @customerId AND ([itemId] = @itemId OR ([isGroup] = 1 AND [itemId] LIKE @itemIdLike))
                                                    ORDER BY [isGroup]";
            using (var incomingConn =
                new SqlConnection(ConfigurationManager.ConnectionStrings["Incoming"].ConnectionString))
            {
                incomingConn.Open();

                using (var command = new SqlCommand(selectSql, incomingConn))
                {
                    command.Parameters.AddWithValue("itemId", itemId?.Attribute("id")?.Value);
                    command.Parameters.AddWithValue("customerId", customerId??"");
                    command.Parameters.AddWithValue("itemIdLike", itemId?.Attribute("id")?.Value.Split(' ')[0] + "%");
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            discount = reader.GetDecimal(0);
                            surcharge = reader.GetDecimal(1);
                            hasCustomPrice = true;
                        }
                    }
                }

            }

            var pricingResponse = new XElement("pricingResponse", new XElement("pricingRecord",
                item,
                //new XElement("GST-percentage", 22.00),
                new XElement("maximumPrice", 20.00),
                new XElement("profitClass", GetProfitClass(cartGuid, itemId?.Attribute("id")?.Value)),
                // profitClass a questo livello
                new XElement("priceKindInformation",
                    new XAttribute("priority", 1),
                    new XElement("priceKindDescription", "Standard 1"),
                    new XElement("minimalPrice", "false"),
                    new XElement("obligatory", "false"),
                    new XElement("priceItem",
                        new XElement("quantityFrom", 1.00),
                        new XElement("grossPrice", 10.00),
                        new XElement("pricingUnit", "UNIT100"),
                        new XElement("discount", 0.00),
                        new XElement("surcharge", 0.00),
                        new XElement("productBasket",
                            GetProductBasket(cartGuid, itemId?.Attribute("id")?.Value)),
                        //productBasket a questo livello
                        new XElement("priceKindId", "Standard1"))),
                new XElement("priceKindInformation",
                    new XAttribute("priority", 2),
                    new XElement("priceKindDescription", "Minimal"),
                    new XElement("minimalPrice", "true"),
                    new XElement("obligatory", "false"),
                    new XElement("priceItem",
                        new XElement("quantityFrom", 1.00),
                        new XElement("grossPrice", 2.00),
                        new XElement("pricingUnit", "UNIT100"),
                        new XElement("discount", 0.00),
                        new XElement("surcharge", 0.00),
                        new XElement("priceKindId", "Minimal"))),
                hasCustomPrice
                    ? new XElement("priceKindInformation",
                        new XAttribute("priority", 0),
                        new XElement("priceKindDescription", "Customer Specific Price"),
                        new XElement("minimalPrice", "false"),
                        new XElement("obligatory", "false"),
                        new XElement("priceItem",
                            new XElement("quantityFrom", 1.00),
                            new XElement("grossPrice", 10.00),
                            new XElement("pricingUnit", "UNIT100"),
                            new XElement("discount", discount),
                            new XElement("surcharge", surcharge),
                            new XElement("priceKindId", "Customer Specific Price")))
                    : null));
            pricingResponse.SetAttributeValue("id", cartGuid);

            var xPriceOverviewResponse = new XElement("priceOverviewResponse",
                pricingResponse);

            return new XDocument(xRequest.Declaration, xPriceOverviewResponse);

        }

        private string GetProfitClass(string cartGuid, string itemId)
        {
            if (cartGuid == null || itemId == null)
                return null;

            return GetCartItemValues(cartGuid, itemId).Item1;
        }

        private string GetProductBasket(string cartGuid, string itemId)
        {
            if (cartGuid == null || itemId == null)
                return null;

            return GetCartItemValues(cartGuid, itemId).Item2;
        }


        private Tuple<string, string> GetCartItemValues(string cartGuid, string itemId)
        {
            using (var incomingConn =
                new SqlConnection(ConfigurationManager.ConnectionStrings["Incoming"].ConnectionString))
            {

                var selectSql = @"SELECT TOP 1 [profitClass], [productBasket]
                                                    FROM [dbo].[AssignedCartItemValues]
                                                    WHERE [cartGuid] = @cartGuid AND [itemId] = @itemId";
                incomingConn.Open();
                using (var command = new SqlCommand(selectSql, incomingConn))
                {
                    command.Parameters.AddWithValue("cartGuid", cartGuid);
                    command.Parameters.AddWithValue("itemId", itemId);
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return new Tuple<string, string>(reader.GetString(0),
                                reader.GetBoolean(1) ? "true" : "false");
                        }
                    }
                }

                var countA = 0;
                var countC = 0;
                var countE = 0;
                var countProdBas = 0;
                var countNotProdBas = 0;

                selectSql = @"SELECT COUNT(*) FROM [dbo].[AssignedCartItemValues]
                                                    WHERE [cartGuid] = @cartGuid AND [profitClass] = @profitClass";
                using (var command = new SqlCommand(selectSql, incomingConn))
                {
                    command.Parameters.AddWithValue("cartGuid", cartGuid);
                    command.Parameters.AddWithValue("profitClass", "A");
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            countA = reader.GetInt32(0);
                        }
                    }
                }

                using (var command = new SqlCommand(selectSql, incomingConn))
                {
                    command.Parameters.AddWithValue("cartGuid", cartGuid);
                    command.Parameters.AddWithValue("profitClass", "C");
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            countC = reader.GetInt32(0);
                        }
                    }
                }

                using (var command = new SqlCommand(selectSql, incomingConn))
                {
                    command.Parameters.AddWithValue("cartGuid", cartGuid);
                    command.Parameters.AddWithValue("profitClass", "E");
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            countE = reader.GetInt32(0);
                        }
                    }
                }

                selectSql = @"SELECT COUNT(*) FROM [dbo].[AssignedCartItemValues]
                                                    WHERE [cartGuid] = @cartGuid AND [productBasket] = @productBasket";
                using (var command = new SqlCommand(selectSql, incomingConn))
                {
                    command.Parameters.AddWithValue("cartGuid", cartGuid);
                    command.Parameters.AddWithValue("productBasket", 1);
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            countProdBas = reader.GetInt32(0);
                        }
                    }
                }

                using (var command = new SqlCommand(selectSql, incomingConn))
                {
                    command.Parameters.AddWithValue("cartGuid", cartGuid);
                    command.Parameters.AddWithValue("productBasket", 0);
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            countNotProdBas = reader.GetInt32(0);
                        }
                    }
                }

                var retProfitClass = countA == new int[] { countA, countC, countE }.Min() ? "A" :
                    countC == new int[] { countA, countC, countE }.Min() ? "C" : "E";
                var retProdBas = countProdBas == Math.Min(countProdBas, countNotProdBas) ? "true" : "false";

                selectSql =
                    @"INSERT INTO [dbo].[AssignedCartItemValues] VALUES (@cartGuid, @itemId, @profitClass, @productBasket)";
                using (var command = new SqlCommand(selectSql, incomingConn))
                {
                    command.Parameters.AddWithValue("cartGuid", cartGuid);
                    command.Parameters.AddWithValue("itemId", itemId);
                    command.Parameters.AddWithValue("profitClass", retProfitClass);
                    command.Parameters.AddWithValue("productBasket", retProdBas == "true" ? 1 : 0);
                    command.ExecuteNonQuery();
                }

                return new Tuple<string, string>(retProfitClass, retProdBas);



            }
        }
    }
}