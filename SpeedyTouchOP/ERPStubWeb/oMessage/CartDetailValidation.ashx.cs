﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace ERPStubWeb.oMessage
{
    public class CartDetailValidation : OMessage
    {

        public override XDocument ProcessOMessageRequest(HttpContext context, XDocument xRequest)
        {
            XElement xCartDetailValidationResponse5 = new XElement("cartValidationResponse5",
                                                            new XElement("message", "OK!"),
                                                            new XElement("serialNumber", "1234567890"),
                                                            new XElement("serialNumberRequired", false),
                                                            new XElement("serialNumberValidated", true)
                                                            );
            return new XDocument(xRequest.Declaration, xCartDetailValidationResponse5);
        }
    }
}