﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Xml.Linq;

namespace ERPStubWeb.oMessage
{
    /// <summary>
    /// Summary description for GetDocument
    /// </summary>
    public class GetDocument : OMessage
    {

        public override XDocument ProcessOMessageRequest(HttpContext context, XDocument xRequest)
        {           
            var resp = new XElement("getDocumentResponse");
            try
            {
                var voucherType = xRequest.Descendants("voucherType").FirstOrDefault().Value;
                var userId = xRequest.Descendants("user").FirstOrDefault().Attribute("id").Value;
                var customerId = xRequest.Descendants("customer").FirstOrDefault().Attribute("id").Value;
                var voucherId = xRequest.Descendants("voucher").FirstOrDefault().Attribute("id").Value;

                var content = new XElement("content", new XAttribute("mime-type", "application/pdf"));
                var pdfString = File.ReadAllText(HostingEnvironment.ApplicationPhysicalPath + @"/pdf/sample.pdf")
                    .Replace("{voucherType}", voucherType)
                    .Replace("{userId}", userId)
                    .Replace("{customerId}", customerId)
                    .Replace("{voucherId}", voucherId);
                content.Add(Convert.ToBase64String(Encoding.ASCII.GetBytes(pdfString)));
                resp.Add(content);
            }
            catch
            {
                resp.Add(new XElement("error",new XAttribute("code","500")));
            }
            return new XDocument(xRequest.Declaration, resp);
        }
        
    }
}