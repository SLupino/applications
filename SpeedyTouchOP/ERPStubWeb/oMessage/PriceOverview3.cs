﻿using System.Xml.Linq;
using System.Linq;
using System.Web;

namespace ERPStubWeb.oMessage
{
    public class PriceOverview3 : PriceOverview
    {
        public override XDocument ProcessOMessageRequest(HttpContext context, XDocument xRequest)
        {
            var result = base.ProcessOMessageRequest(context, xRequest);
            var itemPart = result.Descendants("item").FirstOrDefault();
            var itemId = itemPart?.Attribute("id")?.Value;
            var freeOfChargeValue = true;
            var firstItemChar = itemId?.Trim()[0] ?? new char();
            if ("5678".Contains(firstItemChar))
            {
                freeOfChargeValue = false;
            }
            var pricingResponse = result.Descendants("pricingResponse").FirstOrDefault();
            pricingResponse?.AddFirst(new XElement("currency", "USD"));
            pricingResponse?.Add(new XElement("isFreeOfCharge", freeOfChargeValue));

            var discounts = result.Descendants("discount");
            foreach (var discount in discounts)
            {
                discount.Add(new XAttribute("discountType", "resellerDiscount"));
            }

            var root = result.Descendants("priceOverviewResponse").FirstOrDefault();
            if (root != null)
                root.Name = "priceOverviewResponse3";

            return result;
        }
    }
}