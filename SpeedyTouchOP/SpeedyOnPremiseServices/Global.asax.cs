using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace SpeedyOnPremiseServices
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        public static Dictionary<string, StringBuilder> ValidItemLog = new Dictionary<string, StringBuilder>();

        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
