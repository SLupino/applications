﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Helpers.Classes;
using NLog;
using SpeedyOnPremiseServices.EntityModel;

namespace SpeedyOnPremiseServices.Controllers
{
    public class ValidItemsController : ApiController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public string Post([FromBody] ValidItems validItems)
        {
            Logger.Info(
                $"Post: catalogItems count = {validItems?.CatalogItems?.Count}, connection string = '{validItems?.StagingConnectionString}'");

            if (validItems?.CatalogItems?.Count > 0 && !string.IsNullOrEmpty(validItems.StagingConnectionString))
            {
                Task.Factory.StartNew(() =>
                    ProcessRequest(validItems.CatalogItems, validItems.StagingConnectionString));

                //do not change this value
                return "--START--";
            }

            return "Missing data";
        }

        public string Get(string companyCode)
        {
            Logger.Debug("Get: CatalogItems");

            string log;

            if (WebApiApplication.ValidItemLog.ContainsKey(companyCode))
            {
                var globalLogLines = WebApiApplication.ValidItemLog[companyCode];
                Logger.Debug($"Found log for company {companyCode}");
                log = globalLogLines.ToString();

                if (log.Contains("--END--"))
                {
                    WebApiApplication.ValidItemLog.Remove(companyCode);
                    Logger.Trace("Logger {0} removed", log.Length);
                }
                else
                {
                    globalLogLines.Clear();
                }
            }
            else
            {
                //do not change this value
                log = "--END--";
            }

            Logger.Debug("Release log {0}", log.Length);

            return log;
        }

        private async void ProcessRequest(List<CatalogItem> items, string connectionString)
        {
            await Task.Yield();

            var companyCode = items.Select(x => x.CompanyCode).FirstOrDefault() ?? "0000";
            Logger.Trace($"companyCode = {companyCode}");

            if (WebApiApplication.ValidItemLog.ContainsKey(companyCode))
            {
                //globalLogLines = WebApiApplication.ValidItemLog[companyCode];
                Logger.Info($"Instance already started for companyCode = {companyCode}");
                return;
            }

            var globalLogLines = new StringBuilder();
            WebApiApplication.ValidItemLog.Add(companyCode, globalLogLines);

            try
            {
                globalLogLines.AppendLine(string.Format("processing {1} rows for company: {0}", companyCode,
                    items.Count));
                var itemList = items.Select(item => new ValidArticles_Temp
                {
                    ArticleId = item.ArticleId,
                    CompanyCode = item.CompanyCode,
                    ProductId = item.ProductId,
                    isMinPackSize = item.IsMinSize
                }).ToList();

                using (var context = StagingWS1.GetStagingWS1Context(connectionString, globalLogLines))
                {
                    Logger.Trace("TRUNCATE TABLE ValidArticles_Temp");
                    await context.Database.ExecuteSqlCommandAsync("TRUNCATE TABLE ValidArticles_Temp");
                    await context.SaveChangesAsync();
                    globalLogLines.AppendLine("Temporary table cleaned");
                    Logger.Trace("done");

                    const int batchCount = 5000;
                    for (var idx = 0; idx < itemList.Count; idx += batchCount)
                    {
                        var idxEnd = Math.Min(batchCount, itemList.Count - idx);
                        Logger.Trace($"INSERT Item range from {idx} to {idx + idxEnd} in ValidArticles_Temp");
                        context.ValidArticles_Temp.AddRange(itemList.GetRange(idx, idxEnd));
                        await context.SaveChangesAsync();
                        globalLogLines.AppendLine($"{idx + idxEnd} Rows are added in temporary table");
                        Logger.Trace("done");
                    }

                    await context.Database.ExecuteSqlCommandAsync("SET XACT_ABORT ON");
                    await context.SaveChangesAsync();
                    globalLogLines.AppendLine("Set XACT_ABORT ON");

                    Logger.Trace("BEGIN TRANSACTION");
                    await context.Database.ExecuteSqlCommandAsync($@"BEGIN TRANSACTION;
		                                    DELETE ValidArticles WHERE CompanyCode = '{companyCode}'
                                            INSERT INTO ValidArticles SELECT * FROM ValidArticles_Temp WHERE CompanyCode = '{companyCode}'
                                    COMMIT TRANSACTION");
                    await context.SaveChangesAsync();
                    globalLogLines.AppendLine("Rows are updated");
                    Logger.Trace("done");
                    await context.Database.ExecuteSqlCommandAsync("SET XACT_ABORT OFF");
                    await context.SaveChangesAsync();
                    globalLogLines.AppendLine("Set XACT_ABORT OFF");
                }

                //do not change this value
                globalLogLines.AppendLine("--END--");
            }
            catch (Exception e)
            {
                WebApiApplication.ValidItemLog.Remove(companyCode);
                Logger.Error(e, "Exception");
            }
        }
    }
}