﻿using NLog;
using System;
using System.Web.Http;
using Helpers.Classes;

namespace SpeedyOnPremiseServices.Controllers
{
    public class IncomingMonitorController : ApiController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public string Post([FromBody] SimpleString queryInformation)
        {
            var queryInfo = queryInformation.Value.Split('|');
            Logger.Trace("Start Service with request : {0}", queryInfo[0]);
            var dateNow = DateTime.UtcNow;
            var limitDate = dateNow.AddMinutes(-Convert.ToDouble(queryInfo[1]));
            Logger.Trace("date now is {0} subtract: {1}minutes. The result is: {2}", dateNow, queryInfo[1], limitDate);
            var incomingErrors = Helpers.DatabaseHelper.SelectIncomingErrors(Logger, queryInfo[0], limitDate);
            var lastIscRun = Helpers.DatabaseHelper.SelectLastIscRun(Logger, queryInfo[0]);
            var whenIscRun = dateNow.Subtract((Convert.ToDateTime(lastIscRun)));
            return Helpers.XmlHelper.GetIncomingErrors(Logger, incomingErrors, whenIscRun.ToString());
        }
    }
}
