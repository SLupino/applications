﻿using NLog;
using System.Collections.Generic;
using System.Web.Http;
using Helpers.Classes;

namespace SpeedyOnPremiseServices.Controllers
{
    public class StagingMonitorController : ApiController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public List<Job> Post([FromBody] SimpleString simpleString)
        {
            Logger.Info(
                $"Post: connection string = '{simpleString.Value}'");

            return Helpers.DatabaseHelper.SelectStagingsJobInErrorNames(Logger, simpleString.Value);
        }
    }
}
