﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Helpers.Classes;
using NLog;
using SpeedyOnPremiseServices.Helpers;

namespace SpeedyOnPremiseServices.Controllers
{
    public class OrderReportsController : ApiController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public string Post([FromBody] OrderReportRequest orderReportRequest)
        {
            var companyCode = orderReportRequest.CompanyCode;
            try
            {
                //assign company data into variables
                Logger.Trace("Start Service with request for company: {0}", companyCode);

                var stagingConnection = orderReportRequest.StagingConnectionString;
                var incomingConnection = orderReportRequest.IncomingConnectionString;
                Logger.Trace("Start SQL Queries staging connection: {0} AND incoming connection {1}", stagingConnection,
                    incomingConnection);
                var listUserClass = DatabaseHelper.SelectStagingUsers(Logger, stagingConnection, incomingConnection);
                Logger.Trace("success staging query");
                var lastMonthOrders = DatabaseHelper.SelectOrderForLastMonth(Logger, incomingConnection);
                Logger.Trace("success last month order query");
                var yesterdayOrders = DatabaseHelper.SelectOrderForYesterday(Logger, incomingConnection, -1,
                    listUserClass, stagingConnection);
                Logger.Trace("success yesterday order query");
                var saturdayOrders = new List<OrderUserStatistics>();
                var fridayOrders = new List<OrderUserStatistics>();

                //if today is monday must send friday, saturday and sunday orders
                if (DateTime.Now.DayOfWeek == DayOfWeek.Monday)
                {
                    saturdayOrders = DatabaseHelper.SelectOrderForYesterday(Logger, incomingConnection, -2,
                        listUserClass, stagingConnection);
                    Logger.Trace("success saturday order query");
                    fridayOrders = DatabaseHelper.SelectOrderForYesterday(Logger, incomingConnection, -3, listUserClass,
                        stagingConnection);
                    Logger.Trace("success friday order query");
                }

                var lastMonthAdmOrders =
                    DatabaseHelper.SelectAdmOrderForLastMonth(Logger, listUserClass, incomingConnection);
                Logger.Trace("success last month order query");
                var dateNow = DateTime.Now;
                var monthlyOrders = new List<OrderUserStatistics>();
                if (orderReportRequest.ForceMonthlyReport || dateNow.Day == 1 ||
                    dateNow.DayOfWeek == DayOfWeek.Monday && (dateNow.Day == 2 || dateNow.Day == 3))
                    monthlyOrders = DatabaseHelper.SelectMonthlyOrders(Logger, listUserClass, incomingConnection)
                        .ToList();
                Logger.Trace("success monthly order query");
                return XmlHelper.GetOrderInfo(Logger, companyCode, lastMonthOrders.ToList(), yesterdayOrders, saturdayOrders,
                    fridayOrders, lastMonthAdmOrders.ToList(), monthlyOrders);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Exception in GetOrderData with companyInfo = {0}", companyCode);
                throw;
            }
        }
    }
}