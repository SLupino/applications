﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Helpers.Classes;
using NLog;

namespace SpeedyOnPremiseServices.Helpers
{
    public static class DatabaseHelper
    {
        private static int _maxStringCount;

        public static List<Job> SelectStagingsJobInErrorNames(Logger logger, string stagingConnection)
        {
            const string selectJobWithProblems =
                @"WITH CTE AS (SELECT step_name, message, run_date, run_time, run_status, job_id, step_id,
    ROW_NUMBER() OVER(PARTITION BY job_id, step_id ORDER BY run_date DESC, run_time DESC) as rn 
FROM MSDB.DBO.SYSJOBHISTORY 
WHERE step_name <> '(Job outcome)')
SELECT sysjobs.name as 'job_name', cte.step_name, cte.message, cte.run_date, cte.run_time, sysjobs.enabled as 'JobEnabled', cte.run_status, sysschedules.enabled as 'SchedulerEnabled'
FROM msdb.dbo.sysjobs 
	JOIN  MSDB.[dbo].[sysjobschedules] ON sysjobs.JOB_ID = [sysjobschedules].JOB_ID
	join [dbo].[sysschedules] on sysjobschedules.schedule_id = sysschedules.schedule_id
	LEFT JOIN CTE ON CTE.job_id = sysjobs.job_id AND CTE.rn = 1 AND (CTE.run_status <> 1)
WHERE sysjobs.enabled <> 1 OR CTE.run_status IS NOT NULL or sysschedules.enabled <> 1
ORDER BY sysjobs.name, step_id";

            logger.Trace("Start using sql connection {0}", stagingConnection);
            var jobs = new List<Job>();
            try
            {
                using (var connection = new SqlConnection(stagingConnection))
                {
                    connection.Open();
                    logger.Trace("Connection is opened");
                    using (var command = new SqlCommand(selectJobWithProblems, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            logger.Trace("Start using data reader");
                            while (reader.Read())
                            {
                                if (logger.IsTraceEnabled)
                                {
                                    var sb = new StringBuilder();
                                    for (var i = 0; i < reader.FieldCount; i++)
                                        sb.AppendFormat("{0}: '{1}', ", reader.GetName(i), reader[i]);
                                    logger.Trace(sb.ToString());
                                }


                                jobs.Add(new Job
                                {
                                    Name = reader[0].ToString(),
                                    StepName = reader[1].ToString(),
                                    Message = reader[2].ToString(),
                                    RunDate = reader[3].ToString(),
                                    RunTime = reader[4].ToString(),
                                    Enabled = Convert.ToBoolean(
                                        Convert.ToInt16(reader.IsDBNull(5) ? "1" : reader[5].ToString())),
                                    Status = Convert.ToBoolean(
                                        Convert.ToInt16(reader.IsDBNull(6) ? "1" : reader[6].ToString())),
                                    SchedulerEnabled =
                                        Convert.ToBoolean(
                                            Convert.ToInt16(reader.IsDBNull(7) ? "1" : reader[7].ToString()))
                                });
                            }
                        }
                    }
                }

                return jobs;
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw new Exception("Exception while reading from jobs query", e);
            }
        }

        public static List<OrderUserStatistics> SelectStagingUsers(Logger logger, string stagingConnection, string incomingConnection)
        {
            var users = new List<OrderUserStatistics>();
            try
            {
                using (var connection = new SqlConnection(stagingConnection))
                {
                    logger.Trace("Start using sql connection {0}", stagingConnection);
                    var command =
                        new SqlCommand(
                            string.Format(Queries.SelectUserIdAndName,
                                string.Join(",", GetAllCompanyUsers(logger, incomingConnection))), connection);
                    connection.Open();
                    logger.Trace("Connection is opened");
                    using (var reader = command.ExecuteReader())
                    {
                        try
                        {
                            logger.Trace("Start using data reader");
                            while (reader.Read())
                            {
                                var row = (IDataRecord) reader;
                                users.Add(new OrderUserStatistics
                                {
                                    ManagerId = row[0] == DBNull.Value ? "null" : row[0].ToString(),
                                    UserId = row[1].ToString(),
                                    UserName = row[2].ToString(),
                                    //WnNumber = row[3].ToString(),
                                    UserType = row[3].ToString() == "200" ? "AM" : "ADM"
                                });
                            }
                        }
                        finally
                        {
                            reader.Close();
                            logger.Trace("Connection is closed");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Trace("{0}", e.Message);
            }

            return users;
        }

        private static List<string> GetAllCompanyUsers(Logger logger, string incomingConnection)
        {
            var users = new List<string>();

            try
            {
                using (var connection = new SqlConnection(incomingConnection))
                {
                    logger.Trace("Start using sql connection {0}", incomingConnection);
                    var command = new SqlCommand(Queries.SelectAllUsersInLastTwoMonth, connection);
                    connection.Open();
                    logger.Trace("Connection is opened");
                    using (var reader = command.ExecuteReader())
                    {
                        try
                        {
                            logger.Trace("Start using data reader");
                            while (reader.Read())
                            {
                                var row = (IDataRecord) reader;
                                users.Add("'" + row[0] + "'");
                            }
                        }
                        finally
                        {
                            reader.Close();
                            logger.Trace("Connection is closed");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Trace("{0}", e.Message);
            }

            return users;
        }

        public static List<OrderUserStatistics> SelectOrderForYesterday(Logger logger, string incomingConnection,
            int days, List<OrderUserStatistics> users, string stagingConnection)
        {
            var label = days == -1 ? "Yesterday" : days == -2 ? "Saturday" : "Friday";
            logger.Debug("Select orders for {0}", label);

            var usersForCheck = new List<OrderUserStatistics>();
            var usersForAdd = new List<OrderUserStatistics>();
            var yS = new List<OrderUserStatistics>();
            try
            {
                usersForCheck.AddRange(users.Where(x => x.ManagerId == "null" && x.UserType != "AM").OrderBy(x => x.UserId)
                    .ToList());
                usersForCheck.AddRange(users.Where(x => x.ManagerId == "null" && x.UserType == "AM").OrderBy(x => x.UserId)
                    .ToList());
                usersForCheck.AddRange(users.Where(x => x.ManagerId != "null" && x.UserType != "AM").OrderBy(x => x.UserId)
                    .ToList());
                users = usersForCheck;
                logger.Debug("{0} users to check: {1} ", days, users.Count);

                foreach (var user in users)
                {
                    if (yS.Count(x => x.UserId == user.UserId) <= 0)
                    {
                        logger.Trace("User {0} order", user.UserId);
                        yS.Add(new OrderUserStatistics
                        {
                            ManagerId = user.ManagerId, UserId = user.UserId, UserName = user.UserName, OrderCount = 0,
                            UserType = user.UserType
                        });
                    }

                    if (user.UserName.Length > _maxStringCount)
                        _maxStringCount = user.UserName.Length;
                }

                logger.Trace("{0} list count {1}", label, yS.Count);
                using (var connection = new SqlConnection(incomingConnection))
                {
                    logger.Trace("Start using sql connection {0}", incomingConnection);
                    var command = new SqlCommand(string.Format(Queries.SelectOrderForYesterday, days), connection);
                    connection.Open();
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var row = (IDataRecord) reader;
                            logger.Trace("User {0} order {1}", row[0], row[1]);
                            var valueForChange = yS.FirstOrDefault(x => x.UserId == row[0].ToString());
                            if (valueForChange != null)
                            {
                                valueForChange.OrderCount = (int) row[1];
                                logger.Trace("User ID {0} User Name {1} order {2}", valueForChange.UserId,
                                    valueForChange.UserName, valueForChange.OrderCount);
                            }
                            else
                            {
                                usersForAdd.Add(new OrderUserStatistics
                                    {UserId = row[0].ToString(), OrderCount = (int) row[1]});
                            }
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }

                    logger.Trace("End {0} order", label);
                }
            }
            catch (Exception e)
            {
                logger.Error(e, "{0} users to check: {1}", days, usersForCheck.Count);
            }

            try
            {
                logger.Debug("{0} users to add: {1}", days, usersForAdd.Count);
                if (usersForAdd.Count > 0)
                    using (var connection = new SqlConnection(stagingConnection))
                    {
                        logger.Trace("Start using sql connection {0}", stagingConnection);
                        var command =
                            new SqlCommand(
                                string.Format(Queries.SelectUserIdAndName,
                                    string.Join(",", usersForAdd.Select(x => "'" + x.UserId + "'").Distinct())),
                                connection);
                        logger.Trace("query is : {0}", command.CommandText);
                        connection.Open();
                        logger.Trace("Connection is opened");
                        using (var reader = command.ExecuteReader())
                        {
                            try
                            {
                                logger.Trace("Start using data reader");
                                while (reader.Read())
                                {
                                    var row = (IDataRecord) reader;
                                    var valueForChange =
                                        usersForAdd.FirstOrDefault(x => x.UserId == row[1].ToString());
                                    if (valueForChange != null)
                                    {
                                        logger.Trace("start add user");
                                        valueForChange.ManagerId = row[0] == DBNull.Value
                                            ? "null"
                                            : row[0].ToString();
                                        valueForChange.UserName = row[2].ToString();
                                        valueForChange.UserType = row[4].ToString() == "200" ? "AM" : "ADM";
                                    }
                                }
                            }
                            finally
                            {
                                reader.Close();

                                yS.AddRange(usersForAdd);
                                logger.Trace("Connection is closed");
                            }
                        }
                    }
            }
            catch (Exception e)
            {
                logger.Error(e, "{0} users to add: {1}", days, usersForAdd.Count);
            }

            return yS;
        }

        public static IEnumerable<OrderMonthStatistics> SelectOrderForLastMonth(Logger logger, string incomingConnection)
        {
            var lMs = new List<OrderMonthStatistics>();
            try
            {
                using (var connection = new SqlConnection(incomingConnection))
                {
                    logger.Trace("Start using sql connection {0}", incomingConnection);
                    var command = new SqlCommand(Queries.SelectOrderForLastMonth, connection);
                    connection.Open();
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var row = (IDataRecord) reader;
                            lMs.Add(new OrderMonthStatistics {Date = (DateTime) row[0], OrderCount = (int) row[1]});
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }

                logger.Trace("End last month order");
            }
            catch (Exception e)
            {
                logger.Trace("{0}", e.Message);
            }

            return lMs.OrderBy(x => x.Date).ToList();
        }

        public static Dictionary<string, string> SelectIncomingErrors(Logger logger, string incomingConnection,
            DateTime date)
        {
            logger.Debug("Select Incoming Errors");

            var incomingErrors = new Dictionary<string, string>();
            try
            {
                using (var connection = new SqlConnection(incomingConnection))
                {
                    logger.Trace("Using sql connection {0}", incomingConnection);
                    var command = new SqlCommand(Queries.SelectIncomingErrors, connection);
                    command.Parameters.AddWithValue("date", date);
                    connection.Open();
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var row = (IDataRecord) reader;
                            incomingErrors.Add(row[0].ToString(), row[1].ToString());
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            catch (Exception e)
            {
                logger.Trace("{0}", e.Message);
            }

            return incomingErrors;
        }

        public static string SelectLastIscRun(Logger logger, string incomingConnection)
        {
            logger.Debug("Select Last ISC Run");
            var lastIscRun = string.Empty;
            try
            {
                using (var connection = new SqlConnection(incomingConnection))
                {
                    logger.Trace("Using sql connection {0}", incomingConnection);
                    var command = new SqlCommand(Queries.SelectLastIscRun, connection);
                    connection.Open();
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var row = (IDataRecord) reader;
                            lastIscRun = row[0].ToString();
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            catch (Exception e)
            {
                logger.Trace("{0}", e.Message);
            }

            return lastIscRun;
        }

        public static IEnumerable<OrderUserStatistics> SelectMonthlyOrders(Logger logger, List<OrderUserStatistics> users,
            string incomingConnection)
        {
            var yS = new List<OrderUserStatistics>();
            try
            {
                var idForQuery = string.Join(",", users.Select(u => $"'{u.UserId}'"));

                foreach (var user in users)
                {
                    if (yS.All(x => x.UserId != user.UserId))
                    {
                        logger.Trace("User {0} order", user.UserId);
                        yS.Add(new OrderUserStatistics
                            {UserId = user.UserId, UserName = user.UserName, OrderCount = 0, UserType = user.UserType});
                    }

                    if (user.UserName.Length > _maxStringCount)
                        _maxStringCount = user.UserName.Length;
                }

                if (idForQuery != "")
                {
                    using (var connection = new SqlConnection(incomingConnection))
                    {
                        logger.Trace("Start using sql connection {0}", incomingConnection);
                        var command = new SqlCommand(string.Format(Queries.SelectMonthlyOrders, idForQuery), connection);
                        connection.Open();
                        var reader = command.ExecuteReader();
                        try
                        {
                            while (reader.Read())
                            {
                                var row = (IDataRecord) reader;
                                logger.Trace("User {0} order {1}", row[0], row[1]);
                                var valueForChange = yS.FirstOrDefault(x => x.UserId == row[0].ToString());
                                if (valueForChange != null)
                                {
                                    valueForChange.OrderCount = (int) row[1];
                                    logger.Trace("User Id {0} User Name {1} order {2}", valueForChange.UserId,
                                        valueForChange.UserName, valueForChange.OrderCount);
                                }
                            }
                        }
                        finally
                        {
                            reader.Close();
                        }
                    }
                }

                logger.Trace("End monthly order");
            }
            catch (Exception e)
            {
                logger.Trace("{0}", e.Message);
            }

            return yS;
        }

        public static IEnumerable<OrderUserStatistics> SelectAdmOrderForLastMonth(Logger logger, List<OrderUserStatistics> users,
            string incomingConnection)
        {
            var yS = new List<OrderUserStatistics>();
            try
            {
                var admList = users.Where(user => user.UserType == "ADM").ToList();
                var idForQuery = string.Join(",", admList.Select(u => $"'{u.UserId}'"));

                foreach (var user in admList)
                {
                    if (yS.All(x => x.UserId != user.UserId))
                    {
                        logger.Trace("User {0} order", user.UserId);
                        if (yS.Count(x => x.UserId == user.UserId) == 0)
                            yS.Add(new OrderUserStatistics
                                {UserId = user.UserId, UserName = user.UserName, OrderCount = 0, UserType = user.UserType});
                    }

                    if (user.UserName.Length > _maxStringCount)
                        _maxStringCount = user.UserName.Length;
                }

                if (idForQuery != "")
                    using (var connection = new SqlConnection(incomingConnection))
                    {
                        logger.Trace("Start using sql connection {0}", incomingConnection);
                        var command = new SqlCommand(string.Format(Queries.SelectLastMonthAdm, idForQuery), connection);
                        connection.Open();
                        var reader = command.ExecuteReader();
                        try
                        {
                            while (reader.Read())
                            {
                                var row = (IDataRecord) reader;
                                logger.Trace("User {0} order {1}", row[0], row[1]);
                                var valueForChange = yS.FirstOrDefault(x => x.UserId == row[0].ToString());
                                if (valueForChange != null)
                                {
                                    valueForChange.OrderCount = (int) row[1];
                                    logger.Trace("User Id {0} User Name {1} order {2}", valueForChange.UserId,
                                        valueForChange.UserName, valueForChange.OrderCount);
                                }
                            }
                        }
                        finally
                        {
                            reader.Close();
                        }
                    }

                logger.Trace("End adm last month order");
            }
            catch (Exception e)
            {
                logger.Trace("{0}", e.Message);
            }

            return yS;
        }

        //private static string GenerateGuid(string id)
        //{
        //    if(id.Length == 36) return $"'{id}'";

        //    var guidTemplate = "'00000000-0000-0000-0000-";
        //    var charCount = 12 - id.Count();

        //    for (var i = charCount; i > 0; i--)
        //        guidTemplate += "0";

        //    guidTemplate += id + "'";

        //    return guidTemplate;
        //}
    }
}