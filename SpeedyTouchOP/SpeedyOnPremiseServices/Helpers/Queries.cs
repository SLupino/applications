﻿namespace SpeedyOnPremiseServices.Helpers
{
    public static class Queries
    {
        public static string SelectAllUsersInLastTwoMonth = @"SELECT distinct  UserId
FROM [dbo].[MSG_AsyncMessageQueue]
where CreationTime >= dateadd(day, -60, GETUTCDATE())";

        public static string SelectUserIdAndName = @"
        	with cteUsers as
    (
        select distinct
            u.id , u.name
        from
            Users u with (NOLOCK)
            join
                UsersKey uk
                on
                    u._boId = uk.appliedBy
    )
select distinct
    case when (m.salesManagerUserId = u.id) then null else m.salesManagerUserId end as 'ManagerId' , u.id , u.name , case when (m.salesManagerUserId = u.id) then '200' else '100' end as 'userType' 
from
    cteUsers u
    left join
        (
            select srm.salesManagerUserId,srm.salesRepUserId,operation
            from
                [dbo].[SalesRepManagerRelations] srm
                join
                    [dbo].[SalesRepManagerRelationsKey] srmk
                    on
                        srm._boId = srmk.appliedBy
						where  srm.operation='N'
        )
        m
        on
            u.id = m.salesRepUserId or u.id = m.salesManagerUserId
where id in ({0})";

        public static string SelectOrderForYesterday = @"
        DECLARE @now datetime;
        DECLARE @dateFrom datetime;
        SET @now = GETUTCDATE();
        SET @dateFrom = DATEADD(day , -30 , @now);

        PRINT @now
        PRINT @dateFrom

        SELECT UserId, count(*)
        FROM [dbo].[MSG_AsyncMessageQueue]
        WHERE RequestType like 'sendCartRequest%'
        AND  Request.value('(/*/cart/cartVoucherType)[1]', 'varchar(max)') like '%ord%'
        and CONVERT(date, CreationTime) = CONVERT(date, DATEADD(day , {0} , @now))
        group by UserId";

        public static string SelectOrderForLastMonth = @"
        DECLARE @now datetime;
        DECLARE @dateFrom datetime;
        DECLARE @days int;
        PRINT @now
        PRINT @dateFrom
        SET @now = GETUTCDATE();
        SET @dateFrom = DATEADD(day , -30 , CONVERT(date, @now));
        SET @days= -30

        declare @tableDays table
        (
        date date
        )

        declare @orders table
        (
        date date,
        numberOfOrders int
        )

        while @days < 0
        begin
               insert into @tableDays
               values(DATEADD(day , @days , CONVERT(date, @now)));
               set @days = @days + 1;
        End

        INSERT INTO @orders SELECT CONVERT(date, CreationTime) as [Date],count(*)
        FROM  [dbo].[MSG_AsyncMessageQueue] que  
        where
        RequestType like 'sendCartRequest%'
        and  Request.value('(/*/cart/cartVoucherType)[1]', 'varchar(max)') = 'order'
        AND DATEADD(day , -30 , CONVERT(date, @now)) < CreationTime
        group by CONVERT(date, CreationTime)

        select t.date, CASE WHEN numberOfOrders is NULL then 0 ELSE numberOfOrders END
        from @tableDays t left outer join @orders o
        on o.date  = t.date
        order by t.date desc";

        public static string SelectMonthlyOrders = @"
        DECLARE @now datetime = GETUTCDATE();
        DECLARE @dateFrom datetime = DATEADD(month , -1 , @now);

        SELECT UserId, count(*)
        FROM [dbo].[MSG_AsyncMessageQueue]
        WHERE RequestType like 'sendCartRequest%'
        AND  Request.value('(/*/cart/cartVoucherType)[1]', 'varchar(max)') like '%ord%'
        and UserId in ({0})
        and MONTH(CONVERT(date, @dateFrom)) = MONTH(CONVERT(date, CreationTime)) and YEAR(CONVERT(date, @dateFrom)) = YEAR(CONVERT(date, CreationTime))
        group by UserId
        order by UserId";

        public static string SelectLastMonthAdm = @"
        DECLARE @now datetime;
        DECLARE @dateFrom datetime;
        DECLARE @days int;
        PRINT @now
        PRINT @dateFrom
        SET @now = GETUTCDATE();
        SET @dateFrom = DATEADD(day , -30 , CONVERT(date, @now));
        SET @days= -30

        SELECT UserId, count(*)
        FROM [dbo].[MSG_AsyncMessageQueue]
        WHERE RequestType like 'sendCartRequest%'
        AND  Request.value('(/*/cart/cartVoucherType)[1]', 'varchar(max)') = 'order'
        and UserId in ({0})
        and CONVERT(date, @dateFrom) <= CONVERT(date, CreationTime) and CONVERT(date, CreationTime) < CONVERT(date, @now)
        group by UserId
        order by UserId";

        public static string SelectIncomingErrors = @"SELECT RequestType,count(*)                                               
        FROM [MSG_AsyncMessageQueue]
        where  Status > -1 and ExecutionTime is not null and CreationTime <= @date group by RequestType";

        public static string SelectLastIscRun = @"select max(executionTime) from [MSG_AsyncMessageQueue]";
    }
}
