﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Helpers.Classes;
using NLog;

namespace SpeedyOnPremiseServices.Helpers
{
    public static class XmlHelper
    {
        public static string GetOrderInfo(Logger logger, string companyCode, List<OrderMonthStatistics> lMs,
            List<OrderUserStatistics> yS, List<OrderUserStatistics> sS, List<OrderUserStatistics> fS,
            List<OrderUserStatistics> lastMonthAdm, List<OrderUserStatistics> lMonthlyReport)
        {
            logger.Trace("Start xml responce generator");
            var xOrderInfo = new XElement("OrderInfo", new XElement("CompanyCode", companyCode),
                new XElement("LastMonthData"),
                new XElement("YesterdayData"));

            AddLastMonthDataElements(xOrderInfo, lMs, "LastMonthData");

            AddUsersData(xOrderInfo, yS, "YesterdayData");

            if (sS.Count > 0)
            {
                xOrderInfo.Add(new XElement("SaturdayData"));
                AddUsersData(xOrderInfo, sS, "SaturdayData");
            }

            if (fS.Count > 0)
            {
                xOrderInfo.Add(new XElement("FridayData"));
                AddUsersData(xOrderInfo, fS, "FridayData");
            }

            xOrderInfo.Add(new XElement("LastMonthAdmData"));
            AddUsersData(xOrderInfo, lastMonthAdm, "LastMonthAdmData");

            xOrderInfo.Add(new XElement("MonthlyData"));
            AddUsersData(xOrderInfo, lMonthlyReport, "MonthlyData");

            logger.Trace("End xml responce generator");
            return xOrderInfo.ToString();
        }

        public static string GetIncomingErrors(Logger logger, Dictionary<string, string> reqInError, string lastIscRun)
        {
            logger.Trace("Start xml responce generator");
            var xOrderInfo = new XElement("ErrorInfo");
            var xRequestsInError = new XElement("RequestsInError");
            var xLastIscRun = new XElement("LastIscRun", lastIscRun);
            //var xTime = new XElement("Time...");
            AddErrorData(xRequestsInError, reqInError);
            xOrderInfo.Add(xRequestsInError);
            xOrderInfo.Add(xLastIscRun);

            return xOrderInfo.ToString();
        }

        public static string GetJobsInError(Logger logger, List<Job> jobsInError)
        {
            logger.Trace("Start xml responce generator");
            var xJobsInError = new XElement("JobsInError");
            var xJobs = new XElement("Jobs");

            foreach (var jobInError in jobsInError)
            {
                var xJob = new XElement("Job",
                    new XElement("Name", jobInError.Name),
                    new XElement("StepName", jobInError.StepName),
                    new XElement("Message", jobInError.Message),
                    new XElement("RunDate", jobInError.RunDate),
                    new XElement("RunTime", jobInError.RunTime),
                    new XElement("Enabled", jobInError.Enabled));
                xJobs.Add(xJob);
            }

            xJobsInError.Add(xJobs);
            return xJobsInError.ToString();
        }

        private static void AddLastMonthDataElements(XElement xOrderInfo, List<OrderMonthStatistics> lMs, string pathName)
        {
            var xLastMonthData = xOrderInfo.XPathSelectElement($"//{pathName}");
            lMs.ForEach(dayOrder => xLastMonthData?.Add(new XElement("Statistics", new XElement("Date", dayOrder.Date),
                new XElement("OrderCount", dayOrder.OrderCount))));
        }

        private static void AddUsersData(XElement xOrderInfo, List<OrderUserStatistics> yS, string pathName)
        {
            var xYesterdayData = xOrderInfo.XPathSelectElement($"//{pathName}");
            yS.ForEach(oldDayOrder => xYesterdayData?.Add(new XElement("User",
                new XElement("ManagerId", oldDayOrder.ManagerId), new XElement("Id", oldDayOrder.UserId),
                new XElement("Name", oldDayOrder.UserName),
                new XElement("OrderCount", oldDayOrder.OrderCount),
                new XElement("Type", oldDayOrder.UserType))));
        }

        private static void AddErrorData(XElement xRequestInError, Dictionary<string, string> eL)
        {
            eL.Keys.ToList().ForEach(key => xRequestInError.Add(new XElement(key, eL[key])));
        }
    }
}