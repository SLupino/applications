﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NLog;

namespace CheckServiceTierOnSqlAzure
{
    internal class Settings
    {
        public static readonly Logger Logger = LogManager.GetLogger(typeof(Program).FullName);

        public static List<string> SmtpConfig = new List<string>
        {
            ConfigurationManager.AppSettings["SMTPServer"],
            ConfigurationManager.AppSettings["SMTPUser"],
            ConfigurationManager.AppSettings["SMTPPassword"],
            ConfigurationManager.AppSettings["SMTPDomain"]
        };

        public static string FromEmail = ConfigurationManager.AppSettings["FromEmail"];

        public static List<string> ToEmails = ConfigurationManager.AppSettings["ToEmail"].Split(';').ToList();
        public static List<string> CcEmails = ConfigurationManager.AppSettings["CCEmail"].Split(';').ToList();

        public static string Subject = ConfigurationManager.AppSettings["SubjectEmail"];

        public static string EnvironmentType(string connectionStringName)
        {
            return ConfigurationManager.AppSettings[connectionStringName];
        }

        public static string Body(List<TableClass> dbLevels)
        {
            var body = dbLevels.Aggregate(@"<style type=""text/css"">
table, th, td {
    border: 1px solid lightgrey;
    border-collapse: collapse;
	padding: 5px;
}
table#tbl1 tr:nth-child(even) {
    background-color: #F9F9F9;
}
table#tbl1 tr:nth-child(odd) {
    background-color: #fff;
}
table th {
    color: white;
    background-color: #C0C0D4;
}
</style>

Hi All,<br/>
<br/>
For pricing details you can <a href=""http://azure.microsoft.com/en-us/pricing/details/sql-database/"">click here</a>
<br/>
<br/>
To manage databases open the <a href=""http://prmsptouchdev/"">Environment Portal</a>

<br/>
<p>The following table shows the status of Azure cloud databases of our customers.</p><br/>
<table style='width:100%'>
    <tr>
        <th>DbName</th>
        <th>DbLevel</th>
        <th>BaseLevel</th>
        <th>AVG_CPU</th>
        <th>AVG_Data</th>
        <th>AVG_Log</th>
        <th>MAX_CPU</th>
        <th>MAX_Data</th>
        <th>MAX_Log</th>
    </tr> ", (current, dbLevel) => current + CreateTableInfo(dbLevel));
            body += "</table><br/><p>Best Regards</p>";
            return body;
        }

        private static string CreateTableInfo(TableClass dbLevel)
        {
            return string.Format(@"
    <tr>
        <td align='left' valign='center'>{6}&nbsp;{7}</td>
        <td align='center' valign='center'>{8}</td>
	    <td align='right' valign='center'>{9}</td>
        <td align='right' valign='center'>{0}</td>
        <td align='right' valign='center'>{1}</td>
	    <td align='right' valign='center'>{2}</td>
	    <td align='right' valign='center'>{3}</td>
	    <td align='right' valign='center'>{4}</td>
	    <td align='right' valign='center'>{5}</td>
	   
    </tr>", SetColor(dbLevel.AvgCpu, 80, 20), SetColor(dbLevel.AvgData, 80, 20),
                SetColor(dbLevel.AvgLog, 80, 20), SetColor(dbLevel.MaxCpu, 95, 20),
                SetColor(dbLevel.MaxData, 95, 20), SetColor(dbLevel.MaxLog, 95, 20),
                dbLevel.StringColour, dbLevel.DbName,
                SetDbLevelColour(dbLevel.EnvironmentType, dbLevel.DbLevel, dbLevel.DbBaseLevel), dbLevel.DbBaseLevel);
        }

        private static string SetColor(double dbInfo, int maxRange, int minRange)
        {
            return
                $"<font color='{(maxRange <= dbInfo ? "red" : minRange <= dbInfo && dbInfo < maxRange ? "green" : "#ffbf00")}'>{dbInfo}%</font>";
        }

        private static string SetDbLevelColour(string dbType, string dbLevel, string dbBaseLevel)
        {
            try
            {
                var colorCheck = (dbType.ToLower().Contains("test") || dbType.ToLower().Contains("dev")) &&
                                 TableClass.GetLevelOrder(dbLevel) > TableClass.GetLevelOrder("P2");

                colorCheck = dbLevel != dbBaseLevel || colorCheck;

                return string.Format("<font color='{1}'>{0}</font>", dbLevel, colorCheck ? "red" : "black");
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return string.Format("<font color='{1}'>{0}</font>", dbLevel, "darkblue");
            }
        }
    }
}