﻿using System;
using System.Collections.Generic;

namespace CheckServiceTierOnSqlAzure
{
    public class TableClass
    {
        private static readonly Dictionary<string, int> LevelMapping = new Dictionary<string, int>
        {
            {"?",  12},
            {"P11", 11},
            {"P6",  10},
            {"P4",  9},
            {"P3",  8},
            {"P2",  7},
            {"P1",  6},
            {"ElasticPool", 5},
            {"S4",  4},
            {"S3",  3},
            {"S2",  2},
            {"S1",  1},
            {"S0",  0}
        };

        internal static int GetLevelOrder(string level)
        {
            return LevelMapping[level];
        }

        public string DbName { get; set; }
        private string _dbLevel;
        private string _dbBaseLevel;

        public string DbLevel {
            get => _dbLevel;
            set
            {
                _dbLevel = value;
                DbOrderedLevel = LevelMapping.ContainsKey(value) ? LevelMapping[value] : 0;
            }
        }

        public string DbBaseLevel{
            get => _dbBaseLevel;
            set
            {
                _dbBaseLevel = value;
                DbOrderedBaseLevel = LevelMapping.ContainsKey(value) ? LevelMapping[value] : 0;
            }
        }
        public int DbOrderedLevel { get; private set; }
        public int DbOrderedBaseLevel { get; private set; }
        public double AvgCpu { get; set; }
        public double AvgData { get; set; }
        public double AvgLog { get; set; }
        public double AvgMemory { get; set; }
        public double MaxCpu { get; set; }
        public double MaxData { get; set; }
        public double MaxLog { get; set; }
        public double MaxMemory { get; set; }
        public DateTime Timestamp { get; set; }
        public string StringColour { get; set; }
        public string EnvironmentType { get; set; }

    }
}
