﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using Helpers;
using NLog;

namespace CheckServiceTierOnSqlAzure
{
    internal class Program
    {
        private static readonly Logger Logger = LogManager.GetLogger(typeof(Program).FullName);

        private static void Main()
        {
            var dbLevels = new List<TableClass>();


            using (var dt = GetSqlTable(GetSqlCommand(@"SELECT [DatabaseName]
                      ,[AdminAccount]
                      ,[AdminPassword]
                      ,[ServerName]
                      ,[TypeName]
                      ,[Environment]
                      ,[Acronimo]
                      ,[isSysDb]
                      ,[BaseLevel]
                  FROM [SpeedyTouchEnvironments].[dbo].[View_SqlDatabases];")))
            {
                foreach (DataRow row in dt.Rows)
                {
                    bool.TryParse(row["isSysDb"].ToString(), out var isSys);

                    var dataSource = row["ServerName"].ToString().Contains('.')
                        ? row["ServerName"].ToString()
                        : (row["ServerName"] + ".database.windows.net");

                    Logger.Trace("The Datasource is " + dataSource);

                    var type = row["isSysDb"] == null ? "Backup" : isSys ? "System" : "Consol";
                    var connectionString = new ConnectionStringSettings(
                        $"{row["Environment"]} {type} {row["TypeName"]}",
                        $"Data Source={dataSource};Initial Catalog={row["DatabaseName"]};User ID={row["AdminAccount"]};Password={row["AdminPassword"]};");

                    try
                    {
                        var dbInfo = GetDbLevelInfo(connectionString, row["TypeName"].ToString(),
                            row["BaseLevel"].ToString());

                        //Logger.Trace("Update EnvironmentsSql set BaseLevel = '{0}' WHERE DatabaseName = '{1}' AND ServerName = '{2}';", dbInfo.DbLevel, row["DatabaseName"], row["ServerName"]);

                        dbLevels.Add(dbInfo);
                    }
                    catch (Exception e)
                    {

                        var dbInfo = new TableClass() {DbBaseLevel = row["BaseLevel"].ToString(), DbName = connectionString.Name, DbLevel = "?", EnvironmentType = row["TypeName"].ToString(), StringColour = "<font>&#128561;</font>"};
                        dbLevels.Add(dbInfo);

                        Logger.Error(e, $"{e.Message} server {row["ServerName"]} database {row["DatabaseName"]}" );
                    }
                }
            }

            var deltaDbLevel = dbLevels.Where(x => !x.DbLevel.Equals(x.DbBaseLevel)).OrderByDescending(x => x.DbOrderedLevel);
            var others = dbLevels.Where(i => !deltaDbLevel.Contains(i)).OrderByDescending(x => x.DbOrderedLevel);

            dbLevels = deltaDbLevel.Concat(others).ToList();

            EmailHelper.SendEmail(Settings.SmtpConfig, Settings.FromEmail, Settings.ToEmails, Settings.CcEmails,
                new List<string>(), Settings.Subject,
                Settings.Body(dbLevels));
            //dbLevels.OrderByDescending(x=>!x.DbLevel.Equals(x.DbBaseLevel)).ThenBy(x => x.DbLevel).ToList()
        }

        public static SqlCommand GetSqlCommand(string format, params object[] args)
        {
            var sql = new SqlCommand();

            try
            {
                //Sostituisce i placeholder standard in quelli parametrizzati per sql.
                format = format.Replace("'{", "@param").Replace("{", "@param").Replace("}'", "").Replace("}", "");

                //Divide la stringa nelle sezioni del filtro che dovranno essere lavorate.
                var filtri =
                    new List<string>(Regex.Split(format, @"( where )|( and )|( or )", RegexOptions.IgnoreCase));

                //Cicla per ogni argomento corrispondente ai parametri.
                for (var n = 0; n < args.Length; n++)
                {
                    //Crea il nome del parametro.
                    var parametro = $"@param{n}";

                    //Crea un lista di indici delle sezioni del filtro che conengono il parametro con valore nullo.
                    var indici =
                        Enumerable.Range(0, filtri.Count).Where(i => filtri[i].Contains(parametro)).ToList();

                    //Cicla per tutte le sezioni che contengono il parametro con valore nullo.
                    foreach (var i in indici)
                        //Controlla se il valore del parametro è nullo e nel caso modifica la sezione del filtro.
                        if (args[n] == DBNull.Value)
                        {
                            //Scompone la singola sezione del filtro in campo, operatore e valore/i.
                            var items =
                                new List<string>(
                                    Regex.Split(filtri[i], @"(=)|( like )|( in )", RegexOptions.IgnoreCase));

                            //Se il formato della stringa non è corretto genera un errore.
                            if (items.Count == 3)
                            {
                                //Estrai la parte del campo.
                                var campo = Regex.Replace(items[0], @"[^a-zA-Z0-9]", "").Trim();

                                //Determina se ci sono delle parentesi prima del campo.
                                var inizio = Regex.Replace(items[0], @"[^(]", "");

                                //Determina pa parte originale del filtro partendo dal nome del campo.
                                var filtro =
                                    Regex.Replace(filtri[i], @"[^a-zA-Z0-9@ +'%(),=]", "")
                                        .Substring(filtri[i].IndexOf(campo, StringComparison.Ordinal))
                                        .TrimEnd();

                                var filtroLength =
                                    filtro.IndexOf(parametro, StringComparison.Ordinal) + parametro.Length;

                                //Determina se ci sono delle parentesi dopo il parametro
                                var fine = Regex.Replace(items[2], @"[^)]", "");

                                //Mantieni una sola parentesi chiusa in caso di espressione con 'IN'.
                                if (Regex.Match(items[1], " in ", RegexOptions.IgnoreCase).Success && fine.Length > 0)
                                {
                                    if (fine.Length > 1)
                                        fine = fine.Substring(1);

                                    //Accoda l'eventuale seconda parentesi alla fine della sezione
                                    filtro = filtro.Substring(0, filtroLength) + fine;
                                }
                                else if (items[1].Contains('='))
                                {
                                    filtro = filtro.Substring(0, filtroLength);
                                }
                                else
                                {
                                    filtro = filtro.Replace("'%", "'#'+").Replace("%'", "+'%'").Replace("#", "%");
                                }

                                //Ricompone la sezione del filtro modificata e la memorizza al posto dell'originale.
                                filtri[i] = $" {inizio}({filtro} OR {campo} IS NULL AND {parametro} IS NULL){fine} ";
                            }
                            else
                            {
                                throw new Exception("Formato query errato.");
                            }
                        }
                        else
                        {
                            filtri[i] = filtri[i].Replace("'%", "'#'+").Replace("%'", "+'%'").Replace("#", "%");
                        }

                    //Aggiungi il parametro nel SqlCommand.
                    sql.Parameters.AddWithValue(parametro, args[n]);
                }

                //Inserisci la query dopo che è stata modificata.
                sql.CommandText = string.Join(string.Empty, filtri);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return sql;
        }

        public static DataTable GetSqlTable(SqlCommand cmd)
        {
            try
            {
                using (
                    var conn =
                        new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    cmd.Connection = conn;
                    cmd.Connection.Open();
                    var tempTable = new DataTable();
                    tempTable.Load(cmd.ExecuteReader());
                    return tempTable;
                }
            }
            catch
            {
                return new DataTable();
            }
        }

        private static TableClass GetDbLevelInfo(ConnectionStringSettings connectionString, string environmentType,
            string dbBaseLevel)
        {
            using (var connection = new SqlConnection(connectionString.ConnectionString))
            {
                var connectionStringBuilder = new SqlConnectionStringBuilder(connectionString.ConnectionString);
                Logger.Trace("Connection to server {0} and  database {1}", connectionStringBuilder.DataSource,
                    connectionString.Name);
                var command = new SqlCommand(
                    $@"SELECT DATABASEPROPERTYEX('{connectionStringBuilder.InitialCatalog}', 'ServiceObjective') as 'service_tier', avg(stat.avg_cpu_percent) avg_cpu, avg(stat.avg_data_io_percent) avg_data_io ,
avg(stat.avg_log_write_percent) avg_log, avg(stat.avg_memory_usage_percent) avg_memory, max(stat.avg_cpu_percent) max_cpu
, max(stat.avg_data_io_percent) max_data_io, max(stat.avg_log_write_percent) max_log, max(stat.avg_memory_usage_percent) max_memory, getutcdate() as 'now'
from sys.dm_db_resource_stats as stat
where end_time > dateadd(day, -1, getutcdate())", connection);
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    Logger.Trace("Query {0} executed", command.CommandText);

                    while (reader.Read())
                    {
                        var row = (IDataRecord) reader;
                        var dbInfo = new TableClass
                        {
                            DbName = connectionString.Name,
                            EnvironmentType = environmentType,
                            DbLevel = row[0].ToString(),
                            DbBaseLevel = dbBaseLevel,
                            AvgCpu = Convert.ToDouble(row[1]),
                            AvgData = Convert.ToDouble(row[2]),
                            AvgLog = Convert.ToDouble(row[3]),
                            AvgMemory = Convert.ToDouble(row[4]),
                            MaxCpu = Convert.ToDouble(row[5]),
                            MaxData = Convert.ToDouble(row[6]),
                            MaxLog = Convert.ToDouble(row[7]),
                            MaxMemory = Convert.ToDouble(row[8]),
                            Timestamp = Convert.ToDateTime(row[9])
                        };

                        //if (dbInfo.DbLevel == "S0")
                        //{
                        //    dbInfo.StringColour = "<font color='Green'>&#9658</font>";
                        //}
                        //else
                        //{
                        //    //if ((dbInfo.AvgLog > 80 || dbInfo.AvgCpu > 80 || dbInfo.AvgData > 80 ||
                        //    //     dbInfo.AvgMemory > 95) &&
                        //    //    (dbInfo.MaxLog > 95 || dbInfo.MaxCpu > 95 || dbInfo.MaxData > 95 ||
                        //    //     dbInfo.MaxMemory > 95))
                        //    //    dbInfo.StringColour = "<font color='Red'>&#9650</font>";
                        //    //else if (dbInfo.AvgLog < 80 && dbInfo.AvgLog > 20 ||
                        //    //         dbInfo.AvgCpu < 80 && dbInfo.AvgCpu > 20 ||
                        //    //         dbInfo.AvgData < 80 && dbInfo.AvgData > 20 ||
                        //    //         dbInfo.AvgMemory < 95 && dbInfo.AvgMemory > 20)
                        //    //    dbInfo.StringColour = "<font color='Green'>&#9658</font>";
                        //    //else
                        //    //    dbInfo.StringColour = "<font color='#ffbf00'>&#9660</font>";

                        if (dbInfo.DbOrderedLevel != dbInfo.DbOrderedBaseLevel)
                            dbInfo.StringColour = "<font color='Red'>&#x2757;</font>";
                        else
                            dbInfo.StringColour = "<font color='Green'>&#9658</font>";

                        //}

                        Logger.Trace("The level is {0}", row[0]);

                        return dbInfo;
                    }
                }
            }

            return null;
        }
    }
}