﻿using System;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using Newtonsoft.Json.Linq;
using NLog;

namespace PrudsysBridge
{
    public class Helper
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static bool OnServerCertificateValidationCallback(object sender, X509Certificate certificate,
            X509Chain chain, SslPolicyErrors errors)
        {
            if (errors == SslPolicyErrors.None) return true;

            var httpWebRequest = sender as HttpWebRequest;
            return httpWebRequest != null;
        }

        public static string RecaRequestWorkaround(string number)
        {
            Logger.Trace($"Request for item '{number}'");
            var leafNumber = number.Split('_');
            Logger.Trace($"Request leafNumber length '{leafNumber.Length}'");

            var result = $"CL02_{leafNumber[leafNumber.Length - 1]}";
            Logger.Trace($"Request leafNumber set to '{result}'");
            return result;
        }

        public static JObject RecaResponseWorkaround(JObject jObject, string model)
        {
            if (string.IsNullOrEmpty(model)) return new JObject();

            var valueSplit = model.Split('_');
            var nodeNumber = string.Join("_", valueSplit.Take(valueSplit.Length - 1));
            Logger.Trace($"Request Parent root '{nodeNumber}'");

            foreach (var tokenList in jObject["ProductModels"].ToList())
            {
                var value = tokenList["Model_Nr"].Value<string>();
                Logger.Trace($"Response Model_Nr '{value}'");

                var leafNumber = value.Split('_');
                var tokenNumber = $"{nodeNumber}_{leafNumber[leafNumber.Length - 1]}";
                tokenList["Model_Nr"] = tokenNumber;
                Logger.Trace($"Response Model_Nr set to '{tokenNumber}'");
            }

            return jObject;
        }
    }
}