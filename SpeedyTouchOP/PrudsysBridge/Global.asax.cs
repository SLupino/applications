﻿using System.Net;
using System.Web;
using System.Web.Http;

namespace PrudsysBridge
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            ServicePointManager.ServerCertificateValidationCallback += Helper.OnServerCertificateValidationCallback;
        }
    }
}