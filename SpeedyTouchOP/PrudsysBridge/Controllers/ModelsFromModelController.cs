﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using NLog;

namespace PrudsysBridge.Controllers
{
    public class ModelsFromModelController : ApiController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        // GET: api/ModelsFromModel?Item=&RecoType=&Token=&CustomerNr=&Organization=
        public async Task<JObject> Get(string item, string recoType, string token, string customerNr,
            string organization)
        {
            Logger.Info($"Request for item '{item}'");

            var prudsysApi = Settings.PrudsysApi();
            if (string.IsNullOrEmpty(prudsysApi))
            {
                Logger.Error("Missing Prudsys URL.");
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            var apiResult = new JObject();

            var itemForRequest = organization == "1601" ? Helper.RecaRequestWorkaround(item) : item;

            try
            {
                using (var handler = new HttpClientHandler())
                {
                    var prudsysKey = Settings.PrudsysKey();
                    if (!string.IsNullOrEmpty(prudsysKey) && prudsysKey.Split(':').Length == 2)
                    {
                        var account = prudsysKey.Split(':');
                        handler.Credentials = new NetworkCredential(account[0], account[1]);
                        Logger.Debug($"Added Prudsys key: '{prudsysKey}'");
                    }

                    using (var client = new HttpClient(handler))
                    {
                        var request =
                            $"{prudsysApi}/ModelsFromModel?Item={itemForRequest}&RecoType={recoType}&Token={token}&CustomerNr={customerNr}&Organization={organization}";
                        Logger.Trace($"Request: '{request}'");

                        var result = await client.GetAsync(request);
                        Logger.Info($"Response for item '{itemForRequest}': {result.StatusCode}");

                        if (result.StatusCode == HttpStatusCode.OK)
                        {
                            var resultContent = result.Content.ReadAsStringAsync().Result;
                            Logger.Trace($"Response: '{resultContent}'");

                            apiResult = JObject.Parse(resultContent);
                        }
                        else
                        {
                            Logger.Trace(result.Headers);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }

            return organization == "1601" ? Helper.RecaResponseWorkaround(apiResult, item) : apiResult;
        }

        // POST: api/ModelsFromModel
        public async Task<JObject> Post(string item, string recoType, string token, string customerNr,
            string organization)
        {
            Logger.Info($"Post Request...'");
            //var frWorkaround = item.StartsWith("AC_");
            //if (frWorkaround)
            //    item = item.Substring(6);

            var recoList = await Get(item, recoType, token, customerNr, organization);
            //var recoList = JObject.Parse(@"{""ProductModels"":[{""Model_Nr"":""CL01_3010240101"",""Model_Name"":""Nettoyant pour mousse PU Purlogic Clean<SUP>®</SUP> Nettoyant"",""MediaCode"":""Reco_ModelsFromModel_interestingforyou"",""Reason"":""CL01_2501020610"",""RecoType"":""interestingforyou""},{""Model_Nr"":""CL01_2501020601"",""Model_Name"":""Mousse PU coupe-feu intumescente Bi-composants"",""MediaCode"":""Reco_ModelsFromModel_interestingforyou"",""Reason"":""CL01_2501020610"",""RecoType"":""interestingforyou""},{""Model_Nr"":""CL01_3010180202"",""Model_Name"":""Mousse de montage pistolable monocomposant"",""MediaCode"":""Reco_ModelsFromModel_interestingforyou"",""Reason"":""CL01_2501020610"",""RecoType"":""interestingforyou""},{""Model_Nr"":""CL01_3010180102"",""Model_Name"":""Mousse expansive monocomposant"",""MediaCode"":""Reco_ModelsFromModel_interestingforyou"",""Reason"":""CL01_2501020610"",""RecoType"":""interestingforyou""},{""Model_Nr"":""CL01_3010250503"",""Model_Name"":""Pistolet plastique pour mousse PU Purlogic 100"",""MediaCode"":""Reco_ModelsFromModel_interestingforyou"",""Reason"":""CL01_2501020610"",""RecoType"":""interestingforyou""},{""Model_Nr"":""CL01_3010180226"",""Model_Name"":""Mousse de montage pistolable monocomposant Streetfighter"",""MediaCode"":""Reco_ModelsFromModel_interestingforyou"",""Reason"":""CL01_2501020610"",""RecoType"":""interestingforyou""},{""Model_Nr"":""CL01_2501010107"",""Model_Name"":""Mastic silicone neutre coupe-feu Perfect"",""MediaCode"":""Reco_ModelsFromModel_interestingforyou"",""Reason"":""CL01_2501020610"",""RecoType"":""interestingforyou""},{""Model_Nr"":""CL01_3010250703"",""Model_Name"":""Pistolet métallique à mousse PU"",""MediaCode"":""Reco_ModelsFromModel_interestingforyou"",""Reason"":""CL01_2501020610"",""RecoType"":""interestingforyou""},{""Model_Nr"":""CL01_3010180116"",""Model_Name"":""Mousse expansive monocomposant PURLOGIC<SUP>®</SUP> Easysafe"",""MediaCode"":""Reco_ModelsFromModel_interestingforyou"",""Reason"":""CL01_2501020610"",""RecoType"":""interestingforyou""},{""Model_Nr"":""CL01_2501021508"",""Model_Name"":""Pistolet manuel d'extrusion"",""MediaCode"":""Reco_ModelsFromModel_interestingforyou"",""Reason"":""CL01_2501020610"",""RecoType"":""interestingforyou""},{""Model_Nr"":""CL01_3010180203"",""Model_Name"":""Mousse polyuréthane élastique pistolable PURLOGIC<SUP>® </SUP>Flex"",""MediaCode"":""Reco_ModelsFromModel_interestingforyou"",""Reason"":""CL01_2501020610"",""RecoType"":""interestingforyou""},{""Model_Nr"":""CL01_3010250523"",""Model_Name"":""Pistolet métallique pour mousse PU court"",""MediaCode"":""Reco_ModelsFromModel_interestingforyou"",""Reason"":""CL01_2501020610"",""RecoType"":""interestingforyou""}]}");

            //if (!frWorkaround) return recoList;

            //foreach (var reco in recoList.SelectTokens("$..ProductModels").Children())
            //{
            //    var prop = reco.First as JProperty;
            //    if (prop != null)
            //        prop.Value = $"___AC_{prop.Value}";
            //}

            Logger.Trace($"Post Response: '{recoList}'");

            return recoList;
        }
    }
}