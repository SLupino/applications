﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using NLog;

namespace PrudsysBridge.Controllers
{
    public class ModelsFromProductController : ApiController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        // POST: api/ModelsFromProduct
        public async Task<JObject> Post([FromBody] RecoRequest body)
        {
            Logger.Info($"Request for item '{body.Item}', customer '{body.CustomerNr}', type '{body.RecoType}'");

            var prudsysApi = Settings.PrudsysApi();
            if (string.IsNullOrEmpty(prudsysApi))
            {
                Logger.Error("Missing Prudsys URL.");
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            var headers = Request.Headers;

            string token = "", organization = "", model = "";

            if (headers.Contains("CompanyNumber"))
                organization = headers.GetValues("CompanyNumber").First();

            if (headers.Contains("Token"))
                token = headers.GetValues("Token").First();

            if (headers.Contains("Model"))
                model = headers.GetValues("Model").First();

            var apiResult = new JObject();

            try
            {
                using (var handler = new HttpClientHandler())
                {
                    var prudsysKey = Settings.PrudsysKey();
                    if (!string.IsNullOrEmpty(prudsysKey) && prudsysKey.Split(':').Length == 2)
                    {
                        var account = prudsysKey.Split(':');
                        handler.Credentials = new NetworkCredential(account[0], account[1]);
                        Logger.Debug($"Added Prudsys key: '{prudsysKey}'");
                    }

                    using (var client = new HttpClient(handler))
                    {
                        var request =
                            $"{prudsysApi}/ModelsFromProduct";
                        Logger.Trace($"Request: '{request}'");

                        client.DefaultRequestHeaders.Add("CompanyNumber", organization);
                        Logger.Trace($"Request header CompanyNumber: '{organization}.");
                        client.DefaultRequestHeaders.Add("Token", token);
                        Logger.Trace($"Request header Token: '{token}'.");

                        if (body.Item.Contains("."))
                        {
                            Logger.Trace("Replace '.' with spaces in article id.");
                            body.Item = body.Item.Replace('.', ' ');
                        }

                        var serielized = new JavaScriptSerializer().Serialize(body);
                        Logger.Trace($"Request content: '{serielized}'.");

                        var content = new StringContent(serielized, Encoding.UTF8, "application/json");

                        var result = await client.PostAsync(request, content);
                        Logger.Info($"Response for item '{body.Item}': {result.StatusCode}");

                        if (result.StatusCode == HttpStatusCode.OK)
                        {
                            var resultContent = result.Content.ReadAsStringAsync().Result;
                            Logger.Trace($"Response: '{resultContent}'");

                            apiResult = JObject.Parse(resultContent);
                        }
                        else
                        {
                            Logger.Trace(result.Headers);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }

            return organization == "1601" && !string.IsNullOrEmpty(model) ? Helper.RecaResponseWorkaround(apiResult, model) : apiResult;
        }

        public class RecoRequest
        {
            public string Item { get; set; }
            public string CustomerNr { get; set; }
            public string RecoType { get; set; }
        }
    }
}