﻿using System;
using System.Configuration;
using System.Text;

namespace PrudsysBridge
{
    public static class Settings
    {
        public static string PrudsysApi()
        {
            return ConfigurationManager.AppSettings["prudsysApi"];
        }

        public static string PrudsysKey()
        {
            var prudsysKey = ConfigurationManager.AppSettings["prudsysKey"];

            return string.IsNullOrEmpty(prudsysKey)
                ? ""
                : Encoding.UTF8.GetString(Convert.FromBase64String(prudsysKey));
        }
    }
}