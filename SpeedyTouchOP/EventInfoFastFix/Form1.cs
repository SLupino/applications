﻿using System;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace EventInfoFastFix
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        public class Message
        {
            [JsonProperty("F")]
            public string F { get; set; }

            [JsonProperty("U")]
            public Guid U { get; set; }

            [JsonProperty("M")]
            public M M { get; set; }
        }

        public class M
        {
            [JsonProperty("EI")]
            public Ei[] Ei { get; set; }
        }

        public class Ei
        {
            [JsonProperty("id")]
            public string Id { get; set; }

            [JsonProperty("start")]
            public DateTimeOffset Start { get; set; }

            [JsonProperty("targetUser")]
            public Guid TargetUser { get; set; }

            [JsonProperty("eventType")]
            public long EventType { get; set; }

            [JsonProperty("recurrencePattern")]
            public object RecurrencePattern { get; set; }

            [JsonProperty("recurrenceStart")]
            public object RecurrenceStart { get; set; }

            [JsonProperty("isFixed")]
            public bool IsFixed { get; set; }

            [JsonProperty("details")]
            public string Details { get; set; }

            [JsonProperty("customerId")]
            public string CustomerId { get; set; }

            [JsonProperty("recurrenceEnd")]
            public object RecurrenceEnd { get; set; }

            [JsonProperty("recurrenceType")]
            public object RecurrenceType { get; set; }

            [JsonProperty("isPhoneCall")]
            public bool IsPhoneCall { get; set; }

            [JsonProperty("cv")]
            public long Cv { get; set; }

            [JsonProperty("ownerUser")]
            public Guid OwnerUser { get; set; }

            [JsonProperty("end")]
            public DateTimeOffset End { get; set; }

            [JsonProperty("cd")]
            public long Cd { get; set; }
        }

        private const string Stmt = @"
BEGIN TRANSACTION [Tran1]

  BEGIN TRY

    insert into eventInfo (id,[start],[end],isFixed,isPhoneCall,customerId,details,eventType,recurrenceStart,recurrenceEnd,recurrenceType,recurrencePattern,ownerUser,targetUser,cv,cd,C_consolGuid,operation,version,C_boId,stagingversion,stagingsystem) values
    ('{0}','{1}','{2}',{3},{4},'{5}','{6}',{7},{8},{9},{10},{11},'{12}','{13}',{14},{15},'{0}','N',{16},newid(),-2,-2)

    update SYS_VersionHistory set state = 100 WHERE version = {16}

    COMMIT TRANSACTION [Tran1]

  END TRY

  BEGIN CATCH

    ROLLBACK TRANSACTION [Tran1]

  END CATCH 

  SELECT * FROM eventInfo WHERE id = '{0}';
";
        private void button1_Click(object sender, EventArgs e)
        {
            var message = JsonConvert.DeserializeObject<Message>(richTextBoxJson.Text);
            if (message?.M?.Ei == null || message.M.Ei.Length == 0)
            {
                MessageBox.Show("Missing json message.");
                return;
            }

            if (!int.TryParse(textBoxVersion.Text, out var version))
            {
                MessageBox.Show("Missing version number.");
                return;
            }
        
            var detail = message.M.Ei;

            richTextBoxStmt.Text = string.Format(Stmt, detail[0].Id, detail[0].Start.ToUniversalTime().ToString("s"),
                detail[0].End.ToUniversalTime().ToString("s"), detail[0].IsFixed ? 1 : 0,
                detail[0].IsPhoneCall ? 1 : 0, detail[0].CustomerId, detail[0].Details, detail[0].EventType,
                detail[0].RecurrenceStart == null ? "NULL" : $"'{detail[0].RecurrenceStart}'",
                detail[0].RecurrenceEnd == null ? "NULL" : $"'{detail[0].RecurrenceEnd}'",
                detail[0].RecurrenceType == null ? "NULL" : $"'{detail[0].RecurrenceType}'",
                detail[0].RecurrencePattern == null ? "NULL" : $"'{detail[0].RecurrencePattern}'", detail[0].OwnerUser,
                detail[0].TargetUser, detail[0].Cv, detail[0].Cd, version);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(richTextBoxStmt.Text);
        }
    }
}
