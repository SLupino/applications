﻿using System;
using System.Net;
using System.Web.Mvc;
using System.Web.Routing;

namespace SpeedyServiceConnector
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static bool UseDirectConnectionToAx { get; set; }
        public static string AxUrl { get; set; }
        public static NetworkCredential AxCredential { get; set; }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            bool useDirectConnectionToAx;
            bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["useDirectConnectionToAX"], out useDirectConnectionToAx);
            UseDirectConnectionToAx = useDirectConnectionToAx;

            AxUrl = System.Configuration.ConfigurationManager.AppSettings["AXurl"] ?? "";
            var user = System.Configuration.ConfigurationManager.AppSettings["AXuser"] ?? ""; ;
            var password = System.Configuration.ConfigurationManager.AppSettings["AXpassword"] ?? "";
            AxCredential = new NetworkCredential {UserName = user, Password = password};
        }
    }
}
