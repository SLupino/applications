﻿using System;
using System.Net;
using System.Web.Mvc;


namespace SpeedyServiceConnector.Controllers
{
    [ValidateInput(false)]
    public class AxConnectorController : Controller
    {
        public ActionResult ProcessMessage()
        {
            return Content("Speedy Service AX Connector is up and running.");
        }

        [HttpPost]
        public ActionResult ProcessMessage(string request)
        {
            if (request == "") return Content("Empty request.");

            if (MvcApplication.UseDirectConnectionToAx)
            {
                var soapClient = new BasicHttpBinding_IVTSTCommunicator {Url = MvcApplication.AxUrl, Credentials = MvcApplication.AxCredential, CallContextValue = new CallContext() };

                return GetResponse(request, soapClient.processRequest);
            }
            else
            {
                var user = new { Name = "", Pass = "" };

                var auth = HttpContext.Request.Headers["Authorization"];
                if (!string.IsNullOrEmpty(auth))
                {
                    var cred = System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(auth.Substring(6))).Split(':');
                    user = new { Name = cred[0], Pass = cred[1] };
                }

                var soapClient = new ServiceReferenceAX.SpeedyConSoapClient();

                if (soapClient.ClientCredentials != null)
                {
                    soapClient.ClientCredentials.UserName.UserName = user.Name;
                    soapClient.ClientCredentials.UserName.Password = user.Pass;
                }

                return GetResponse(request, soapClient.processRequest);
            }
        }

        private ActionResult GetResponse(string request, Func<string, string> processRequest)
        {
            try
            {
                var response = processRequest(request);
                return Content(response, "text/xml");
            }
            catch (Exception ex)
            {
                var webEx = ex.InnerException as WebException;
                if (webEx == null) return Content(ex.Message);

                var webResponse = (HttpWebResponse)webEx.Response;
                if (webResponse != null)
                    return new HttpStatusCodeResult(webResponse.StatusCode, $"The server {webResponse.ResponseUri.Host} returned an error: {webResponse.StatusDescription}.");

                return Content(webEx.Message);
            }
        }
    }
}