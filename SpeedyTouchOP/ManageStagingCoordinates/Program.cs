﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;

namespace ManageStagingCoordinates
{
    internal class Program
    {
        /// <summary>
        ///     receive in input file with ; separator. All value is or systemAccount or salesRepId.
        ///     execute query with file data and call bingMaps web service.
        ///     create file with geocordinates bing sended
        /// </summary>
        private static void Main()
        {
            //sql connection string
            _connectionString = ConfigurationManager.AppSettings["SqlConnectionString"];
            _dbVersion = ConfigurationManager.AppSettings["databaseVersion"];
            //account bing key
            _key = ConfigurationManager.AppSettings["BingKey"];
            Console.WriteLine("select source \n\r    f = (salesRep file)\n\r  a = (all customer)");
            var sourceSelection = Console.ReadLine();
            if (sourceSelection == "f")
            {
                Console.WriteLine("Insert Path file with this format 'value;value;value;....'");
                var pathFile = Console.ReadLine();
                Console.WriteLine(
                    "insert value type \n\r    0 = (systemAccount)\n\r    1 = (salesRepId)\n\r    2 = (customerId)");
                var valueType = Console.ReadLine();
                if (pathFile != null)
                {
                    List<string> valueList;
                    //split file for obtain list with data for query
                    using (var streamReader = new StreamReader(pathFile))
                    {
                        valueList = streamReader.ReadToEnd().Split(';').ToList();
                    }

                    valueList.Remove("");
                    var count = 1;
                    Console.Write("\rLoading 0%\n\rElaborated element = 0/{0}", valueList.Count);

                    //modify query depending user input
                    if (valueType == "0")
                    {
                        const string type = "u.systemAccount";
                        foreach (var value in valueList)
                        {
                            var queryStringWithValue = string.Format(
                                _dbVersion == "15.04" ? QueryString1504 : QueryString1507, type, value);

                            using (var writer = new StreamWriter(@"c:\tmp\Result.txt", true))
                            {
                                writer.WriteLine("Start UsersId: {0}", value);
                            }

                            //GetCoordinates(count, valueList.Count, queryStringWithValue);
                            GetCoordinates(queryStringWithValue);
                            //var count1 = 0;
                            //int calculatepercentage = (response.Count() - 1)/100;
                            //foreach (var resp in response)
                            //{
                            //    if (count1 == calculatepercentage && advantageCount < 100)
                            //    {
                            //        Console.Clear();
                            //        advantageCount++;
                            //        Console.Write("\rLoading {0}%\n\rElaborating element = {1}/{2}", advantageCount, count, (valueList.Count()));
                            //        count1 = 0;
                            //    }
                            //    str.AppendLine(string.Format("{0},{1},{2};", resp.Key, resp.Value[0], resp.Value[1]));
                            //    count1++;
                            //}
                            using (var writer = new StreamWriter(@"c:\tmp\Result.txt", true))
                            {
                                writer.WriteLine("End UsersId: {0}", value);
                            }

                            count++;
                        }
                    }
                    else if (valueType == "1")
                    {
                        const string type = "u.salesRepId";
                        foreach (var value in valueList)
                        {
                            var queryStringWithValue = string.Format(
                                _dbVersion == "15.04" ? QueryString1504 : QueryString1507, type, value);

                            using (var writer = new StreamWriter(@"c:\tmp\Result.txt", true))
                            {
                                writer.WriteLine("Start UsersId: {0}", value);
                            }

                            //GetCoordinates(count, valueList.Count, queryStringWithValue);
                            GetCoordinates(queryStringWithValue);

                            using (var writer = new StreamWriter(@"c:\tmp\Result.txt", true))
                            {
                                writer.WriteLine("End UsersId: {0}", value);
                            }

                            count++;
                        }
                    }
                    else if (valueType == "2")
                    {
                        const string type = "c.id";
                        foreach (var value in valueList)
                        {
                            var queryStringWithValue = string.Format(
                                _dbVersion == "15.04" ? QueryString1504 : QueryString1507, type, value);

                            //GetCoordinates(count, valueList.Count, queryStringWithValue);
                            GetCoordinates(queryStringWithValue);

                            count++;
                        }
                    }
                    Console.Clear();
                    Console.WriteLine("Elaboration completed\n\r");
                    //pathFile = Console.ReadLine();
                    //Console.WriteLine("\n\r insert file name:");
                    //pathFile += "/" + Console.ReadLine() + ".txt";
                    //using (var strWriter = new StreamWriter(pathFile))
                    //{
                    //    strWriter.Write(str);
                    //}
                    //Console.Clear();
                    //Console.WriteLine("Writing succesfully in : " + pathFile);
                }
            }
            else if (sourceSelection == "a")
            {
                string queryStringWithValue;

                switch (_dbVersion)
                {
                    case "15.04":
                        queryStringWithValue = QueryCustomerString1504;
                        break;

                    case "15.07":
                        queryStringWithValue = QueryCustomerString1507;
                        break;

                    default:
                        queryStringWithValue = QueryCustomerString1604;
                        break;
                }

                using (var writer = new StreamWriter(@"c:\tmp\Result.txt", true))
                {
                    writer.WriteLine("Start all customers.");
                }

                GetCoordinates(queryStringWithValue);

                using (var writer = new StreamWriter(@"c:\tmp\Result.txt", true))
                {
                    writer.WriteLine("End all customers");
                }
            }
            else
            {
                Console.WriteLine("wrong selection.");
            }

            Console.ReadLine();
        }

        private static void GetCoordinates(string queryStringWithValue)
        {
            var addressList = new Dictionary<string, KeyValuePair<string,string>>();
            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    var command = new SqlCommand(queryStringWithValue, connection);
                    connection.Open();
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        var countryRegion = reader["isoCountryCode"].ToString();
                        var locality = reader["city"].ToString().Replace("\r\n", string.Empty).Replace("\r", string.Empty).Replace("\n", string.Empty);
                        var postalCode = reader["zipCode"].ToString();
                        var addressLine = reader["street"].ToString().Replace("\r\n", string.Empty).Replace("\r", string.Empty).Replace("\n", string.Empty);

                        var givenAddress = $"{addressLine}, {postalCode} {locality}, {countryRegion}";
                        var query = $"countryRegion={countryRegion}&locality={locality}&postalCode={postalCode}&addressLine={addressLine}&maxResults=2";

                        addressList.Add(reader["id"].ToString(), new KeyValuePair<string, string>(givenAddress, query));
                    }
                    reader.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine($"{e.Message}\n\r");
                    Console.ReadKey();
                    return;
                }
            }
            if (addressList.Count > 0)
                foreach (var address in addressList)
                {
                    Thread.Sleep(1000);
                    var customerId = address.Key;
                    //var addressValue = address.Value.Split();
                    var queryString = address.Value.Value;

                    KeyValuePair<string, List<string>> result;
                    try
                    {
                        result = GetResponse(GenerateUri(queryString));
                    }
                    catch (Exception e)
                    {
                        Logger.Error($"Exception : {e.Message}");
                        Logger.Info($"the address: {address.Value} for this customerId : {address.Key} is not found");
                        continue;
                    }

                    var coordinates = result.Value;

                    using (var writer = new StreamWriter(@"c:\tmp\Result.txt", true))
                    {
                        if (coordinates != null && coordinates.Count == 2)
                            writer.WriteLine("{0}|{1}|{2}|{3}|{4}", customerId, coordinates[0], coordinates[1], address.Value.Key, result.Key);
                        else
                            writer.WriteLine("{0}|||{1}|", customerId, address.Value.Key);
                    }
                }
        }

        private static KeyValuePair<string, List<string>> GetResponse(Uri uri)
        {
            var orderedCoordinates = new KeyValuePair<string, List<string>>();

            using (var httpClient = new HttpClient())
            {
                //take a response
                var postAsyncTask = httpClient.GetAsync(uri,
                    HttpCompletionOption.ResponseContentRead);
                //read a response and convert to string
                var response = new StreamReader(postAsyncTask.GetAwaiter().GetResult().Content.ReadAsStreamAsync().Result).ReadToEnd();

                var jResponse = JObject.Parse(response);

                var jResources = jResponse.SelectTokens("$.resourceSets[0].resources").ToList();
                var resourceList = jResources[0].ToList();

                Logger.Debug($"URL: '{uri}'");

                if (resourceList.Count > 1)
                {
                    Logger.Info("Found more than one result:");
                    var n = 1;
                    foreach (var jResource in resourceList)
                    {
                        var address = jResource.SelectToken("$.address");
                        var confidence = jResource.SelectToken("$.confidence");
                        Logger.Info($"{n}) Address with confidence '{confidence}': {address}");
                        n++;
                    }
                    return orderedCoordinates;
                }

                var jCoordinates = jResponse.SelectToken("$.resourceSets[0].resources[0].point.coordinates");
                if (jCoordinates == null)
                {
                    Logger.Info("Address not found.");
                    return orderedCoordinates;
                }

                var jAddress = jResponse.SelectToken("$.resourceSets[0].resources[0].address.formattedAddress");
                Logger.Debug($"Address found: {jAddress}");

                orderedCoordinates= new KeyValuePair<string, List<string>>(jAddress.ToString(), jCoordinates.Select(c => c.ToString(Formatting.None)).ToList());
            }

            return orderedCoordinates;
        }

        private static Uri GenerateUri(string query)
        {
            var uri = new Uri(string.Format(DefaultUri, query, _key));

            return uri;
        }

        #region Property

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        //this is URI for call bingMaps service. in q insert query value and in key the account key
        private const string DefaultUri = "http://dev.virtualearth.net/REST/v1/Locations?{0}&key={1}"; //"http://maps.googleapis.com/maps/api/geocode/xml?address={0}&sensor=true";

        //sql query for take all address for one SalesRepId
        private const string QueryString1504 = @"select c.id, c.street, c.city, c.isoCountryCode, c.zipCode
                                        from customers c WITH (NOLOCK) --on c.C_consolGuid = v.C_consolFK_Vouchers_Customers
                                        join SalesHierarchy sh WITH (NOLOCK) on sh.C_consolGuid = c.C_consolFK_Customers_SalesHierarchy
                                        join salesRepAreaRelations srar WITH (NOLOCK) on srar.C_consolFK_SalesRepAreaRelations_SalesHierarchy = sh.C_consolGuid and srar.[validTo] > getdate()
                                        join EXT_UsersView u WITH (NOLOCK) on srar.C_consolFK_SalesRepAreaRelations_Users = u.C_consolGuid
                                        where c.version = (select max(version) from customers c2 where c.c_consolGuid = c2.c_consolGuid) 
                                        and c.operation = 'N'
                                        and sh.version = (select max(version) from SalesHierarchy sh2 where sh.c_consolGuid = sh2.c_consolGuid) 
                                        and sh.operation = 'N'
                                        and srar.version = (select max(version) from salesRepAreaRelations srar2 where srar.c_consolGuid = srar2.c_consolGuid) 
                                        and srar.operation = 'N'
                                        and u.version = (select max(version) from EXT_UsersView u2 where u.c_consolGuid = u2.c_consolGuid) 
                                        and u.operation = 'N'
                                        and {0} = '{1}'
                                        and srar.isPrimarySalesRep = 1";

        private const string QueryString1507 = @"select c.id, c.street, c.city, c.isoCountryCode, c.zipCode
                                        from customers c WITH (NOLOCK)
                                        join CustomerSalesAreaRelation csar WITH (NOLOCK) on csar.C_consolFK_CustomerSalesAreaRelation_Customers = c.C_consolGuid 
                                        join SalesHierarchy sh WITH (NOLOCK) on sh.C_consolGuid = csar.C_consolFK_CustomerSalesAreaRelation_SalesHierarchy
                                        join salesRepAreaRelations srar WITH (NOLOCK) on srar.C_consolFK_SalesRepAreaRelations_SalesHierarchy = sh.C_consolGuid and srar.[validTo] > getdate()
                                        join EXT_UsersView u WITH (NOLOCK) on srar.C_consolFK_SalesRepAreaRelations_Users = u.C_consolGuid
                                        where c.version = (select max(version) from customers c2 where c.c_consolGuid = c2.c_consolGuid) 
                                        and c.operation = 'N'
                                        and sh.version = (select max(version) from SalesHierarchy sh2 where sh.c_consolGuid = sh2.c_consolGuid) 
                                        and sh.operation = 'N'
                                        and srar.version = (select max(version) from salesRepAreaRelations srar2 where srar.c_consolGuid = srar2.c_consolGuid) 
                                        and srar.operation = 'N'
                                        and u.version = (select max(version) from EXT_UsersView u2 where u.c_consolGuid = u2.c_consolGuid) 
                                        and u.operation = 'N'
                                        and {0} = '{1}'
                                        and srar.isPrimarySalesRep = 1";

        private const string QueryCustomerString1507 = @"select c.id, c.street, c.city, c.isoCountryCode, c.zipCode
                                        from customers c WITH (NOLOCK)
                                        join CustomerSalesAreaRelation csar WITH (NOLOCK) on csar.C_consolFK_CustomerSalesAreaRelation_Customers = c.C_consolGuid 
                                        join SalesHierarchy sh WITH (NOLOCK) on sh.C_consolGuid = csar.C_consolFK_CustomerSalesAreaRelation_SalesHierarchy
                                        join salesRepAreaRelations srar WITH (NOLOCK) on srar.C_consolFK_SalesRepAreaRelations_SalesHierarchy = sh.C_consolGuid and srar.[validTo] > getdate()
                                        where c.version = (select max(version) from customers c2 where c.c_consolGuid = c2.c_consolGuid) 
                                        and c.operation = 'N'
                                        and sh.version = (select max(version) from SalesHierarchy sh2 where sh.c_consolGuid = sh2.c_consolGuid) 
                                        and sh.operation = 'N'
                                        and srar.version = (select max(version) from salesRepAreaRelations srar2 where srar.c_consolGuid = srar2.c_consolGuid) 
                                        and srar.operation = 'N'
                                        and srar.isPrimarySalesRep = 1";


        private const string QueryCustomerString1504 = @"select c.id, c.street, c.city, c.isoCountryCode, c.zipCode
                                        from customers c WITH (NOLOCK)
                                        join CustomerSalesAreaRelation csar WITH (NOLOCK) on csar.C_consolFK_CustomerSalesAreaRelation_Customers = c.C_consolGuid 
                                        join SalesHierarchy sh WITH (NOLOCK) on sh.C_consolGuid = csar.C_consolFK_CustomerSalesAreaRelation_SalesHierarchy
                                        join salesRepAreaRelations srar WITH (NOLOCK) on srar.C_consolFK_SalesRepAreaRelations_SalesHierarchy = sh.C_consolGuid and srar.[validTo] > getdate()
                                        where c.version = (select max(version) from customers c2 where c.c_consolGuid = c2.c_consolGuid) 
                                        and c.operation = 'N'
                                        and sh.version = (select max(version) from SalesHierarchy sh2 where sh.c_consolGuid = sh2.c_consolGuid) 
                                        and sh.operation = 'N'
                                        and srar.version = (select max(version) from salesRepAreaRelations srar2 where srar.c_consolGuid = srar2.c_consolGuid) 
                                        and srar.operation = 'N'
                                        and srar.isPrimarySalesRep = 1";

        private const string QueryCustomerString = @"select c.id, c.street, c.city, c.isoCountryCode, c.zipCode
                                        from customers c WITH (NOLOCK)
                                        where c.version = (select max(version) from customers c2 where c.c_consolGuid = c2.c_consolGuid) 
                                        and c.operation = 'N'
                                        and {0} = '{1}'";

        private const string QueryCustomerString1604 = @";with CTE as (
	                                    select 
		                                    id, street, city, isoCountryCode, zipCode, longitude, latitude, operation, 
		                                    row_number() OVER (PARTITION BY id ORDER BY version desc) as rm
	                                    from Customers c WITH (NOLOCK)
	                                    )
	                                    select id, street, city, isoCountryCode, zipCode from CTE where rm = 1 and operation = 'N' and (longitude = '0.000000' or latitude = '0.000000' or longitude is null or latitude is null)
                                        and not zipCode in ('','-','0') and street <> '' and city <> ''";

        //set this froperties from config file
        private static string _connectionString = "";

        private static string _key = "";
        private static string _dbVersion = "";

        #endregion

        //    WebClient c = new WebClient();
        //    XmlSerializer xs = new XmlSerializer(typeof(GeocodeResponse));
        //{

        //public static void Test()
        //    byte[] response = c.DownloadData("http://maps.googleapis.com/maps/api/geocode/xml?address=1+Microsoft+Way,+Redmond,+WA&sensor=true");
        //    MemoryStream ms = new MemoryStream(response);
        //    GeocodeResponse geocodeResponse = (GeocodeResponse)xs.Deserialize(ms);
        //    Console.WriteLine(geocodeResponse);

        //    c = new WebClient();
        //    response = c.DownloadData("http://maps.googleapis.com/maps/api/geocode/xml?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&sensor=true");
        //    ms = new MemoryStream(response);
        //    geocodeResponse = (GeocodeResponse)xs.Deserialize(ms);
        //    Console.WriteLine(geocodeResponse);
        //}
    }
}