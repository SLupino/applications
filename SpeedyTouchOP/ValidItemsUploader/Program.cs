﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Helpers;
using Helpers.Classes;
using Microsoft.Practices.EnterpriseLibrary.WindowsAzure.TransientFaultHandling.SqlAzure;
using Microsoft.Practices.TransientFaultHandling;
using Newtonsoft.Json;
using NLog;
using ValidItemsUploader.Models;

namespace ValidItemsUploader
{
    internal class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly RetryPolicy ConsolidatedRetryPolicy =
            new RetryPolicy<SqlAzureTransientErrorDetectionStrategy>(3, TimeSpan.FromSeconds(10));

        private static string _host;
        private static string _hostCredentialKey;
        private static string _speedyEnvironmentApiUrl;

        private static void Main()
        {
            _host = ConfigurationManager.AppSettings["host"];
            _hostCredentialKey = ConfigurationManager.AppSettings["HostCredentialKey"];
            _speedyEnvironmentApiUrl = string.Equals(
            ConfigurationManager.AppSettings["IsProdEnvironment"], "true", StringComparison.InvariantCultureIgnoreCase)
            ? "https://speedyenvironmentprodapi.azurewebsites.net"
            : Debugger.IsAttached
                ? "https://speedyenvironmentsdevapi.azurewebsites.net"
                : "https://speedyenvironmentapi.azurewebsites.net";

            GetDataFromConsolDatabase();
        }

        private static void GetDataFromConsolDatabase()
        {
            Logger.Info("Process start.");
            var companiesConfig = CompaniesConfig.GetConfig().Companies.Where(c => c.Enabled);

            var exceptions = false;
            foreach (var companyInfo in companiesConfig)
                try
                {
                    DownloadAndSend(companyInfo).GetAwaiter().GetResult();
                }
                catch (Exception e)
                {
                    exceptions = true;
                    Logger.Error("Exception processing company {0} {1}: {2}", companyInfo.Code, companyInfo.EnvType,
                        e.InnerException?.Message ?? e.Message);
                }

            Logger.Info(exceptions ? "Process finished with exception." : "Process successfully finished.");
        }

        private static async Task DownloadAndSend(CompanyInfo companyInfo)
        {
            Logger.Debug("Start DownloadAndSend");

            var consolConnectionKey= companyInfo.ConsolConnectionKey;
            Logger.Debug("Get consolidated connection key '{0}", consolConnectionKey);
            var consolConnectionString = KeyVaultReader.GetValue(Logger, consolConnectionKey,_speedyEnvironmentApiUrl);
            if (string.IsNullOrEmpty(consolConnectionString))
            {
                Logger.Trace("Consolidated connection string not found");
                return;
            }

            var connectionStringBuilder = new SqlConnectionStringBuilder(consolConnectionString);

            Logger.Info("Processing company {0}", companyInfo.Code);
            var stopwatch = Stopwatch.StartNew();

            var started = false;

            Logger.Debug("Get credential key '{0}", _hostCredentialKey);
            var credentialsString = KeyVaultReader
                .GetValue(Logger, _hostCredentialKey, _speedyEnvironmentApiUrl)
                .Split(new[] {':'}, 2);
            var username = credentialsString[0];
            var password = credentialsString.Length == 2 ? credentialsString[1] : "";
            Logger.Trace("Found account '{0}", username);

            var credentials = new NetworkCredential(username, password);
            using (var handler = new HttpClientHandler {Credentials = credentials})
            {
                handler.MaxRequestContentBufferSize = 2147483647;

                using (var client = new HttpClient(handler))
                {
                    var url = $"{_host}/api/ValidItems?companyCode={companyInfo.Code}";
                    var response = await client.GetAsync(url);
                    var content = await response.Content.ReadAsStringAsync();

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var result = JsonConvert.DeserializeObject<string>(content);
                        foreach (var line in result.Split(new[] {Environment.NewLine}, StringSplitOptions.None))
                            Logger.Trace("Service response: {0}", line);

                        if (!result.Contains("--END--")) started = true;
                    }
                    else
                    {
                        Logger.Error("Service response error: {0}", content);
                        return;
                    }

                    if (!started)
                    {
                        Logger.Info("Connection to server '{0}' and database '{1}', CompanyCode {2} environment {3}.",
                            connectionStringBuilder.DataSource, connectionStringBuilder.InitialCatalog,
                            companyInfo.Code,
                            companyInfo.EnvType);

                        var items = new List<CatalogItem>();

                        const string stmt = @"
;with ART as
    (
        SELECT
            a.ArticleId
          , b.Status
          , b.ProductId
          , b.Operation
          , ROW_NUMBER() over (
                           partition by a.TechKey
                           order by
                               DataVersion desc) as 'rn'
        FROM
            cat.HEAD_Article a
          , cat.DATA_Article b
        WHERE
            a.TechKey = b.TechKey
    )
  , VART as
    (
        SELECT *
        FROM
            ART
        WHERE
            rn            = 1
            and Operation = 'N'
            and Status   <> 'old_article'
    )
SELECT DISTINCT
    ArticleId
  , ProductId
FROM
    VART
";

                        using (var connection = new SqlConnection(consolConnectionString))
                        {
                            try
                            {
                                connection.Open();
                                Logger.Trace("Connection opened.");

                                var command = new SqlCommand(stmt, connection);
                                using (command)
                                {
                                    command.CommandTimeout = 300;
                                    using (var reader = command.ExecuteReaderWithRetry(ConsolidatedRetryPolicy))
                                    {
                                        Logger.Trace("Query executed.");
                                        //Logger.Trace("Query {0} executed", command.CommandText);
                                        while (reader.Read())
                                        {
                                            var row = (IDataRecord) reader;
                                            var articleId = row.GetString(0);
                                            if (articleId.Length != 10 && articleId.Length != 18) continue;

                                            int.TryParse(articleId.Substring(articleId.Length - 5, 5).TrimStart(),
                                                out var packageSize);

                                            var productId = row.GetString(1);

                                            items.Add(new CatalogItem
                                            {
                                                ArticleId = articleId,
                                                CompanyCode = companyInfo.Code,
                                                ProductId = productId,
                                                IsMinSize = articleId == productId,
                                                PackageSize = articleId == productId ? 1 : packageSize
                                            });
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(
                                    $"Service exception processing company {companyInfo.Code} {companyInfo.EnvType}",
                                    ex);
                            }
                        }

                        stopwatch.Stop();
                        Logger.Info("Query took {0}, retrieved {1} items", stopwatch.Elapsed, items.Count);

                        if (items.Count == 0)
                        {
                            Logger.Info("No items to send.");
                            return;
                        }

                        if (companyInfo.MinValidArticle > 0 && items.Count < companyInfo.MinValidArticle)
                        {
                            Logger.Info("Not enough articles to proceed: found {0}, min {1}.", items.Count,
                                companyInfo.MinValidArticle);
                            return;
                        }

                        stopwatch.Start();
                        var itemWithIsMinPackSize = new List<CatalogItem>();
                        var itemsByProductId = (from CatalogItem catalogItem in items
                            group catalogItem by catalogItem.ProductId
                            into ng
                            select ng).ToList();
                        foreach (var group in itemsByProductId)
                        {
                            var orderCatalogItems =
                                (from CatalogItem item in @group orderby item.PackageSize select item).ToList();
                            orderCatalogItems[0].IsMinSize = true;
                            itemWithIsMinPackSize.AddRange(orderCatalogItems);
                        }

                        stopwatch.Stop();
                        Logger.Info("Calculating min pack size took {0}, processed {1} items.", stopwatch.Elapsed,
                            itemWithIsMinPackSize.Count);

                        stopwatch.Start();

                        var result = "";
                        try
                        {
                            var validItems = new ValidItems
                            {
                                CatalogItems = itemWithIsMinPackSize,
                                StagingConnectionString = companyInfo.StagingConnector
                            };
                            var json = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(validItems));

                            url = $"{_host}/api/ValidItems";
                            var data = new StringContent(json, Encoding.UTF8, "application/json");
                            response = await client.PostAsync(url, data);
                            content = await response.Content.ReadAsStringAsync();

                            if (response.StatusCode == HttpStatusCode.OK)
                            {
                                result = JsonConvert.DeserializeObject<string>(content);
                                Logger.Trace("Service response: {0}", result);
                            }
                            else
                            {
                                Logger.Error("Service response error: {0}", content);
                            }
                        }
                        catch (Exception ex)
                        {
                            stopwatch.Stop();
                            throw new Exception(
                                $"Service exception processing company {companyInfo.Code} {companyInfo.EnvType}",
                                ex.InnerException ?? ex);
                        }

                        if (result == "--START--") started = true;
                    }

                    if (started)
                    {
                        var retryTimes = 0;
                        retry:
                        try
                        {
                            var result = "";
                            do
                            {
                                Thread.Sleep(60000);

                                url = $"{_host}/api/ValidItems?companyCode={companyInfo.Code}";
                                response = await client.GetAsync(url);
                                content = await response.Content.ReadAsStringAsync();

                                if (response.StatusCode == HttpStatusCode.OK)
                                {
                                    result = JsonConvert.DeserializeObject<string>(content);
                                    foreach (var line in result.Split(new[] {Environment.NewLine},
                                        StringSplitOptions.None))
                                        Logger.Debug("Service response: {0}", line);
                                }
                                else
                                {
                                    Logger.Debug("Service response error: {0}", result);
                                }
                            } while (!result.Contains("--END--"));

                            stopwatch.Stop();
                            Logger.Info("Send items to preStaging took {0}", stopwatch.Elapsed);
                            return;
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex, "Service exception processing company {0} {1}", companyInfo.Code,
                                companyInfo.EnvType);
                            if (retryTimes > 3)
                            {
                                stopwatch.Stop();
                                Logger.Info("Impossible to reconnect the server, process aborted in {0}",
                                    stopwatch.Elapsed);
                                throw;
                            }

                            retryTimes++;
                            Logger.Debug("Trying to reconnect...{0}", retryTimes);
                            goto retry;
                        }
                    }
                }
            }

            stopwatch.Stop();
            Logger.Error("Unexpected end");
        }
    }
}