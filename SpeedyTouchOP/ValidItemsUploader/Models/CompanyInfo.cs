﻿using System.Collections.Generic;
using System.Configuration;

namespace ValidItemsUploader.Models
{
    public class CompaniesConfig : ConfigurationSection
    {
        public static CompaniesConfig GetConfig()
        {
            return (CompaniesConfig)System.Configuration.ConfigurationManager.GetSection("CompaniesConfig") ?? new CompaniesConfig();
        }

        [System.Configuration.ConfigurationProperty("Companies")]
        [ConfigurationCollection(typeof(Companies), AddItemName = "Company")]
        public Companies Companies
        {
            get
            {
                object obj = this["Companies"];
                return obj as Companies;
            }
        }
    }

    public class Companies : ConfigurationElementCollection, IEnumerable<CompanyInfo>
    {
        public CompanyInfo this[int index]
        {
            get
            {
                return base.BaseGet(index) as CompanyInfo;
            }
            set
            {
                if (base.BaseGet(index) != null)
                {
                    base.BaseRemoveAt(index);
                }
                this.BaseAdd(index, value);
            }
        }

        public new CompanyInfo this[string responseString]
        {
            get { return (CompanyInfo)BaseGet(responseString); }
            set
            {
                if (BaseGet(responseString) != null)
                {
                    BaseRemoveAt(BaseIndexOf(BaseGet(responseString)));
                }
                BaseAdd(value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new CompanyInfo();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return $"{((CompanyInfo)element).Code}-{((CompanyInfo)element).EnvType.ToUpper()}";
        }

        public new IEnumerator<CompanyInfo> GetEnumerator()
        {
            for (int i = 0; i < base.Count; i++)
            {
                yield return base.BaseGet(i) as CompanyInfo;
            }
        }
    }

    public class CompanyInfo : ConfigurationElement
    {
        [ConfigurationProperty("code", IsRequired = true)]
        public string Code
        {
            get { return (string)this["code"]; }
            set { this["code"] = value; }
        }

        [ConfigurationProperty("envType", IsRequired = true)]
        public string EnvType
        {
            get { return (string)this["envType"]; }
            set { this["envType"] = value; }
        }

        [ConfigurationProperty("enabled", IsRequired = true)]
        public bool Enabled
        {
            get { return (bool)this["enabled"]; }
            set { this["enabled"] = value; }
        }

        [ConfigurationProperty("stagingConnector", IsRequired = true)]
        public string StagingConnector
        {
            get { return (string)this["stagingConnector"]; }
            set { this["stagingConnector"] = value; }
        }

        [ConfigurationProperty("consolConnectionKey", IsRequired = true)]
        public string ConsolConnectionKey
        {
            get { return (string)this["consolConnectionKey"]; }
            set { this["consolConnectionKey"] = value; }
        }

        [ConfigurationProperty("minValidArticle", IsRequired = false)]
        public int MinValidArticle
        {
            get { return (int)this["minValidArticle"]; }
            set { this["minValidArticle"] = value; }
        }
    }
}
