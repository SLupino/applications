﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using Helpers;
using Newtonsoft.Json;
using NLog;
using SpeedyOrderReports.Helpers;
using Helpers.Classes;

namespace SpeedyOrderReports
{
    internal class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static void Main()
        {
            Logger.Info("Report Generator");

            try
            {
                foreach (var companyCode in Settings.CompanyCodes)
                {
                    Logger.Info("Start process for company: {0}", companyCode);
                    string result;

                    var host = Settings.Host(companyCode);
                    var hostKey = Settings.HostCredentialKey(companyCode);
                    Logger.Debug("Get credential key '{0}", hostKey);
                    var credentialsString = KeyVaultReader
                        .GetValue(Logger, hostKey, Settings.SpeedyEnvironmentApiUrl)
                        .Split(new[] {':'}, 2);
                    var username = credentialsString[0];
                    var password = credentialsString.Length == 2 ? credentialsString[1] : "";
                    Logger.Debug("Found account '{0}", username);

                    var credentials = new NetworkCredential(username, password);
                    using (var handler = new HttpClientHandler {Credentials = credentials})
                    {
                        handler.MaxRequestContentBufferSize = 2147483647;

                        using (var client = new HttpClient(handler))
                        {
                            Logger.Trace("client instantiated");

                            var request = new OrderReportRequest
                            {
                                CompanyCode = companyCode,
                                StagingConnectionString = Settings.StagingDatabase(companyCode),
                                IncomingConnectionString = Settings.IncomingDatabase(companyCode),
                                ForceMonthlyReport = Settings.ForceMonthlyReport
                            };
                            var json = JsonConvert.SerializeObject(request);

                            var url = $"{host}/api/OrderReports";
                            var data = new StringContent(json, Encoding.UTF8, "application/json");
                            var response = client.PostAsync(url, data).Result;
                            if (response.StatusCode != HttpStatusCode.OK)
                            {
                                Logger.Error("Service Response: HTTP {0}", response.StatusCode);
                                return;
                            }
                            var content = response.Content.ReadAsStringAsync().Result;
                            result = JsonConvert.DeserializeObject<string>(content);

                            Logger.Trace("Service Response: {0}", result);
                        }
                    }

                    var generalReportPath = ExcelHelper.WriteGeneralReportExcel(result, companyCode);
                    Logger.Trace("general report path: {0}", generalReportPath);
                    var monthlyReportPath = string.Empty;
                    var dateNow = DateTime.Now;
                    if (Settings.ForceMonthlyReport || dateNow.Day == 1 ||
                        dateNow.DayOfWeek == DayOfWeek.Monday && (dateNow.Day == 2 || dateNow.Day == 3))
                    {
                        monthlyReportPath = ExcelHelper.WriteMonthlyReportExcel(result, companyCode);
                        Logger.Trace("monthly report path: {0}", monthlyReportPath);
                    }

                    EmailHelper.SendEmail(Settings.SmtpConfig,
                        Settings.FromEmail(companyCode), Settings.ToEmails(companyCode),
                        Settings.CcEmails(companyCode), new List<string> {generalReportPath, monthlyReportPath},
                        Settings.Subject(companyCode), Settings.Body(companyCode));
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Exception: {0}", e.InnerException?.Message ?? e.Message);
                throw;
            }
        }
    }
}