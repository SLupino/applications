﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;

namespace SpeedyOrderReports
{
    public static class Settings
    {
        public static List<string> SmtpConfig =>
            new List<string> { ConfigurationManager.AppSettings["SMTPServer"],
                ConfigurationManager.AppSettings["SMTPUser"],
                ConfigurationManager.AppSettings["SMTPPassword"],
                ConfigurationManager.AppSettings["SMTPDomain"] };

        public static List<string> CompanyCodes => ConfigurationManager.AppSettings.AllKeys
            .Where(x => x.Split('.').Length > 1)
            .Select(x => x.Split('.')[0]).Distinct().ToList();

        public static bool ForceMonthlyReport => string.Equals(ConfigurationManager.AppSettings["ForceMonthlyReport"] ?? "",
            "true", StringComparison.InvariantCultureIgnoreCase);

        public static string ReportTemplate => ConfigurationManager.AppSettings["ReportTemplate"];

        public static string ReportTemplateMonday => ConfigurationManager.AppSettings["ReportTemplateMonday"];

        public static string OrderReportDestination(string companyCode)
        {
            return ConfigurationManager.AppSettings[$"{companyCode}.OrderReportDestination"];
        }

        public static int UserCharacterCount(string companyCode)
        {
            return Convert.ToInt32(ConfigurationManager.AppSettings[$"{companyCode}.UserCharacterCount"]);
        }

        public static string ReportTemplateMonthly => ConfigurationManager.AppSettings["ReportTemplateMonthly"];

        public static string MonthlyReportDestination(string companyCode)
        {
            return  ConfigurationManager.AppSettings[$"{companyCode}.MonthlyReportDestination"];
        }

        public static string Host(string companyCode)
        {
            return ConfigurationManager.AppSettings[companyCode + ".host"] ?? ConfigurationManager.AppSettings["host"];
        }
        public static string HostCredentialKey(string companyCode)
        {
            return ConfigurationManager.AppSettings[companyCode + ".HostCredentialKey"] ?? ConfigurationManager.AppSettings["HostCredentialKey"];
        }

        public static string SpeedyEnvironmentApiUrl => string.Equals(
            ConfigurationManager.AppSettings["IsProdEnvironment"], "true", StringComparison.InvariantCultureIgnoreCase)
            ? "https://speedyenvironmentprodapi.azurewebsites.net"
            : Debugger.IsAttached
                ? "https://speedyenvironmentsdevapi.azurewebsites.net"
                : "https://speedyenvironmentapi.azurewebsites.net";

        public static string FromEmail(string companyCode)
        {
            return ConfigurationManager.AppSettings[companyCode + ".FromEmail"];
        }
        public static List<string> ToEmails(string companyCode)
        {
            return ConfigurationManager.AppSettings[companyCode + ".ToEmail"].Split(';').ToList();
        }
        public static List<string> CcEmails(string companyCode)
        {
            return ConfigurationManager.AppSettings[companyCode + ".CCEmail"].Split(';').ToList();
        }
        public static string Subject(string companyCode)
        {
            return ConfigurationManager.AppSettings[companyCode + ".SubjectEmail"];
        }
        public static string Body(string companyCode)
        {
            return ConfigurationManager.AppSettings[companyCode + ".BodyEmail"].Replace("|", @"<br\>");
        }
        public static string StagingDatabase(string companyCode)
        {
            return ConfigurationManager.ConnectionStrings[companyCode + ".StagingDatabase"].ConnectionString;
        }
        public static string IncomingDatabase(string companyCode)
        {
            return ConfigurationManager.ConnectionStrings[companyCode + ".IncomingDatabase"].ConnectionString;
        }
        public static string ConsolidatedConnection(string companyCode)
        {
            return ConfigurationManager.ConnectionStrings[companyCode + ".ConsolidatedConnection"].ConnectionString;
        }
    }
    
}

                                                                        