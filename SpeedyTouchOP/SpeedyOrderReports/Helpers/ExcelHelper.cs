﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using System.Xml.XPath;
using Microsoft.Office.Interop.Excel;
using NLog;
using Helpers.Classes;

namespace SpeedyOrderReports.Helpers
{
    internal class ExcelHelper
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static string WriteGeneralReportExcel(string data, string companyCode)
        {
            Logger.Trace("Start general report generator");
            var generalPathTemplate = Settings.ReportTemplate;
            var mondayPathTemplate = Settings.ReportTemplateMonday;
            var outputPath = Settings.OrderReportDestination(companyCode);
            var characterNumber = Settings.UserCharacterCount(companyCode);
            var dateNow = DateTime.Now;
            var attachmentPath = $"{outputPath}_{dateNow.Year}-{dateNow.Month}-{dateNow.Day}.xlsx";

            try
            {
                var xlApp = new Application {DisplayAlerts = false};
                Workbook excelWorkbook;
                Worksheet saturdayOrdersShit = null;
                Worksheet fridayOrdersShit = null;
                Sheets excelSheets;
                if (DateTime.Now.DayOfWeek == DayOfWeek.Monday)
                {
                    Logger.Trace("Is monday");
                    excelWorkbook = xlApp.Workbooks.Open(mondayPathTemplate, 0, false, 5, "", "",
                        true, XlPlatform.xlWindows, "", true, false, 0, true, false, false);

                    excelSheets = excelWorkbook.Worksheets;

                    saturdayOrdersShit = (Worksheet) excelSheets.Item["Saturday orders"];
                    fridayOrdersShit = (Worksheet) excelSheets.Item["Friday orders"];
                }
                else
                {
                    Logger.Trace("Isn't monday");
                    excelWorkbook =
                        xlApp.Workbooks.Open(
                            generalPathTemplate, 0, false, 5, "", "", true,
                            XlPlatform.xlWindows, "", true, false, 0, true, false, false);

                    excelSheets = excelWorkbook.Worksheets;
                }

                var lastMonthOrdersShit = (Worksheet) excelSheets.Item["Last 30 days"];
                var yesterdayOrdersShit = (Worksheet) excelSheets.Item["Yesterday orders"];
                var adminAMonthShit = (Worksheet) excelSheets.Item["ADM in a month"];

                var xData = XElement.Parse(data);
                var monthOrders = OrderMonthStatisticsList(xData.XPathSelectElement("//LastMonthData"));
                var yesterdayOrders = OldDayList(xData.XPathSelectElement("//YesterdayData"));
                var saturdayOrders = OldDayList(xData.XPathSelectElement("//SaturdayData"));
                var fridayOrders = OldDayList(xData.XPathSelectElement("//FridayData"));
                var adminMonth = OldDayList(xData.XPathSelectElement("//LastMonthAdmData"));

                ModifyReport(monthOrders, lastMonthOrdersShit, true, characterNumber);
                ModifyReport(yesterdayOrders, yesterdayOrdersShit, false, characterNumber);
                if (saturdayOrdersShit != null)
                    ModifyReport(saturdayOrders, saturdayOrdersShit, false, characterNumber);
                if (fridayOrdersShit != null)
                    ModifyReport(fridayOrders, fridayOrdersShit, false, characterNumber);
                ModifyReport(adminMonth, adminAMonthShit, false, characterNumber);
                Logger.Trace("start save file in path: {0}", attachmentPath);
                excelWorkbook.SaveAs(attachmentPath);
                xlApp.Workbooks.Close();
                Logger.Trace("end save file in path: {0}", attachmentPath);
            }
            catch (Exception e)
            {
                Logger.Trace("EXCEPTION: {0}", e.Message);
                throw;
            }

            return attachmentPath;
        }

        public static string WriteMonthlyReportExcel(string data, string companyCode)
        {
            Logger.Trace("Start monthly report generator");
            var monthlyPathTemplate = Settings.ReportTemplateMonthly;
            var outputMonthlyReportPath = Settings.MonthlyReportDestination(companyCode);
            var characterNumber = Settings.UserCharacterCount(companyCode);
            var dateNow = DateTime.Now;
            var year = dateNow.Month == 1 ? dateNow.Year - 1 : dateNow.Year;
            var month = dateNow.Month == 1 ? 12 : dateNow.Month - 1;
            var attachmentPath = $"{outputMonthlyReportPath}_{year}-{month}.xlsx";

            try
            {
                var xlApp = new Application {DisplayAlerts = false};

                var excelWorkbook = xlApp.Workbooks.Open(
                    monthlyPathTemplate, 0, false, 5, "", "", true,
                    XlPlatform.xlWindows, "", true, false, 0, true, false, false);

                var excelSheets = excelWorkbook.Worksheets;

                var lastMonthOrdersShit = (Worksheet) excelSheets.Item["Monthly Order"];

                var xData = XElement.Parse(data);
                var monthOrders = OldDayList(xData.XPathSelectElement("//MonthlyData"));

                ModifyReport(monthOrders, lastMonthOrdersShit, false, characterNumber);

                excelWorkbook.SaveAs(attachmentPath);
                xlApp.Workbooks.Close();
            }
            catch (Exception e)
            {
                Logger.Trace("EXCEPTION: {0}", e.Message);
            }

            return attachmentPath;
        }

        private static List<OrderMonthStatistics> OrderMonthStatisticsList(XElement lastMonth)
        {
            var statisticsList = lastMonth.XPathSelectElements("Statistics");

            return statisticsList.Select(statistics => new OrderMonthStatistics
            {
                Date = Convert.ToDateTime(statistics.XPathSelectElement("Date")?.Value),
                OrderCount = Convert.ToInt32(statistics.XPathSelectElement("OrderCount")?.Value)
            }).ToList();
        }

        private static List<OrderUserStatistics> OldDayList(XElement oldDay)
        {
            if (oldDay != null)
            {
                var statisticsList = oldDay.XPathSelectElements("User");

                return statisticsList.Select(statistics => new OrderUserStatistics
                {
                    ManagerId = statistics.XPathSelectElement("ManagerId")?.Value,
                    UserId = statistics.XPathSelectElement("Id")?.Value,
                    UserName = statistics.XPathSelectElement("Name")?.Value,
                    UserType = statistics.XPathSelectElement("Type")?.Value,
                    OrderCount = Convert.ToInt32(statistics.XPathSelectElement("OrderCount")?.Value)
                }).ToList();
            }

            return new List<OrderUserStatistics>();
        }

        private static void ModifyReport(IEnumerable<object> orders, Worksheet dataShit, bool isMonthlyShit,
            int characterNumber)
        {
            Logger.Trace("Start modify report");
            var rowCount = 2;
            object misValue = Missing.Value;
            var stats = orders as IList<object> ?? orders.ToList();
            var allRowsCount = 0;
            IEnumerable<OrderUserStatistics> admStats = new List<OrderUserStatistics>();

            if (!isMonthlyShit)
            {
                admStats = stats.Cast<OrderUserStatistics>().Where(x => x.UserType != "AM").OrderBy(x => x.UserType)
                    .ToList();
                allRowsCount =
                    dataShit.Name == "Yesterday orders" || dataShit.Name == "Saturday orders" ||
                    dataShit.Name == "Friday orders"
                        ? stats.Count
                        : admStats.Count();
                Logger.Trace("Monthly sheet");
                dataShit.Range[$"B{2 + allRowsCount}", $"C{2 + allRowsCount}"]
                    .Merge();
                dataShit.Range[$"B{2 + allRowsCount}", $"C{2 + allRowsCount}"]
                    .HorizontalAlignment = XlHAlign.xlHAlignCenter;
                dataShit.Range[$"B{2 + allRowsCount}", $"C{2 + allRowsCount}"].Value =
                    "TOTAL";

                dataShit.Cells[2 + allRowsCount, 4].Formula = $"=SUM(D2:D{allRowsCount + 1})";
                dataShit.Range[$"B{2 + allRowsCount}", $"C{2 + allRowsCount}"]
                    .BorderAround(Type.Missing, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic,
                        Type.Missing);
                dataShit.Range[$"D{2 + allRowsCount}", $"D{2 + allRowsCount}"]
                    .BorderAround(Type.Missing, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic,
                        Type.Missing);

                dataShit.Range["B2", $"B{1 + allRowsCount}"]
                    .BorderAround(Type.Missing, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic,
                        Type.Missing);
                dataShit.Range["C2", $"C{1 + allRowsCount}"]
                    .BorderAround(Type.Missing, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic,
                        Type.Missing);
                dataShit.Range["D2", $"D{1 + allRowsCount}"]
                    .BorderAround(Type.Missing, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic,
                        Type.Missing);

                var xlCharts = (ChartObjects) dataShit.ChartObjects(Type.Missing);
                var myChart = xlCharts.Add(400, 13, 3000, 600);
                var chartPage = myChart.Chart;

                var chartRange = dataShit.Range["C1", $"D{1 + allRowsCount}"];
                chartPage.SetSourceData(chartRange, misValue);
                chartPage.ChartType = XlChartType.xlColumnClustered;
            }

            if (isMonthlyShit)
            {
                foreach (OrderMonthStatistics order in stats)
                {
                    ((Range) dataShit.Cells[rowCount, 2]).Value = order.Date;
                    ((Range) dataShit.Cells[rowCount, 3]).Value = order.OrderCount;
                    rowCount++;
                }
            }
            else
            {
                if (dataShit.Name == "Yesterday orders" || dataShit.Name == "Saturday orders" ||
                    dataShit.Name == "Friday orders")
                {
                    IEnumerable<OrderUserStatistics> admWithoutAmStats =
                        stats.Cast<OrderUserStatistics>()
                            .Where(x => x.UserType != "AM" && (x.ManagerId == "null" || x.ManagerId == ""))
                            .OrderBy(x => x.UserName)
                            .ToList();

                    stats = stats.Except(admWithoutAmStats).ToList();

                    var amStats =
                        stats.Cast<OrderUserStatistics>()
                            .Where(x => x.UserType == "AM")
                            .OrderBy(x => x.UserType)
                            .ToList();
                    stats = stats.Except(amStats).ToList();

                    var admStatsFiltered = (from OrderUserStatistics amStat in amStats.AsEnumerable()
                        join OrderUserStatistics stat in stats.AsEnumerable()
                            on amStat.UserId equals stat.ManagerId
                        select stat).ToList();
                    stats = stats.Except(admStatsFiltered).ToList();

                    foreach (var admStat in admWithoutAmStats)
                    {
                        if (rowCount <= allRowsCount) SetCelStyle(dataShit, rowCount);
                        SetCelValues(admStat, dataShit, rowCount, characterNumber);

                        rowCount++;
                    }

                    foreach (OrderUserStatistics stat in stats.ToList())
                    {
                        if (rowCount <= allRowsCount) SetCelStyle(dataShit, rowCount);
                        SetCelValues(stat, dataShit, rowCount, characterNumber);
                        rowCount++;
                    }

                    foreach (var amStat in amStats)
                    {
                        if (rowCount <= allRowsCount) SetCelStyle(dataShit, rowCount);
                        SetCelValues(amStat, dataShit, rowCount, characterNumber);

                        //var rowCell = rowCount;
                        var formatRange = dataShit.Range[$"b{rowCount}",
                            $"e{rowCount}"];
                        formatRange.Font.Color = 255;
                        var admStatsFromAm =
                            admStatsFiltered.Where(x => x.ManagerId == amStat.UserId).ToList();
                        foreach (var admStat in admStatsFromAm)
                        {
                            rowCount++;
                            if (rowCount <= allRowsCount) SetCelStyle(dataShit, rowCount);
                            SetCelValues(admStat, dataShit, rowCount, characterNumber);
                        }

                        rowCount++;
                    }
                }
                else
                {
                    foreach (var admStat in admStats)
                    {
                        if (rowCount <= allRowsCount) SetCelStyle(dataShit, rowCount);
                        SetCelValues(admStat, dataShit, rowCount, characterNumber);

                        rowCount++;
                    }
                }
            }

            if (dataShit.Name == "Yesterday orders" || dataShit.Name == "Saturday orders" ||
                dataShit.Name == "Friday orders")
                dataShit.Range["E2", $"E{1 + allRowsCount}"].BorderAround(Type.Missing, XlBorderWeight.xlMedium,
                    XlColorIndex.xlColorIndexAutomatic, Type.Missing);

            dataShit.Columns["B:B"].ColumnWidth = dataShit.Name != "Last 30 days" ? characterNumber + 2 : 10;
        }

        private static void SetCelValues(OrderUserStatistics orderAdm, Worksheet dataShit, int rowCount,
            int characterNumber)
        {
            var admId = orderAdm.UserId;
            ((Range) dataShit.Cells[rowCount, 2]).Value =
                admId.Substring(admId.Length - characterNumber, characterNumber);
            ((Range) dataShit.Cells[rowCount, 3]).Value = orderAdm.UserName;
            ((Range) dataShit.Cells[rowCount, 4]).Value = orderAdm.OrderCount;
            if (dataShit.Name == "Yesterday orders" || dataShit.Name == "Saturday orders" ||
                dataShit.Name == "Friday orders")
                ((Range) dataShit.Cells[rowCount, 5]).Value = orderAdm.UserType;
        }

        private static void SetCelStyle(Worksheet dataShit, int rowCount)
        {
            try
            {
                Logger.Trace("start pasteSpecial row {0}", rowCount + 1);
                var r1 = (Range) dataShit.Cells[rowCount, 2];
                r1.Copy(Type.Missing);
                var r2 = (Range) dataShit.Cells[rowCount + 1, 2];
                r2.PasteSpecial(XlPasteType.xlPasteFormats,
                    XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);

                r1 = (Range) dataShit.Cells[rowCount, 3];
                r1.Copy(Type.Missing);
                r2 = (Range) dataShit.Cells[rowCount + 1, 3];
                r2.PasteSpecial(XlPasteType.xlPasteFormats,
                    XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);

                r1 = (Range) dataShit.Cells[rowCount, 4];
                r1.Copy(Type.Missing);
                r2 = (Range) dataShit.Cells[rowCount + 1, 4];
                r2.PasteSpecial(XlPasteType.xlPasteFormats,
                    XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);

                r1 = (Range) dataShit.Cells[rowCount, 5];
                r1.Copy(Type.Missing);
                r2 = (Range) dataShit.Cells[rowCount + 1, 5];
                r2.PasteSpecial(XlPasteType.xlPasteFormats,
                    XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);
                Logger.Trace("end pasteSpecial row {0}", rowCount + 1);
            }
            catch (Exception e)
            {
                Logger.Trace("Exception: {0}", e.Message);
            }
        }
    }
}