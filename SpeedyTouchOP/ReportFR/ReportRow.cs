﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportFR
{
    class ReportRow
    {
        public string Id { get; set; }
        public string ParamUserSystemAccount { get; set; }
        public string RequestType { get; set; }
        public string ParamLastUpdate { get; set; }
        public string DownloadType { get; set; }
        public string WorkerProcessCreated { get; set; }
        public string RequestReceived { get; set; }
        public string RequestEnqueued { get; set; }
        public string RequestProcessingStart { get; set; }
        public string RequestProcessingEnd { get; set; }
        public string Status { get; set; }
        public string WebResultUrl1 { get; set; }
    }
}