﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Outlook;
using ApplicationExcel = Microsoft.Office.Interop.Excel.Application;
using ApplicationOutlook = Microsoft.Office.Interop.Outlook.Application;

namespace ReportFR
{
    class Program
    {
        static void Main()
        {
            var xlApp = new ApplicationExcel { DisplayAlerts = false };
            Workbook excelWorkbook = xlApp.Workbooks.Open(Path.GetFullPath("../ExternalFiles/Template.xlsx"), 0, false, 5, "", "", true,
                XlPlatform.xlWindows, "", true, false, 0, true, false, false);

            var excelSheets = excelWorkbook.Worksheets;
            ModifyReport((Worksheet)excelSheets[ConfigurationManager.AppSettings["Sheet1"]]);
            var list = SelectReport();
            ModifyReport2((Worksheet)excelSheets[ConfigurationManager.AppSettings["Sheet2"]], list);

            excelWorkbook.SaveAs(ConfigurationManager.AppSettings["PathOutputFile"] + @"\" + ConfigurationManager.AppSettings["OutputFileName"]);
            xlApp.Workbooks.Close();

           SendEmailtoContacts(list);
        }

        private static void ModifyReport2(Worksheet dataShit, IEnumerable<ReportRow> list )
        {
            int rowcount = 3;
            foreach (var row in list.ToList())
            {
                ((Range)dataShit.Cells[rowcount, 2]).Value = row.Id;
                ((Range)dataShit.Cells[rowcount, 3]).Value = row.ParamUserSystemAccount;
                ((Range)dataShit.Cells[rowcount, 4]).Value = row.RequestType;
                ((Range)dataShit.Cells[rowcount, 5]).Value = row.ParamLastUpdate;
                ((Range)dataShit.Cells[rowcount, 6]).Value = row.DownloadType;
                ((Range)dataShit.Cells[rowcount, 7]).Value = row.WorkerProcessCreated;
                ((Range)dataShit.Cells[rowcount, 8]).Value = row.RequestReceived;
                ((Range)dataShit.Cells[rowcount, 9]).Value = row.RequestEnqueued;
                ((Range)dataShit.Cells[rowcount, 10]).Value = row.RequestProcessingStart;
                ((Range)dataShit.Cells[rowcount, 11]).Value = row.RequestProcessingEnd;
                ((Range)dataShit.Cells[rowcount, 12]).Value = row.Status;
                ((Range)dataShit.Cells[rowcount, 13]).Value = row.WebResultUrl1;

                rowcount++;
            }
        }

        private static  void ModifyReport(Worksheet dataShit)
        {
            int rowcount = 24;
            var orderList = SelectUpdates();
            var columnInfo = new Dictionary<string, int>();

            var orders = orderList as IList<Order> ?? orderList.ToList();
            foreach (var order in orders.ToList())
            {
                if (!columnInfo.ContainsKey(order.Id))
                {
                    ((Range) dataShit.Cells[rowcount, 2]).Value = order.Id;
                    switch (order.Name)
                    {
                        case "GetCatalogData_3":
                            ((Range)dataShit.Cells[rowcount, 3]).Value = order.Count;
                            break;
                        case "GetCustomerData_3":
                            ((Range)dataShit.Cells[rowcount, 4]).Value = order.Count;
                            break;
                        case "Personalize":
                            ((Range)dataShit.Cells[rowcount, 5]).Value = order.Count;
                            break;
                        default:
                            ((Range) dataShit.Cells[rowcount, 5]).Value = 0;
                            break;
                    }
                    columnInfo.Add(order.Id, rowcount);

                    rowcount++;
                }
                else
                {
                    switch (order.Name)
                    {
                        case "GetCatalogData_3":
                            ((Range)dataShit.Cells[columnInfo[order.Id], 3]).Value = order.Count;
                            break;
                        case "GetCustomerData_3":
                            ((Range)dataShit.Cells[columnInfo[order.Id], 4]).Value = order.Count;
                            break;
                        case "Personalize":
                            ((Range)dataShit.Cells[columnInfo[order.Id], 5]).Value = order.Count;
                            break;
                        default:
                            ((Range)dataShit.Cells[rowcount, 5]).Value = 0;
                            break;
                    }
                }
            }

            object misValue = Missing.Value;
            var xlCharts = (ChartObjects)dataShit.ChartObjects(Type.Missing);
            var myChart = xlCharts.Add(60, 13, 668.5, 255);
            Chart chartPage = myChart.Chart;

            Range chartRange = dataShit.Range["B23", string.Format("E{0}", rowcount -1)];
            chartPage.SetSourceData(chartRange, misValue);
        }

        private static IEnumerable<Order> SelectUpdates()
        {
            var orders = new List<Order>();
            using (var connection = new SqlConnection(ConfigurationManager.AppSettings["PathDB"]))
            {
                connection.Open();
                var dateNow = DateTime.Now;
                var yesterday = dateNow.AddDays(-1);
                using (var command = new SqlCommand(string.Format(ConfigurationManager.AppSettings["QuerySheet1"], yesterday.Year, yesterday.Month, yesterday.Day, dateNow.Year, dateNow.Month, dateNow.Day), connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                var row = ((IDataRecord)reader);
                                orders.Add(new Order
                                {
                                    Id = row[0].ToString(),
                                    Name = row[1].ToString(),
                                    Count = row[2].ToString()
                                });
                            }
                        }
                        finally
                        {
                            reader.Close();
                        }
                    }  
                }
            }

            return orders;
        }

        private static IEnumerable<ReportRow> SelectReport()
        {
            var reportRow = new List<ReportRow>();
            using (var connection = new SqlConnection(ConfigurationManager.AppSettings["PathDB"]))
            {
                connection.Open();
                var dateNow = DateTime.Now;
                var yesterday = dateNow.AddDays(-1);
                using (var command = new SqlCommand(string.Format(ConfigurationManager.AppSettings["QuerySheet2"], yesterday.Year, yesterday.Month, yesterday.Day, dateNow.Year, dateNow.Month, dateNow.Day), connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                var row = ((IDataRecord)reader);
                                reportRow.Add(new ReportRow
                                {
                                    Id = row[0].ToString(),
                                    ParamUserSystemAccount = row[1].ToString(),
                                    RequestType = row[2].ToString(),
                                    ParamLastUpdate = row[3].ToString(),
                                    DownloadType = row[4].ToString(),
                                    WorkerProcessCreated = row[5].ToString(),
                                    RequestReceived = row[6].ToString(),
                                    RequestEnqueued = row[7].ToString(),
                                    RequestProcessingStart = row[8].ToString(),
                                    RequestProcessingEnd = row[9].ToString(),
                                    Status = row[10].ToString(),
                                    WebResultUrl1 = row[11].ToString()
                                });
                            }
                        }
                        finally
                        {
                            reader.Close();
                        }
                    }
                }
            }

            return reportRow;
        }

        private static void SendEmailtoContacts(IEnumerable<ReportRow> list)
        {
            CreateEmailItem(ConfigurationManager.AppSettings["SubjectEmail"], ConfigurationManager.AppSettings["ToEmail"], ConfigurationManager.AppSettings["CCEmail"], list);
        }

        private static string GenerateBodyEMail(IEnumerable<ReportRow> list)
        {
            var dateNow = DateTime.Now;
            var yesterday = dateNow.AddDays(-1);
            string paramUserSystemAccount = string.Empty;
            string blobs = string.Empty;
            list.Select(x => x.ParamUserSystemAccount).Distinct().ToList().ForEach(x =>
            {
                paramUserSystemAccount += string.Format("<b>{0}</b><br/>", x);
                blobs += string.Format("<b><u>{0}</u></b><br/>", x);
                list.Where(y => y.ParamUserSystemAccount == x).Select(y => y.WebResultUrl1).ToList().ForEach(y => { blobs += string.Format(@"<a href='{0}'>{0}</a><br/><br/>", y); });
            });

            string body = string.Format(
                "Hi Franck,<br/><br/>" +
                "In attachment the connections of {0}<br/><br/>" +
                "Shortly the explanation of the connections of the users.<br/><br/>" +
                "For each users the system generated update for<br/>" +
                "{1}<br/>" +
                "Below details for the last connection for each users:<br/><br/>" +
                "{2}" +
                "Regards,", yesterday.ToString("d"), paramUserSystemAccount, blobs);

            return body;
        }

        private static void CreateEmailItem(string subjectEmail,
               string toEmail, string cc, IEnumerable<ReportRow> list)
        {
            var application = new ApplicationOutlook();
            var eMail = (MailItem)application.CreateItem(OlItemType.olMailItem);
            eMail.Subject = subjectEmail;
            eMail.To = toEmail;
            eMail.CC = cc;
            eMail.Body = GenerateBodyEMail(list);
            eMail.Importance = OlImportance.olImportanceLow;
            eMail.Attachments.Add(ConfigurationManager.AppSettings["PathOutputFile"] + @"\" + ConfigurationManager.AppSettings["OutputFileName"], OlAttachmentType.olByValue, Type.Missing, Type.Missing);

            var billede1 = string.Empty;
            var billede2 = string.Empty;

            string signature = ReadSignature();
            if (signature.Contains("img"))
            {
                int position = signature.LastIndexOf("img", StringComparison.Ordinal);
                int position1 = signature.IndexOf("src", position, StringComparison.Ordinal);
                position1 = position1 + 5;
                position = signature.IndexOf("\"", position1, StringComparison.Ordinal);

                const string schemaPrAttachContentId = @"http://schemas.microsoft.com/mapi/proptag/0x3712001E";
                string contentId = Guid.NewGuid().ToString();

                eMail.Attachments.Add(@billede1, OlAttachmentType.olByValue, eMail.Body.Length, Type.Missing);
                eMail.Attachments[eMail.Attachments.Count].PropertyAccessor.SetProperty(schemaPrAttachContentId, contentId);

                string banner = string.Format(@"cid:{0}", contentId);
                signature = signature.Remove(position1, position - position1);
                signature = signature.Insert(position1, banner);

                position = signature.LastIndexOf("imagedata", StringComparison.Ordinal);
                position1 = signature.IndexOf("src", position, StringComparison.Ordinal);
                position1 = position1 + 5;
                position = signature.IndexOf("\"", position1, StringComparison.Ordinal);

                contentId = Guid.NewGuid().ToString();

                eMail.Attachments.Add(@billede2, OlAttachmentType.olByValue, eMail.Body.Length, Type.Missing);
                eMail.Attachments[eMail.Attachments.Count].PropertyAccessor.SetProperty(schemaPrAttachContentId, contentId);

                banner = string.Format(@"cid:{0}", contentId);
                signature = signature.Remove(position1, position - position1);
                signature = signature.Insert(position1, banner);
            }
            eMail.HTMLBody = eMail.Body + "<br/>" + signature;

            (eMail).Send();
        }

        private static string ReadSignature()
        {
            string appDataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Microsoft\\Signatures";
            string signature = string.Empty;
            var diInfo = new DirectoryInfo(appDataDir);

            if (diInfo.Exists)
            {
                FileInfo[] fiSignature = diInfo.GetFiles("*.htm");

                if (fiSignature.Length > 0)
                {
                    var sr = new StreamReader(fiSignature[0].FullName, Encoding.Default);
                    signature = sr.ReadToEnd();

                    if (!string.IsNullOrEmpty(signature))
                    {
                        string fileName = fiSignature[0].Name.Replace(fiSignature[0].Extension, string.Empty);
                        signature = signature.Replace(fileName + "_files/", appDataDir + "/" + fileName + "_files/");
                    }
                }
            }
            else
            {
                appDataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Microsoft\\Signaturer";
                signature = string.Empty;
                diInfo = new DirectoryInfo(appDataDir);

                if (diInfo.Exists)
                {
                    FileInfo[] fiSignature = diInfo.GetFiles("*.htm");

                    if (fiSignature.Length > 0)
                    {
                        var sr = new StreamReader(fiSignature[0].FullName, Encoding.Default);
                        signature = sr.ReadToEnd();

                        if (!string.IsNullOrEmpty(signature))
                        {
                            string fileName = fiSignature[0].Name.Replace(fiSignature[0].Extension, string.Empty);
                            signature = signature.Replace(fileName + "_files/", appDataDir + "/" + fileName + "_files/");
                        }
                    }
                }
            }

            return signature;
        }
    }
}
