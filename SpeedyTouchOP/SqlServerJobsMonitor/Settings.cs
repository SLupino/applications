﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using Helpers.Classes;

namespace SqlServerJobsMonitor
{
    public static class Settings
    {
        public static string Host => ConfigurationManager.AppSettings["Host"];

        public static string SpeedyEnvironmentApiUrl => string.Equals(
            ConfigurationManager.AppSettings["IsProdEnvironment"], "true", StringComparison.InvariantCultureIgnoreCase)
            ? "https://speedyenvironmentprodapi.azurewebsites.net"
            : Debugger.IsAttached
                ? "https://speedyenvironmentsdevapi.azurewebsites.net"
                : "https://speedyenvironmentapi.azurewebsites.net";

        public static string HostCredentialKey => ConfigurationManager.AppSettings["HostCredentialKey"];

        public static List<string> SmtpConfig = new List<string> { ConfigurationManager.AppSettings["SMTPServer"],
            ConfigurationManager.AppSettings["SMTPUser"],
            ConfigurationManager.AppSettings["SMTPPassword"],
            ConfigurationManager.AppSettings["SMTPDomain"] };

        public static string FromEmail()
        {
            return ConfigurationManager.AppSettings["FromEmail"];
        }
        public static List<string> ToEmails()
        {             
            return ConfigurationManager.AppSettings["ToEmail"].Split(';').ToList();
        }

        public static List<string> CcEmails()
        {
            return ConfigurationManager.AppSettings["CCEmail"].Split(';').ToList();
        }
        public static string Subject(string databaseName)
        {
            return ConfigurationManager.AppSettings["SubjectEmail"] + databaseName;
        }
        public static string Body(Job[] jobs)
        {
            var jobsNotEnabled = jobs.Where(x => !x.Enabled).Select(x => x.Name).Distinct().ToList();
            var schedulersNotEnabled = jobs.Where(x => !x.SchedulerEnabled).Select(x => x.Name).Distinct().ToList();
            var jobsInError = jobs.Where(x => !x.Status).Select(x => x).ToList();
            var body = string.Empty;

            if (!jobsNotEnabled.Any() && !jobsInError.Any() && !schedulersNotEnabled.Any())
                return "";

            if (jobsNotEnabled.Any())
            {
                body += jobsNotEnabled.Aggregate(@"<strong>There are Jobs disabled:</strong><br/><ol type=""disc"">", (current, jobNotEnabled) => current + string.Format("<li>{0}</li>", jobNotEnabled));

                body += "</ol><br/><br/>";
            }
            else
                body += "<strong>All jobs are enabled</strong><br/><br/>";
      
            if (schedulersNotEnabled.Any())
            {
                body += schedulersNotEnabled.Aggregate(@"<strong>There are Scheduler disabled:</strong><br/><ol type=""disc"">", (current, schedulerNotEnabled) => current + string.Format("<li>{0}</li>", schedulerNotEnabled));

                body += "</ol><br/><br/>";
            }
            else
                body += "<strong>All scheduler are enabled</strong><br/><br/>";
          
            if (jobsInError.Any())
            {
                body += jobsInError.Aggregate(
                    @"<strong>There are Jobs in error:</strong><ol type=""disc"">",
                    (current, jobNotEnabled) => current + (string.Format(@"
<li style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;mso-list:l1 level1 lfo2'><table style='width:80%' border='0'>
    <tr valign=""top"">
        <th width=""5%"" valign=""top""> Name:</th>
        <td width=""95%"" valign=""top"">{0}</td>
    </tr> 
    <tr valign=""top"">
        <th width=""5%"" valign=""top"">Step:</th>
        <td width=""95%"" valign=""top"">{1}</td>
    </tr>
    <tr valign=""top"">
        <th width=""5%"" valign=""top"">Message:</th>
        <td width=""95%"" valign=""top"">{2}</td>
    </tr>
    <tr valign=""top"">
        <th width=""5%"" valign=""top"">RunDate:</th>
        <td width=""95%"" valign=""top"">{3}</td>
    </tr>
    <tr valign=""top"">
        <th width=""5%"" valign=""top"">RunTime:</th>
        <td width=""95%"" valign=""top"">{4}</td>
    </tr>
</table></li>", jobNotEnabled.Name, jobNotEnabled.StepName, jobNotEnabled.Message, jobNotEnabled.RunDate, jobNotEnabled.RunTime)));

                body += "</ol>";
            }
            else
                body += "<strong>There are not Jobs in error</strong>";

            return body;
        }

        public static bool UseEndpoint()
        {
            return !string.Equals(ConfigurationManager.AppSettings["UseEndpoint"], "false", StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
