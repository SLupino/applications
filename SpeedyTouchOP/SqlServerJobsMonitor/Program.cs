﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Helpers;
using Helpers.Classes;
using Newtonsoft.Json;
using NLog;

namespace SqlServerJobsMonitor
{
    public class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static async Task Main()
        {
            Logger.Info("SqlServerJobsMonitor start");

            var stagingConnectionList =
                (from ConnectionStringSettings a in ConfigurationManager.ConnectionStrings
                    where a.Name.StartsWith("staging")
                    select a).ToList();

            if (stagingConnectionList.Count == 0)
            {
                Logger.Error("No staging connection string found");
                Logger.Info("SqlServerJobsMonitor end");
                return;
            }

            if (Settings.UseEndpoint())
            {
                Logger.Info("Use StagingMonitor api");

                var hostKey = Settings.HostCredentialKey;
                Logger.Debug("Get credential key '{0}", hostKey);
                var credentialsString = KeyVaultReader
                    .GetValue(Logger, hostKey, Settings.SpeedyEnvironmentApiUrl)
                    .Split(new[] {':'}, 2);
                var username = credentialsString[0];
                var password = credentialsString.Length == 2 ? credentialsString[1] : "";
                Logger.Debug("Found account '{0}", username);

                var credentials = new NetworkCredential(username, password);
                using (var handler = new HttpClientHandler {Credentials = credentials})
                {
                    handler.MaxRequestContentBufferSize = 2147483647;

                    using (var client = new HttpClient(handler))
                    {
                        foreach (var stagingConnection in stagingConnectionList)
                        {
                            Logger.Debug("Call StagingMonitor api for '{0}'", stagingConnection.Name);
                            try
                            {
                                var payload = new SimpleString
                                {
                                    Value = stagingConnection.ConnectionString
                                };
                                var json = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(payload));
                                Logger.Trace("HttpClient set payload: {0}", json);
                                var url = $"{Settings.Host}/api/StagingMonitor";
                                Logger.Trace("HttpClient post to api: {0}", url);
                                var data = new StringContent(json, Encoding.UTF8, "application/json");
                                var response = await client.PostAsync(url, data);
                                Logger.Trace("HttpClient post response: {0}", response.StatusCode);

                                var content = await response.Content.ReadAsStringAsync();
                                if (response.StatusCode == HttpStatusCode.OK)
                                {
                                    var result = JsonConvert.DeserializeObject<List<Job>>(content);
                                    Logger.Trace("Deserialized '{0}' jobs", result.Count);

                                    foreach (var job in result)
                                    {
                                        Logger.Trace(
                                            "Job: Name: {0} JobsEnabled: {1} SchedulerEnabled: {2} Status: {3}",
                                            job.Name, job.Enabled,
                                            job.SchedulerEnabled, job.Status);
                                    }

                                    var body = Settings.Body(result.ToArray());

                                    if (body != "")
                                    {
                                        EmailHelper.SendEmail(Settings.SmtpConfig, Settings.FromEmail(),
                                            Settings.ToEmails(),
                                            Settings.CcEmails(), new List<string>(),
                                            Settings.Subject(stagingConnection.Name), body);
                                    }
                                    else
                                    {
                                        Logger.Info("Email not sent: missing body");
                                    }
                                }
                                else
                                {
                                    Logger.Error("StagingMonitor api error: {0}", content);
                                }
                            }
                            catch (Exception e)
                            {
                                Logger.Error(e, "StagingMonitor exception");
                            }
                        }
                    }
                }
            }
            else
            {
                Logger.Info("Using direct query");

                foreach (var connectionString in stagingConnectionList)
                {
                    try
                    {
                        Logger.Debug("GetStagingJobsMonitoring for '{0}'", connectionString);
                        var response = GetStagingJobsMonitoring(connectionString.ConnectionString).ToArray();
                        foreach (var job in response)
                        {
                            Logger.Trace("Query Response: Name: {0} JobsEnabled: {1} SchedulerEnabled: {2} Status: {3}",
                                job.Name, job.Enabled,
                                job.SchedulerEnabled, job.Status);
                        }

                        var body = Settings.Body(response);

                        if (body != "")
                        {
                            EmailHelper.SendEmail(Settings.SmtpConfig, Settings.FromEmail(), Settings.ToEmails(),
                                Settings.CcEmails(), new List<string>(), Settings.Subject(connectionString.Name), body);
                        }
                        else
                        {
                            Logger.Info("Email not sent: missing body");
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Error(e, "SqlServerJobsMonitor Exception");
                    }
                }
            }

            Logger.Info("SqlServerJobsMonitor end");
        }

        public static List<Job> GetStagingJobsMonitoring(string stagingConnection)
        {
            const string selectJobWithProblems = @"WITH CTE AS (SELECT step_name, message, run_date, run_time, run_status, job_id, step_id,
    ROW_NUMBER() OVER(PARTITION BY job_id, step_id ORDER BY run_date DESC, run_time DESC) as rn 
FROM MSDB.DBO.SYSJOBHISTORY 
WHERE step_name <> '(Job outcome)')
SELECT DISTINCT sysjobs.name as 'job_name', cte.step_name, cte.message, cte.run_date, cte.run_time, sysjobs.enabled as 'JobEnabled', cte.run_status, sysschedules.enabled as 'SchedulerEnabled', step_id
FROM msdb.dbo.sysjobs 
	JOIN  MSDB.[dbo].[sysjobschedules] ON sysjobs.JOB_ID = [sysjobschedules].JOB_ID
	join [dbo].[sysschedules] on sysjobschedules.schedule_id = sysschedules.schedule_id
	LEFT JOIN CTE ON CTE.job_id = sysjobs.job_id AND CTE.rn = 1 AND (CTE.run_status <> 1)
WHERE sysjobs.enabled <> 1 OR CTE.run_status IS NOT NULL or sysschedules.enabled <> 1
ORDER BY sysjobs.name, step_id";

            Logger.Trace("Start using sql connection {0}", stagingConnection);
            var jobs = new List<Job>();
            try
            {
                using (var connection = new SqlConnection(stagingConnection))
                {
                    connection.Open();
                    Logger.Trace("Connection is opened");
                    using (var command = new SqlCommand(selectJobWithProblems, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            Logger.Trace("Start using data reader");
                            while (reader.Read())
                            {
                                if (Logger.IsTraceEnabled)
                                {
                                    var sb = new StringBuilder();
                                    for (var i = 0; i < reader.FieldCount; i++)
                                    {
                                        sb.AppendFormat("{0}: '{1}', ", reader.GetName(i), reader[i]);
                                    }
                                    Logger.Trace(sb.ToString());
                                }

                                jobs.Add(new Job
                                {
                                    Name = reader[0].ToString(),
                                    StepName = reader[1].ToString(),
                                    Message = reader[2].ToString(),
                                    RunDate = reader[3].ToString(),
                                    RunTime = reader[4].ToString(),
                                    Enabled = Convert.ToBoolean(Convert.ToInt16(reader.IsDBNull(5) ? "1" : reader[5].ToString())),
                                    Status = Convert.ToBoolean(Convert.ToInt16(reader.IsDBNull(6) ? "1" : reader[6].ToString())),
                                    SchedulerEnabled = Convert.ToBoolean(Convert.ToInt16(reader.IsDBNull(7) ? "1" : reader[7].ToString()))
                                });
                            }
                        }
                    }
                }
                return jobs;
            }
            catch (Exception e)
            {
                Logger.Error(e, e.Message);
                throw new Exception("Exception while reading from jobs query", e);
            }
        }
    }
}