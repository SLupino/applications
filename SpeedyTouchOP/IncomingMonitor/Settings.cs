﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace IncomingMonitor
{
    class Settings
    {
        public static List<string> SmtpConfig = new List<string>
        {
            ConfigurationManager.AppSettings["SMTPServer"],
            ConfigurationManager.AppSettings["SMTPUser"],
            ConfigurationManager.AppSettings["SMTPPassword"],
            ConfigurationManager.AppSettings["SMTPDomain"]
        };

        public static string Host(string companyCode)
        {
            return ConfigurationManager.AppSettings[companyCode + ".host"] ?? ConfigurationManager.AppSettings["host"];
        }
        public static string HostCredentialKey(string companyCode)
        {
            return ConfigurationManager.AppSettings[companyCode + ".HostCredentialKey"] ?? ConfigurationManager.AppSettings["HostCredentialKey"];
        }

        public static string SpeedyEnvironmentApiUrl => string.Equals(
            ConfigurationManager.AppSettings["IsProdEnvironment"], "true", StringComparison.InvariantCultureIgnoreCase)
            ? "https://speedyenvironmentprodapi.azurewebsites.net"
            : Debugger.IsAttached
                ? "https://speedyenvironmentsdevapi.azurewebsites.net"
                : "https://speedyenvironmentapi.azurewebsites.net";

        public static string FromEmail(string companyCode)
        {
            return ConfigurationManager.AppSettings[companyCode + ".FromEmail"];
        }

        public static List<string> ToEmails(List<XElement> xrequestInError, string companyCode)
        {
            if (xrequestInError.Count > 0)
            {
                var toEmailWithError = ConfigurationManager.AppSettings[companyCode + ".ToEmailWithError"] ??
                              ConfigurationManager.AppSettings[companyCode + ".ToEmail"];
                return toEmailWithError.Split(';').ToList();
            }

            var toEmailWithoutError = ConfigurationManager.AppSettings[companyCode + ".ToEmailWithoutError"] ??
                          ConfigurationManager.AppSettings[companyCode + ".ToEmail"];
            return toEmailWithoutError.Split(';').ToList();
        }

        public static List<string> CcEmails(List<XElement> xrequestInError, string companyCode)
        {
            if (xrequestInError.Count > 0)
            {
                var toEmailWithError = ConfigurationManager.AppSettings[companyCode + ".CCEmailWithError"] ??
                              ConfigurationManager.AppSettings[companyCode + ".CCEmail"];
                return toEmailWithError.Split(';').ToList();
            }

            var toEmailWithoutError = ConfigurationManager.AppSettings[companyCode + ".CCEmailWithoutError"] ??
                          ConfigurationManager.AppSettings[companyCode + ".CCEmail"];
            return toEmailWithoutError.Split(';').ToList();
        }

        public static string Subject(List<XElement> xrequestInError, string companyCode)
        {
            if (xrequestInError.Count > 0)
                return ConfigurationManager.AppSettings[companyCode + ".SubjectEmailWithError"] ?? ConfigurationManager.AppSettings[companyCode + ".SubjectEmail"];

            return ConfigurationManager.AppSettings[companyCode + ".SubjectEmailWithoutError"] ?? ConfigurationManager.AppSettings[companyCode + ".SubjectEmail"];
        }
        
        public static string Body(List<XElement> xRequestInError, string companyCode, double lastIscRun)
        {
            var body = new StringBuilder("<p>Hello</p>");

            if (xRequestInError.Count == 0)
            {
                body.Append("<p>There are no errors in the incoming database.</p>");
            }
            else
            {
                body.Append("<p>There are messages with errors in incoming.</p><ul>");
                foreach (XElement error in xRequestInError)
                {
                    body.AppendFormat("<li>there {0} {1} error{2} for {3}</li>", (error.Value == "1" ? "is" : "are"), error.Value,
                        (error.Value == "1" ? "" : "s"), error.Name);
                }
                body.AppendFormat("</ul><p>Please check the problem on <a href=\"{0}\">{0}</a></p>", ConfigurationManager.AppSettings[String.Format("{0}.InterfaceMonitorUrl", companyCode)]);
                var minutesForIsc = ConfigurationManager.AppSettings[String.Format("{0}.MinutesLastProcess", companyCode)];
                if (lastIscRun >= Convert.ToDouble(minutesForIsc))
                {
                    body.AppendFormat("<p>IncomingSeeburgerConnector is not running since {0} minutes</p>", lastIscRun);
                }
            }
            return body.Append(@"<p>Kind regards</p>
<p>Speedy<sup>TOUCH</sup> Operation Team</p>").ToString();
        }
    }
}
