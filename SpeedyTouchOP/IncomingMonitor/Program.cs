﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;
using Helpers;
using Helpers.Classes;
using Newtonsoft.Json;
using NLog;

namespace IncomingMonitor
{
    public class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static void Main()
        {
            Logger.Info("IncomingMonitor start");

            var connectionStringList = (from ConnectionStringSettings con in ConfigurationManager.ConnectionStrings
                where con.Name.EndsWith("IncomingInfo")
                select con).ToList();

            Parallel.ForEach(connectionStringList, conn =>
            {
                var companyCode = conn.Name.Split('.')[0];

                try
                {
                    var hostKey = Settings.HostCredentialKey(companyCode);
                    Logger.Debug("{0} | Get credential key '{1}'", companyCode, hostKey);
                    var credentialsString = KeyVaultReader
                        .GetValue(Logger, hostKey, Settings.SpeedyEnvironmentApiUrl)
                        .Split(new[] {':'}, 2);
                    var username = credentialsString[0];
                    var password = credentialsString.Length == 2 ? credentialsString[1] : "";
                    Logger.Debug("{0} | Found account '{1}'", companyCode, username);

                    var result = "";
                    var credentials = new NetworkCredential(username, password);
                    using (var handler = new HttpClientHandler {Credentials = credentials})
                    {
                        handler.MaxRequestContentBufferSize = 2147483647;

                        using (var client = new HttpClient(handler))
                        {
                            Logger.Debug("{0} | Call IncomingMonitor api", companyCode);

                            var payload = new SimpleString
                            {
                                Value =
                                    $"{conn.ConnectionString}|{ConfigurationManager.AppSettings[$"{companyCode}.LimitDateFrom"]}"
                            };
                            var json = JsonConvert.SerializeObject(payload);
                            Logger.Trace("{0} | HttpClient set payload: {1}", companyCode , json);
                            var url = $"{Settings.Host(companyCode)}/api/IncomingMonitor";
                            Logger.Trace("{0} | HttpClient post to api: {1}", companyCode, url);
                            var data = new StringContent(json, Encoding.UTF8, "application/json");
                            var response = client.PostAsync(url, data).GetAwaiter().GetResult();
                            ;
                            Logger.Trace("{0} | HttpClient post response: {1}", companyCode, response.StatusCode);

                            var content = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                            if (response.StatusCode == HttpStatusCode.OK)
                            {
                                result = JsonConvert.DeserializeObject<string>(content);
                                Logger.Trace("{0} | Deserialized result length {1}", companyCode, result.Length);
                            }
                            else
                            {
                                Logger.Error("{0} | StagingMonitor api error: {1}", companyCode, content);
                            }
                        }
                    }

                    var xData = XElement.Parse(result);
                    Logger.Trace("The server response is :\n{0}", result);
                    var xRequestInError = xData.XPathSelectElement("//RequestsInError")?.Elements().ToList();
                    var lastIscRun = TimeSpan.Parse(xData.XPathSelectElement("//LastIscRun")?.Value ?? "00:00:00");
                    EmailHelper.SendEmail(Settings.SmtpConfig, Settings.FromEmail(companyCode),
                        Settings.ToEmails(xRequestInError, companyCode),
                        Settings.CcEmails(xRequestInError, companyCode), new List<string>(),
                        Settings.Subject(xRequestInError, companyCode),
                        Settings.Body(xRequestInError, companyCode, Convert.ToDouble(lastIscRun.Minutes)));
                }
                catch (Exception e)
                {
                    Logger.Error(e, "IncomingMonitor Exception for company {0}", companyCode);
                }

            });

            Logger.Info("IncomingMonitor end");
        }
    }
}