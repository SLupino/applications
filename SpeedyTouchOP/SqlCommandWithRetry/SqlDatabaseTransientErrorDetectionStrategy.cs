﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SqlCommandWithRetry
{
    class SqlDatabaseTransientErrorDetectionStrategy
    {
        // https://azure.microsoft.com/en-us/documentation/articles/sql-database-develop-error-messages/
        private static readonly List<int> TransientErrorNumbers = new List<int>
        {
               -2, // Timeout
               64, // A transport-level error has occurred when receiving results from the server. (provider: TCP Provider, error: 0 - The specified network name is no longer available.)
            40501, // The service is currently busy. Retry the request after 10 seconds.
            40549, // Session is terminated because you have a long-running transaction. Try shortening your transaction.
            40550, // The session has been terminated because it has acquired too many locks. Try reading or modifying fewer rows in a single transaction.
            40551, // The session has been terminated because of excessive TEMPDB usage. Try modifying your query to reduce the temporary table space usage.
            40552, // The session has been terminated because of excessive transaction log space usage. Try modifying fewer rows in a single transaction.
            40553, // The session has been terminated because of excessive memory usage. Try modifying your query to process fewer rows.
            40613, // Database '%.*ls' on server '%.*ls' is not currently available. Please retry the connection later. 
                   // If the problem persists, contact customer support, and provide them the session tracing ID of '%.*ls'.
            49918, // Cannot process request. Not enough resources to process request.
            49919, //Cannot process create or update request. Too many create or update operations in progress for subscription "%ld".
            49920  //Cannot process request. Too many operations in progress for subscription "%ld".
        };

        static public bool IsTransientStatic(Exception exc)
        {
            if (!(exc is SqlException)) return false;
            return TransientErrorNumbers.Contains((exc as SqlException).Number);
        }
    }
}
