﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading;

namespace SqlCommandWithRetry
{
    public class RetrySqlCommand
    {
        public int MaxRetries { get; private set; }
        public int MsecBetweenRetries { get; private set; }
        private readonly TransientErrorCallback _transientErrorCallback;

        public RetrySqlCommand(int maxRetries, int msecBetweenRetries, TransientErrorCallback transientErrorCallback = null)
        {
            MaxRetries = maxRetries;
            MsecBetweenRetries = msecBetweenRetries;
            _transientErrorCallback = transientErrorCallback;
        }

        public delegate void TransientErrorCallback(SqlException sqlException,
            int currentAttempt, int maxAttempts, int millisecBetweenRetries);

        private static void DefaultTransientErrorCallback(SqlException sqlException, int currentAttempt, int maxAttempts,
            int msecBetweenRetries)
        {
            Console.WriteLine("Attempt {0}/{1}: transient error encountered. SqlException.Number = {2}. Message: \"{3}\". Retry in {4} msec.", currentAttempt,
                maxAttempts,
                sqlException.Number, sqlException.Message, msecBetweenRetries);
        }

        public void Execute(SqlConnection sqlConnection, Action<SqlConnection, int> sqlAction, TransientErrorCallback transientErrorCallback = null)
        {
            for (int currentAttempt = 1; currentAttempt <= MaxRetries; currentAttempt++)
            {
                try
                {
                    switch (sqlConnection.State)
                    {
                        case ConnectionState.Broken:
                            sqlConnection.Close();
                            sqlConnection.Open();
                            break;
                        case ConnectionState.Closed:
                            sqlConnection.Open();
                            break;
                    }
                    sqlAction(sqlConnection, currentAttempt);
                    sqlConnection.Close();
                    break;
                }
                catch (SqlException sqlExc)
                {
                    bool isTransientError = SqlDatabaseTransientErrorDetectionStrategy.IsTransientStatic(sqlExc);
                    if (isTransientError)
                    {
                        if (transientErrorCallback != null)
                        {
                            transientErrorCallback(sqlExc, currentAttempt, MaxRetries, MsecBetweenRetries);
                        }
                        else if (_transientErrorCallback != null)
                        {
                            _transientErrorCallback(sqlExc, currentAttempt, MaxRetries, MsecBetweenRetries);
                        }
                        else
                        {
                            DefaultTransientErrorCallback(sqlExc, currentAttempt, MaxRetries, MsecBetweenRetries);
                        }
                    }
                    else
                    {
                        throw;
                    }
                }
                Thread.Sleep(MsecBetweenRetries);
            }
        }
    }
}
