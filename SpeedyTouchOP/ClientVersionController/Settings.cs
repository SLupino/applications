﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace ClientVersionController
{
    class Settings
    {
        public static List<string> SmtpConfig = new List<string>
        {
            ConfigurationManager.AppSettings["SMTPServer"],
            ConfigurationManager.AppSettings["SMTPUser"],
            ConfigurationManager.AppSettings["SMTPPassword"],
            ConfigurationManager.AppSettings["SMTPDomain"]
        };

        public static string FromEmail()
        {
            return ConfigurationManager.AppSettings["FromEmail"];
        }

        public static List<string> ToEmails(bool withError)
        {
            if (withError)
            {
                var toEmailWithError = ConfigurationManager.AppSettings["ToEmailWithError"] ??
                              ConfigurationManager.AppSettings["ToEmail"];
                return toEmailWithError.Split(';').ToList();
            }

            var toEmailWithoutError = ConfigurationManager.AppSettings["ToEmail"];
            return toEmailWithoutError.Split(';').ToList();
        }

        public static List<string> CcEmails(bool withError)
        {
            if (withError)
                return new List<string>();

            var toEmailWithoutError = ConfigurationManager.AppSettings["CCEmail"];
            return toEmailWithoutError.Split(';').ToList();
        }

        public static string Subject(bool withError)
        {
            if (withError)
                return ConfigurationManager.AppSettings["SubjectEmailWithError"] ?? ConfigurationManager.AppSettings["SubjectEmail"];

            return ConfigurationManager.AppSettings["SubjectEmail"];
        }
        
        public static string Body(bool withError, string table)
        {
            var body = new StringBuilder("<p>Hello</p>");

            if (withError)
            {
                body.Append("<p>ClientVersionController failed.</p>");
            }
            else
            {
                body.Append(table);
            }
            return body.Append(@"<p>Kind regards</p>
<p>Speedy<sup>TOUCH</sup> Operation Team</p>").ToString();
        }
    }
}
