﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Xml.Linq;
using System.Xml.XPath;
using Helpers;
using Helpers.Classes;
using NLog;
using NLog.Internal;

namespace ClientVersionController
{
    class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            Logger.Info("Process successfully started.");
            GetDataFromConsolDatabase();
        }

        private static void GetDataFromConsolDatabase()
        {
            var comaniesConfig = CompaniesConfig.GetConfig().Companies.Where(c => c.Enabled).ToList();
            var exceptions = false;
            var table = new StringBuilder();

            Logger.Info("{0} request to do.", comaniesConfig.Count);

            foreach (var companyInfo in comaniesConfig)
            {
                if (!companyInfo.Enabled) continue;

                var companyCode = companyInfo.Code;
                var environment = companyInfo.EnvType;
                var targetVersion = companyInfo.TargetVersion;

                Logger.Debug("Company {0} {1} environment, target version {2}.", companyCode, environment, targetVersion);

                var connectionString = companyInfo.IncomingConnector;
     
                using (var client = new OrderInformationServiceReference.OrderInformationServiceClient("OrderInformationService"))
                {
                    try
                    {
                        var response = client.GetUserClientChanegedInfos(connectionString, targetVersion).ToList();
                        Logger.Debug("Service response {0} records.", response.Count);

                        table.AppendLine(
                            $"<table border='1'><caption>{companyCode} {environment} environment, executed on {DateTime.UtcNow} (UTC)</caption>");
                        table.AppendLine(
                            "<tr><th>User</th><th>Target version</th><th>Date</th></tr>");

                        foreach (var userClientInfo in response)
                        {
                            var userId = userClientInfo.User;
                            var target = userClientInfo.Target;
                            var from = userClientInfo.From;

                            table.AppendLine(
                                $"<tr><td>{userId}</td><td>{target}</td><td>{from}</td></tr>");
                        }

                        table.AppendLine("</table>");
                     }
                    catch (Exception ex)
                    {
                        exceptions = true;
                        Logger.Error(ex.Message, "Exception processing company {0} {1}", companyInfo.Code,
                            companyInfo.EnvType);
                    }
                }

                if (!SendMail(table.ToString(), exceptions))
                    exceptions = true;
            }

            Logger.Info(exceptions ? "Process finished with exception." : "Process successfully finished.");
        }

        private static bool SendMail(string response, bool withError)
        {
            try
            {
                EmailHelper.SendEmail(Settings.SmtpConfig, Settings.FromEmail(),
                    Settings.ToEmails(withError),
                    Settings.CcEmails(withError), new List<string>(),
                    Settings.Subject(withError),
                    Settings.Body(withError, response));
            }
            catch (Exception e)
            {
                Logger.Error(e.Message, "Mail not sent.");
                return false;
            }
            return true;
        }
    }
}
