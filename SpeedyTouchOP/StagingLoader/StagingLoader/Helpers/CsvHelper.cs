﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace StagingLoader.Helpers
{
    class CsvHelper
    {
        public static List<object> CsvReader(string path, string tableName)
        {
            return Reader(path, tableName);
        }

        private static List<object> Reader(string path, string tableName)
        {
            var allPath = path + "\\" + tableName + ".csv";
            if (File.Exists(allPath))
            {
                var csvRows = new List<string>();

                using (var reader = new StreamReader(allPath))
                {
                    while (reader.Peek() >= 0)
                    {
                        csvRows.Add(reader.ReadLine());
                    }
                }

                return LoadTableClasses(csvRows, tableName);
            }
            return null;
        }

        private static List<object> LoadTableClasses(IEnumerable<string> csvRows, string tableName)
        {
            var classList = new List<object>();
            Type tableType = Type.GetType(string.Format("StagingLoader.Classes.{0}Table", tableName));
            if (tableType != null)
                classList.AddRange(csvRows.Select(csvRow => Activator.CreateInstance(tableType, new object[] { csvRow })));

            return classList;
        }
    }
}
