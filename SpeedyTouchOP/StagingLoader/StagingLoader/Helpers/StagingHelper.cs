﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using NLog;
using StagingLoader.DataModel;

namespace StagingLoader.Helpers
{
    class StagingHelper
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static Guid StartRequest(SpeedyStagingWIEProdEntities context, Enumerations.RequestType requestType)
        {
            var prRequestId = new ObjectParameter("prRequestId", typeof(Guid));

            context.sp_startRequest(prRequestId, (int?)requestType, null);

            return (Guid)prRequestId.Value;
        }

        private static void EndRequest(SpeedyStagingWIEProdEntities context, Guid prRequestId)
        {
            context.sp_endRequest(prRequestId);
        }

        public static void LoadStagingTables(IEnumerable<object> userList, string table, Enumerations.RequestType requestType)
        {
            Guid prRequestId;
            using (var context = new SpeedyStagingWIEProdEntities())
            {
                Logger.Info("Opening request");
                prRequestId = StartRequest(context, requestType);
                Logger.Info("request {0} is open", prRequestId);
            }
                
            Type tableType = Type.GetType(string.Format("StagingLoader.Classes.{0}Table", table));

            if (tableType != null)
            {
                var x = tableType.GetMethod("GetTableValues");
                Logger.Info("Invoke method for {0}", table);
                x.Invoke(null, new object[] { userList, prRequestId });
            }

            using (var context = new SpeedyStagingWIEProdEntities())
            {
                Logger.Info("Closing request");
                EndRequest(context, prRequestId);
                Logger.Info("request {0} is Closed", prRequestId);
            }
        }

        public static Guid GenerateGuid(string id)
        {
            var guidTemplate = "00000000-0000-0000-0000-";
            var charCount = 12 - id.Count();

            for (var i = charCount; i > 0; i--)
                guidTemplate += "0";

            guidTemplate += id;

            return new Guid(guidTemplate);
        }

        public static void PrepareRequestForConsolidateDatbase(int count)
        {
            if(count <= 4)
            using (var context = new SpeedyStagingWIEProdEntities())
            {
                context.sp_processRequests();
                context.sp_processRequests_consol(10000);

                context.SaveChanges();

                PrepareRequestForConsolidateDatbase(count + 1);
            }
        }
    }
}
