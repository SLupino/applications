﻿using System;
using System.Collections.Generic;
using System.Linq;
using StagingLoader.DataModel;

namespace StagingLoader.Classes
{
    class SalesHierarchyTable
    {
        public string Id { get; set; }
        public string ParentId { get; set; }
        public string Description { get; set; }

        public SalesHierarchyTable(string csvRow)
        {
            var splittedRow = csvRow.Split(',');

            if (splittedRow.Any())
            {
                Id = splittedRow[0];
                ParentId = splittedRow[1];
                Description = splittedRow[2];
            }
        }

        public static void GetTableValues(IEnumerable<object> shList, Guid prRequestId)
        {
            foreach (SalesHierarchyTable sh in shList)
            {
                using (var context = new SpeedyStagingWIEProdEntities())
                {
                    var salesHierarchiesRow = new SalesHierarchy
                    {
                        id = sh.Id,
                        description = sh.Description,
                        parentId = sh.ParentId == "NULL" || sh.ParentId == "Null" || sh.ParentId == "null" ? null : sh.ParentId,
                        requestId = prRequestId,
                        operation = "N",
                        C_boId = Guid.NewGuid(),
                        C_boCreated = DateTime.Now,
                        C_boElaborationErrorMessage = "",
                        C_boOperationState = 0,
                        C_boreplacedBy = null,
                        C_consolGuid = null,
                        C_boElaborationErrorCode = 0,
                        C_consolRequestID = null
                    };

                    context.SalesHierarchies.Add(salesHierarchiesRow);
                    context.SaveChanges();
                }
            }
        }
    }
}
