﻿using System;
using System.Collections.Generic;
using System.Linq;
using StagingLoader.DataModel;
using StagingLoader.Helpers;

namespace StagingLoader.Classes
{
    class VoucherDetailsTable
    {
        public string CustomerId { get; set; }
        public string VoucherDate { get; set; }
        public string VoucherNumber { get; set; }
        public string VoucherType { get; set; }
        public string Number { get; set; }
        public string ItemNumber { get; set; }
        public string QuantityOrdered { get; set; }
        public string PriceUnit { get; set; }
        public string GrossPrice { get; set; }
        public string Discount { get; set; }
        public string NetPrice { get; set; }

        public VoucherDetailsTable(string csvRow)
        {
            var splittedRow = csvRow.Split(',');

            if (splittedRow.Any())
            {
                CustomerId = splittedRow[0];
                VoucherDate = splittedRow[1];
                VoucherNumber = splittedRow[2];
                VoucherType = splittedRow[3];
                Number = splittedRow[4];
                ItemNumber = splittedRow[5];
                QuantityOrdered = splittedRow[6];
                PriceUnit = (splittedRow[7]).Contains(".") ? (splittedRow[7]).Split('.')[0] : (splittedRow[7]);
                GrossPrice = splittedRow[8];
                Discount = splittedRow[9];
                NetPrice = splittedRow[10];
            }
        }

        public static void GetTableValues(IEnumerable<object> voucherList, Guid prRequestId)
        {
            var priceUnit = new Dictionary<int, int> { { 1, 100 }, { 10, 200 }, { 100, 300 }, { 1000, 400 }, { 10000, 500 } };
            var voucherTypes = new Dictionary<int, int> { { 11, 200 }, { 8, 200 }, { 9, 200 }, { 19, 200 }, { 31, 200 }, { 7, 700 }, { 16, 700 } };
            Console.WriteLine("Start insert in entityFramework Voucher class");
            int rowCount = 0;
            foreach (VoucherDetailsTable v in voucherList)
            {
                rowCount++;
                Console.WriteLine("[{0}]  Start insert Row {1}", DateTime.Now, rowCount);
                using (var context = new SpeedyStagingWIEProdEntities())
                {
                    var splittedDate = v.VoucherDate.Split('.');
                    var openItemsRow = new VoucherDetail
                    {
                        customerId = v.CustomerId,
                        voucherDate =
                            new DateTime(Convert.ToInt32(splittedDate[2]), Convert.ToInt32(splittedDate[1]),
                                Convert.ToInt32(splittedDate[0])),
                        voucherNumber = v.VoucherNumber,
                        voucherType = voucherTypes.ContainsKey(Convert.ToInt32(v.VoucherType))? voucherTypes[Convert.ToInt32(v.VoucherType)] : 200,
                        number = v.Number,
                        itemNumber = v.ItemNumber,
                        quanitityOrdered = Convert.ToDecimal(v.QuantityOrdered),
                        quantityDelivered = 0,
                        priceUnit = priceUnit.ContainsKey(Convert.ToInt32(v.PriceUnit))? priceUnit[Convert.ToInt32(v.PriceUnit)] : 100,
                        grossPrice = Convert.ToDecimal(v.GrossPrice),
                        discount = Convert.ToDecimal(v.Discount),
                        surcharge = 0,
                        netPrice = Convert.ToDecimal(v.NetPrice),
                        isSpecialItem = 1,
                        specialItemDescription = "",
                        isAuspreiser = 1,
                        value = 0,
                        noCharge = 1,
                        requestId = prRequestId,
                        operation = "N",
                        C_boId = Guid.NewGuid(),
                        C_boCreated = DateTime.Now,
                        C_boElaborationErrorMessage = "",
                        C_boOperationState = 0,
                        C_boreplacedBy = null,
                        C_consolGuid = null,
                        C_boElaborationErrorCode = 0,
                        C_consolRequestID = null
                    };
                    
                    context.VoucherDetails.Add(openItemsRow);
                    Console.WriteLine("[{0}]  Start Save in database", DateTime.Now);
                    context.SaveChanges();
                    Console.WriteLine("[{0}]  End insert Row {1}", DateTime.Now, rowCount);
                }
            }
        }
    }
}
