﻿using System;
using System.Collections.Generic;
using System.Linq;
using StagingLoader.DataModel;
using StagingLoader.Helpers;

namespace StagingLoader.Classes
{
    class SalesRepAreaManagerRelationsTable
    {
        public string AreaManagerId { get; set; }
        public string SalesRepId { get; set; }

        public SalesRepAreaManagerRelationsTable(string csvRow)
        {
            var splittedRow = csvRow.Split(',');

            if (splittedRow.Any())
            {
                AreaManagerId = splittedRow[0];
                SalesRepId = splittedRow[1];
            }
        }

        public static void GetTableValues(SpeedyStagingWIEProdEntities context, IEnumerable<object> sramList, Guid prRequestId)
        {
            foreach (SalesRepAreaManagerRelationsTable salesRepAreaManagerRelations in sramList)
            {

                var salesRepAreaManagerRelationRow = new SalesRepManagerRelation
                {
                    salesManagerUserId = StagingHelper.GenerateGuid(salesRepAreaManagerRelations.AreaManagerId),
                    salesRepUserId = StagingHelper.GenerateGuid(salesRepAreaManagerRelations.SalesRepId),
                    requestId = prRequestId,
                    operation = "N",
                    C_boId = Guid.NewGuid(),
                    C_boCreated = DateTime.Now,
                    C_boElaborationErrorMessage = "",
                    C_boOperationState = 0,
                    C_boreplacedBy = null,
                    C_consolGuid = null,
                    C_boElaborationErrorCode = 0,
                    C_consolRequestID = null
                };

                context.SalesRepManagerRelations.Add(salesRepAreaManagerRelationRow);
                context.SaveChanges();
            }
        }
    }
}
