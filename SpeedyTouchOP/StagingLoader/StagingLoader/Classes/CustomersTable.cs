﻿using System;
using System.Collections.Generic;
using System.Linq;
using StagingLoader.DataModel;

namespace StagingLoader.Classes
{
    class CustomersTable
    {
        public string Id { get; set; }
        public string Areaid { get; set; }
        public string Asm { get; set; }
        public string Dsm { get; set; }
        public string Name1 { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string ZipCode { get; set; }
        public string State { get; set; }
        public string Tel { get; set; }
        public string Mobile { get; set; }
        public string Contact { get; set; }
        public string Mail { get; set; }
        public string Currency { get; set; }
        public string IndustryHierarchy { get; set; }
        public int EndUsers { get; set; }
        public short Tnt { get; set; }
        public string Orsy { get; set; }

        public CustomersTable(string csvRow)
        {
            var splittedRow = csvRow.Split(',');

            if (splittedRow.Any())
            {
                Id = splittedRow[0];
                IndustryHierarchy = splittedRow[1];
                Areaid = splittedRow[2];
                Name1 = splittedRow[3];
                Address1 = splittedRow[4];
                City = splittedRow[5];
                County = splittedRow[6];
                ZipCode = splittedRow[7];
                State = splittedRow[8];
                Tel = splittedRow[9];
                Mobile = splittedRow[10];
                Contact = splittedRow[11];
                Mail = splittedRow[12];
                Currency = splittedRow[13];
                EndUsers = splittedRow[14] == "" ? 0 : Convert.ToInt32(splittedRow[14]);
                Tnt = splittedRow[15] == "N" ? (short)0 : (short)1;
            }
        }

        public static void GetTableValues(IEnumerable<object> customerList, Guid prRequestId)
        {
            using (var context = new SpeedyStagingWIEProdEntities())
            {
                foreach (CustomersTable customers in customerList)
                {
                    var customerRow = new Customer
                    {
                        id = customers.Id,
                        state = 500,
                        VMIstate = 100,
                        isRegular = 0,
                        numberOfEmployees = customers.EndUsers,
                        name1 = customers.Name1,
                        name2 = "",
                        street =
                            customers.Address1.Count() < 100 ? customers.Address1 : customers.Address1.Substring(0, 99),
                        isoCountryCode = customers.State,
                        zipCode = customers.ZipCode,
                        city = customers.City,
                        postOfficeBox = "",
                        postOfficeBoxZipCode = "",
                        longitude = 0,
                        latitude = 0,
                        phone = customers.Tel,
                        fax = "",
                        website = "",
                        email = customers.Mail,
                        orderReceipt = 100,
                        orderConfirmation = 100,
                        invoice = 100,
                        invoiceParty = "",
                        isInvoiceParty = 0,
                        association = "",
                        industryId = customers.IndustryHierarchy,
                        salesAreaId = customers.Areaid,
                        pricingGroup =
                            customers.IndustryHierarchy != "" ? customers.IndustryHierarchy : "",
                        paymentTerms = "",
                        collectiveInvoicing = "",
                        grossNetPrice = 100,
                        hasMandatoryConditions = 0,
                        freightCharge = 0,
                        packageingCharge = 0,
                        minimalValueThreshold = 0,
                        minimalValueSurcharge = 0,
                        customerSince = new DateTime(1980, 1, 1),
                        isEShopCustomer = 0,
                        SMLClassificationSalesPotential = 100,
                        visitsRecurrenceRule = "",
                        visitsStartDate = new DateTime(2014, 1, 1),
                        visitsHour = new TimeSpan(10, 10, 10),
                        visitsIsFixedAppointment = 0,
                        visitsCreateRecurringAppointments = 0,
                        visitsAlarmRelativeOffset = "",
                        hasBonusAgreement = 0,
                        tntCustomer = customers.Tnt,
                        requestId = prRequestId,
                        operation = "N",
                        C_boId = Guid.NewGuid(),
                        C_boCreated = DateTime.Now,
                        C_boElaborationErrorMessage = "",
                        C_boOperationState = 0,
                        C_boreplacedBy = null,
                        C_consolGuid = null,
                        C_boElaborationErrorCode = 0,
                        C_consolRequestID = null
                    };

                    context.Customers.Add(customerRow);
                    context.SaveChanges();
                }  
            }
        }
    }
}
