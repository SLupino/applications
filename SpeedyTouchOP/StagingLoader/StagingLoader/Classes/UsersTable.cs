﻿using System;
using System.Collections.Generic;
using System.Linq;
using StagingLoader.DataModel;
using StagingLoader.Helpers;

namespace StagingLoader.Classes
{
    class UsersTable
    {
        public string Id { get; set; }
        public int AccountType { get; set; }
        public string SystemAccount { get; set; }
        public string Name { get; set; }
        public int UserType { get; set; }
        public string UserId { get; set; }

        public UsersTable(string csvRow)
        {
            var splittedRow = csvRow.Split(',');

            if (splittedRow.Any())
            {
                Id = splittedRow[0];
                AccountType = Convert.ToInt32(splittedRow[1]);
                SystemAccount = splittedRow[2];
                Name = splittedRow[3];
                UserType = Convert.ToInt32(splittedRow[4]);
                UserId = splittedRow[5];
            }
        }

        public static void GetTableValues(IEnumerable<object> userList, Guid prRequestId)
        {
            foreach (UsersTable user in userList)
            {
                using (var context = new SpeedyStagingWIEProdEntities())
                {
                    var userRow = new User
                    {
                        id = StagingHelper.GenerateGuid(user.Id),
                        accountType = user.AccountType,
                        systemAccount = user.SystemAccount,
                        name = user.Name,
                        userType = user.UserType,
                        salesRepId = user.UserId,
                        requestId = prRequestId,
                        operation = "N",
                        C_boId = Guid.NewGuid(),
                        C_boCreated = DateTime.Now,
                        C_boElaborationErrorMessage = "",
                        C_boOperationState = 0,
                        C_boreplacedBy = null,
                        C_consolGuid = null,
                        C_boElaborationErrorCode = 0,
                        C_consolRequestID = null
                    };

                    context.Users.Add(userRow);
                    context.SaveChanges();
                }
            }
        }
    }
}
