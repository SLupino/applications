﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using StagingLoader.DataModel;
using StagingLoader.Helpers;

namespace StagingLoader.Classes
{
    class VouchersTable
    {
        public string CustomerId { get; set; }
        public string VoucherDate { get; set; }
        public string VoucherNumber { get; set; }
        public string VoucherType { get; set; }

        public VouchersTable(string csvRow)
        {
            var splittedRow = csvRow.Split(',');

            if (splittedRow.Any())
            {
                CustomerId = splittedRow[0];
                VoucherDate = splittedRow[1];
                VoucherNumber = splittedRow[2];
                VoucherType = splittedRow[3];
            }
        }

        public static void GetTableValues(IEnumerable<object> voucherList, Guid prRequestId)
        {
            var voucherTypes = new Dictionary<int, int> { { 11, 200 }, { 8, 200 }, { 9, 200 }, { 19, 200 }, { 31, 200 }, { 7, 700 }, { 16, 700 } };
            
            int rowCount = 0;
            try
            {
                foreach (VouchersTable v in voucherList)
                {
                    rowCount++;
                    Console.WriteLine("[{0}]  Start insert Row {1}", DateTime.Now, rowCount);
                    using (var context = new SpeedyStagingWIEProdEntities())
                    {
                        var splittedDate = v.VoucherDate.Split('.');
                        var openItemsRow = new Voucher
                        {
                            customerId = v.CustomerId,
                            voucherDate =
                                new DateTime(Convert.ToInt32(splittedDate[2]), Convert.ToInt32(splittedDate[1]),
                                    Convert.ToInt32(splittedDate[0])),
                            voucherNumber = v.VoucherNumber,
                            voucherType = voucherTypes[Convert.ToInt32(v.VoucherType)],
                            state = 100,
                            numberOfDetails = 0,
                            sourceVoucherNumber = "",
                            valueOfGoods = 0, //Convert.ToDecimal(v.ValueOfGoods),
                            totalAmount = 0, //Convert.ToDecimal(v.TotalAmount),
                            orderEntrySource = 50,
                            note1 = "",
                            note2 = "",
                            note3 = "",
                            note4 = "",
                            note5 = "",
                            customerNote1 = "",
                            customerNote2 = "",
                            customerNote3 = "",
                            customerNote4 = "",
                            customerNote5 = "",
                            freightCharge = 0,
                            packageingCharge = 0,
                            minimalValueSurcharge = 0,
                            deliveryAddressName1 = "",
                            deliveryAddressName2 = "",
                            deliveryAddressStreet = "",
                            deliveryAddressIsoCountryCode = "",
                            deliveryAddressZipCode = "",
                            deliveryAddressCity = "",
                            quoteFollowUpBy = 100,
                            quoteDeliveryBy = 100,
                            quoteFaxNumber = "",
                            quoteEmail = "",
                            quoteForAttentionOf = "",
                            isSplitted = 1,
                            hasAuspreiser = 1,
                            hasOrderConfirmation = 1,
                            region = "",
                            postOfficeBoxCity = "",
                            warehouseNote1 = "",
                            warehouseNote2 = "",
                            warehouseNote3 = "",
                            warehouseNote4 = "",
                            warehouseNote5 = "",
                            salesRepOrderNumber = "",
                            requestId = prRequestId,
                            operation = "N",
                            C_boId = Guid.NewGuid(),
                            C_boCreated = DateTime.Now,
                            C_boElaborationErrorMessage = "",
                            C_boOperationState = 0,
                            C_boreplacedBy = null,
                            C_consolGuid = null,
                            C_boElaborationErrorCode = 0,
                            C_consolRequestID = null
                        };
                        
                        context.Vouchers.Add(openItemsRow);
                        context.SaveChanges();
                        Console.WriteLine("[{0}]  End insert Row {1}", DateTime.Now, rowCount);
                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
        }
    }
}
