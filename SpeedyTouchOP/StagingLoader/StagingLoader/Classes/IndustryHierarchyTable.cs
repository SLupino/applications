﻿using System;
using System.Collections.Generic;
using System.Linq;
using StagingLoader.DataModel;

namespace StagingLoader.Classes
{
    class IndustryHierarchyTable
    {
        public string Id { get; set; }
        public string ParentId { get; set; }
        public string Description { get; set; }
        public string PricingGroup { get; set; }
        public string FatherDescription { get; set; }

        public IndustryHierarchyTable(string csvRow)
        {
            var splittedRow = csvRow.Split(',');

            if (splittedRow.Any())
            {
                Id = splittedRow[0];
                ParentId = splittedRow[2];
                Description = splittedRow[3];
                PricingGroup = splittedRow[1];
                FatherDescription = splittedRow[4];
            }
        }

        public static void GetTableValues(IEnumerable<object> ihList, Guid prRequestId)
        {
            foreach (IndustryHierarchyTable industryHierarchy in ihList)
            {
                using (var context = new SpeedyStagingWIEProdEntities())
                {
                    var ih = new IndustryHierarchy
                    {
                        id = industryHierarchy.Id,
                        description = industryHierarchy.Description,
                        pricingGroup = industryHierarchy.PricingGroup,
                        parentId = industryHierarchy.ParentId == "null" ? null : industryHierarchy.ParentId,
                        requestId = prRequestId,
                        operation = "N",
                        C_boId = Guid.NewGuid(),
                        C_boCreated = DateTime.Now,
                        C_boElaborationErrorMessage = "",
                        C_boOperationState = 0,
                        C_boreplacedBy = null,
                        C_consolGuid = null,
                        C_boElaborationErrorCode = 0,
                        C_consolRequestID = null
                    };
                    context.IndustryHierarchies.Add(ih);
                    context.SaveChanges();
                }
            }
        }
    }
}
