﻿using System;
using System.Collections.Generic;
using System.Linq;
using StagingLoader.DataModel;
using StagingLoader.Helpers;

namespace StagingLoader.Classes
{
    class SalesRepAreaRelationsTable
    {
        public string AreaId { get; set; }
        public string SalesRepId { get; set; }
        public short IsPrimary { get; set; }

        public SalesRepAreaRelationsTable(string csvRow)
        {
            var splittedRow = csvRow.Split(',');

            if (splittedRow.Any())
            {
                AreaId = splittedRow[0];
                SalesRepId = splittedRow[1];
                IsPrimary = Convert.ToInt16(splittedRow[2]);
            }
        }

        public static void GetTableValues(IEnumerable<object> sraList, Guid prRequestId)
        {
            foreach (SalesRepAreaRelationsTable salesRepAreaRelations in sraList)
            {
                using (var context = new SpeedyStagingWIEProdEntities())
                {
                    var salesRepAreaRelationRow = new SalesRepAreaRelation
                    {
                        salesAreaId = salesRepAreaRelations.AreaId,
                        salesRepUserId = StagingHelper.GenerateGuid(salesRepAreaRelations.SalesRepId),
                        validFrom = new DateTime(1980, 1, 1),
                        validTo = new DateTime(9999, 12, 31),
                        isPrimarySalesRep = salesRepAreaRelations.IsPrimary,
                        requestId = prRequestId,
                        operation = "N",
                        C_boId = Guid.NewGuid(),
                        C_boCreated = DateTime.Now,
                        C_boElaborationErrorMessage = "",
                        C_boOperationState = 0,
                        C_boreplacedBy = null,
                        C_consolGuid = null,
                        C_boElaborationErrorCode = 0,
                        C_consolRequestID = null
                    };

                    context.SalesRepAreaRelations.Add(salesRepAreaRelationRow);
                    context.SaveChanges();
                }
            }
        }
    }
}
