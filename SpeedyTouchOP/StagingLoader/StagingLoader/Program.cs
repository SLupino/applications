﻿using System.Collections.Generic;
using System.Configuration;
using NLog;
using StagingLoader.Helpers;

namespace StagingLoader
{
    class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        static void Main()
        {
            Logger.Info("Staging Loading");
            foreach(string appSettingKey in ConfigurationManager.AppSettings.Keys)
                LoadTables(appSettingKey, RecognizeRequestType(ConfigurationManager.AppSettings[appSettingKey]));

            StagingHelper.PrepareRequestForConsolidateDatbase(1);
        }

        private static void LoadTables(string tableName, Enumerations.RequestType requestType)
        {
            Logger.Info("Reading File for {0}", tableName);
            List<object> dataList = CsvHelper.CsvReader(ConfigurationManager.ConnectionStrings["FileSource"].ConnectionString, tableName);
            if (dataList == null) return;
            Logger.Info("{0} rows in the file", dataList.Count);
            if (dataList.Count > 0)
                StagingHelper.LoadStagingTables(dataList, tableName, requestType);
            Logger.Info("{0} is loaded", tableName);
        }

        private static Enumerations.RequestType RecognizeRequestType(string requestType)
        {
            if (requestType.ToLower() == "full")
                return Enumerations.RequestType.Full;
            return Enumerations.RequestType.Delta;
        }
    }
}
