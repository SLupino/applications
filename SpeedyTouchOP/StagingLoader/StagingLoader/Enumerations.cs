﻿
namespace StagingLoader
{
    class Enumerations
    {
        public enum Tables
        {
            IndustryHierarchy,
            SalesHierarchy,
            Users,
            SalesRepAreaRelations,
            SalesRepAreaManagerRelations,
            Customers,
            Vouchers,
            VoucherDetails
        };

        public enum RequestType
        {
            Full = 10,
            Delta = 0
        };

        public static string GetPricingGroupParentId(string pricingGroup)
        {
            switch (pricingGroup)
            {
                case "AU":
                    return "1";
                case "ME":
                    return "2";
                case "CA":
                    return "3";
                case "CO":
                    return "4";
                case "WO":
                    return "5";
                default:
                    return "0";
            }
        }
    }
}
